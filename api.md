---
author: 'Silver Peak Systems Inc.'
lang: 'en-US'
title: 'GMS\_API\_Documentation'
---

GMS\_API\_Documentation

8.8.4

13/01/2020

::: {#console}
:::

::: {#toc}
### Table of Contents
:::

::: {#contents}
\

Login and logout - authentication for REST APIs
===============================================

/authentication/login
---------------------

### POST

Summary

Authentication for REST API HTTP session

Input type

application/json

Login

Description: Json object containing user id, password and token. Token
is optional if user is not 2-factor authentication user.

Required: true

Parameter type: POST Body

Data Type: application/json

user

-   Type: string
-   Required: true
-   Description: User id

password

-   Type: string
-   Required: true
-   Description: Password

token

-   Type: string
-   Description: 2-factor code, it is optional if you are not using
    2-factor for the user

Output type

None

/authentication/login
---------------------

### POST

Summary

Use this api to login to Orchestrator using FORM post. Commonly used to
embed orchestrator UI or proxy orchestrator login via a web proxy.

Input type

application/x-www-form-urlencoded

user

-   Description: User id. Sent as a query param when login form is
    submitted
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

password

-   Description: Password. Sent as a query param when login form is
    submitted
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/authentication/logout
----------------------

### GET

Summary

Logout of current HTTP session

Input type

None

Output type

None

/authentication/loginStatus
---------------------------

### GET

Summary

Get the current authentication status of the HTTP session

Input type

None

Output type

application/json

isLoggedIn

-   Type: string
-   Required: true
-   Description: True if the current HTTP session is authenticated

user

-   Type: string
-   Required: true
-   Description: Current user for the HTTP session

/authentication/loginToken
--------------------------

### POST

Summary

Send a 2-factor code to user\'s mailbox for 2-factor authentication

Input type

application/json

RequestLoginToken

Description: Json object containing user id and password, sent as user
authentication information when user want to receive 2-factor code

Required: true

Parameter type: POST Body

Data Type: application/json

user

-   Type: string
-   Required: true
-   Description: User id

password

-   Type: string
-   Required: true
-   Description: Password

TempCode

-   Type: boolean
-   Required: false
-   Description: Request a temporary authentication code be sent to your
    email even if email based two factor authentication is not enabled
    for your account

Output type

None

/authentication/password/validation
-----------------------------------

### POST

Summary

Authenticate user\'s password only, but doesn\'t mark user\'s status as
login

Input type

application/json

LoginToken

Description: Json object containing user id and password, sent as query
param when user want to validate user\'s password

Required: true

Parameter type: POST Body

Data Type: application/json

user

-   Type: string
-   Required: true
-   Description: User id

password

-   Type: string
-   Required: true
-   Description: Password

Output type

None

/authentication/userAuthType
----------------------------

### POST

Summary

Check the two factor authentication methods the user requires to login

Notes

Possible authentication types include PASSWORDONLY, MAIL, APP,
MAIL\_AND\_APP

Input type

application/json: undefined

LoginToken

Description: Json object containing user id and password

Required: true

Parameter type: POST Body

Data Type: application/json

user

-   Type: string
-   Required: true
-   Description: User id

password

-   Type: string
-   Required: true
-   Description: Password

Output type

application/json

user

-   Type: string
-   Required: true
-   Description: Current user

authType

-   Type: string
-   Required: true
-   Description: Current user\'s authentication type

/authentication/userAuthTypeToken
---------------------------------

### POST

Summary

Check the two factor authentication methods the user has active using a
reset password token

Notes

Possible authentication types include PASSWORDONLY, MAIL, APP,
MAIL\_AND\_APP

Input type

application/json: undefined

ResetPasswordToken

Description: Json object containing the reset password token

Required: true

Parameter type: POST Body

Data Type: application/json

token

-   Type: string
-   Required: true
-   Description: Token for the reset password request

Output type

application/json

Reference: login.json

\

Orchestrator Licensing
======================

/gmsLicense
-----------

### GET

Summary

Get current license key and information

Input type

None

Output type

application/json

availableAppliances

-   Type: integer
-   Required: true
-   Description: count of available appliances

licenseKey

-   Type: string
-   Required: true
-   Description: license key

code

-   Type: integer
-   Required: true
-   Description: If license key decryption fails it returns -1 otherwise
    it returns 0

serialNumber

-   Type: string
-   Required: true
-   Description: serialNumber

usedAppliances

-   Type: integer
-   Required: true
-   Description: count of used Appliances

maxAppliances

-   Type: integer
-   Required: true
-   Description: total Appliances allowed for this license

daysToExpiration

-   Type: integer
-   Required: true
-   Description: Number of days left for the license to expire

message

-   Type: string
-   Required: true
-   Description: message

expirationDate

-   Type: string
-   Required: true
-   Description: date of expiry - epoch time in milliseconds

### POST

Summary

Set Orchestrator license key

Input type

application/json

licenseKey

Description: Orchestrator license key

Required: true

Parameter type: POST Body

Data Type: application/json

licenseKey

-   Type: string
-   Required: true
-   Description: new license key value

Output type

application/json

availableAppliances

-   Type: integer
-   Required: true
-   Description: count of available appliances

licenseKey

-   Type: string
-   Required: true
-   Description: license key

code

-   Type: integer
-   Required: true
-   Description: If license key decryption fails it returns -1 otherwise
    it returns 0

rc

-   Type: integer
-   Required: true
-   Description: value is set to -1 if any exception occurs during
    license key validation

serialNumber

-   Type: string
-   Required: true
-   Description: serialNumber

usedAppliances

-   Type: integer
-   Required: true
-   Description: count of used Appliances

maxAppliances

-   Type: integer
-   Required: true
-   Description: total Appliances allowed for this license

daysToExpiration

-   Type: integer
-   Required: true
-   Description: Number of days left for the license to expire

message

-   Type: string
-   Required: true
-   Description: message

expirationDate

-   Type: string
-   Required: true
-   Description: date of expiry - epoch time in milliseconds

/gmsLicense/validation
----------------------

### GET

Summary

Validate a new license key

Input type

None

licenseKey

-   Description: License key to validate
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

licenseKey

-   Type: string
-   Required: true
-   Description: license key

code

-   Type: integer
-   Required: true
-   Description: If license key decryption fails it returns -1 otherwise
    it returns 0

serialNumber

-   Type: string
-   Required: true
-   Description: serialNumber

maxAppliances

-   Type: integer
-   Required: true
-   Description: total Appliances allowed for this license

message

-   Type: string
-   Required: true
-   Description: message

Reference: gmsLicense.json

\

Add, delete and modify appliances
=================================

/appliance
----------

### GET

Summary

Returns information about appliance(s) managed by Orchestrator

Input type

None

Output type

None

/appliance/{nePk}
-----------------

### DELETE

Summary

Delete an appliance from network.

Notes

Only use this if you truly want to delete the appliance from the
network. This will remove the portal account name/key from the
Orchestrator. The primary key must exist in Orchestrator.

Input type

None

nePk

-   Description: nePk.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/appliance/queuedForDeletion
----------------------------

### GET

Summary

Lists appliances ( appliance primary keys) that are queued on the
Orchestrator for deletion.

Notes

When you delete an appliance on Orchestrator, it is not immediately
deleted. It gets queued for deletion. Use this api to get appliances
that are in queue for deletion

Input type

None

Output type

None

/appliance/deleteForDiscovery/{nePk}
------------------------------------

### DELETE

Summary

Delete for rediscovery.

Notes

The primary key must exist in Orchestrator.

Input type

None

nePk

-   Description: nePk.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/appliance/{nePk}
-----------------

### POST

Summary

Modify an appliance\'s IP address, username, password, networkRole, and
webProtocol.

Input type

application/json: undefined

nePk

-   Description: Appliance primary key
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

applianceInfo

Description: Appliance information

Required: true

Parameter type: POST Body

Data Type: application/json

userName

-   Type: string
-   Required: true
-   Description: the username for logging on to this appliance

password

-   Type: string
-   Required: true
-   Description: the password for logging on to this appliance

IP

-   Type: string
-   Required: true
-   Description: the IP address of this appliance

webProtocolType

-   Type: integer
-   Required: true
-   Description: protocol used by Orchestrator to talk to appliance.
    Http = 1, Https = 2, Both = 3

networkRole

-   Type: string
-   Required: true
-   Description: the network role the appliance is configured in
    Orchestrator. spoke = 0, hub = 1, mesh= 2

Output type

None

/appliance/changeGroup/{groupPk}
--------------------------------

### POST

Summary

Change one or more appliances\' group

Notes

To get primary key of Orchestrator groups, perform GET /gms/group

Input type

application/json: undefined

groupPk

-   Description: group primary key
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

appliance primary keys

Description: An array of appliance primary keys

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array
-   Required: true
-   Description: Array of appliance primary keys. Eg: Appliance primary
    key looks like \"1.NE\"

Output type

None

/appliance/{nePk}
-----------------

### GET

Summary

Get appliance information

Input type

None

nePk

-   Description: the primary key of appliance
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

id

-   Type: string
-   Required: true
-   Description: This is the primary key of the appliance. Every
    appliance managed by Orchestrator is given a primary key and it
    uniquely identifies an appliance among all appliances managed by
    Orchestrator

uuid

-   Type: string
-   Required: true
-   Description: This is also an unique identifier. But it is generated
    and self assigned by the appliance itself.

networkRole

-   Type: string
-   Required: true
-   Description: The network role the appliance is configured in
    Orchestrator. spoke = 0, hub = 1, mesh= 2

site

-   Type: string
-   Required: true
-   Description: An identifier used to tag an appliance with a site name

sitePriority

-   Type: integer
-   Required: true
-   Description: Priority given to the appliance with the site it
    belongs to

userName

-   Type: string
-   Required: true
-   Description: User name used to login to appliance

password

-   Type: null
-   Required: true
-   Description: Password used to login to appliance. They are not
    returned for security reasons

groupId

-   Type: string
-   Required: true
-   Description: Primary key identifier of the Orchestrator group the
    appliance belongs to

IP

-   Type: string
-   Required: true
-   Description: IP Address of the appliance

webProtocolType

-   Type: integer
-   Required: true
-   Description: Protocol used by Orchestrator to talk to appliance.
    Http = 1, Https = 2, Both = 3

serial

-   Type: string
-   Required: true
-   Description: Serial number of the appliance

hasUnsavedChanges

-   Type: boolean
-   Required: true
-   Description: If true, there are unsaved changes on the appliance.

rebootRequired

-   Type: boolean
-   Required: true
-   Description: If true, the appliance requires a reboot

model

-   Type: string
-   Required: true
-   Description: Appliance model. Eg: NX-2610

hardwareRevision

-   Type: string
-   Required: true
-   Description: Hardware revision of the appliance. Eg: 200193-004 Rev
    A

hostName

-   Type: string
-   Required: true
-   Description: Appliance hostname

applianceId

-   Type: integer
-   Required: true
-   Description: This is yet another identifier of the appliance. But it
    is mainly used by the appliance to identify its peer

mode

-   Type: string
-   Required: true
-   Description: Deployment mode the appliance is configured in

bypass

-   Type: boolean
-   Required: true
-   Description: If true, appliance is currently in bypass

softwareVersion

-   Type: string
-   Required: true
-   Description: Software version running on the appliance

startupTime

-   Type: integer
-   Required: true
-   Description: Need description here.

webProtocol

-   Type: string
-   Required: true
-   Description: String version of the protocol field. Possible values
    are BOTH, HTTP, HTTPS

systemBandwidth

-   Type: integer
-   Required: true
-   Description: Appliance system bandwidth

state

-   Type: integer
-   Required: true
-   Description: State of the appliance. 0 - Unknown ( When an appliance
    is added to Orchestrator, it is in this state ), 1 - Normal (
    Appliance is reachable from Orchestrator), 2 - Unreachable (
    Appliance is unreachable from Orchestrator ), 3 - Unsupported
    Version ( Orchestrator does not support this version of the
    appliance ), 4 - Out of Synchronization ( Orchestrator\'s cache of
    appliance configuration/state is out of sync with the
    configuration/state on the appliance ), 5 - Synchronization in
    Progress ( Orchestrator is currently synchronizing appliances\'s
    configuration and state )

dynamicUuid

-   Type: string
-   Required: true
-   Description: Need description here.

portalObjectId

-   Type: string
-   Required: true
-   Description: Need description here

discoveredFrom

-   Type: integer
-   Required: true
-   Description: How the appliance was added to Orchestrator. 1 =
    MANUAL, 2 = PORTAL, 3 = APPLIANCE.

reachabilityChannel

-   Type: number
-   Required: true
-   Description: Reachability channel of the appliance. 0 - Unknown, 1 -
    Orchestrator talks to appliance using HTTP/HTTPS using user id and
    password. This is not used for EdgeConnects and SD-WAN, 2 -
    Appliance connects to Orchestrator using HTTPS Websocket.
    Orchestrator uses this permanent connection to configure/monitor
    appliance. This is done because Appliance may be behind a firewall
    making it hard for Orchestrator to contact appliance using IP
    address, 4 - Orchestrator sends configuration/monitoring request to
    Silver Peak Cloud Portal which relays those requests to Appliance.
    Appliance sends its response to Cloud Portal which relays it back to
    Orchestrator.

ip

-   Type: string
-   Required: true
-   Description: Same as IP

nePk

-   Type: string
-   Required: true
-   Description: Same as ID field

zoneList

-   Type: zoneListResponse
-   Required: true
-   Description: list of zones being used by appliance

interfaceList

-   Type: interfaceLabels
-   Required: true
-   Description: list of interface labels being used by appliance

/appliance/discovered
---------------------

### GET

Summary

Returns the all discovered appliances

Input type

None

Output type

None

/appliance/approved
-------------------

### GET

Summary

Get the all approved appliances

Input type

None

Output type

None

/appliance/denied
-----------------

### GET

Summary

Get the all denied appliances

Input type

None

Output type

None

/appliance/discovered/approve/{key}
-----------------------------------

### POST

Summary

Add and approve discovered appliances

Notes

This API is to approve a discovered appliance and add it to
Orchestrator. To find the ID of the orchestrator groups, see the
documentation for the \'group\' api. To find the ID of a particular
discovered appliance, use the /appliance/discovered api.

Input type

application/json: undefined

key

-   Description: Primary key of discovered appliance.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

id

-   Description: group ID
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/appliance/discovered/add/{key}
-------------------------------

### POST

Summary

Add discovered appliances to Orchestrator

Notes

This API is to add the discovered appliance to Orchestrator. To find the
ID of the orchestrator groups, see the documentation for the \'group\'
api. To find the ID of a particular discovered appliance, use the
/appliance/discovered api.\
To have the appliance licensed/approved. You should use the
/license/portal/appliance/grant API (after using this api to add the
discovered appliance)

Input type

application/json: undefined

key

-   Description: Primary key of discovered appliance.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

id

-   Description: group ID
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/appliance/discovered/deny/{id}
-------------------------------

### POST

Summary

Deny discovered appliances.

Input type

Unknown

id

-   Description: Primary key of the discovered appliance.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/appliance/discovered/update
----------------------------

### PUT

Summary

Trigger discovered appliances update

Notes

A request to this API will trigger the background job that updates
discovered appliances to start immediately.

Input type

Output type

None

/appliance/changePassword/{nePk}/{username}
-------------------------------------------

### POST

Summary

Change a user\'s password on appliance.

Notes

Change a user\'s password on appliance

Input type

application/json: undefined

nePk

-   Description: The nepk of the appliance.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

username

-   Description: The username you want to change the password for
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

password

-   Description: An object with the password you want to apply
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/appliance/rest/{nePk}/{url : (.\*)}
------------------------------------

### GET

Summary

To communicate with appliance GET APIs directly.

Notes

Just need to input /shaper in {url} to communicate with
/rest/json/shaper.

Input type

None

nePk

-   Description: The nepk of the appliance.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

url

-   Description: The API url part behind \'rest/json/\' of the
    appliance.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

### POST

Summary

To communicate with appliance POST APIs directly.

Notes

Just need to input /shaper in {url} to communicate with
/rest/json/shaper.

Input type

application/json: undefined

nePk

-   Description: The nepk of the appliance.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

url

-   Description: The API url part behind \'rest/json/\' of the
    appliance.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

data

-   Description: The data to post to the API of the appliance.
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/appliance/statsConfig
----------------------

### GET

Summary

To get stats config which will be synchronized to appliances.

Notes

To get stats config which will be synchronized to appliances.

Input type

None

Output type

application/json

verticalRetention

-   Type: integer
-   Required: true
-   Description: The setting of how many days of vertical stats this
    appliance will keep

minuteRetention

-   Type: integer
-   Required: true
-   Description: The setting of how many minutes of minute stats this
    appliance will keep

app

max\_items

-   Type: integer
-   Required: true
-   Description: -1 indicates that appliance controls the number of
    entries

evict\_enable

-   Type: boolean
-   Required: true
-   Description: Need some description here\....

port

max\_items

-   Type: integer
-   Required: true
-   Description: -1 indicates that appliance controls the number of
    entries

evict\_enable

-   Type: boolean
-   Required: true
-   Description: Need some description here\....

dns

max\_items

-   Type: integer
-   Required: true
-   Description: -1 indicates that appliance controls the number of
    entries

evict\_enable

-   Type: boolean
-   Required: true
-   Description: Need some description here\....

ip

max\_items

-   Type: integer
-   Required: true
-   Description: -1 indicates that appliance controls the number of
    entries

evict\_enable

-   Type: boolean
-   Required: true
-   Description: Need some description here\....

flows\_csv\_enable

-   Type: boolean
-   Required: true
-   Description: Need some description here\....

### POST

Summary

To modify stats config which will be synchronized to appliances.

Notes

To modify stats config which will be synchronized to appliances.

Input type

application/json: undefined

statsConfigPostData

Description: The config data to set for this API.

Required: true

Parameter type: POST Body

Data Type: application/json

verticalRetention

-   Type: integer
-   Required: true
-   Description: The setting of how many days of vertical stats this
    appliance will keep

minuteRetention

-   Type: integer
-   Required: true
-   Description: The setting of how many minutes of minute stats this
    appliance will keep

app

max\_items

-   Type: integer
-   Required: true
-   Description: -1 indicates that appliance controls the number of
    entries

evict\_enable

-   Type: boolean
-   Required: true
-   Description: Need some description here\....

port

max\_items

-   Type: integer
-   Required: true
-   Description: -1 indicates that appliance controls the number of
    entries

evict\_enable

-   Type: boolean
-   Required: true
-   Description: Need some description here\....

dns

max\_items

-   Type: integer
-   Required: true
-   Description: -1 indicates that appliance controls the number of
    entries

evict\_enable

-   Type: boolean
-   Required: true
-   Description: Need some description here\....

ip

max\_items

-   Type: integer
-   Required: true
-   Description: -1 indicates that appliance controls the number of
    entries

evict\_enable

-   Type: boolean
-   Required: true
-   Description: Need some description here\....

flows\_csv\_enable

-   Type: boolean
-   Required: true
-   Description: Need some description here\....

Output type

None

/appliance/statsConfig/default
------------------------------

### GET

Summary

To get default stats config which will be synchronized to appliances.

Notes

To get default stats config which will be synchronized to appliances.

Input type

None

Output type

application/json

verticalRetention

-   Type: integer
-   Required: true
-   Description: The setting of how many days of vertical stats this
    appliance will keep

minuteRetention

-   Type: integer
-   Required: true
-   Description: The setting of how many minutes of minute stats this
    appliance will keep

app

max\_items

-   Type: integer
-   Required: true
-   Description: -1 indicates that appliance controls the number of
    entries

evict\_enable

-   Type: boolean
-   Required: true
-   Description: Need some description here\....

port

max\_items

-   Type: integer
-   Required: true
-   Description: -1 indicates that appliance controls the number of
    entries

evict\_enable

-   Type: boolean
-   Required: true
-   Description: Need some description here\....

dns

max\_items

-   Type: integer
-   Required: true
-   Description: -1 indicates that appliance controls the number of
    entries

evict\_enable

-   Type: boolean
-   Required: true
-   Description: Need some description here\....

ip

max\_items

-   Type: integer
-   Required: true
-   Description: -1 indicates that appliance controls the number of
    entries

evict\_enable

-   Type: boolean
-   Required: true
-   Description: Need some description here\....

flows\_csv\_enable

-   Type: boolean
-   Required: true
-   Description: Need some description here\....

/appliance/dnsCache/config/{neId}?cached={cached}
-------------------------------------------------

### GET

Summary

Gets DNS Cache configurations

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

http

-   Type: boolean
-   Description: enable/disable flag for DNS http snooping

Reference: appliance.json

\

Manage appliance groups
=======================

/gms/group
----------

### GET

Summary

Get all orchestrator groups. A group is a logical collection of
appliances managed by orchestrator

Notes

There are system groups and there are user defined groups. System groups
are groups with id: 0.Network(Root group), 1.Network(Internal purposes)
and 2.Network(Auto discovered group). User defined groups start with id:
3.Network onwards

Input type

None

Output type

None

/gms/group/{id}
---------------

### GET

Summary

Get a single group

Notes

There are system groups and there are user defined groups. System groups
are groups with id: 0.Network(Root group), 1.Network(Internal purposes)
and 2.Network(Auto discovered group). User defined groups start with id:
3.Network onwards

Input type

None

id

-   Description: group primary key. Looks like \"10.Network\"
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

id

-   Type: string
-   Required: true
-   Description: Group primary key, like \'10.Network\'

name

-   Type: string
-   Required: true
-   Description: Unique name given to a group

subType

-   Type: integer
-   Required: true
-   Description: Network sub type: Root Group(0), Auto discovered
    group(2), User defined group(3)

parentId

-   Type: string
-   Required: true
-   Description: Primary key of parent group. If this group has no
    parent, the value will be null

backgroundImage

-   Type: string
-   Required: true
-   Description: image filename

### POST

Summary

Update a group

Notes

There are system groups and there are user defined groups. System groups
are groups with id: 0.Network(Root group), 1.Network(Internal purposes)
and 2.Network(Auto discovered group). User defined groups start with id:
3.Network onwards

Input type

application/json

id

-   Description: group primary key. Looks like \"10.Network\"
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

GroupUpdatePostBody

Description: group information

Required: true

Parameter type: POST Body

Data Type: application/json

id

-   Type: string
-   Required: true
-   Description: Primarky key. Like \'10.Network\'

name

-   Type: string
-   Required: true
-   Description: Unique group name

backgroundImage

-   Type: string
-   Description: Optional image filename

Output type

None

### DELETE

Summary

Delete a group

Input type

None

id

-   Description: group primary key
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/gms/group/new
--------------

### POST

Summary

Add new group

Notes

Only name and parentId are required. Background image URL is optional.

Input type

application/json

GroupNewPostBody

Description: Group information

Required: true

Parameter type: POST Body

Data Type: application/json

name

-   Type: string
-   Required: true
-   Description: Unique group name

parentId

-   Type: string
-   Required: true
-   Description: Primary key of parent group. Like \'10.Network\'

backgroundImage

-   Type: string
-   Description: image filename

Output type

None

/gms/group/root
---------------

### GET

Summary

Get root group

Input type

None

Output type

application/json

id

-   Type: string
-   Required: true
-   Description: Group primary key, like \'10.Network\'

name

-   Type: string
-   Required: true
-   Description: Unique name given to a group

subType

-   Type: integer
-   Required: true
-   Description: Network sub type: Root Group(0), Auto discovered
    group(2), User defined group(3)

parentId

-   Type: string
-   Required: true
-   Description: Primary key of parent group. If this group has no
    parent, the value will be null

backgroundImage

-   Type: string
-   Required: true
-   Description: image filename

/gms/grNode
-----------

### GET

Summary

Get appliance positions on a map for topology

Input type

None

Output type

None

/gms/grNode/{grNodePk}
----------------------

### GET

Summary

Get appliance position by graphical node primary key

Input type

None

grNodePk

-   Description: Graphical node primary key
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

id

-   Type: string
-   Description: ID, assigned by Orchestrator like 1.GrNode

groupId

-   Type: string
-   Required: true
-   Description: ID of the group belonged to

sourceId

-   Type: string
-   Required: true
-   Description: Source ID: appliance(neId like 0.NE), group(group ID
    like 10.Network)

appliance

-   Type: boolean
-   Required: true
-   Description: Appliance(true), Group(false)

wx

-   Type: integer
-   Required: true
-   Description: Coordinates X in map window

wy

-   Type: integer
-   Required: true
-   Description: Coordinates Y in map window

latitude

-   Type: double
-   Required: true
-   Description: Latitude

longitude

-   Type: double
-   Required: true
-   Description: Longitude

### POST

Summary

Update appliance position

Input type

application/json: undefined

grNodePk

-   Description: Graphical node primary key
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

GRNodeUpdatePostBody

Description: Position object

Required: true

Parameter type: POST Body

Data Type: application/json

wx

-   Type: integer
-   Required: true
-   Description: Coordinates X in map window

wy

-   Type: integer
-   Required: true
-   Description: Coordinates Y in map window

latitude

-   Type: double
-   Required: true
-   Description: Latitude

longitude

-   Type: double
-   Required: true
-   Description: Longitude

Output type

application/json

id

-   Type: string
-   Description: ID, assigned by Orchestrator like 1.GrNode

groupId

-   Type: string
-   Required: true
-   Description: ID of the group belonged to

sourceId

-   Type: string
-   Required: true
-   Description: Source ID: appliance(neId like 0.NE), group(group ID
    like 10.Network)

appliance

-   Type: boolean
-   Required: true
-   Description: Appliance(true), Group(false)

wx

-   Type: integer
-   Required: true
-   Description: Coordinates X in map window

wy

-   Type: integer
-   Required: true
-   Description: Coordinates Y in map window

latitude

-   Type: double
-   Required: true
-   Description: Latitude

longitude

-   Type: double
-   Required: true
-   Description: Longitude

/gms/grNode/forNePk/{nePk}
--------------------------

### POST

Summary

Update appliance position by nePk

Notes

Updates an appliance position by nePk. Will only update the values (wx,
wy, latitude, longitude) if the value is not 0.

Input type

application/json: undefined

nePk

-   Description: The appliance\'s nePk
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

GRNodeUpdatePostBody

Description: Position object

Required: true

Parameter type: POST Body

Data Type: application/json

wx

-   Type: integer
-   Required: true
-   Description: Coordinates X in map window

wy

-   Type: integer
-   Required: true
-   Description: Coordinates Y in map window

latitude

-   Type: double
-   Required: true
-   Description: Latitude

longitude

-   Type: double
-   Required: true
-   Description: Longitude

Output type

None

Reference: group.json

\

Change the topology location for appliances
===========================================

/gms/grNode
-----------

### GET

Summary

Get appliance positions on a map for topology

Input type

None

Output type

None

/gms/grNode/{grNodePk}
----------------------

### GET

Summary

Get appliance position by graphical node primary key

Input type

None

grNodePk

-   Description: Graphical node primary key
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

id

-   Type: string
-   Description: ID, assigned by Orchestrator like 1.GrNode

groupId

-   Type: string
-   Required: true
-   Description: ID of the group belonged to

sourceId

-   Type: string
-   Required: true
-   Description: Source ID: appliance(neId like 0.NE), group(group ID
    like 10.Network)

appliance

-   Type: boolean
-   Required: true
-   Description: Appliance(true), Group(false)

wx

-   Type: integer
-   Required: true
-   Description: Coordinates X in map window

wy

-   Type: integer
-   Required: true
-   Description: Coordinates Y in map window

latitude

-   Type: double
-   Required: true
-   Description: Latitude

longitude

-   Type: double
-   Required: true
-   Description: Longitude

### POST

Summary

Update appliance position

Input type

application/json: undefined

grNodePk

-   Description: Graphical node primary key
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

GRNodeUpdatePostBody

Description: Position object

Required: true

Parameter type: POST Body

Data Type: application/json

wx

-   Type: integer
-   Required: true
-   Description: Coordinates X in map window

wy

-   Type: integer
-   Required: true
-   Description: Coordinates Y in map window

latitude

-   Type: double
-   Required: true
-   Description: Latitude

longitude

-   Type: double
-   Required: true
-   Description: Longitude

Output type

application/json

id

-   Type: string
-   Description: ID, assigned by Orchestrator like 1.GrNode

groupId

-   Type: string
-   Required: true
-   Description: ID of the group belonged to

sourceId

-   Type: string
-   Required: true
-   Description: Source ID: appliance(neId like 0.NE), group(group ID
    like 10.Network)

appliance

-   Type: boolean
-   Required: true
-   Description: Appliance(true), Group(false)

wx

-   Type: integer
-   Required: true
-   Description: Coordinates X in map window

wy

-   Type: integer
-   Required: true
-   Description: Coordinates Y in map window

latitude

-   Type: double
-   Required: true
-   Description: Latitude

longitude

-   Type: double
-   Required: true
-   Description: Longitude

/gms/grNode/forNePk/{nePk}
--------------------------

### POST

Summary

Update appliance position by nePk

Notes

Updates an appliance position by nePk. Will only update the values (wx,
wy, latitude, longitude) if the value is not 0.

Input type

application/json: undefined

nePk

-   Description: The appliance\'s nePk
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

GRNodeUpdatePostBody

Description: Position object

Required: true

Parameter type: POST Body

Data Type: application/json

wx

-   Type: integer
-   Required: true
-   Description: Coordinates X in map window

wy

-   Type: integer
-   Required: true
-   Description: Coordinates Y in map window

latitude

-   Type: double
-   Required: true
-   Description: Latitude

longitude

-   Type: double
-   Required: true
-   Description: Longitude

Output type

None

Reference: grnode.json

\

VXOA aggregate statistics
=========================

/stats/aggregate/tunnel
-----------------------

### GET

Summary

Get aggregate tunnel stats data filter by query parameters.

Notes

This operation is used to retrieve tunnel aggregate stats. Depend on
what query parameters are provided. The response format may look
different.

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

groupPk

-   Description: Filter data for a specific group
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

metric

-   Description: If this query parameter is given, we aggregate stats in
    a way that we sort stats by this {metric}. This query parameter
    could also be used with {top} query parameters to limit how many
    items we want to retrieve.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: This parameter should be provided together with
    {metric} as this query parameter is used to indicate top x items of
    a metric. Example, if metric=throughput&top=10 is provided, you\'re
    saying you want to retrieve top 10 tunnels by throughput.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

overlay

-   Description: when it is \'all\',return all bonded tunnels;when it is
    \'0\',return all physical tunnels;when it is not given,return all
    bonded and physical tunnels;otherwise,return bonded tunnels
    associated with the overlay id.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<tunnelName\>

\<nePk/IP\>

nePk

-   Type: string
-   Required: true
-   Description: Internal ID of appliance

IP

-   Type: string
-   Required: true
-   Description: Management IP address of appliance

HOSTNAME

-   Type: string
-   Required: true
-   Description: Hostname of the appliance

MAX\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual starting boundary of data time range

MIN\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual ending boundary of data time range

SUM\_LRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side received

SUM\_LTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side transmitted

SUM\_LRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_LTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received

SUM\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted

SUM\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received

SUM\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted

MAX\_COMP\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate

MAX\_COMP\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate

MAX\_COMP\_NOOHEAD\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate
    without overhead

MAX\_COMP\_NOOHEAD\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate
    without overhead

MAX\_LRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side received

MAX\_LTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side transmitted

MAX\_LRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side received

MAX\_LTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side transmitted

MAX\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side received

MAX\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side transmitted

MAX\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side received

MAX\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side transmitted

MAX\_COMP\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate

MAX\_COMP\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate

MAX\_COMP\_NOOHEAD\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate without overhead

MAX\_COMP\_NOOHEAD\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate without overhead

SUM\_OHEAD\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received with overhead

SUM\_OHEAD\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted with overhead

SUM\_OHEAD\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received with overhead

SUM\_OHEAD\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted with overhead

SUM\_OHEAD\_WRX\_HDR\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of header
    bytes WAN side received with overhead

SUM\_OHEAD\_WTX\_HDR\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of header
    bytes WAN side transmitted with overhead

AVG\_BW\_PCT\_UTIL\_AVG

-   Type: integer
-   Required: true
-   Description: Integer value of average bandwidth utilization
    percentage

MAX\_OHEAD\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    bytes WAN side received with overhead

MAX\_OHEAD\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    bytes WAN side transmitted with overhead

MAX\_OHEAD\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    packets WAN side received with overhead

MAX\_OHEAD\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    packets WAN side transmitted with overhead

MAX\_OHEAD\_WRX\_HDR\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    header bytes WAN side received with overhead

MAX\_OHEAD\_WTX\_HDR\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    header bytes WAN side transmitted with overhead

MAX\_BW\_PCT\_UTIL\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak bandwidth utilization
    percentage

SUM\_TCP\_FLOWS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of TCP flows

SUM\_TCP\_ACC\_FLOWS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of TCP accelerated flows

SUM\_NON\_TCP\_FLOWS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of non-TCP flows

SUM\_CREATED

-   Type: integer
-   Required: true
-   Description: Integer value of total number of created flows

MAX\_CREATED\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of created flows

SUM\_DELETED

-   Type: integer
-   Required: true
-   Description: Integer value of total number of deleted flows

MAX\_DELETED\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of deleted flows

MAX\_TCP\_FLOWS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of TCP flows

MAX\_TCP\_ACC\_FLOWS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of TCP flows

MAX\_NON\_TCP\_FLOWS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of TCP flows

AVG\_LATENCY\_AVG

-   Type: integer
-   Required: true
-   Description: Integer value of average latency, unit is millisecond

MIN\_LATENCY\_MIN

-   Type: integer
-   Required: true
-   Description: Integer value of minimum lowest latency, unit is
    millisecond

MAX\_LATENCY\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum highest latency, unit is
    millisecond

SUM\_PRE\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of pre-corrected packets
    loss

SUM\_POST\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of post-corrected packets
    loss

SUM\_PRE\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of total number of pre-POC packets loss

SUM\_POST\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of total number of post-POC packets loss

MAX\_PRE\_PCT\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of maximum pre-corrected packets loss
    percentage

MAX\_POST\_PCT\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of maximum post-corrected packets loss
    percentage

MAX\_PRE\_PCT\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of maximum pre-POC packets loss
    percentage

SUM\_POST\_PCT\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of maximum post-POC packets loss
    percentage

MAX\_PRE\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-corrected packets
    loss

MAX\_POST\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-corrected packets
    loss

MAX\_PRE\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-POC packets loss

MAX\_POST\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-POC packets loss

MAX\_PRE\_PCT\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-corrected packets
    loss

MAX\_POST\_PCT\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-corrected packets
    loss

MAX\_PRE\_PCT\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-POC packets loss
    percentage

POST\_PCT\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-POC packets loss
    percentage

LATENCY\_MIN\_FROM\_PEAK

-   Type: integer
-   Required: true
-   Description: Integer value of the second peak latency

### POST

Summary

Get aggregate tunnel stats data filter by query parameters.

Notes

This operation is used to retrieve tunnel aggregate stats and it\'s
identical to the GET operation on this URL except that you need to pass
a request body containing appliance ids that you want to filter on to
it. Depend on what query parameters are provided. The response format
may look different.

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

metric

-   Description: If this query parameter is given, we aggregate stats in
    a way that we sort stats by this {metric}. This query parameter
    could also be used with {top} query parameters to limit how many
    items we want to retrieve.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: This parameter should be provided together with
    {metric} as this query parameter is used to indicate top x items of
    a metric. Example, if metric=throughput&top=10 is provided, you\'re
    saying you want to retrieve top 10 tunnels by throughput.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

overlay

-   Description: when it is \'all\',return all bonded tunnels;when it is
    \'0\',return all physical tunnels;when it is not given,return all
    bonded and physical tunnels;otherwise,return bonded tunnels
    associated with the overlay id.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<tunnelName\>

\<nePk/IP\>

nePk

-   Type: string
-   Required: true
-   Description: Internal ID of appliance

IP

-   Type: string
-   Required: true
-   Description: Management IP address of appliance

HOSTNAME

-   Type: string
-   Required: true
-   Description: Hostname of the appliance

MAX\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual starting boundary of data time range

MIN\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual ending boundary of data time range

SUM\_LRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side received

SUM\_LTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side transmitted

SUM\_LRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_LTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received

SUM\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted

SUM\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received

SUM\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted

MAX\_COMP\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate

MAX\_COMP\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate

MAX\_COMP\_NOOHEAD\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate
    without overhead

MAX\_COMP\_NOOHEAD\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate
    without overhead

MAX\_LRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side received

MAX\_LTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side transmitted

MAX\_LRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side received

MAX\_LTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side transmitted

MAX\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side received

MAX\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side transmitted

MAX\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side received

MAX\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side transmitted

MAX\_COMP\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate

MAX\_COMP\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate

MAX\_COMP\_NOOHEAD\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate without overhead

MAX\_COMP\_NOOHEAD\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate without overhead

SUM\_OHEAD\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received with overhead

SUM\_OHEAD\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted with overhead

SUM\_OHEAD\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received with overhead

SUM\_OHEAD\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted with overhead

SUM\_OHEAD\_WRX\_HDR\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of header
    bytes WAN side received with overhead

SUM\_OHEAD\_WTX\_HDR\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of header
    bytes WAN side transmitted with overhead

AVG\_BW\_PCT\_UTIL\_AVG

-   Type: integer
-   Required: true
-   Description: Integer value of average bandwidth utilization
    percentage

MAX\_OHEAD\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    bytes WAN side received with overhead

MAX\_OHEAD\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    bytes WAN side transmitted with overhead

MAX\_OHEAD\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    packets WAN side received with overhead

MAX\_OHEAD\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    packets WAN side transmitted with overhead

MAX\_OHEAD\_WRX\_HDR\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    header bytes WAN side received with overhead

MAX\_OHEAD\_WTX\_HDR\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    header bytes WAN side transmitted with overhead

MAX\_BW\_PCT\_UTIL\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak bandwidth utilization
    percentage

SUM\_TCP\_FLOWS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of TCP flows

SUM\_TCP\_ACC\_FLOWS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of TCP accelerated flows

SUM\_NON\_TCP\_FLOWS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of non-TCP flows

SUM\_CREATED

-   Type: integer
-   Required: true
-   Description: Integer value of total number of created flows

MAX\_CREATED\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of created flows

SUM\_DELETED

-   Type: integer
-   Required: true
-   Description: Integer value of total number of deleted flows

MAX\_DELETED\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of deleted flows

MAX\_TCP\_FLOWS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of TCP flows

MAX\_TCP\_ACC\_FLOWS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of TCP flows

MAX\_NON\_TCP\_FLOWS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of TCP flows

AVG\_LATENCY\_AVG

-   Type: integer
-   Required: true
-   Description: Integer value of average latency, unit is millisecond

MIN\_LATENCY\_MIN

-   Type: integer
-   Required: true
-   Description: Integer value of minimum lowest latency, unit is
    millisecond

MAX\_LATENCY\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum highest latency, unit is
    millisecond

SUM\_PRE\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of pre-corrected packets
    loss

SUM\_POST\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of post-corrected packets
    loss

SUM\_PRE\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of total number of pre-POC packets loss

SUM\_POST\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of total number of post-POC packets loss

MAX\_PRE\_PCT\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of maximum pre-corrected packets loss
    percentage

MAX\_POST\_PCT\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of maximum post-corrected packets loss
    percentage

MAX\_PRE\_PCT\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of maximum pre-POC packets loss
    percentage

SUM\_POST\_PCT\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of maximum post-POC packets loss
    percentage

MAX\_PRE\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-corrected packets
    loss

MAX\_POST\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-corrected packets
    loss

MAX\_PRE\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-POC packets loss

MAX\_POST\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-POC packets loss

MAX\_PRE\_PCT\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-corrected packets
    loss

MAX\_POST\_PCT\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-corrected packets
    loss

MAX\_PRE\_PCT\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-POC packets loss
    percentage

POST\_PCT\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-POC packets loss
    percentage

LATENCY\_MIN\_FROM\_PEAK

-   Type: integer
-   Required: true
-   Description: Integer value of the second peak latency

/stats/aggregate/tunnel/{nePk}
------------------------------

### GET

Summary

Get aggregate tunnel stats data for a single appliance filter by query
parameters.

Notes

This operation is used to retrieve tunnel aggregate stats for a single
appliance. Depend on what query parameters are provided. The response
format may look different.

Input type

None

nePk

-   Description: Appliance internal id to filter from.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

tunnelName

-   Description: Filter for data which belongs to tunnel with name
    {tunnelName}
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

metric

-   Description: If this query parameter is given, we aggregate stats in
    a way that we sort stats by this {metric}. This query parameter
    could also be used with {top} query parameters to limit how many
    items we want to retrieve.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: This parameter should be provided together with
    {metric} as this query parameter is used to indicate top x items of
    a metric. Example, if metric=throughput&top=10 is provided, you\'re
    saying you want to retrieve top 10 tunnels by throughput.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

overlay

-   Description: when it is \'all\',return all bonded tunnels;when it is
    \'0\',return all physical tunnels;when it is not given,return all
    bonded and physical tunnels;otherwise,return bonded tunnels
    associated with the overlay id.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<tunnelName\>

\<nePk/IP\>

nePk

-   Type: string
-   Required: true
-   Description: Internal ID of appliance

IP

-   Type: string
-   Required: true
-   Description: Management IP address of appliance

HOSTNAME

-   Type: string
-   Required: true
-   Description: Hostname of the appliance

MAX\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual starting boundary of data time range

MIN\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual ending boundary of data time range

SUM\_LRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side received

SUM\_LTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side transmitted

SUM\_LRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_LTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received

SUM\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted

SUM\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received

SUM\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted

MAX\_COMP\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate

MAX\_COMP\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate

MAX\_COMP\_NOOHEAD\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate
    without overhead

MAX\_COMP\_NOOHEAD\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate
    without overhead

MAX\_LRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side received

MAX\_LTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side transmitted

MAX\_LRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side received

MAX\_LTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side transmitted

MAX\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side received

MAX\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side transmitted

MAX\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side received

MAX\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side transmitted

MAX\_COMP\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate

MAX\_COMP\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate

MAX\_COMP\_NOOHEAD\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate without overhead

MAX\_COMP\_NOOHEAD\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate without overhead

SUM\_OHEAD\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received with overhead

SUM\_OHEAD\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted with overhead

SUM\_OHEAD\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received with overhead

SUM\_OHEAD\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted with overhead

SUM\_OHEAD\_WRX\_HDR\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of header
    bytes WAN side received with overhead

SUM\_OHEAD\_WTX\_HDR\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of header
    bytes WAN side transmitted with overhead

AVG\_BW\_PCT\_UTIL\_AVG

-   Type: integer
-   Required: true
-   Description: Integer value of average bandwidth utilization
    percentage

MAX\_OHEAD\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    bytes WAN side received with overhead

MAX\_OHEAD\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    bytes WAN side transmitted with overhead

MAX\_OHEAD\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    packets WAN side received with overhead

MAX\_OHEAD\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    packets WAN side transmitted with overhead

MAX\_OHEAD\_WRX\_HDR\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    header bytes WAN side received with overhead

MAX\_OHEAD\_WTX\_HDR\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    header bytes WAN side transmitted with overhead

MAX\_BW\_PCT\_UTIL\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak bandwidth utilization
    percentage

SUM\_TCP\_FLOWS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of TCP flows

SUM\_TCP\_ACC\_FLOWS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of TCP accelerated flows

SUM\_NON\_TCP\_FLOWS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of non-TCP flows

SUM\_CREATED

-   Type: integer
-   Required: true
-   Description: Integer value of total number of created flows

MAX\_CREATED\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of created flows

SUM\_DELETED

-   Type: integer
-   Required: true
-   Description: Integer value of total number of deleted flows

MAX\_DELETED\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of deleted flows

MAX\_TCP\_FLOWS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of TCP flows

MAX\_TCP\_ACC\_FLOWS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of TCP flows

MAX\_NON\_TCP\_FLOWS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of TCP flows

AVG\_LATENCY\_AVG

-   Type: integer
-   Required: true
-   Description: Integer value of average latency, unit is millisecond

MIN\_LATENCY\_MIN

-   Type: integer
-   Required: true
-   Description: Integer value of minimum lowest latency, unit is
    millisecond

MAX\_LATENCY\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum highest latency, unit is
    millisecond

SUM\_PRE\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of pre-corrected packets
    loss

SUM\_POST\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of post-corrected packets
    loss

SUM\_PRE\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of total number of pre-POC packets loss

SUM\_POST\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of total number of post-POC packets loss

MAX\_PRE\_PCT\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of maximum pre-corrected packets loss
    percentage

MAX\_POST\_PCT\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of maximum post-corrected packets loss
    percentage

MAX\_PRE\_PCT\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of maximum pre-POC packets loss
    percentage

SUM\_POST\_PCT\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of maximum post-POC packets loss
    percentage

MAX\_PRE\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-corrected packets
    loss

MAX\_POST\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-corrected packets
    loss

MAX\_PRE\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-POC packets loss

MAX\_POST\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-POC packets loss

MAX\_PRE\_PCT\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-corrected packets
    loss

MAX\_POST\_PCT\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-corrected packets
    loss

MAX\_PRE\_PCT\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-POC packets loss
    percentage

POST\_PCT\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-POC packets loss
    percentage

LATENCY\_MIN\_FROM\_PEAK

-   Type: integer
-   Required: true
-   Description: Integer value of the second peak latency

/stats/aggregate/appliance
--------------------------

### GET

Summary

Get aggregate appliance stats data filter by query parameters.

Notes

This operation is used to retrieve appliance aggregate stats. Depend on
what query parameters are provided. The response format may look
different.

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

groupPk

-   Description: Filter data for a specific group
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

metric

-   Description: If this query parameter is given, we aggregate stats in
    a way that we sort stats by this {metric}. This query parameter
    could also be used with {top} query parameters to limit how many
    items we want to retrieve.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: This parameter should be provided together with
    {metric} as this query parameter is used to indicate top x items of
    a metric. Example, if metric=throughput&top=10 is provided, you\'re
    saying you want to retrieve top 10 tunnels by throughput.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<trafficType\>

\<nePk/IP\>

nePk

-   Type: string
-   Required: true
-   Description: Internal ID of appliance

IP

-   Type: string
-   Required: true
-   Description: Management IP address of appliance

HOSTNAME

-   Type: string
-   Required: true
-   Description: Hostname of the appliance

MAX\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual starting boundary of data time range

MIN\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual ending boundary of data time range

SUM\_LRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side received

SUM\_LTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side transmitted

SUM\_LRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_LTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received

SUM\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted

SUM\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received

SUM\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted

MAX\_COMP\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate

MAX\_COMP\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate

MAX\_COMP\_NOOHEAD\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate
    without overhead

MAX\_COMP\_NOOHEAD\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate
    without overhead

MAX\_LRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side received

MAX\_LTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side transmitted

MAX\_LRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side received

MAX\_LTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side transmitted

MAX\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side received

MAX\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side transmitted

MAX\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side received

MAX\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side transmitted

MAX\_COMP\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate

MAX\_COMP\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate

MAX\_COMP\_NOOHEAD\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate without overhead

MAX\_COMP\_NOOHEAD\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate without overhead

SUM\_OHEAD\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received with overhead

SUM\_OHEAD\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted with overhead

SUM\_OHEAD\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received with overhead

SUM\_OHEAD\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted with overhead

SUM\_OHEAD\_WRX\_HDR\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of header
    bytes WAN side received with overhead

SUM\_OHEAD\_WTX\_HDR\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of header
    bytes WAN side transmitted with overhead

AVG\_BW\_PCT\_UTIL\_AVG

-   Type: integer
-   Required: true
-   Description: Integer value of average bandwidth utilization
    percentage

MAX\_OHEAD\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    bytes WAN side received with overhead

MAX\_OHEAD\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    bytes WAN side transmitted with overhead

MAX\_OHEAD\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    packets WAN side received with overhead

MAX\_OHEAD\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    packets WAN side transmitted with overhead

MAX\_OHEAD\_WRX\_HDR\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    header bytes WAN side received with overhead

MAX\_OHEAD\_WTX\_HDR\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    header bytes WAN side transmitted with overhead

MAX\_BW\_PCT\_UTIL\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak bandwidth utilization
    percentage

SUM\_TCP\_FLOWS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of TCP flows

SUM\_TCP\_ACC\_FLOWS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of TCP accelerated flows

SUM\_NON\_TCP\_FLOWS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of non-TCP flows

SUM\_CREATED

-   Type: integer
-   Required: true
-   Description: Integer value of total number of created flows

MAX\_CREATED\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of created flows

SUM\_DELETED

-   Type: integer
-   Required: true
-   Description: Integer value of total number of deleted flows

MAX\_DELETED\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of deleted flows

MAX\_TCP\_FLOWS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of TCP flows

MAX\_TCP\_ACC\_FLOWS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of TCP flows

MAX\_NON\_TCP\_FLOWS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of TCP flows

AVG\_LATENCY\_AVG

-   Type: integer
-   Required: true
-   Description: Integer value of average latency, unit is millisecond

MIN\_LATENCY\_MIN

-   Type: integer
-   Required: true
-   Description: Integer value of minimum lowest latency, unit is
    millisecond

MAX\_LATENCY\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum highest latency, unit is
    millisecond

SUM\_PRE\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of pre-corrected packets
    loss

SUM\_POST\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of post-corrected packets
    loss

SUM\_PRE\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of total number of pre-POC packets loss

SUM\_POST\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of total number of post-POC packets loss

MAX\_PRE\_PCT\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of maximum pre-corrected packets loss
    percentage

MAX\_POST\_PCT\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of maximum post-corrected packets loss
    percentage

MAX\_PRE\_PCT\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of maximum pre-POC packets loss
    percentage

SUM\_POST\_PCT\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of maximum post-POC packets loss
    percentage

MAX\_PRE\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-corrected packets
    loss

MAX\_POST\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-corrected packets
    loss

MAX\_PRE\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-POC packets loss

MAX\_POST\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-POC packets loss

MAX\_PRE\_PCT\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-corrected packets
    loss

MAX\_POST\_PCT\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-corrected packets
    loss

MAX\_PRE\_PCT\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-POC packets loss
    percentage

POST\_PCT\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-POC packets loss
    percentage

LATENCY\_MIN\_FROM\_PEAK

-   Type: integer
-   Required: true
-   Description: Integer value of the second peak latency

### POST

Summary

Get aggregate appliance stats data filter by query parameters.

Notes

This operation is used to retrieve appliance aggregate stats and it\'s
identical to the GET operation on this URL except that you need to pass
a request body containing appliance ids that you want to filter on to
it. Depend on what query parameters are provided. The response format
may look different.

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

metric

-   Description: If this query parameter is given, we aggregate stats in
    a way that we sort stats by this {metric}. This query parameter
    could also be used with {top} query parameters to limit how many
    items we want to retrieve.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: This parameter should be provided together with
    {metric} as this query parameter is used to indicate top x items of
    a metric. Example, if metric=throughput&top=10 is provided, you\'re
    saying you want to retrieve top 10 tunnels by throughput.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<trafficType\>

\<nePk/IP\>

nePk

-   Type: string
-   Required: true
-   Description: Internal ID of appliance

IP

-   Type: string
-   Required: true
-   Description: Management IP address of appliance

HOSTNAME

-   Type: string
-   Required: true
-   Description: Hostname of the appliance

MAX\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual starting boundary of data time range

MIN\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual ending boundary of data time range

SUM\_LRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side received

SUM\_LTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side transmitted

SUM\_LRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_LTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received

SUM\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted

SUM\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received

SUM\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted

MAX\_COMP\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate

MAX\_COMP\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate

MAX\_COMP\_NOOHEAD\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate
    without overhead

MAX\_COMP\_NOOHEAD\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate
    without overhead

MAX\_LRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side received

MAX\_LTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side transmitted

MAX\_LRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side received

MAX\_LTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side transmitted

MAX\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side received

MAX\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side transmitted

MAX\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side received

MAX\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side transmitted

MAX\_COMP\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate

MAX\_COMP\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate

MAX\_COMP\_NOOHEAD\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate without overhead

MAX\_COMP\_NOOHEAD\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate without overhead

SUM\_OHEAD\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received with overhead

SUM\_OHEAD\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted with overhead

SUM\_OHEAD\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received with overhead

SUM\_OHEAD\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted with overhead

SUM\_OHEAD\_WRX\_HDR\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of header
    bytes WAN side received with overhead

SUM\_OHEAD\_WTX\_HDR\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of header
    bytes WAN side transmitted with overhead

AVG\_BW\_PCT\_UTIL\_AVG

-   Type: integer
-   Required: true
-   Description: Integer value of average bandwidth utilization
    percentage

MAX\_OHEAD\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    bytes WAN side received with overhead

MAX\_OHEAD\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    bytes WAN side transmitted with overhead

MAX\_OHEAD\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    packets WAN side received with overhead

MAX\_OHEAD\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    packets WAN side transmitted with overhead

MAX\_OHEAD\_WRX\_HDR\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    header bytes WAN side received with overhead

MAX\_OHEAD\_WTX\_HDR\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    header bytes WAN side transmitted with overhead

MAX\_BW\_PCT\_UTIL\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak bandwidth utilization
    percentage

SUM\_TCP\_FLOWS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of TCP flows

SUM\_TCP\_ACC\_FLOWS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of TCP accelerated flows

SUM\_NON\_TCP\_FLOWS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of non-TCP flows

SUM\_CREATED

-   Type: integer
-   Required: true
-   Description: Integer value of total number of created flows

MAX\_CREATED\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of created flows

SUM\_DELETED

-   Type: integer
-   Required: true
-   Description: Integer value of total number of deleted flows

MAX\_DELETED\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of deleted flows

MAX\_TCP\_FLOWS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of TCP flows

MAX\_TCP\_ACC\_FLOWS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of TCP flows

MAX\_NON\_TCP\_FLOWS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of TCP flows

AVG\_LATENCY\_AVG

-   Type: integer
-   Required: true
-   Description: Integer value of average latency, unit is millisecond

MIN\_LATENCY\_MIN

-   Type: integer
-   Required: true
-   Description: Integer value of minimum lowest latency, unit is
    millisecond

MAX\_LATENCY\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum highest latency, unit is
    millisecond

SUM\_PRE\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of pre-corrected packets
    loss

SUM\_POST\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of post-corrected packets
    loss

SUM\_PRE\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of total number of pre-POC packets loss

SUM\_POST\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of total number of post-POC packets loss

MAX\_PRE\_PCT\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of maximum pre-corrected packets loss
    percentage

MAX\_POST\_PCT\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of maximum post-corrected packets loss
    percentage

MAX\_PRE\_PCT\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of maximum pre-POC packets loss
    percentage

SUM\_POST\_PCT\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of maximum post-POC packets loss
    percentage

MAX\_PRE\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-corrected packets
    loss

MAX\_POST\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-corrected packets
    loss

MAX\_PRE\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-POC packets loss

MAX\_POST\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-POC packets loss

MAX\_PRE\_PCT\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-corrected packets
    loss

MAX\_POST\_PCT\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-corrected packets
    loss

MAX\_PRE\_PCT\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-POC packets loss
    percentage

POST\_PCT\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-POC packets loss
    percentage

LATENCY\_MIN\_FROM\_PEAK

-   Type: integer
-   Required: true
-   Description: Integer value of the second peak latency

/stats/aggregate/appliance/{nePk}
---------------------------------

### GET

Summary

Get aggregate appliance stats data for a single appliance filter by
query parameters.

Notes

This operation is used to retrieve appliance aggregate stats for a
single appliance. Depend on what query parameters are provided. The
response format may look different.

Input type

None

nePk

-   Description: Appliance internal id to filter from.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

metric

-   Description: If this query parameter is given, we aggregate stats in
    a way that we sort stats by this {metric}. This query parameter
    could also be used with {top} query parameters to limit how many
    items we want to retrieve.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: This parameter should be provided together with
    {metric} as this query parameter is used to indicate top x items of
    a metric. Example, if metric=throughput&top=10 is provided, you\'re
    saying you want to retrieve top 10 tunnels by throughput.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<trafficType\>

\<nePk/IP\>

nePk

-   Type: string
-   Required: true
-   Description: Internal ID of appliance

IP

-   Type: string
-   Required: true
-   Description: Management IP address of appliance

HOSTNAME

-   Type: string
-   Required: true
-   Description: Hostname of the appliance

MAX\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual starting boundary of data time range

MIN\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual ending boundary of data time range

SUM\_LRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side received

SUM\_LTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side transmitted

SUM\_LRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_LTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received

SUM\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted

SUM\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received

SUM\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted

MAX\_COMP\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate

MAX\_COMP\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate

MAX\_COMP\_NOOHEAD\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate
    without overhead

MAX\_COMP\_NOOHEAD\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate
    without overhead

MAX\_LRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side received

MAX\_LTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side transmitted

MAX\_LRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side received

MAX\_LTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side transmitted

MAX\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side received

MAX\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side transmitted

MAX\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side received

MAX\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side transmitted

MAX\_COMP\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate

MAX\_COMP\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate

MAX\_COMP\_NOOHEAD\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate without overhead

MAX\_COMP\_NOOHEAD\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate without overhead

SUM\_OHEAD\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received with overhead

SUM\_OHEAD\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted with overhead

SUM\_OHEAD\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received with overhead

SUM\_OHEAD\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted with overhead

SUM\_OHEAD\_WRX\_HDR\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of header
    bytes WAN side received with overhead

SUM\_OHEAD\_WTX\_HDR\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of header
    bytes WAN side transmitted with overhead

AVG\_BW\_PCT\_UTIL\_AVG

-   Type: integer
-   Required: true
-   Description: Integer value of average bandwidth utilization
    percentage

MAX\_OHEAD\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    bytes WAN side received with overhead

MAX\_OHEAD\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    bytes WAN side transmitted with overhead

MAX\_OHEAD\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    packets WAN side received with overhead

MAX\_OHEAD\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    packets WAN side transmitted with overhead

MAX\_OHEAD\_WRX\_HDR\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    header bytes WAN side received with overhead

MAX\_OHEAD\_WTX\_HDR\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of maximum peak number of
    header bytes WAN side transmitted with overhead

MAX\_BW\_PCT\_UTIL\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak bandwidth utilization
    percentage

SUM\_TCP\_FLOWS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of TCP flows

SUM\_TCP\_ACC\_FLOWS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of TCP accelerated flows

SUM\_NON\_TCP\_FLOWS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of non-TCP flows

SUM\_CREATED

-   Type: integer
-   Required: true
-   Description: Integer value of total number of created flows

MAX\_CREATED\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of created flows

SUM\_DELETED

-   Type: integer
-   Required: true
-   Description: Integer value of total number of deleted flows

MAX\_DELETED\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of deleted flows

MAX\_TCP\_FLOWS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of TCP flows

MAX\_TCP\_ACC\_FLOWS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of TCP flows

MAX\_NON\_TCP\_FLOWS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of TCP flows

AVG\_LATENCY\_AVG

-   Type: integer
-   Required: true
-   Description: Integer value of average latency, unit is millisecond

MIN\_LATENCY\_MIN

-   Type: integer
-   Required: true
-   Description: Integer value of minimum lowest latency, unit is
    millisecond

MAX\_LATENCY\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum highest latency, unit is
    millisecond

SUM\_PRE\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of pre-corrected packets
    loss

SUM\_POST\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of total number of post-corrected packets
    loss

SUM\_PRE\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of total number of pre-POC packets loss

SUM\_POST\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of total number of post-POC packets loss

MAX\_PRE\_PCT\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of maximum pre-corrected packets loss
    percentage

MAX\_POST\_PCT\_LOSS

-   Type: integer
-   Required: true
-   Description: Integer value of maximum post-corrected packets loss
    percentage

MAX\_PRE\_PCT\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of maximum pre-POC packets loss
    percentage

SUM\_POST\_PCT\_POC

-   Type: integer
-   Required: true
-   Description: Integer value of maximum post-POC packets loss
    percentage

MAX\_PRE\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-corrected packets
    loss

MAX\_POST\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-corrected packets
    loss

MAX\_PRE\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-POC packets loss

MAX\_POST\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-POC packets loss

MAX\_PRE\_PCT\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-corrected packets
    loss

MAX\_POST\_PCT\_LOSS\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-corrected packets
    loss

MAX\_PRE\_PCT\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak pre-POC packets loss
    percentage

POST\_PCT\_POCMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak post-POC packets loss
    percentage

LATENCY\_MIN\_FROM\_PEAK

-   Type: integer
-   Required: true
-   Description: Integer value of the second peak latency

/stats/aggregate/application
----------------------------

### GET

Summary

Get aggregate application stats data filter by query parameters

Notes

This operation is used to retrieve application aggregate stats. Depend
on what query parameters are provided. The response format may look
different.

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

groupPk

-   Description: Filter data for a specific group
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

application

-   Description: Filter for data which belongs to application with name
    {application}
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

metric

-   Description: If this query parameter is given, we aggregate stats in
    a way that we sort stats by this {metric}. This query parameter
    could also be used with {top} query parameters to limit how many
    items we want to retrieve.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: This parameter should be provided together with
    {metric} as this query parameter is used to indicate top x items of
    a metric. Example, if metric=throughput&top=10 is provided, you\'re
    saying you want to retrieve top 10 tunnels by throughput.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<Traffic Type\>

\<Application Name\>

\<nePk/IP\>

nePk

-   Type: string
-   Required: true
-   Description: Internal ID of appliance

IP

-   Type: string
-   Required: true
-   Description: Management IP address of appliance

HOSTNAME

-   Type: string
-   Required: true
-   Description: Hostname of the appliance

MAX\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual starting boundary of data time range

MIN\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual ending boundary of data time range

SUM\_LRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side received

SUM\_LTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side transmitted

SUM\_LRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_LTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received

SUM\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted

SUM\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received

SUM\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted

MAX\_COMP\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate

MAX\_COMP\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate

MAX\_COMP\_NOOHEAD\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate
    without overhead

MAX\_COMP\_NOOHEAD\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate
    without overhead

MAX\_LRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side received

MAX\_LTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side transmitted

MAX\_LRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side received

MAX\_LTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side transmitted

MAX\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side received

MAX\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side transmitted

MAX\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side received

MAX\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side transmitted

MAX\_COMP\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate

MAX\_COMP\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate

MAX\_COMP\_NOOHEAD\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate without overhead

MAX\_COMP\_NOOHEAD\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate without overhead

APPNAME

-   Type: string
-   Required: true
-   Description: String value of application name to which these stats
    belong to

TRAFFIC\_TYPE

-   Type: integer
-   Required: true
-   Description: Integer value indicating the traffic type these stats
    belong to. 1: Optimized Traffic, 2: Pass-through Shaped, 3:
    Pass-through Unshaped, 4: All Traffic

### POST

Summary

Get aggregate application stats data filter by query parameters

Notes

This operation is used to retrieve application aggregate stats. Depend
on what query parameters are provided. The response format may look
different.

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

application

-   Description: Filter for data which belongs to application with name
    {application}
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

metric

-   Description: If this query parameter is given, we aggregate stats in
    a way that we sort stats by this {metric}. This query parameter
    could also be used with {top} query parameters to limit how many
    items we want to retrieve.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: This parameter should be provided together with
    {metric} as this query parameter is used to indicate top x items of
    a metric. Example, if metric=throughput&top=10 is provided, you\'re
    saying you want to retrieve top 10 tunnels by throughput.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<Traffic Type\>

\<Application Name\>

\<nePk/IP\>

nePk

-   Type: string
-   Required: true
-   Description: Internal ID of appliance

IP

-   Type: string
-   Required: true
-   Description: Management IP address of appliance

HOSTNAME

-   Type: string
-   Required: true
-   Description: Hostname of the appliance

MAX\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual starting boundary of data time range

MIN\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual ending boundary of data time range

SUM\_LRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side received

SUM\_LTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side transmitted

SUM\_LRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_LTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received

SUM\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted

SUM\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received

SUM\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted

MAX\_COMP\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate

MAX\_COMP\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate

MAX\_COMP\_NOOHEAD\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate
    without overhead

MAX\_COMP\_NOOHEAD\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate
    without overhead

MAX\_LRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side received

MAX\_LTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side transmitted

MAX\_LRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side received

MAX\_LTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side transmitted

MAX\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side received

MAX\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side transmitted

MAX\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side received

MAX\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side transmitted

MAX\_COMP\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate

MAX\_COMP\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate

MAX\_COMP\_NOOHEAD\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate without overhead

MAX\_COMP\_NOOHEAD\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate without overhead

APPNAME

-   Type: string
-   Required: true
-   Description: String value of application name to which these stats
    belong to

TRAFFIC\_TYPE

-   Type: integer
-   Required: true
-   Description: Integer value indicating the traffic type these stats
    belong to. 1: Optimized Traffic, 2: Pass-through Shaped, 3:
    Pass-through Unshaped, 4: All Traffic

/stats/aggregate/application/{nePk}
-----------------------------------

### GET

Summary

Get aggregate application stats data for a single appliance filter by
query parameters

Notes

This operation is used to retrieve application aggregate stats for a
single appliance. Depend on what query parameters are provided. The
response format may look different.

Input type

None

nePk

-   Description: Appliance internal id to filter from.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

application

-   Description: Filter for data which belongs to application with name
    {application}
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

metric

-   Description: If this query parameter is given, we aggregate stats in
    a way that we sort stats by this {metric}. This query parameter
    could also be used with {top} query parameters to limit how many
    items we want to retrieve.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: This parameter should be provided together with
    {metric} as this query parameter is used to indicate top x items of
    a metric. Example, if metric=throughput&top=10 is provided, you\'re
    saying you want to retrieve top 10 tunnels by throughput.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

\<Traffic Type\>

\<Application Name\>

\<nePk/IP\>

nePk

-   Type: string
-   Required: true
-   Description: Internal ID of appliance

IP

-   Type: string
-   Required: true
-   Description: Management IP address of appliance

HOSTNAME

-   Type: string
-   Required: true
-   Description: Hostname of the appliance

MAX\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual starting boundary of data time range

MIN\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual ending boundary of data time range

SUM\_LRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side received

SUM\_LTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side transmitted

SUM\_LRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_LTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received

SUM\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted

SUM\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received

SUM\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted

MAX\_COMP\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate

MAX\_COMP\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate

MAX\_COMP\_NOOHEAD\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate
    without overhead

MAX\_COMP\_NOOHEAD\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate
    without overhead

MAX\_LRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side received

MAX\_LTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side transmitted

MAX\_LRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side received

MAX\_LTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side transmitted

MAX\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side received

MAX\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side transmitted

MAX\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side received

MAX\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side transmitted

MAX\_COMP\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate

MAX\_COMP\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate

MAX\_COMP\_NOOHEAD\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate without overhead

MAX\_COMP\_NOOHEAD\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate without overhead

APPNAME

-   Type: string
-   Required: true
-   Description: String value of application name to which these stats
    belong to

TRAFFIC\_TYPE

-   Type: integer
-   Required: true
-   Description: Integer value indicating the traffic type these stats
    belong to. 1: Optimized Traffic, 2: Pass-through Shaped, 3:
    Pass-through Unshaped, 4: All Traffic

/stats/aggregate/application2
-----------------------------

### GET

Summary

Get aggregate application stats data filter by query parameters

Notes

This operation is used to retrieve application aggregate stats. Depend
on what query parameters are provided. The response format may look
different.

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

groupPk

-   Description: Filter data for a specific group
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

application

-   Description: Filter for data which belongs to application with name
    {application}
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: Retrieve top 10 applications by throughput.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

### POST

Summary

Get aggregate application stats data filter by query parameters

Notes

This operation is used to retrieve application aggregate stats. Depend
on what query parameters are provided. The response format may look
different.

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

application

-   Description: Filter for data which belongs to application with name
    {application}
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: Retrieve top 10 applications by throughput.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

/stats/aggregate/application2/{nePk}
------------------------------------

### GET

Summary

Get aggregate application stats data for a single appliance filter by
query parameters

Notes

This operation is used to retrieve application aggregate stats for a
single appliance. Depend on what query parameters are provided. The
response format may look different.

Input type

None

nePk

-   Description: Appliance internal id to filter from.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

application

-   Description: Filter for data which belongs to application with name
    {application}
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: Retrieve top 10 applications by throughput.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/stats/aggregate/trafficClass
-----------------------------

### GET

Summary

Get aggregate traffic class stats data filter by query parameters

Notes

This operation is used to retrieve traffic class aggregate stats. Depend
on what query parameters are provided. The response format may look
different.

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

groupPk

-   Description: Filter data for a specific group
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

trafficClass

-   Description: Filter for data which belongs to trafficClass by
    integer value {trafficClass} 1 - 10
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

trafficType

-   Description: Filter for data for given traffic type
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

metric

-   Description: If this query parameter is given, we aggregate stats in
    a way that we sort stats by this {metric}. This query parameter
    could also be used with {top} query parameters to limit how many
    items we want to retrieve.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: This parameter should be provided together with
    {metric} as this query parameter is used to indicate top x items of
    a metric. Example, if metric=throughput&top=10 is provided, you\'re
    saying you want to retrieve top 10 tunnels by throughput.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<Traffic Type\>

\<Traffic Class\>

\<nePk/IP\>

nePk

-   Type: string
-   Required: true
-   Description: Internal ID of appliance

IP

-   Type: string
-   Required: true
-   Description: Management IP address of appliance

HOSTNAME

-   Type: string
-   Required: true
-   Description: Hostname of the appliance

MAX\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual starting boundary of data time range

MIN\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual ending boundary of data time range

SUM\_LRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side received

SUM\_LTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side transmitted

SUM\_LRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_LTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received

SUM\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted

SUM\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received

SUM\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted

MAX\_COMP\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate

MAX\_COMP\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate

MAX\_COMP\_NOOHEAD\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate
    without overhead

MAX\_COMP\_NOOHEAD\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate
    without overhead

MAX\_LRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side received

MAX\_LTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side transmitted

MAX\_LRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side received

MAX\_LTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side transmitted

MAX\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side received

MAX\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side transmitted

MAX\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side received

MAX\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side transmitted

MAX\_COMP\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate

MAX\_COMP\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate

MAX\_COMP\_NOOHEAD\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate without overhead

MAX\_COMP\_NOOHEAD\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate without overhead

TRAFFIC\_CLASS

-   Type: integer
-   Required: true
-   Description: Integer value of traffic class to which these stats
    belong to

TRAFFIC\_TYPE

-   Type: integer
-   Required: true
-   Description: Integer value indicating the traffic type these stats
    belong to. 1: Optimized Traffic, 2: Pass-through Shaped, 3:
    Pass-through Unshaped, 4: All Traffic

### POST

Summary

Get aggregate traffic class stats data filter by query parameters

Notes

This operation is used to retrieve traffic class aggregate stats. Depend
on what query parameters are provided. The response format may look
different.

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficClass

-   Description: Filter for data which belongs to trafficClass by
    integer value {trafficClass} 1 - 10
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

trafficType

-   Description: Filter for data for given traffic type
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

metric

-   Description: If this query parameter is given, we aggregate stats in
    a way that we sort stats by this {metric}. This query parameter
    could also be used with {top} query parameters to limit how many
    items we want to retrieve.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: This parameter should be provided together with
    {metric} as this query parameter is used to indicate top x items of
    a metric. Example, if metric=throughput&top=10 is provided, you\'re
    saying you want to retrieve top 10 tunnels by throughput.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<Traffic Type\>

\<Traffic Class\>

\<nePk/IP\>

nePk

-   Type: string
-   Required: true
-   Description: Internal ID of appliance

IP

-   Type: string
-   Required: true
-   Description: Management IP address of appliance

HOSTNAME

-   Type: string
-   Required: true
-   Description: Hostname of the appliance

MAX\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual starting boundary of data time range

MIN\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual ending boundary of data time range

SUM\_LRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side received

SUM\_LTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side transmitted

SUM\_LRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_LTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received

SUM\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted

SUM\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received

SUM\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted

MAX\_COMP\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate

MAX\_COMP\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate

MAX\_COMP\_NOOHEAD\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate
    without overhead

MAX\_COMP\_NOOHEAD\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate
    without overhead

MAX\_LRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side received

MAX\_LTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side transmitted

MAX\_LRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side received

MAX\_LTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side transmitted

MAX\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side received

MAX\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side transmitted

MAX\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side received

MAX\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side transmitted

MAX\_COMP\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate

MAX\_COMP\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate

MAX\_COMP\_NOOHEAD\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate without overhead

MAX\_COMP\_NOOHEAD\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate without overhead

TRAFFIC\_CLASS

-   Type: integer
-   Required: true
-   Description: Integer value of traffic class to which these stats
    belong to

TRAFFIC\_TYPE

-   Type: integer
-   Required: true
-   Description: Integer value indicating the traffic type these stats
    belong to. 1: Optimized Traffic, 2: Pass-through Shaped, 3:
    Pass-through Unshaped, 4: All Traffic

/stats/aggregate/trafficClass/{nePk}
------------------------------------

### GET

Summary

Get aggregate trafficClass stats data for a single appliance filter by
query parameters

Notes

This operation is used to retrieve trafficClass aggregate stats for a
single appliance. Depend on what query parameters are provided. The
response format may look different.

Input type

None

nePk

-   Description: Appliance internal id to filter from.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficClass

-   Description: Filter for data which belongs to trafficClass by
    integer value {trafficClass} 1 - 10
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

trafficType

-   Description: Filter for data for given traffic type
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

metric

-   Description: If this query parameter is given, we aggregate stats in
    a way that we sort stats by this {metric}. This query parameter
    could also be used with {top} query parameters to limit how many
    items we want to retrieve.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: This parameter should be provided together with
    {metric} as this query parameter is used to indicate top x items of
    a metric. Example, if metric=throughput&top=10 is provided, you\'re
    saying you want to retrieve top 10 tunnels by throughput.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

\<Traffic Type\>

\<Traffic Class\>

\<nePk/IP\>

nePk

-   Type: string
-   Required: true
-   Description: Internal ID of appliance

IP

-   Type: string
-   Required: true
-   Description: Management IP address of appliance

HOSTNAME

-   Type: string
-   Required: true
-   Description: Hostname of the appliance

MAX\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual starting boundary of data time range

MIN\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual ending boundary of data time range

SUM\_LRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side received

SUM\_LTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side transmitted

SUM\_LRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_LTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received

SUM\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted

SUM\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received

SUM\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted

MAX\_COMP\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate

MAX\_COMP\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate

MAX\_COMP\_NOOHEAD\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate
    without overhead

MAX\_COMP\_NOOHEAD\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate
    without overhead

MAX\_LRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side received

MAX\_LTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side transmitted

MAX\_LRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side received

MAX\_LTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side transmitted

MAX\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side received

MAX\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side transmitted

MAX\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side received

MAX\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side transmitted

MAX\_COMP\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate

MAX\_COMP\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate

MAX\_COMP\_NOOHEAD\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate without overhead

MAX\_COMP\_NOOHEAD\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate without overhead

TRAFFIC\_CLASS

-   Type: integer
-   Required: true
-   Description: Integer value of traffic class to which these stats
    belong to

TRAFFIC\_TYPE

-   Type: integer
-   Required: true
-   Description: Integer value indicating the traffic type these stats
    belong to. 1: Optimized Traffic, 2: Pass-through Shaped, 3:
    Pass-through Unshaped, 4: All Traffic

/stats/aggregate/flow
---------------------

### GET

Summary

Get aggregate flow stats data filter by query parameters

Notes

This operation is used to retrieve flow aggregate stats. Depend on what
query parameters are provided. The response format may look different.

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

groupPk

-   Description: Filter data for a specific group
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

flow

-   Description: Filter for data which belongs to a certain flow type.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<Traffic Type\>

\<Flow Type\>

\<nePk/IP\>

nePk

-   Type: string
-   Required: true
-   Description: Internal ID of appliance

IP

-   Type: string
-   Required: true
-   Description: Management IP address of appliance

HOSTNAME

-   Type: string
-   Required: true
-   Description: Hostname of the appliance

MAX\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual starting boundary of data time range

MIN\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual ending boundary of data time range

SUM\_LRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side received

SUM\_LTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side transmitted

SUM\_LRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_LTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received

SUM\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted

SUM\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received

SUM\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted

MAX\_COMP\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate

MAX\_COMP\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate

MAX\_COMP\_NOOHEAD\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate
    without overhead

MAX\_COMP\_NOOHEAD\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate
    without overhead

MAX\_LRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side received

MAX\_LTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side transmitted

MAX\_LRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side received

MAX\_LTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side transmitted

MAX\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side received

MAX\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side transmitted

MAX\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side received

MAX\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side transmitted

MAX\_COMP\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate

MAX\_COMP\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate

MAX\_COMP\_NOOHEAD\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate without overhead

MAX\_COMP\_NOOHEAD\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate without overhead

FLOW\_TYPE

-   Type: integer
-   Required: true
-   Description: Integer value indicating flow type to which the stats
    object belongs. 1: TCP\_Accelerated, 2: TCP\_Not\_Accelerated, 3:
    Non\_TCP

TRAFFIC\_TYPE

-   Type: integer
-   Required: true
-   Description: Integer value indicating the traffic type these stats
    belong to. 1: Optimized Traffic, 2: Pass-through Shaped, 3:
    Pass-through Unshaped, 4: All Traffic

SUM\_CREATED

-   Type: integer
-   Required: true
-   Description: Integer value of total number of flows created

MAX\_CREATED\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum number of flows created

SUM\_DELETED

-   Type: integer
-   Required: true
-   Description: Integer value of total number of flows deleted

MAX\_DELETED\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum number of flows deleted

SUM\_EXT

-   Type: integer
-   Required: true
-   Description: Integer value of total number of flows exist

MAX\_FLOW\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum number of flows exist

MAX\_FLOW\_PEAK

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of flows

MAX\_FLOW\_EXT\_PEAK

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of flows exist

### POST

Summary

Get aggregate flow stats data filter by query parameters

Notes

This operation is used to retrieve flow aggregate stats. Depend on what
query parameters are provided. The response format may look different.

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

flow

-   Description: Filter for data which belongs to a certain flow type.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<Traffic Type\>

\<Flow Type\>

\<nePk/IP\>

nePk

-   Type: string
-   Required: true
-   Description: Internal ID of appliance

IP

-   Type: string
-   Required: true
-   Description: Management IP address of appliance

HOSTNAME

-   Type: string
-   Required: true
-   Description: Hostname of the appliance

MAX\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual starting boundary of data time range

MIN\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual ending boundary of data time range

SUM\_LRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side received

SUM\_LTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side transmitted

SUM\_LRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_LTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received

SUM\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted

SUM\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received

SUM\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted

MAX\_COMP\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate

MAX\_COMP\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate

MAX\_COMP\_NOOHEAD\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate
    without overhead

MAX\_COMP\_NOOHEAD\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate
    without overhead

MAX\_LRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side received

MAX\_LTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side transmitted

MAX\_LRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side received

MAX\_LTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side transmitted

MAX\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side received

MAX\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side transmitted

MAX\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side received

MAX\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side transmitted

MAX\_COMP\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate

MAX\_COMP\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate

MAX\_COMP\_NOOHEAD\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate without overhead

MAX\_COMP\_NOOHEAD\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate without overhead

FLOW\_TYPE

-   Type: integer
-   Required: true
-   Description: Integer value indicating flow type to which the stats
    object belongs. 1: TCP\_Accelerated, 2: TCP\_Not\_Accelerated, 3:
    Non\_TCP

TRAFFIC\_TYPE

-   Type: integer
-   Required: true
-   Description: Integer value indicating the traffic type these stats
    belong to. 1: Optimized Traffic, 2: Pass-through Shaped, 3:
    Pass-through Unshaped, 4: All Traffic

SUM\_CREATED

-   Type: integer
-   Required: true
-   Description: Integer value of total number of flows created

MAX\_CREATED\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum number of flows created

SUM\_DELETED

-   Type: integer
-   Required: true
-   Description: Integer value of total number of flows deleted

MAX\_DELETED\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum number of flows deleted

SUM\_EXT

-   Type: integer
-   Required: true
-   Description: Integer value of total number of flows exist

MAX\_FLOW\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum number of flows exist

MAX\_FLOW\_PEAK

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of flows

MAX\_FLOW\_EXT\_PEAK

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of flows exist

/stats/aggregate/flow/active
----------------------------

### POST

Summary

Get active flow counts by NE id

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

top

-   Description: No. of rows to return
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

\<nePk\>

TIMESTAMP

-   Type: integer

TCP\_ACCELERATED\_COUNT

-   Type: integer

TCP\_NOT\_ACCELERATED\_COUNT

-   Type: integer

NON\_TCP\_COUNT

-   Type: integer

/stats/aggregate/flow/{nePk}
----------------------------

### GET

Summary

Get aggregate flow stats data for a single appliance filter by query
parameters

Notes

This operation is used to retrieve flow aggregate stats for a single
appliance. Depend on what query parameters are provided. The response
format may look different.

Input type

None

nePk

-   Description: Appliance internal id to filter from.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficClass

-   Description: Filter for data which belongs to trafficClass by
    integer value {trafficClass} 1 - 10
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

flow

-   Description: Filter for data which belongs to a certain flow type.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

\<Traffic Type\>

\<Flow Type\>

\<nePk/IP\>

nePk

-   Type: string
-   Required: true
-   Description: Internal ID of appliance

IP

-   Type: string
-   Required: true
-   Description: Management IP address of appliance

HOSTNAME

-   Type: string
-   Required: true
-   Description: Hostname of the appliance

MAX\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual starting boundary of data time range

MIN\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual ending boundary of data time range

SUM\_LRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side received

SUM\_LTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side transmitted

SUM\_LRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_LTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received

SUM\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted

SUM\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received

SUM\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted

MAX\_COMP\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate

MAX\_COMP\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate

MAX\_COMP\_NOOHEAD\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate
    without overhead

MAX\_COMP\_NOOHEAD\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate
    without overhead

MAX\_LRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side received

MAX\_LTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side transmitted

MAX\_LRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side received

MAX\_LTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side transmitted

MAX\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side received

MAX\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side transmitted

MAX\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side received

MAX\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side transmitted

MAX\_COMP\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate

MAX\_COMP\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate

MAX\_COMP\_NOOHEAD\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate without overhead

MAX\_COMP\_NOOHEAD\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate without overhead

FLOW\_TYPE

-   Type: integer
-   Required: true
-   Description: Integer value indicating flow type to which the stats
    object belongs. 1: TCP\_Accelerated, 2: TCP\_Not\_Accelerated, 3:
    Non\_TCP

TRAFFIC\_TYPE

-   Type: integer
-   Required: true
-   Description: Integer value indicating the traffic type these stats
    belong to. 1: Optimized Traffic, 2: Pass-through Shaped, 3:
    Pass-through Unshaped, 4: All Traffic

SUM\_CREATED

-   Type: integer
-   Required: true
-   Description: Integer value of total number of flows created

MAX\_CREATED\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum number of flows created

SUM\_DELETED

-   Type: integer
-   Required: true
-   Description: Integer value of total number of flows deleted

MAX\_DELETED\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum number of flows deleted

SUM\_EXT

-   Type: integer
-   Required: true
-   Description: Integer value of total number of flows exist

MAX\_FLOW\_MAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum number of flows exist

MAX\_FLOW\_PEAK

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of flows

MAX\_FLOW\_EXT\_PEAK

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak number of flows exist

/stats/aggregate/dscp
---------------------

### GET

Summary

Get aggregate dscp stats data filter by query parameters

Notes

This operation is used to retrieve dscp aggregate stats. Depend on what
query parameters are provided. The response format may look different.

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

groupPk

-   Description: Filter data for a specific group
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

dscp

-   Description: Filter for data which belongs to a certain flow type.
    Valid DSCP: 0 - 63
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

trafficType

-   Description: Filter for data for given traffic type
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

metric

-   Description: If this query parameter is given, we aggregate stats in
    a way that we sort stats by this {metric}. This query parameter
    could also be used with {top} query parameters to limit how many
    items we want to retrieve.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: This parameter should be provided together with
    {metric} as this query parameter is used to indicate top x items of
    a metric. Example, if metric=throughput&top=10 is provided, you\'re
    saying you want to retrieve top 10 tunnels by throughput.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<Traffic Type\>

\<DSCP\>

\<nePk/IP\>

nePk

-   Type: string
-   Required: true
-   Description: Internal ID of appliance

IP

-   Type: string
-   Required: true
-   Description: Management IP address of appliance

HOSTNAME

-   Type: string
-   Required: true
-   Description: Hostname of the appliance

MAX\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual starting boundary of data time range

MIN\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual ending boundary of data time range

SUM\_LRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side received

SUM\_LTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side transmitted

SUM\_LRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_LTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received

SUM\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted

SUM\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received

SUM\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted

MAX\_COMP\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate

MAX\_COMP\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate

MAX\_COMP\_NOOHEAD\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate
    without overhead

MAX\_COMP\_NOOHEAD\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate
    without overhead

MAX\_LRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side received

MAX\_LTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side transmitted

MAX\_LRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side received

MAX\_LTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side transmitted

MAX\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side received

MAX\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side transmitted

MAX\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side received

MAX\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side transmitted

MAX\_COMP\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate

MAX\_COMP\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate

MAX\_COMP\_NOOHEAD\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate without overhead

MAX\_COMP\_NOOHEAD\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate without overhead

DSCP

-   Type: integer
-   Required: true
-   Description: Integer value indicating DSCP to which the stats object
    belongs.

TRAFFIC\_TYPE

-   Type: integer
-   Required: true
-   Description: Integer value indicating the traffic type these stats
    belong to. 1: Optimized Traffic, 2: Pass-through Shaped, 3:
    Pass-through Unshaped, 4: All Traffic

### POST

Summary

Get aggregate dscp stats data filter by query parameters

Notes

This operation is used to retrieve dscp aggregate stats. Depend on what
query parameters are provided. The response format may look different.

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

dscp

-   Description: Filter for data which belongs to a certain flow type.
    Valid DSCP: 0 - 63
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

trafficType

-   Description: Filter for data for given traffic type
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

metric

-   Description: If this query parameter is given, we aggregate stats in
    a way that we sort stats by this {metric}. This query parameter
    could also be used with {top} query parameters to limit how many
    items we want to retrieve.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: This parameter should be provided together with
    {metric} as this query parameter is used to indicate top x items of
    a metric. Example, if metric=throughput&top=10 is provided, you\'re
    saying you want to retrieve top 10 tunnels by throughput.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<Traffic Type\>

\<DSCP\>

\<nePk/IP\>

nePk

-   Type: string
-   Required: true
-   Description: Internal ID of appliance

IP

-   Type: string
-   Required: true
-   Description: Management IP address of appliance

HOSTNAME

-   Type: string
-   Required: true
-   Description: Hostname of the appliance

MAX\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual starting boundary of data time range

MIN\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual ending boundary of data time range

SUM\_LRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side received

SUM\_LTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side transmitted

SUM\_LRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_LTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received

SUM\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted

SUM\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received

SUM\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted

MAX\_COMP\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate

MAX\_COMP\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate

MAX\_COMP\_NOOHEAD\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate
    without overhead

MAX\_COMP\_NOOHEAD\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate
    without overhead

MAX\_LRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side received

MAX\_LTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side transmitted

MAX\_LRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side received

MAX\_LTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side transmitted

MAX\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side received

MAX\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side transmitted

MAX\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side received

MAX\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side transmitted

MAX\_COMP\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate

MAX\_COMP\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate

MAX\_COMP\_NOOHEAD\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate without overhead

MAX\_COMP\_NOOHEAD\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate without overhead

DSCP

-   Type: integer
-   Required: true
-   Description: Integer value indicating DSCP to which the stats object
    belongs.

TRAFFIC\_TYPE

-   Type: integer
-   Required: true
-   Description: Integer value indicating the traffic type these stats
    belong to. 1: Optimized Traffic, 2: Pass-through Shaped, 3:
    Pass-through Unshaped, 4: All Traffic

/stats/aggregate/dscp/{nePk}
----------------------------

### GET

Summary

Get aggregate dscp stats data for a single appliance filter by query
parameters

Notes

This operation is used to retrieve dscp aggregate stats for a single
appliance. Depend on what query parameters are provided. The response
format may look different.

Input type

None

nePk

-   Description: Appliance internal id to filter from.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

trafficType

-   Description: Filter for data for given traffic type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficClass

-   Description: Filter for data which belongs to trafficClass by
    integer value {trafficClass} 1 - 10
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

dscp

-   Description: Filter for data which belongs to a certain flow type.
    Valid DSCP: 0 - 63
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

metric

-   Description: If this query parameter is given, we aggregate stats in
    a way that we sort stats by this {metric}. This query parameter
    could also be used with {top} query parameters to limit how many
    items we want to retrieve.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: This parameter should be provided together with
    {metric} as this query parameter is used to indicate top x items of
    a metric. Example, if metric=throughput&top=10 is provided, you\'re
    saying you want to retrieve top 10 tunnels by throughput.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

\<Traffic Type\>

\<DSCP\>

\<nePk/IP\>

nePk

-   Type: string
-   Required: true
-   Description: Internal ID of appliance

IP

-   Type: string
-   Required: true
-   Description: Management IP address of appliance

HOSTNAME

-   Type: string
-   Required: true
-   Description: Hostname of the appliance

MAX\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual starting boundary of data time range

MIN\_TIMESTAMP

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of EPOCH time indicating
    actual ending boundary of data time range

SUM\_LRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side received

SUM\_LTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes LAN
    side transmitted

SUM\_LRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_LTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    LAN side transmitted

SUM\_WRX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side received

SUM\_WTX\_BYTES

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of bytes WAN
    side transmitted

SUM\_WRX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side received

SUM\_WTX\_PKTS

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of total number of packets
    WAN side transmitted

MAX\_COMP\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate

MAX\_COMP\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate

MAX\_COMP\_NOOHEAD\_L2W

-   Type: integer
-   Required: true
-   Description: Integer value of maximum LAN to WAN compression rate
    without overhead

MAX\_COMP\_NOOHEAD\_W2L

-   Type: integer
-   Required: true
-   Description: Integer value of maximum WAN to LAN compression rate
    without overhead

MAX\_LRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side received

MAX\_LTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes LAN
    side transmitted

MAX\_LRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side received

MAX\_LTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    LAN side transmitted

MAX\_WRX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side received

MAX\_WTX\_BYTES\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of bytes WAN
    side transmitted

MAX\_WRX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side received

MAX\_WTX\_PKTS\_MAX

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of peak number of packets
    WAN side transmitted

MAX\_COMP\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate

MAX\_COMP\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate

MAX\_COMP\_NOOHEAD\_L2WMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak LAN to WAN compression
    rate without overhead

MAX\_COMP\_NOOHEAD\_W2LMAX

-   Type: integer
-   Required: true
-   Description: Integer value of maximum peak WAN to LAN compression
    rate without overhead

DSCP

-   Type: integer
-   Required: true
-   Description: Integer value indicating DSCP to which the stats object
    belongs.

TRAFFIC\_TYPE

-   Type: integer
-   Required: true
-   Description: Integer value indicating the traffic type these stats
    belong to. 1: Optimized Traffic, 2: Pass-through Shaped, 3:
    Pass-through Unshaped, 4: All Traffic

/stats/aggregate/dns
--------------------

### GET

Summary

Get aggregate dns stats data filter by query parameters.

Notes

This operation is used to retrieve DNS aggregate stats. Depend on what
query parameters are provided. The response format may look different.

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

isSource

-   Description: Integer value representing whether dns is from source
    or destination
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

splitType

-   Description: Filter data for a specific type, where 0 is for
    \'http\', 1 is for \'https\', 2 is for \'unassigned\' and 3 is for
    \'others\'
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

top

-   Description: No. of rows to return
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

splitByNe

-   Description: Boolean value indicating whether you want to split
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each stats object indicating what
    appliance the inner stats object belongs to. Default value is null.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

groupBy

-   Description: Group the data based on this value. Default value is
    dns.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupBySubDomains

-   Description: Group the data based on this value.Integer value
    represents number of sub-domains. Default value is 2.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

### POST

Summary

Get aggregate dns stats data filter by query parameters.

Notes

This operation is used to retrieve dns aggregate stats and it\'s
identical to the GET operation on this URL except that you need to pass
a request body containing appliance ids that you want to filter on to
it. Depend on what query parameters are provided. The response format
may look different.

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

isSource

-   Description: Integer value representing whether dns is from source
    or destination
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

splitType

-   Description: Filter data for a specific type, where 0 is for
    \'http\', 1 is for \'https\', 2 is for \'unassigned\' and 3 is for
    \'others\'
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

top

-   Description: No. of rows to return
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

splitByNe

-   Description: Boolean value indicating whether you want to split
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each stats object indicating what
    appliance the inner stats object belongs to. Default value is null.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

groupBy

-   Description: Group the data based on this value. Default value is
    dns.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupBySubDomains

-   Description: Group the data based on this value.Integer value
    represents number of sub-domains. Default value is 2.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/stats/aggregate/dns/{nePk}
---------------------------

### GET

Summary

Get aggregate dns stats data for a single appliance filter by query
parameters.

Notes

This operation is used to retrieve dns aggregate stats for a single
appliance. Depend on what query parameters are provided. The response
format may look different.

Input type

None

nePk

-   Description: Appliance internal id to filter from.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

isSource

-   Description: Integer value representing whether dns is from source
    or destination
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

splitType

-   Description: Filter data for a specific type, where 0 is for
    \'http\', 1 is for \'https\', 2 is for \'unassigned\' and 3 is for
    \'others\'
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

top

-   Description: No. of rows to return
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

splitByNe

-   Description: Boolean value indicating whether you want to split
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each stats object indicating what
    appliance the inner stats object belongs to. Default value is null.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

groupBy

-   Description: Group the data based on this value. Default value is
    dns.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupBySubDomains

-   Description: Group the data based on this value.Integer value
    represents number of sub-domains. Default value is 2.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/stats/aggregate/ports/{nePk}
-----------------------------

### GET

Summary

Get aggregate ports stats data filter by query parameters.

Notes

This operation is used to retrieve ports aggregate stats. Depend on what
query parameters are provided. The response format may look different.

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

isSource

-   Description: Integer value representing whether port is from source
    or destination
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

isKnown

-   Description: Integer value representing whether port is assigned to
    a application(1) or not(0)
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

protocol

-   Description: Integer value representing whether protocol used by
    port
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

port

-   Description: Integer value representing whether port number
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

top

-   Description: No. of rows to return
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

splitByNe

-   Description: Boolean value indicating whether you want to split
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each stats object indicating what
    appliance the inner stats object belongs to. Default value is null.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

lastHour

-   Description: Boolean value representing whether to fetch data one
    hour behind from last hour if data is not found in last hour time
    range
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

/stats/aggregate/ports
----------------------

### GET

Summary

Get aggregate ports stats data filter by query parameters.

Notes

This operation is used to retrieve ports aggregate stats. Depend on what
query parameters are provided. The response format may look different.

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

isSource

-   Description: Integer value representing whether port is from source
    or destination
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

isKnown

-   Description: Integer value representing whether port is assigned to
    a application(1) or not(0)
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

protocol

-   Description: Integer value representing whether protocol used by
    port
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

port

-   Description: Integer value representing whether port number
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

top

-   Description: No. of rows to return
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

splitByNe

-   Description: Boolean value indicating whether you want to split
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each stats object indicating what
    appliance the inner stats object belongs to. Default value is null.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

lastHour

-   Description: Boolean value representing whether to fetch data one
    hour behind from last hour if data is not found in last hour time
    range
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

### POST

Summary

Get aggregate ports stats data filter by query parameters.

Notes

This operation is used to retrieve ports aggregate stats and it\'s
identical to the GET operation on this URL except that you need to pass
a request body containing appliance ids that you want to filter on to
it. Depend on what query parameters are provided. The response format
may look different.

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

isSource

-   Description: Integer value representing whether dns is from source
    or destination
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

isKnown

-   Description: Integer value representing whether port is assigned to
    a application(1) or not(0)
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

protocol

-   Description: Integer value representing whether protocol used by
    port
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

port

-   Description: Integer value representing whether port number
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

top

-   Description: No. of rows to return
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

splitByNe

-   Description: Boolean value indicating whether you want to split
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each stats object indicating what
    appliance the inner stats object belongs to. Default value is null.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

lastHour

-   Description: Boolean value representing whether to fetch data one
    hour behind from last hour if data is not found in last hour time
    range
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

/stats/aggregate/topTalkers
---------------------------

### GET

Summary

Get aggregate topTalkers stats data filter by query parameters.

Notes

This operation is used to retrieve topTalkers aggregate stats. Depend on
what query parameters are provided. The response format may look
different.

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

top

-   Description: No. of rows to return
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

splitByNe

-   Description: Boolean value indicating whether you want to split
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each stats object indicating what
    appliance the inner stats object belongs to. Default value is null.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

### POST

Summary

Get aggregate topTalkers stats data filter by query parameters.

Notes

This operation is used to retrieve topTalkers aggregate stats and it\'s
identical to the GET operation on this URL except that you need to pass
a request body containing appliance ids that you want to filter on to
it. Depend on what query parameters are provided. The response format
may look different.

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

top

-   Description: No. of rows to return
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

splitByNe

-   Description: Boolean value indicating whether you want to split
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each stats object indicating what
    appliance the inner stats object belongs to. Default value is null.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/stats/aggregate/topTalkers/{nePk}
----------------------------------

### GET

Summary

Get aggregate topTalkers stats data for a single appliance filter by
query parameters.

Notes

This operation is used to retrieve topTalkers aggregate stats for a
single appliance. Depend on what query parameters are provided. The
response format may look different.

Input type

None

nePk

-   Description: Appliance internal id to filter from.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

top

-   Description: No. of rows to return
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/stats/aggregate/topTalkers/split/{nePk}
----------------------------------------

### GET

Summary

Get aggregate topTalkers stats data for a single appliance filter by
query parameters.

Notes

This operation is used to retrieve topTalkers aggregate stats for a
single appliance. Depend on what query parameters are provided. The
response format may look different.

Input type

None

nePk

-   Description: Appliance internal id to filter from.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

sourceIp

-   Description: Source IP address
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/stats/aggregate/trafficBehavior
--------------------------------

### GET

Summary

Get aggregate Traffic Behavioral stats data filter by query parameters

Notes

This operation is used to retrieve Traffic Behavioral aggregate stats.
Depend on what query parameters are provided. The response format may
look different.

Input type

None

startTime

-   Description: Start time ( epoch time in seconds ) of time range that
    stats are requested for
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: End time ( epoch time in seconds ) of time range that
    stats are requested for
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

groupPk

-   Description: Filter data for a specific group
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

behavioralCate

-   Description: Filter for data which belongs to behavioral Category
    with name { behavioral\_category }
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/stats/aggregate/trafficBehavior
--------------------------------

### POST

Summary

Get aggregate Traffic Behavioral stats data for a single appliance
filter by query parameters

Notes

This operation is used to retrieve Traffic Behavioral stats. Depend on
what query parameters are provided. The response format may look
different.

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Start time ( epoch time in seconds ) of time range that
    stats are requested for
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: End time ( epoch time in seconds ) of time range that
    stats are requested for
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

behavioralCate

-   Description: Filter for data which belongs to behavioral Category
    with name { behavioral\_category }
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

application

-   Description: Filter for data which belongs to application with name
    {application}
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: Restrict Applications for each behavior category to Top
    N
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

lastHour

-   Description: Boolean value representing whether to fetch data one
    hour behind from last hour if data is not found in last hour time
    range
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

isAggregated

-   Description: Get (aggregate) Traffic Behavioral stats data
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

/stats/aggregate/trafficBehavior/{nePk}
---------------------------------------

### GET

Summary

Get aggregate Traffic Behavioral stats data filter by query parameters

Notes

This operation is used to retrieve Traffic Behavioral stats for a single
appliance. Depend on what query parameters are provided. The response
format may look different.

Input type

None

nePk

-   Description: Appliance internal id to filter from.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Start time ( epoch time in seconds ) of time range that
    stats are requested for
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: End time ( epoch time in seconds ) of time range that
    stats are requested for
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

behavioralCate

-   Description: Filter for data which belongs to behavioral Category
    with name { behavioral\_category }
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

application

-   Description: Filter for data which belongs to application with name
    {application}
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: Restrict Applications for each behavior category to Top
    N
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

lastHour

-   Description: Boolean value representing whether to fetch data one
    hour behind from last hour if data is not found in last hour time
    range
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

/stats/aggregate/jitter
-----------------------

### GET

Summary

Get aggregate jitter stats data filter by query parameters.

Notes

This operation is used to retrieve jitter aggregate stats. Depend on
what query parameters are provided.

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

overlay

-   Description: when it is \'all\',return all bonded tunnels;when it is
    \'0\',return all physical tunnels;when it is not given,return all
    bonded and physical tunnels;otherwise,return bonded tunnels
    associated with the overlay id.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

### POST

Summary

Get aggregate jitter stats data filter by query parameters.

Notes

This operation is identical to the GET operation on this URL except that
you need to pass a request body containing appliance ids that you want
to filter on to it.

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

top

-   Description: No. of rows to return
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

overlay

-   Description: when it is \'all\',return all bonded tunnels;when it is
    \'0\',return all physical tunnels;when it is not given,return all
    bonded and physical tunnels;otherwise,return bonded tunnels
    associated with the overlay id.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/stats/aggregate/jitter/{nePk}
------------------------------

### GET

Summary

Get aggregate jitter stats data filter by query parameters.

Notes

This operation is used to retrieve jitter aggregate stats. Depend on
what query parameters are provided.

Input type

None

nePk

-   Description: Appliance internal id to filter from.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

overlay

-   Description: when it is \'all\',return all bonded tunnels;when it is
    \'0\',return all physical tunnels;when it is not given,return all
    bonded and physical tunnels;otherwise,return bonded tunnels
    associated with the overlay id.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

tunnelName

-   Description: Filter for data which belongs to tunnel with name
    {tunnelName}
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/stats/aggregate/drc
--------------------

### GET

Summary

Get aggregate tunnel drc stats data filter by query parameters.

Notes

This operation is used to retrieve tunnel aggregate drc stats. Depend on
what query parameters are provided. The response format may look
different.

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

groupPk

-   Description: Filter data for a specific group
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: Indicate top x items
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

overlay

-   Description: when it is \'all\',return all bonded tunnels;when it is
    \'0\',return all physical tunnels;when it is not given,return all
    bonded and physical tunnels;otherwise,return bonded tunnels
    associated with the overlay id.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra column inside each tunnel stats record in CSV format
    indicating what appliance the inner stats object belongs to. Default
    value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

### POST

Summary

Get aggregate tunnel drc stats data filter by query parameters.

Notes

This operation is used to retrieve tunnel aggregate stats and it\'s
identical to the GET operation on this URL except that you need to pass
a request body containing appliance ids that you want to filter on to
it. Depend on what query parameters are provided. The response format
may look different.

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: Indicate top x items
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

overlay

-   Description: when it is \'all\',return all bonded tunnels;when it is
    \'0\',return all physical tunnels;when it is not given,return all
    bonded and physical tunnels;otherwise,return bonded tunnels
    associated with the overlay id.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra column inside each tunnel stats record in CSV format
    indicating what appliance the inner stats object belongs to. Default
    value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

/stats/aggregate/drc/{nePk}
---------------------------

### GET

Summary

Get aggregate tunnel drc stats data for a single appliance filter by
query parameters.

Notes

This operation is used to retrieve tunnel aggregate drc stats for a
single appliance. Depend on what query parameters are provided. The
response format may look different.

Input type

None

nePk

-   Description: Appliance internal id to filter from.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of EPOCH time indicating the
    ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

tunnelName

-   Description: Filter for data which belongs to tunnel with name
    {tunnelName}
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: Indicate top x items
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

overlay

-   Description: when it is \'all\',return all bonded tunnels;when it is
    \'0\',return all physical tunnels;when it is not given,return all
    bonded and physical tunnels;otherwise,return bonded tunnels
    associated with the overlay id.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra column inside each tunnel stats record in CSV format
    indicating what appliance the inner stats object belongs to. Default
    value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

/stats/aggregate/interface
--------------------------

### GET

Summary

Get aggregate interface stats data for a all appliance filter by query
parameters

Notes

This operation is used to retrieve interface aggregate stats for a
single appliance. Depend on what query parameters are provided. The
response format may look different.

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/stats/aggregate/interface
--------------------------

### POST

Summary

Get aggregate interface stats data

Notes

This operation is used to retrieve interface aggregate stats. Depend on
what query parameters are provided. The response format may look
different.

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/stats/aggregate/interfaceOverlay
---------------------------------

### POST

Summary

Get aggregate interface overlay transport stats data

Notes

This operation is used to retrieve interface overlay transport aggregate
stats. Depend on what query parameters are provided. The response format
may look different.

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

interfaceName

-   Description: Interface name you want to filter on.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

overlay

-   Description: When it is \'0\',return all physical tunnels;when it is
    not given,return all bonded and physical tunnels;otherwise,return
    bonded tunnels associated with the overlay id.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/stats/aggregate/mos
--------------------

### POST

Summary

Get aggregate Mean Opinion Score stats data

Notes

This operation is used to retrieve mos aggregate stats. Depend on what
query parameters are provided. The response format may look different.

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: Indicate top x items
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

overlay

-   Description: When it is \'0\',return all physical tunnels;when it is
    not given,return all bonded and physical tunnels;otherwise,return
    bonded tunnels associated with the overlay id.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

/stats/aggregate/mos/{nePk}
---------------------------

### Get

Summary

Get aggregate Mean Opinion Score stats data

Notes

This operation is used to retrieve mos aggregate stats. Depend on what
query parameters are provided. The response format may look different.

Input type

None

nePk

-   Description: Appliance internal id to filter from.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

tunnelName

-   Description: Filter for data which belongs to tunnel with name
    {tunnelName}
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: Indicate top x items
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

overlay

-   Description: When it is \'0\',return all physical tunnels;when it is
    not given,return all bonded and physical tunnels;otherwise,return
    bonded tunnels associated with the overlay id.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

/stats/aggregate/boost
----------------------

### POST

Summary

Get aggregate boost stats data

Notes

This operation is used to retrieve boost aggregate stats. Depend on what
query parameters are provided. The response format may look different.

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: Indicate top x items
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

/stats/aggregate/boost/{nePk}
-----------------------------

### Get

Summary

Get aggregate boost stats data

Notes

This operation is used to retrieve boost aggregate stats. Depend on what
query parameters are provided. The response format may look different.

Input type

None

nePk

-   Description: Appliance internal id to filter from.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

tunnelName

-   Description: Filter for data which belongs to tunnel with name
    {tunnelName}
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: Indicate top x items
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

overlay

-   Description: When it is \'0\',return all physical tunnels;when it is
    not given,return all bonded and physical tunnels;otherwise,return
    bonded tunnels associated with the overlay id.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance, if true is provided, there will be an
    extra level of key inside each tunnel stats object indicating what
    appliance the inner stats object belongs to. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

/stats/aggregate/securityPolicy
-------------------------------

### POST

Summary

Get aggregate security policy stats data

Notes

This operation is used to retrieve security policy aggregate stats.

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: Indicate top x items
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv, and the response data format would be different
    if this field is provided
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

/stats/aggregate/securityPolicy/{nePk}
--------------------------------------

### Get

Summary

Get aggregate security policy stats data

Notes

This operation is used to retrieve security policy aggregate stats.

Input type

None

nePk

-   Description: Appliance internal id to filter from.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

fromZone

-   Description: Filter for data which come from the zone indicated by
    this zone internal ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

toZone

-   Description: Filter for data which go to the zone indicated by this
    zone internal ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: Indicate top x items
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv, and the response data format would be different
    if this field is provided
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

groupByNE

-   Description: Boolean value indicating whether you want to group
    aggregate stats by appliance. Default value is true.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

Reference: aggregateStats.json

\

VXOA time series statistics
===========================

/stats/timeseries/applianceProcessState/{nePk}
----------------------------------------------

### GET

Summary

Stats of appliance Process State.

Notes

This operation returns a JSON object containing Appliance Process State.

Input type

None

nePk

-   Description: nepk id
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/stats/timeseries/metrics
-------------------------

### GET

Summary

Get tunnel time series stats data filter by certain query parameters.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

key

-   Description: for filter resource by key.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

data

\<nePk/IP\>

-   Type: array

column\_def

-   Type: array
-   Description: These names correspond to each stats field in the array
    of stats.

/stats/timeseries/tunnel
------------------------

### GET

Summary

Get tunnel time series stats data filter by certain query parameters.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

groupPk

-   Description: Internal group ID of a group for which you want to
    retrieve stats
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

\<nePk/IP\>

-   Type: array

column\_def

-   Type: array
-   Description: These names correspond to each stats field in the array
    of stats.

### POST

Summary

Get tunnel time series stats data filter by certain query parameters and
certain appliance IDs.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

\<nePk/IP\>

-   Type: array

column\_def

-   Type: array
-   Description: These names correspond to each stats field in the array
    of stats.

/stats/timeseries/tunnel/{nePk}
-------------------------------

### GET

Summary

Get tunnel time series stats data of a single appliance filter by
certain query parameters.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

None

nePk

-   Description: Internal ID of the appliance to filter from
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

tunnelName

-   Description: Filter for data which belongs to tunnel with name
    {tunnelName}
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

-   Type: array

column\_def

-   Type: array
-   Description: These names correspond to each stats field in the array
    of stats.

/stats/timeseries/appliance
---------------------------

### GET

Summary

Get appliance time series stats data filter by certain query parameters.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

groupPk

-   Description: Internal group ID of a group for which you want to
    retrieve stats
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

\<nePk/IP\>

-   Type: array

column\_def

-   Type: array
-   Description: These names correspond to each stats field in the array
    of stats.

### POST

Summary

Get appliance time series stats data filter by certain query parameters
and certain appliance IDs.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

\<nePk/IP\>

-   Type: array

column\_def

-   Type: array
-   Description: These names correspond to each stats field in the array
    of stats.

/stats/timeseries/appliance/{nePk}
----------------------------------

### GET

Summary

Get appliance time series stats data of a single appliance filter by
certain query parameters.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

None

nePk

-   Description: Internal ID of the appliance to filter from
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

-   Type: array

column\_def

-   Type: array
-   Description: These names correspond to each stats field in the array
    of stats.

/stats/timeseries/application
-----------------------------

### GET

Summary

Get application time series stats data filter by certain query
parameters.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

groupPk

-   Description: Internal group ID of a group for which you want to
    retrieve stats
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

application

-   Description: Filter for data which belongs to application with name
    {application}
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

\<nePk/IP\>

-   Type: array

column\_def

-   Type: array
-   Description: These names correspond to each stats field in the array
    of stats.

### POST

Summary

Get application time series stats data filter by certain query
parameters and certain appliance IDs.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

application

-   Description: Filter for data which belongs to application with name
    {application}
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

\<nePk/IP\>

-   Type: array

column\_def

-   Type: array
-   Description: These names correspond to each stats field in the array
    of stats.

/stats/timeseries/application/{nePk}
------------------------------------

### GET

Summary

Get application time series stats data of a single appliance filter by
certain query parameters.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

None

nePk

-   Description: Internal ID of the appliance to filter from
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

application

-   Description: Filter for data which belongs to application with name
    {application}
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

-   Type: array

column\_def

-   Type: array
-   Description: These names correspond to each stats field in the array
    of stats.

/stats/timeseries/trafficClass
------------------------------

### GET

Summary

Get traffic class time series stats data filter by certain query
parameters.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

groupPk

-   Description: Internal group ID of a group for which you want to
    retrieve stats
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficClass

-   Description: Filter for data which belongs to trafficClass by
    integer value {trafficClass} 1 - 10
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

\<nePk/IP\>

-   Type: array

column\_def

-   Type: array
-   Description: These names correspond to each stats field in the array
    of stats.

### POST

Summary

Get traffic class time series stats data filter by certain query
parameters and certain appliance IDs.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficClass

-   Description: Filter for data which belongs to trafficClass by
    integer value {trafficClass} 1 - 10
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

\<nePk/IP\>

-   Type: array

column\_def

-   Type: array
-   Description: These names correspond to each stats field in the array
    of stats.

/stats/timeseries/trafficClass/{nePk}
-------------------------------------

### GET

Summary

Get traffic class time series stats data of a single appliance filter by
certain query parameters.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

None

nePk

-   Description: Internal ID of the appliance to filter from
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficClass

-   Description: Filter for data which belongs to trafficClass by
    integer value {trafficClass} 1 - 10
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

-   Type: array

column\_def

-   Type: array
-   Description: These names correspond to each stats field in the array
    of stats.

/stats/timeseries/flow
----------------------

### GET

Summary

Get flow time series stats data filter by certain query parameters.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

groupPk

-   Description: Internal group ID of a group for which you want to
    retrieve stats
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

flowType

-   Description: Filter for data which belongs to a certain flow type.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

-   Type: array

column\_def

-   Type: array
-   Description: These names correspond to each stats field in the array
    of stats.

### POST

Summary

Get flow time series stats data filter by certain query parameters and
certain appliance IDs.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

flowType

-   Description: Filter for data which belongs to a certain flow type.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

-   Type: array

column\_def

-   Type: array
-   Description: These names correspond to each stats field in the array
    of stats.

/stats/timeseries/flow/{nePk}
-----------------------------

### GET

Summary

Get flow time series stats data of a single appliance filter by certain
query parameters.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

None

nePk

-   Description: Internal ID of the appliance to filter from
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

flowType

-   Description: Filter for data which belongs to a certain flow type.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

TIMESTAMP

-   Type: integer
-   Description: Timestamp in EPOCH time, all timestamp fields are in
    EPOCH time

LRX\_BYTES

-   Type: integer
-   Description: Number of bytes LAN side received

LTX\_BYTES

-   Type: integer
-   Description: Number of bytes LAN side transmitted

LRX\_PKTS

-   Type: integer
-   Description: Number of packets LAN side received

LTX\_PKTS

-   Type: integer
-   Description: Number of packets LAN side transmitted

WRX\_BYTES

-   Type: integer
-   Description: Number of bytes WAN side received

WTX\_BYTES

-   Type: integer
-   Description: Number of bytes WAN side transmitted

WRX\_PKTS

-   Type: integer
-   Description: Number of packets WAN side received

WTX\_PKTS

-   Type: integer
-   Description: Number of packets WAN side transmitted

COMP\_L2W

-   Type: integer
-   Description: LAN to WAN compression

COMP\_W2L

-   Type: integer
-   Description: WAN to LAN compression

COMP\_NOOHEAD\_L2W

-   Type: integer
-   Description: LAN to WAN compression without protocol headers

COMP\_NOOHEAD\_W2L

-   Type: integer
-   Description: WAN to LAN compression without protocol headers

LRX\_BYTES\_MAX

-   Type: integer
-   Description: Peak number of bytes LAN side received in one second

LRX\_BYTES\_MAX\_TS

-   Type: integer
-   Description: Timestamp of when peak number of bytes LAN side
    received

LTX\_BYTES\_MAX

-   Type: integer
-   Description: Peak number of bytes LAN side transmitted in one second

LTX\_BYTES\_MAX\_TS

-   Type: integer
-   Description: Timestamp of when peak number of bytes LAN side
    transmitted

LRX\_PKTS\_MAX

-   Type: integer
-   Description: Peak number of packets LAN side received in one second

LRX\_PKTS\_MAX\_TS

-   Type: integer
-   Description: Timestamp of when peak number of packets LAN side
    received

LTX\_PKTS\_MAX

-   Type: integer
-   Description: Peak number of packets LAN side transmitted in one
    second

LTX\_PKTS\_MAX\_TS

-   Type: integer
-   Description: Timestamp of when peak number of packets LAN side
    transmitted

WRX\_BYTES\_MAX

-   Type: integer
-   Description: Peak number of bytes WAN side received in one second

WRX\_BYTES\_MAX\_TS

-   Type: integer
-   Description: Timestamp of when peak number of bytes WAN side
    received

WTX\_BYTES\_MAX

-   Type: integer
-   Description: Peak number of bytes WAN side transmitted in one second

WTX\_BYTES\_MAX\_TS

-   Type: integer
-   Description: Timestamp of when peak number of bytes WAN side
    transmitted

WRX\_PKTS\_MAX

-   Type: integer
-   Description: Peak number of packets WAN side received in one second

WRX\_PKTS\_MAX\_TS

-   Type: integer
-   Description: Timestamp of when peak number of packets WAN side
    received

WTX\_PKTS\_MAX

-   Type: integer
-   Description: Peak number of packets WAN side transmitted in one
    second

WTX\_PKTS\_MAX\_TS

-   Type: integer
-   Description: Timestamp of when peak number of packets WAN side
    transmitted

COMP\_L2WMAX

-   Type: integer
-   Description: Peak LAN to WAN compression for one second interval

COMP\_L2WMAX\_TS

-   Type: integer
-   Description: Timestamp of when peak LAN to WAN compression occurred

COMP\_W2LMAX

-   Type: integer
-   Description: Peak WAN to LAN compression for one second interval

COMP\_W2LMAX\_TS

-   Type: integer
-   Description: Timestamp of when peak WAN to LAN compression occurred

COMP\_NOOHEAD\_L2WMAX

-   Type: integer
-   Description: Peak LAN to WAN compression without protocol headers

COMP\_NOOHEAD\_L2WMAX\_TS

-   Type: integer
-   Description: Timestamp of when peak LAN to WAN compression without
    protocol headers occurred

COMP\_NOOHEAD\_W2LMAX

-   Type: integer
-   Description: Peak WAN to LAN compression without protocol headers

COMP\_NOOHEAD\_W2LMAX\_TS

-   Type: integer
-   Description: Timestamp of when peak WAN to LAN compression without
    protocol headers occurred

FLOW\_TYPE

-   Type: integer
-   Description: Number of bytes of protocol headers WAN side received

TRAFFIC\_TYPE

-   Type: integer
-   Description: Integer value indicating the traffic type these stats
    belong to. 1: Optimized Traffic, 2: Pass-through Shaped, 3:
    Pass-through Unshaped, 4: All Traffic

CREATED

-   Type: integer
-   Description: Number of created flows

CREATED\_MAX

-   Type: integer
-   Description: Peak number of created flows

CREATED\_MAX\_TS

-   Type: integer
-   Description: Timestamp of when peak number of created flows occurred

DELETED

-   Type: integer
-   Description: Number of deleted flows

DELETED\_MAX

-   Type: integer
-   Description: Peak number of deleted flows

DELETED\_MAX\_TS

-   Type: integer
-   Description: Timestamp of when peak number of deleted flows occurred

EXT

-   Type: integer
-   Description: Number of existing flows at the time when the stats
    sample was taken

FLOW\_MAX

-   Type: integer
-   Description: Peak number of flows

FLOW\_PEAK

-   Type: integer
-   Description: Same as FLOW\_MAX

FLOW\_PEAK\_TS

-   Type: integer
-   Description: Timestamp of when peak number of flows occurred

FLOW\_EXT\_PEAK

-   Type: integer
-   Description: Peak number of existing flows at the time when the
    stats sample was taken

FLOW\_EXT\_PEAK\_TS

-   Type: integer
-   Description: Timestamp of when peak number of existing flow occurred

/stats/timeseries/dscp
----------------------

### GET

Summary

Get dscp time series stats data filter by certain query parameters.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

groupPk

-   Description: Internal group ID of a group for which you want to
    retrieve stats
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

dscp

-   Description: Filter for data which belongs to a certain flow type.
    Valid DSCP: 0 - 63
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

\<nePk/IP\>

-   Type: array

column\_def

-   Type: array
-   Description: These names correspond to each stats field in the array
    of stats.

### POST

Summary

Get dscp time series stats data filter by certain query parameters and
certain appliance IDs.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

dscp

-   Description: Filter for data which belongs to a certain flow type.
    Valid DSCP: 0 - 63
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

\<nePk/IP\>

-   Type: array

column\_def

-   Type: array
-   Description: These names correspond to each stats field in the array
    of stats.

/stats/timeseries/dscp/{nePk}
-----------------------------

### GET

Summary

Get dscp time series stats data of a single appliance filter by certain
query parameters.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

None

nePk

-   Description: Internal ID of the appliance to filter from
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

dscp

-   Description: Filter for data which belongs to a certain flow type.
    Valid DSCP: 0 - 63
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

TIMESTAMP

-   Type: integer
-   Description: Timestamp in EPOCH time, all timestamp fields are in
    EPOCH time

LRX\_BYTES

-   Type: integer
-   Description: Number of bytes LAN side received

LTX\_BYTES

-   Type: integer
-   Description: Number of bytes LAN side transmitted

LRX\_PKTS

-   Type: integer
-   Description: Number of packets LAN side received

LTX\_PKTS

-   Type: integer
-   Description: Number of packets LAN side transmitted

WRX\_BYTES

-   Type: integer
-   Description: Number of bytes WAN side received

WTX\_BYTES

-   Type: integer
-   Description: Number of bytes WAN side transmitted

WRX\_PKTS

-   Type: integer
-   Description: Number of packets WAN side received

WTX\_PKTS

-   Type: integer
-   Description: Number of packets WAN side transmitted

COMP\_L2W

-   Type: integer
-   Description: LAN to WAN compression

COMP\_W2L

-   Type: integer
-   Description: WAN to LAN compression

COMP\_NOOHEAD\_L2W

-   Type: integer
-   Description: LAN to WAN compression without protocol headers

COMP\_NOOHEAD\_W2L

-   Type: integer
-   Description: WAN to LAN compression without protocol headers

LRX\_BYTES\_MAX

-   Type: integer
-   Description: Peak number of bytes LAN side received in one second

LRX\_BYTES\_MAX\_TS

-   Type: integer
-   Description: Timestamp of when peak number of bytes LAN side
    received

LTX\_BYTES\_MAX

-   Type: integer
-   Description: Peak number of bytes LAN side transmitted in one second

LTX\_BYTES\_MAX\_TS

-   Type: integer
-   Description: Timestamp of when peak number of bytes LAN side
    transmitted

LRX\_PKTS\_MAX

-   Type: integer
-   Description: Peak number of packets LAN side received in one second

LRX\_PKTS\_MAX\_TS

-   Type: integer
-   Description: Timestamp of when peak number of packets LAN side
    received

LTX\_PKTS\_MAX

-   Type: integer
-   Description: Peak number of packets LAN side transmitted in one
    second

LTX\_PKTS\_MAX\_TS

-   Type: integer
-   Description: Timestamp of when peak number of packets LAN side
    transmitted

WRX\_BYTES\_MAX

-   Type: integer
-   Description: Peak number of bytes WAN side received in one second

WRX\_BYTES\_MAX\_TS

-   Type: integer
-   Description: Timestamp of when peak number of bytes WAN side
    received

WTX\_BYTES\_MAX

-   Type: integer
-   Description: Peak number of bytes WAN side transmitted in one second

WTX\_BYTES\_MAX\_TS

-   Type: integer
-   Description: Timestamp of when peak number of bytes WAN side
    transmitted

WRX\_PKTS\_MAX

-   Type: integer
-   Description: Peak number of packets WAN side received in one second

WRX\_PKTS\_MAX\_TS

-   Type: integer
-   Description: Timestamp of when peak number of packets WAN side
    received

WTX\_PKTS\_MAX

-   Type: integer
-   Description: Peak number of packets WAN side transmitted in one
    second

WTX\_PKTS\_MAX\_TS

-   Type: integer
-   Description: Timestamp of when peak number of packets WAN side
    transmitted

COMP\_L2WMAX

-   Type: integer
-   Description: Peak LAN to WAN compression for one second interval

COMP\_L2WMAX\_TS

-   Type: integer
-   Description: Timestamp of when peak LAN to WAN compression occurred

COMP\_W2LMAX

-   Type: integer
-   Description: Peak WAN to LAN compression for one second interval

COMP\_W2LMAX\_TS

-   Type: integer
-   Description: Timestamp of when peak WAN to LAN compression occurred

COMP\_NOOHEAD\_L2WMAX

-   Type: integer
-   Description: Peak LAN to WAN compression without protocol headers

COMP\_NOOHEAD\_L2WMAX\_TS

-   Type: integer
-   Description: Timestamp of when peak LAN to WAN compression without
    protocol headers occurred

COMP\_NOOHEAD\_W2LMAX

-   Type: integer
-   Description: Peak WAN to LAN compression without protocol headers

COMP\_NOOHEAD\_W2LMAX\_TS

-   Type: integer
-   Description: Timestamp of when peak WAN to LAN compression without
    protocol headers occurred

DSCP

-   Type: integer
-   Description: DSCP value

TRAFFIC\_TYPE

-   Type: integer
-   Description: Integer value indicating the traffic type these stats
    belong to. 1: Optimized Traffic, 2: Pass-through Shaped, 3:
    Pass-through Unshaped, 4: All Traffic

column\_def

-   Type: array
-   Description: These names correspond to each stats field in the array
    of stats.

/stats/timeseries/shaper
------------------------

### POST

Summary

Get Shaper time series stats data filter by certain query parameters and
certain appliance IDs.

Input type

application/json

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficClass

-   Description: Filter for data which belongs to trafficClass by
    integer value {trafficClass} 1 - 10
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

direction

-   Description: 0 - Outbound/1 - Inbound
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

data

\<nePk/IP\>

-   Type: array

column\_def

-   Type: array
-   Description: These names correspond to each stats field in the array
    of stats.

/stats/timeseries/shaper
------------------------

### GET

Summary

Get Shaper time series stats data filter by certain query parameters and
certain appliance IDs.

Input type

application/json

groupPk

-   Description: Internal group ID of a group for which you want to
    retrieve stats
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficClass

-   Description: Filter for data which belongs to trafficClass by
    integer value {trafficClass} 1 - 10
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

direction

-   Description: 0 - Outbound/1 - Inbound
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

data

\<nePk/IP\>

-   Type: array

column\_def

-   Type: array
-   Description: These names correspond to each stats field in the array
    of stats.

/stats/timeseries/internalDrops/{nePk}
--------------------------------------

### GET

Summary

Get internalDrops time series stats data of a single appliance filter by
certain query parameters.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

None

nePk

-   Description: Internal ID of the appliance to filter from
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

data

\<dropId\>

-   Type: array

/stats/timeseries/drc
---------------------

### GET

Summary

Get tunnel drc time series stats data filter by certain query
parameters.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

groupPk

-   Description: Internal group ID of a group for which you want to
    retrieve stats
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

\<nePk/IP\>

-   Type: array
-   Required: true
-   Description: An array that contains the stats data of current
    appliance

column\_def

-   Type: array
-   Required: true
-   Description: These names correspond to each stats field in the array
    of stats.

### POST

Summary

Get tunnel time series drc stats data filter by certain query parameters
and certain appliance IDs.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

\<nePk/IP\>

-   Type: array
-   Required: true
-   Description: An array that contains the stats data of current
    appliance

column\_def

-   Type: array
-   Required: true
-   Description: These names correspond to each stats field in the array
    of stats.

/stats/timeseries/drc/{nePk}
----------------------------

### GET

Summary

Get tunnel time series drc stats data of a single appliance filter by
certain query parameters.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

None

nePk

-   Description: Internal ID of the appliance to filter from
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

tunnelName

-   Description: Filter for data which belongs to tunnel with name
    {tunnelName}
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

ip

-   Description: Using IP as key for grouping stats instead of using
    internal appliance id as key. Default value if false
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

-   Type: array
-   Required: true
-   Description: An array that contains the stats data of specific
    tunnel of current appliance

column\_def

-   Type: array
-   Required: true
-   Description: These names correspond to each stats field in the array
    of stats.

/stats/timeseries/interface/{nePk}
----------------------------------

### GET

Summary

Get interface time series stats data of a single appliance filter by
certain query parameters.

Notes

This operation returns a JSON object containing data objects of each
interface is an array, each data object contains the stats for a
particular timestamp.

Input type

None

nePk

-   Description: Internal ID of the appliance to filter from
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

trafficType

-   Description: Filter for data for given traffic type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

interfaceName

-   Description: Filter for data which belongs to interface with name
    {interface}
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/stats/timeseries/interfaceOverlay/{nePk}
-----------------------------------------

### GET

Summary

Get interface overlay transport time series stats data of a single
appliance filter by certain query parameters.

Notes

This operation returns a JSON object containing data objects of each
interface overlay transport is an array, each data object contains the
stats for a particular timestamp.

Input type

None

nePk

-   Description: Internal ID of the appliance to filter from
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

overlay

-   Description: When it is \'0\',return all physical tunnels;when it is
    not given,return all bonded and physical tunnels;otherwise,return
    bonded tunnels associated with the overlay id.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/stats/timeseries/mos/{nePk}
----------------------------

### Get

Summary

Get Mean Opinion Score time series stats data of a single appliance
filter by certain query parameters.

Notes

This operation is used to retrieve mos time series stats. Depend on what
query parameters are provided. The response format may look different.

Input type

None

nePk

-   Description: Internal ID of the appliance to filter from
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

tunnel

-   Description: Filter for data which belongs to tunnel with name
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/stats/timeseries/application2
------------------------------

### GET

Summary

Get new application time series stats data filter by certain query
parameters.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

application

-   Description: Filter for data which belongs to application with name
    {application}
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

groupPk

-   Description: Internal group ID of a group for which you want to
    retrieve stats
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

total

-   Description: get all app\'s total value
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

-   Type: array

### POST

Summary

Get new application time series stats data filter by certain query
parameters and certain appliance IDs.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

application/json: undefined

ApplianceIDs

Description: JSON array containing appliance IDs you want to filter on.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

application

-   Description: Filter for data which belongs to application with name
    {application}
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

total

-   Description: get all app\'s total value
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

latest

-   Description: Instead of providing {startTime} and {endTime}, using
    {latest} query parameter could provide you stats for, say latest 10
    minutes. Unit is minute. Example: latest=10. Default is to use
    {startTime} and {endTime} but if {latest} is presented then {latest}
    takes priority.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

data

-   Type: array

/stats/timeseries/application2/{nePk}
-------------------------------------

### GET

Summary

Get new application time series stats data of a single appliance filter
by certain query parameters.

Notes

This operation returns a JSON object containing COLUMN\_DEF and DATA.
COLUMN\_DEF is an array containing the names indicating what
corresponding number in data array means. Data objects of each appliance
is an array of data arrays, each data array contains the stats for a
particular timestamp. Each number is data array corresponds to a name in
COLUMN\_DEF.\
**DATA object contains only values not keys.**

Input type

None

nePk

-   Description: Internal ID of the appliance to filter from
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

application

-   Description: Filter for data which belongs to application with name
    {application}
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

total

-   Description: get all app\'s total value
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

format

-   Description: The only format other than JSON we support currently is
    CSV, so format=csv.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

data

-   Type: array

/stats/timeseries/boost/{nePk}
------------------------------

### Get

Summary

Get boost time series stats data of a single appliance filter by certain
query parameters.

Notes

This operation is used to retrieve boost time series stats. Depend on
what query parameters are provided. The response format may look
different.

Input type

None

nePk

-   Description: Internal ID of the appliance to filter from
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: Limit the number of stats entity retrieved. Default and
    maximum limits are both 10000.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/stats/timeseries/securityPolicy/{nePk}
---------------------------------------

### Get

Summary

Get security policy time series stats data of a single appliance filter
by certain query parameters.

Notes

This operation is used to retrieve security policy time series stats.

Input type

None

nePk

-   Description: Internal ID of the appliance to filter from
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

granularity

-   Description: Data granularity filtering whether data is minutely
    data, hourly data or daily data.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

fromZone

-   Description: Limit the id of source zone from which the traffic
    passes through.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

toZone

-   Description: Limit the id of destination zone to which the traffic
    goes.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

data

-   Type: array
-   Required: true
-   Description: An array that contains the stats data of specific
    security policy of current appliance

column\_def

-   Type: array
-   Required: true
-   Description: These names correspond to each stats field in the array
    of stats.

Reference: timeseriesStats.json

\

VXOA per second statistics
==========================

/realtimeStats/{nePk}
---------------------

### POST

Summary

Get per second statistics from a VXOA appliance

Notes

Tunnel stats: Set \'type\' to \'tunnel\'. Set \'name\' to one of the
tunnel names or \'pass-through\' or \'pass-through-unshaped\'.
\'filter\' is not used for tunnel statistics.\
TrafficType stats: Set the \'type\' to \'trafficType\' to retrieve two
aggregate real-time stats: optimized, all-traffic. For optimized, set
\'name\' to \'0\' and for all-traffic, set \'name\' to \'3\'. \'filter\'
is not used.\
Application stats: Set \'type\' to \'app\'. Set \'name\' to application
name. \'filter\' is required.\
DSCP stats: Set \'type\' to \'dscp\', set name to one of DSCP values
from \'0\' to \'63\'. \'filter\' is required.(Possible \'filter\' values
are \'0\' for optimized\_traffic,\'1\' for pass\_through\_shaped,\'2\'
for pass\_through\_unshaped,\'3\' for all\_traffic)\
MOS stats: Set \'name\' to tunnel id, set \'type\' to tunnel.\
Traffic Class Stats: set \'type\' to \'trafficClass\', set \'name\' to
one of traffic classes from \'0\' to \'9\'. \'filter\' is required.\
Flow stats: set type to \'flow\', name to \'0\' for TCP accelerated,
\'1\' for TCP unaccelerated and \'2\' for non-TCP flows. \'filter\' is
required.(Possible \'filter\' values are \'0\' for
optimized\_traffic,\'1\' for pass\_through\_shaped,\'2\' for
pass\_through\_unshaped,\'3\' for all\_traffic)\
Shaper stats: Set \'type\' to \'shaper\'. Set \'name\' to one of traffic
classes from \'0\' to \'9\'.\'filter\' to traffic direction(\'0\' for
Outbound and \'1\' for Inbound).\
Drops stats: Set \'type\' to \'drops\'. Set \'name\' to empty string.\
Interface stats: Set \'type\' to \'interface\'. Set \'name\' to
interface name.Set \'filter\' to traffic type.(Possible \'filter\'
values are \'0\' for optimized\_traffic,\'1\' for
pass\_through\_shaped,\'2\' for pass\_through\_unshaped,\'3\' for
all\_traffic)\
Important: Appliances store per second statistics for 3 seconds. You
must poll at a frequency faster than 3 seconds to make sure to not have
gaps in the results. The result format for all statistics looks like:
\'{\'nameofstat\': \[time, value\], \[time, value\], \[time, value\]}\'
where you get statistics for last three seconds.

Input type

application/json: undefined

nePk

-   Description: Appliance primary key
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

body

Description:

Required: true

Parameter type: POST Body

Data Type: application/json

type

-   Type: string
-   Required: true

name

-   Type: string
-   Required: true

filter

-   Type: string
-   Required: true
-   Description: For traffic Type : 0 - optimized traffic, 1 -
    passthrough-shaped, 2 - passthrough-unshaped, 3 - all traffic.
    Default is 0 - optimized traffic if omitted. For traffic direction:
    1 - inbound, 0-outbound

Output type

None

Reference: realtimeStats.json

\

VXOA configuration templates
============================

/template/templateGroups
------------------------

### GET

Summary

Returns template configurations for all template groups

Notes

The returned objects inside the array are template-specific.

Input type

None

Output type

None

/template/templateGroups/{templateGroup}
----------------------------------------

### GET

Summary

Returns template configurations for the specified template group only

Notes

The returned objects inside the array are template-specific.

Input type

None

templateGroup

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

name

-   Type: string
-   Description: Name of the template group

templates

-   Type: array
-   Description: All templates data in the template group

selectedTemplates

-   Type: array
-   Description: All selected template(s) data in the template group

### POST

Summary

Creates a new template group from an existing one or updates the
configuration of the requested template group

Notes

In the request body, you will pass in the templates you want to
create/modify with the template group. For details on the structure of
the template configuration, please see the default template group. One
way to get the exact JSON object to use for your configuration is to
view the rest API call the Orchestrator Templates tab makes when you
save a template.

Input type

application/json: undefined

templateGroup

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

undefined

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

name

-   Type: string
-   Required: true

templates

-   Type: array
-   Required: true
-   Description: An array of the templates you want to create/update

Output type

None

### DELETE

Summary

Deletes a template group

Input type

None

templateGroup

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/template/templateCreate
------------------------

### POST

Summary

Creates a new template group

Notes

In the request body, you will pass in the templates you want to
create/modify with the template group. For details on the structure of
the template configuration, please see the default template group. One
way to get the exact JSON object to use for your configuration is to
view the rest API call the Orchestrator Templates tab makes when you
save a template.

Input type

application/json: undefined

undefined

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

name

-   Type: string
-   Required: true

templates

-   Type: array
-   Required: true
-   Description: An array of the templates you want to create/update

Output type

None

/template/templateSelection/{templateGroup}
-------------------------------------------

### GET

Summary

Returns the selected templates in the template group

Input type

None

templateGroup

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

### POST

Summary

Selects the provided templates in the request body

Input type

application/json: undefined

templateGroup

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

undefined

-   Description: Template names
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/template/history/{nePk}
------------------------

### GET

Summary

Returns history of applied templates by nePk

Input type

None

nePk

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

latestOnly

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

/template/history/groupList/{nePk}
----------------------------------

### GET

Summary

Returns the list of template groups applied to the appliance

Input type

None

nePk

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/template/applianceAssociation/
-------------------------------

### GET

Summary

Returns the complete association map of appliances to template groups

Input type

None

Output type

application/json

nepk

-   Type: array
-   Description: Names of the template groups associated with this
    appliance

/template/applianceAssociation/{nePk}
-------------------------------------

### GET

Summary

Returns the association map of template groups for a single appliance

Input type

None

nePk

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

templateIds

-   Type: array
-   Description: Names of the template groups associated with this
    appliance

### POST

Summary

Associates the templates with a specific appliance. The array posted is
the complete association for that appliance.

Input type

application/json: undefined

undefined

-   Description: Template names
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

nePk

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/template/templateGroupsPriorities
----------------------------------

### GET

Summary

Returns the order that template groups will be applied in

Input type

None

Output type

None

### POST

Summary

Post the order that template groups should be applied in, groups not
listed are not in a deterministic order

Input type

application/json: undefined

undefined

-   Description: Template names
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

Reference: template.json

\

Link integrity and bandwidth test
=================================

/linkIntegrityTest/status/{neId}
--------------------------------

### GET

Summary

Operation to retrieve current link integrity test status for desired
appliance.

Input type

None

neId

-   Description: Internal ID of the appliance from which you want to
    retrieve status information.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

active

-   Type: boolean
-   Required: true
-   Description: Boolean value indicating whether link integrity test is
    running

result

-   Type: string
-   Required: true
-   Description: Console output of current link integrity test

/linkIntegrityTest/run
----------------------

### POST

Summary

POST operation to start link integrity test between two appliances.

Notes

This API allows you to start link integrity test between two appliances
using desired test program.

Input type

application/json: undefined

LinkIntegrityTestRun

Description: JSON object that contains the setups for link integrity
test.

Required: true

Parameter type: POST Body

Data Type: application/json

appA

nePk

-   Type: string
-   Required: true
-   Description: Internal ID of the appliance. Use /appliance rest api
    to fetch the IDs. They look like 1.NE, 2.NE etc.,

bandwidth

-   Type: string
-   Required: true
-   Description: Data transfer rate to try to use from this appliance.

path

-   Type: string
-   Required: true
-   Description: Path setups for link integrity test. Path could be
    either \'pass-through\', \'pass-through-unshaped\' or
    \'{tunnelID}\'. Fetch the tunnelIDs using tunnels rest apis. They
    typically look like tunnel\_1, tunnel\_2 etc.,

appB

nePk

-   Type: string
-   Required: true
-   Description: Internal ID of the appliance. Use /appliance rest api
    to fetch the IDs. They look like 1.NE, 2.NE etc.,

bandwidth

-   Type: string
-   Required: true
-   Description: Data transfer rate to try to use from this appliance.

path

-   Type: string
-   Required: true
-   Description: Path setups for link integrity test. Path could be
    either \'pass-through\', \'pass-through-unshaped\' or
    \'{tunnelID}\'. Fetch the tunnelIDs using tunnels rest apis. They
    typically look like tunnel\_1, tunnel\_2 etc.,

duration

-   Type: string
-   Required: true
-   Description: Duration of the run for each side. Unit is second.

testProgram

-   Type: string
-   Required: true
-   Description: Test program desired for link integrity test. Currently
    we support \'iperf\' and \'tcpperf\'.

DSCP

-   Type: string
-   Required: true
-   Description: DSCP setup for link integrity test. If you don\'t want
    to specify a DSCP value, put \'any\'.

Output type

None

Reference: linkIntegrity.json

\

VXOA route policies
===================

/routeMaps/{neId}?cached={cached}
---------------------------------

### GET

Summary

Get route policy settings

Notes

the returned data contains options object and data object. data objects
includes Route map(s), each map includes Route rule(s), each rule has a
match part and a set part. The schema of returned data: { \'options\' :
{\'activeMap\': \'map1\'}, \'data\': {\'map1\' : {\'prio\': {\...},
\'self\':\'map1\'}}} The \'prio\' object contains rules key value pairs,
each key is the priority of a rule, each value is the rule object.

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

Reference: routePolicy.json

\

VXOA optimization policies
==========================

/optimizationMaps/{neId}?cached={cached}
----------------------------------------

### GET

Summary

Get route policy settings

Notes

the returned data contains options object and data object. data objects
includes optimization map(s), each map includes optimization rule(s),
each rule has a match part and a set part. The schema of returned data:
{ \'options\' : {\'activeMap\': \'map1\'}, \'data\': {\'map1\' :
{\'prio\': {\...}, \'self\':\'map1\'}}} The \'prio\' object contains
rules key value pairs, each key is the priority of a rule, each value is
the rule object.

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

Reference: optimizationPolicy.json

\

VXOA QoS Policies
=================

/qosMaps/{neId}?cached={cached}
-------------------------------

### GET

Summary

Get qos policy settings

Notes

the returned data contains options object and data object. data objects
includes qos map(s), each map includes qos rule(s), each rule has a
match part and a set part. The schema of returned data: { \'options\' :
{\'activeMap\': \'map1\'}, \'data\': {\'map1\' : {\'prio\': {\...},
\'self\':\'map1\'}}} The \'prio\' object contains rules key value pairs,
each key is the priority of a rule, each value is the rule object.

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

Reference: qosPolicy.json

\

VXOA SaaS NAT policies
======================

/natMaps/{neId}?cached={cached}
-------------------------------

### GET

Summary

Get NAT policy settings

Notes

the returned data contains options object and data object. data objects
includes NAT map(s), each map includes NAT rule(s), each rule has a
match part and a set part. The schema of returned data: { \'options\' :
{\'activeMap\': \'map1\'}, \'data\': {\'map1\' : {\'prio\': {\...},
\'self\':\'map1\'}}} The \'prio\' object contains rules key value pairs,
each key is the priority of a rule, each value is the rule object.

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

/natAll/{neId}?cached={cached}
------------------------------

### GET

Summary

Get NAT all inbound/outbound settings

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

/natMapsDynamic/{neId}?cached={cached}
--------------------------------------

### GET

Summary

Get dynamic NAT rules settings

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

Reference: natPolicy.json

\

VXOA ACLs (access control lists)
================================

/acls/{neId}?cached={cached}
----------------------------

### GET

Summary

Get Access lists settings

Notes

the returned data is an object, each key is name of an ACL, each value
is an object of the ACL\'s settings. Each ACL\'s settings contains
\'entry\' and \'rmap\'. \'rmap\' give info about the routemap which uses
this ACL.

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

Reference: acls.json

\

VXOA bandwidth shaping
======================

/shaper/{neId}?cached={cached}
------------------------------

### GET

Summary

Get shapers settings

Notes

the returned data is an object, each key is name of an interface, each
value is an object defining shaper properties of that interface which
includes \'traffic-class\', \'accuracy\', \'max\_bw\' fields.

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

Reference: shaper.json

\

VXOA inbound bandwidth shaping
==============================

/inboundShaper/{neId}?cached={cached}
-------------------------------------

### GET

Summary

Get inbound shapers settings

Notes

the returned data is an object, each key is name of an interface, each
value is an object defining shaper properties of that interface which
includes \'traffic-class\', \'accuracy\', \'max\_bw\' and \'enable\'
fields.

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

Reference: inboundShaper.json

\

VXOA current flows
==================

/flow/{neId}/q
--------------

### GET

Summary

Returns active, inactive, or both types of flows on the appliance based
on the query parameters supplied

Notes

Flows can be more specifically filtered by the available query
parameters.

Input type

None

neId

-   Description: Appliance ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

ip1

-   Description: First IP endpoint
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

mask1

-   Description: Mask for ip1
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

port1

-   Description: Port of first IP endpoint
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

ip2

-   Description: Second IP endpoint
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

mask2

-   Description: Mask for ip2
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

port2

-   Description: Port of second IP endpoint
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

ipEitherFlag

-   Description: Enable directionality for IP. If true, ip1 will be
    treated as the source IP, and ip2 will be treated as the destination
    IP
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

portEitherFlag

-   Description: Enable directionality for port. If true, port1 will be
    treated as the source port, and port2 will be treated as the
    destination port
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

application

-   Description: Application specific flows. Drop-down only shows
    built-in apps and not user-defined apps
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

applicationGroup

-   Description: Application groups. Drop-down only shows a small subset
    of application groups
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

protocol

-   Description: Application protocol
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

vlan

-   Description: Virtual LAN ID
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

dscp

-   Description: DSCP marking
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

overlays

-   Description: Overlay ID. Multiple values must be separated with
    \"\|\". E.g. \"1\|2\"
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

transport

-   Description: Transport type. Accepted values: \"fabric\",
    \"underlay\", \"breakout\". Multiple values must be separated with
    \"\|\". E.g. \"fabric\|underlay\". Note: Fabric refers to SD-WAN.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

services

-   Description: Service name. Third party services should be formatted
    with an asterisk symbol at the end in order to filter by prefix
    instead of finding a complete match. Multiple services must be
    separated with \"\|\". E.g. \"Zscaler\_\*\|PaloAlto\"
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

zone1

-   Description: Flows to zone1. Values: \"any\", \"0\" (default),
    \"zone\_id\"
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

zone2

-   Description: Flows from zone2. Values: \"any\", \"0\" (default),
    \"zone\_id\"
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

zoneEither

-   Description: Flows either to zoneEither or from zoneEither. Values:
    \"any\", \"0\" (default), \"zone\_id\"
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

filter

-   Description: Flow categories
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

edgeHA

-   Description: Edge HA flows. \"True\" to include, \"False\" to
    exclude
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

builtIn

-   Description: Built-in policy flows. \"True\" to include, \"False\"
    to exclude
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

uptime

-   Description: Active and ended flow timing. If not specified, it
    defaults to \"anytime\" plus \"term\"
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

bytes

-   Description: Bytes transferred
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

duration

-   Description: Flows that have lasted less than (\"\<\") or greater
    than (\"\>\") the specified duration (in minutes). Value should be
    \"any\" or a number formatted with a \"\<\" or \"\>\" in front of a
    value between 0 and 18446744073709551615. E.g. \"\>5000\"
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

anytimeSlowFlows

-   Description: Slow Flows flag. If this flag is present, it will show
    slow flows only
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

total\_flows

-   Type: integer
-   Required: true

matched\_flows

-   Type: integer
-   Required: true

now

-   Type: integer
-   Required: true

returned\_flows

-   Type: integer
-   Required: true

stale\_flows

-   Type: integer
-   Required: true

inconsistent\_flows

-   Type: integer
-   Required: true

flows\_with\_issues

-   Type: integer
-   Required: true

flows\_optimized

-   Type: integer
-   Required: true

flows\_with\_ignores

-   Type: integer
-   Required: true

flows\_passthrough

-   Type: integer
-   Required: true

flows\_management

-   Type: integer
-   Required: true

active

total\_flows

-   Type: integer
-   Required: true

stale\_flows

-   Type: integer
-   Required: true

inconsistent\_flows

-   Type: integer
-   Required: true

flows\_with\_issues

-   Type: integer
-   Required: true

flows\_optimized

-   Type: integer
-   Required: true

flows\_with\_ignores

-   Type: integer
-   Required: true

flows\_passthrough

-   Type: integer
-   Required: true

flows\_management

-   Type: integer
-   Required: true

flows\_asymmetric

-   Type: integer
-   Required: true

flows\_route\_dropped

-   Type: integer
-   Required: true

flows\_firewall\_dropped

-   Type: integer
-   Required: true

inactive

total\_flows

-   Type: integer
-   Required: true

stale\_flows

-   Type: integer
-   Required: true

inconsistent\_flows

-   Type: integer
-   Required: true

flows\_with\_issues

-   Type: integer
-   Required: true

flows\_optimized

-   Type: integer
-   Required: true

flows\_with\_ignores

-   Type: integer
-   Required: true

flows\_passthrough

-   Type: integer
-   Required: true

flows\_management

-   Type: integer
-   Required: true

flows\_asymmetric

-   Type: integer
-   Required: true

flows\_route\_dropped

-   Type: integer
-   Required: true

flows\_firewall\_dropped

-   Type: integer
-   Required: true

ret\_code

-   Type: integer
-   Required: true

err\_msg

-   Type: string
-   Required: true

flows

-   Type: array
-   Required: true
-   Description: Each array element consists of the following items in
    the listed order: \[Flow Id, Flow Sequence Id, SilverPeak Flow Id,
    Application Name, IP1\_1, IP1\_2, IP1\_3, IP1\_4, Ip1 Version, IP1
    String, Port1, IP2\_1, IP2\_2, IP2\_3, IP2\_4, Ip2 Version, IP2
    String, Port2, Flow Optimization Status, Percentage Reduction
    Inbound, Inbound TX Bytes, Inbound RX Bytes, Outbound RX Bytes,
    Outbound TX Bytes, Percentage Reduction OutBound, Uptime, Protocol,
    Outbound Tunnel Id, Inbound Tunnel Id, Configured Outbound Tunnel
    Id, LAN Side VLAN, Traffic Class, LAN DSCP, WAN DSCP, Flow
    Redirected From, ISCI Flow Info, SnatIp1, SnatIp2, Flow Start Time,
    Flow End Time, Service Id, SaaS Id, TCP UTC Slow Start Time, TCP
    Slow Duration, IP1 Domain Name, IP2 Domain Name, From Zone, To Zone,
    Zone Dropped, EdgeHA, LAN TX DSCP, LAN RX DSCP, WAN TX DSCP, WAN RX
    DSCP, NAT IP, NAT Port, Original Port, NAT Type, Application Type\].

/flow/flowReset/{neId}
----------------------

### POST

Summary

Resets the flow(s)

Input type

application/json

neId

-   Description: Appliance ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

FlowsResetBody

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

spIds

-   Type: array
-   Required: true
-   Description: Contains the flow IDs.

Output type

application/json

results

-   Type: array
-   Required: true

rc

-   Type: integer
-   Required: true
-   Description: Indicates success or failure

value

-   Type: string
-   Required: true

/flow/flowReClassification/{neId}
---------------------------------

### POST

Summary

Reclassifies the flow(s)

Input type

application/json

neId

-   Description: Appliance ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

FlowsReClassificationBody

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

spIds

-   Type: array
-   Required: true
-   Description: Contains the flow IDs.

Output type

application/json

results

-   Type: array
-   Required: true

rc

-   Type: integer
-   Required: true
-   Description: Indicates success or failure

value

-   Type: string
-   Required: true

/flow/flowBandwidthStats/{neId}/q
---------------------------------

### GET

Summary

Returns the so far accumulated bandwidth stats about the flow

Input type

None

neId

-   Description: Appliance ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

id

-   Description: Flow ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

seq

-   Description: Flow sequence number
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/flow/flowDetails/{neId}/q
--------------------------

### GET

Summary

Returns flow details

Input type

None

neId

-   Description: Appliance ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

id

-   Description: Flow ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

seq

-   Description: Flow sequence number
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/flow/flowDetails2/{neId}/q
---------------------------

### GET

Summary

Returns more detailed info about the flow

Input type

None

neId

-   Description: Appliance ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

id

-   Description: Flow ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

seq

-   Description: Flow sequence number
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

Reference: flow.json

\

Orchestrator to/from VXOA connectivity
======================================

/reachability/appliance/{neId}
------------------------------

### GET

Summary

Get the reachability status from the appliance

Notes

Returned data is an object, each key is IP of the appliance, each value
is an object of the reachability status. Each the reachability status
contains \'rest\', \'ssh\', \'https\' and \'webSocket\'.

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

neIP

rest

-   Type: string
-   Required: true
-   Description: The status of the REST

ssh

-   Type: string
-   Required: true
-   Description: The status of the SSH

https

-   Type: string
-   Required: true
-   Description: The status of the HTTPs

webSocket

-   Type: string
-   Required: true
-   Description: The status of the WebSocket

/reachability/gms/{neId}
------------------------

### GET

Summary

Get the reachability status from the Orchestrator

Notes

Returned data is an object. Each the reachability status contains
\'id\', \'userName\', \'state\', \'neId\', \'hostName\',
\'actualWebProtocolType\' and \'unsavedChanges\'.

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

id

-   Type: string
-   Required: true
-   Description: The device key assigned by Orchestrator

userName

-   Type: string
-   Required: true
-   Description: User Name

state

-   Type: integer
-   Required: true
-   Description: The state of the appliance

neId

-   Type: string
-   Required: true
-   Description: The IP of the appliance

hostName

-   Type: string
-   Required: true
-   Description: The hostname of the appliance

actualWebProtocolType

-   Type: string
-   Required: true
-   Description: The actual web protocol type

unsavedChanges

-   Type: integer
-   Required: true
-   Description: The status of the unsavedChanges(1-Yes, 0-NO)

Reference: reachability.json

\

VXOA Packet capture
===================

/tcpdump/run/
-------------

### POST

Summary

POST operation to start the packet capture process.

Notes

This API allows you to start the packet capture process.

Input type

application/json

PostRunConfig

Description: The run data

Required: true

Parameter type: POST Body

Data Type: application/json

nePks

-   Type: array
-   Required: true
-   Description: The nePks of the appliances

max\_packet

-   Type: string
-   Required: true
-   Description: Desired number of packets to capture

ip

-   Type: string
-   Description: Filter only for this IP to capture

port

-   Type: string
-   Description: Filter only for this port to capture

Output type

None

/tcpdump/status/{neId}
----------------------

### GET

Summary

GET operation to show current packet capture process status.

Notes

This API allows you to retrieve status information of current packet
capture process.

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

active

-   Type: boolean
-   Required: true
-   Description: Boolean value indicating whether packet capture process
    is running

progress

-   Type: double
-   Required: true
-   Description: Progress info of current packet capture, percentage

lastOneDone

-   Type: boolean
-   Required: true
-   Description: Boolean value indication whether packet capture is
    already finish but still remains in post run processing stage

/tcpdump/tcpdumpStatus
----------------------

### GET

Summary

Get the primary keys of all running tcp dumps.

Notes

Get the primary keys of all running tcp dumps.

Input type

None

Output type

None

Reference: tcpdump.json

\

VXOA debug files like tcpdump, snapshots, sysdumps
==================================================

/debugFiles/{nePk}
------------------

### GET

Summary

Get the debug files

Notes

Returned data is an object, The key is \'debugDump\', \'techDump\',
\'snapshots\', \'tcpDump\', \'debugFile\' and \'log\', each value is an
object of the debug file info. Each the debug file info contains
\'name\' and \'stats\'. The value of the stats is an object of the
stats.

Input type

None

nePk

-   Description: nePk is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

debugDump

-   Type: array
-   Description: The info of the Sys Dump file

techDump

-   Type: array
-   Description: The info of the Show Tech file

snapshots

-   Type: array
-   Description: The info of the Snapshot file

tcpDump

-   Type: array
-   Description: The info of the TCP Dump file

debugFile

-   Type: array
-   Description: The info of the debug file

log

-   Type: array
-   Description: The info of the Log file

/debugFiles/delete
------------------

### POST

Summary

Delete the Orchestrator debug file

Notes

Delete the Orchestrator debug file

Input type

application/json

deleteGMSDebugFile

Description: The name of the orchestrator debug file

Required: true

Parameter type: POST Body

Data Type: application/json

fileName

-   Type: string
-   Description: The name of the file

Output type

None

/debugFiles/proxyConfig
-----------------------

### GET

Summary

Get the proxy config settings

Notes

Return data is an object, the keys contains \'proxyHost\',
\'proxyPassword\', \'proxyPort\', \'proxyUser\' and \'useProxy\'

Input type

None

Output type

application/json

useProxy

-   Type: boolean
-   Required: true
-   Description: Use the proxy or not

proxyHost

-   Type: string
-   Required: true
-   Description: The host of the proxy

proxyUser

-   Type: string
-   Required: true
-   Description: The user name

proxyPassword

-   Type: string
-   Required: true
-   Description: The password

proxyPort

-   Type: integer
-   Required: true
-   Description: The port of the proxy

### POST

Summary

Set the proxy config settings

Notes

Set the proxy config settings

Input type

application/json

proxyConfig

Description: The proxy config settings

Required: true

Parameter type: POST Body

Data Type: application/json

useProxy

-   Type: boolean
-   Required: true
-   Description: Use the proxy or not

proxyHost

-   Type: string
-   Required: true
-   Description: The host of the proxy

proxyUser

-   Type: string
-   Required: true
-   Description: The user name

proxyPassword

-   Type: string
-   Required: true
-   Description: The password

proxyPort

-   Type: integer
-   Required: true
-   Description: The port of the proxy

Output type

None

/debugFiles/cancel
------------------

### POST

Summary

Cancel download the file

Notes

Cancel download the file

Input type

Output type

None

/debugFiles/delete/{nePk}
-------------------------

### POST

Summary

Delete the debug file on the appliance

Notes

Delete the debug file on the appliance

Input type

application/json

nePk

-   Description: nePk is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

deleteInfoClass

Description: The info of the deleted file

Required: true

Parameter type: POST Body

Data Type: application/json

group

-   Type: string
-   Description: The group of the file which need to delete

local\_filename

-   Type: string
-   Description: The name of the file which need to delete

Output type

None

Reference: debugFiles.json

\

VXOA syslog
===========

/logging/{neId}
---------------

### GET

Summary

Get logging settings form appliance or gmsdb

Notes

Returned data is an object, contains logging remote and config settings.

Input type

application/json

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: If true, get settings from gmsdb, otherwise get from
    appliance.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

remote

elements

-   Type: array
-   Required: true

config

members

min\_priority

-   Type: string
-   Required: true
-   Description: The value should be one of the none, emergence, alert,
    critical, error, warning, notice, info and debug

threshold\_size

-   Type: integer
-   Required: true
-   Description: The valid range is 1M-50M, inclusive

keep\_number

-   Type: integer
-   Required: true
-   Description: The valid range is 1-100, inclusive

auditlog

-   Type: string
-   Required: true
-   Description: The value should be one of the local0, local1, local2,
    local3, local4, local5, local6 and local7

flow

-   Type: string
-   Required: true
-   Description: The value should be one of the local0, local1, local2,
    local3, local4, local5, local6 and local7

system

-   Type: string
-   Required: true
-   Description: The value should be one of the local0, local1, local2,
    local3, local4, local5, local6 and local7

Reference: logging.json

\

VXOA software version
=====================

/appliancesSoftwareVersions/{nePK}?cached={cached}
--------------------------------------------------

### GET

Summary

Returns VXOA software versions information

Input type

None

nePK

-   Description: nePK is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

Reference: appliancesSoftwareVersions.json

\

VXOA WCCP configuration
=======================

/wccp/config/system/{neId}?cached={cached}
------------------------------------------

### GET

Summary

Get WCCP system settings for the appliance

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

mcast\_ttl

-   Type: integer

enable

-   Type: boolean

/wccp/config/group/{neId}?cached={cached}
-----------------------------------------

### GET

Summary

Get WCCP service groups settings for the appliance.

Notes

the returned data is an object in which each key is the group Id, each
value is an object for the service group settings.

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

51

password

-   Type: string
-   Description: WCCP service group password

mask\_src\_port

-   Type: integer
-   Description: WCCP service group mask source port

force\_l2\_return

-   Type: boolean
-   Description: WCCP service group force l2 return

hash\_dst\_ip

-   Type: boolean
-   Description: WCCP service group hash destination ip

self

-   Type: integer
-   Description: Integer value of service group Id

weight

-   Type: integer
-   Description: WCCP service group weight

hash\_src\_port

-   Type: boolean
-   Description: WCCP service group hash source port

assign\_method

-   Type: string
-   Description: Assignment Method

hash\_dst\_port

-   Type: boolean
-   Description: WCCP service group hash destination port

hash\_src\_ip

-   Type: boolean
-   Description: WCCP service group hash source ip

encap

-   Type: string
-   Description: WCCP service group forwarding method

protocol

-   Type: string
-   Description: WCCP service group protocol

assign\_detail

-   Type: string
-   Description: WCCP service group assignment detail

compatibility

-   Type: string
-   Description: WCCP service group compatibility mode. Valid values:
    ios, nexus

interface

-   Type: string
-   Description: WCCP service group interface.

mask\_dst\_ip

-   Type: integer
-   Description: WCCP service group mask destination ip

mask\_dst\_port

-   Type: integer
-   Description: WCCP service group mask destination port

mask\_src\_ip

-   Type: integer
-   Description: WCCP service group mask source ip

priority

-   Type: integer
-   Description: WCCP service group priority. Valid range: \[0, 255\].

router

-   Type: object
-   Description: WCCP service group router information.

52

password

-   Type: string
-   Description: WCCP service group password

mask\_src\_port

-   Type: integer
-   Description: WCCP service group mask source port

force\_l2\_return

-   Type: boolean
-   Description: WCCP service group force l2 return

hash\_dst\_ip

-   Type: boolean
-   Description: WCCP service group hash destination ip

self

-   Type: integer
-   Description: Integer value of service group Id

weight

-   Type: integer
-   Description: WCCP service group weight

hash\_src\_port

-   Type: boolean
-   Description: WCCP service group hash source port

assign\_method

-   Type: string
-   Description: Assignment Method

hash\_dst\_port

-   Type: boolean
-   Description: WCCP service group hash destination port

hash\_src\_ip

-   Type: boolean
-   Description: WCCP service group hash source ip

encap

-   Type: string
-   Description: WCCP service group forwarding method

protocol

-   Type: string
-   Description: WCCP service group protocol

assign\_detail

-   Type: string
-   Description: WCCP service group assignment detail

compatibility

-   Type: string
-   Description: WCCP service group compatibility mode. Valid values:
    ios, nexus

interface

-   Type: string
-   Description: WCCP service group interface.

mask\_dst\_ip

-   Type: integer
-   Description: WCCP service group mask destination ip

mask\_dst\_port

-   Type: integer
-   Description: WCCP service group mask destination port

mask\_src\_ip

-   Type: integer
-   Description: WCCP service group mask source ip

priority

-   Type: integer
-   Description: WCCP service group priority. Valid range: \[0, 255\].

router

-   Type: object
-   Description: WCCP service group router information.

/wccp/state/{neId}?cached={cached}
----------------------------------

### GET

Summary

Get WCCP state

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

system

debug\_global

-   Type: string
-   Required: true
-   Description: global debug information for WCCP

debug\_all

-   Type: string
-   Required: true
-   Description: all debug information for WCCP

is\_alive

-   Type: string
-   Required: true
-   Description: is alive information for WCCP

proto\_ver

-   Type: string
-   Required: true
-   Description: prototype version information for WCCP

group

51

debug

-   Type: string
-   Description: debug information for the WCCP service group

app\_id

-   Type: string

self

-   Type: integer
-   Description: integer value of the group Id

assignment

-   Type: string
-   Description: assignment information for the WCCP service group

num\_appliances

-   Type: integer
-   Description: number of appliances for the WCCP service group

num\_routers

-   Type: integer
-   Description: number of routers for the WCCP service group

oper

-   Type: string
-   Description: operational status for the WCCP service group

uptime

-   Type: integer
-   Description: uptime for the WCCP service group

52

debug

-   Type: string
-   Description: debug information for the WCCP service group

app\_id

-   Type: string

self

-   Type: integer
-   Description: integer value of the group Id

assignment

-   Type: string
-   Description: assignment information for the WCCP service group

num\_appliances

-   Type: integer
-   Description: number of appliances for the WCCP service group

num\_routers

-   Type: integer
-   Description: number of routers for the WCCP service group

oper

-   Type: string
-   Description: operational status for the WCCP service group

uptime

-   Type: integer
-   Description: uptime for the WCCP service group

Reference: wccp.json

\

Alarms
======

/alarm/appliance
----------------

### POST

Summary

Returns active, historical, or all alarms for appliances whose ids
provided in the request body

Input type

application/json

ApplianceAlarmGetPostBody

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array
-   Required: true
-   Description: Contains appliance IDs

view

-   Description: active=current alarms, closed=historical alarms,
    all=both active and historical alarms
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

severity

-   Description: Filters by alarm severity
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

orderBySeverity

-   Description: order by alarm severity
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

maxAlarms

-   Description: max Alarms
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

from

-   Description: Long(Signed 64 bits) value of milliseconds since EPOCH
    time indicating the starting time boundary of data time range
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

to

-   Description: Long(Signed 64 bits) value of milliseconds since EPOCH
    time indicating the ending time boundary of data time range
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/alarm/acknowledgement/appliance/{neId}
---------------------------------------

### POST

Summary

Acknowledges alarms on appliance whose sequence IDs are provided in the
request body

Input type

application/json

neId

-   Description: Appliance ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

AlarmAcknowledgementBody

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

actions

-   Type: array

acknowledge

-   Type: boolean
-   Required: true
-   Description: Ack (true), Un-Ack(false)

Output type

None

/alarm/clearance/appliance/{neId}
---------------------------------

### POST

Summary

Clears alarms whose sequence IDs are provided in the request body

Input type

application/json

neId

-   Description: Appliance ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

AlarmClearanceBody

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

actions

-   Type: array

Output type

None

/alarm/count/appliance
----------------------

### GET

Summary

Returns summary of active alarms for each appliance

Input type

None

Output type

None

/alarm/notification
-------------------

### GET

Summary

Returns alarm notification

Input type

None

Output type

application/json

enable

-   Type: boolean

### POST

Summary

Set alarm notification

Input type

application/json: undefined

Alarm Notification

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/alarm/count/appliance/{neId}
-----------------------------

### GET

Summary

Returns summary of active alarms for the appliance

Input type

None

neId

-   Description: Appliance ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

numOfCriticalAlarms

-   Type: integer
-   Required: true

numOfWarningAlarms

-   Type: integer
-   Required: true

numOfMinorAlarms

-   Type: integer
-   Required: true

numOfMajorAlarms

-   Type: integer
-   Required: true

/alarm/summary
--------------

### GET

Summary

Returns summary of active Orchestrator alarms as well as summary of
active alarms across all appliances

Input type

None

Output type

application/json

numOfCriticalAlarms

-   Type: integer
-   Required: true

numOfWarningAlarms

-   Type: integer
-   Required: true

numOfMinorAlarms

-   Type: integer
-   Required: true

numOfMajorAlarms

-   Type: integer
-   Required: true

/alarm/summary/{type}
---------------------

### GET

Summary

Returns summary of active Orchestrator alarms or summary of active
alarms across all appliances

Input type

None

type

-   Description: Alarm Type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

numOfCriticalAlarms

-   Type: integer
-   Required: true

numOfWarningAlarms

-   Type: integer
-   Required: true

numOfMinorAlarms

-   Type: integer
-   Required: true

numOfMajorAlarms

-   Type: integer
-   Required: true

/alarm/gms
----------

### GET

Summary

Returns active, closed/historical, or all Orchestrator alarms

Input type

None

view

-   Description: active=current alarms, closed=historical alarms,
    all=both active and historical alarms
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

severity

-   Description: Filters by alarm severity
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

from

-   Description: Long(Signed 64 bits) value of milliseconds since EPOCH
    time indicating the starting time boundary of data time range
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

to

-   Description: Long(Signed 64 bits) value of milliseconds since EPOCH
    time indicating the ending time boundary of data time range
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/alarm/acknowledgement/gms
--------------------------

### POST

Summary

Acknowledges alarms whose IDs are provided in the request body

Input type

application/json

GmsAlarmAcknowledgementBody

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array
-   Required: true
-   Description: Orchestrator alarm IDs

acknowledge

-   Type: boolean
-   Required: true
-   Description: Ack (true), Un-Ack(false)

Output type

None

/alarm/clearance/gms
--------------------

### POST

Summary

Clears alarms whose IDs are provided in the request body

Input type

application/json

GmsAlarmClearanceBody

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array
-   Required: true
-   Description: Orchestrator alarm IDs

Output type

None

/alarm/description2
-------------------

### GET

Summary

Get Orchestrator and appliance alarm descriptions

Input type

None

format

-   Description: format=csv, ask browser to download a file of
    Orchestrator alarm descriptions in .csv format.
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

default

-   Description: default=true, export alarm descriptions with default
    values, default=false, export alarm descriptions with customized
    values
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

typeId

-   Type: integer
-   Description: Alarm type id

name

-   Type: string
-   Description: Alarm name

severity

-   Type: string
-   Description: Alarm severity info

description

-   Type: string
-   Description: Alarm description

recommendedAction

-   Type: string
-   Description: recommended action

serviceAffecting

-   Type: boolean
-   Description: Is alarm service affecting

source

-   Type: string
-   Description: Module/system that generates alarm

systemType

-   Type: integer
-   Description: Identifies which system generated the alaram.0 =
    Appliance,100 = Orchestartor

sourceType

-   Type: integer
-   Description: Identifies the category of alarm. 1 = Tunnel,2 =
    Traffic Class, 3 = Equipment, 4 = Software, 5 = Threshold

alarmType

-   Type: integer
-   Description: Uniquely identifies the type of alarm within a
    sourceType

/alarm/customization/severity
-----------------------------

### GET

Summary

Get all customized alarm severity

Notes

API returns an object in which keys are alarmTypeIds and values are
Severities

Input type

None

Output type

application/json

65536

-   Type: string
-   Required: true
-   Description: \"Critical\"

### POST

Summary

Create customized alarm severities

Notes

API consumes an object in which keys are alarmTypeIds and values are
Severities

Input type

application/json

Request Body

Description: Request body is an object in which keys are alarmTypeIds
and values are Severities

Required: true

Parameter type: POST Body

Data Type: application/json

65536

-   Type: string
-   Required: true
-   Description: \"Critical\"

Output type

None

### PUT

Summary

Update customized alarm severities

Notes

API consumes an object in which keys are alarmTypeIds and values are
Severities

Input type

application/json

Request Body

Description: Request body is an object in which keys are alarmTypeIds
and values are Severities

Required: true

Parameter type: POST Body

Data Type: application/json

65536

-   Type: string
-   Required: true
-   Description: \"Critical\"

Output type

None

### DELETE

Summary

Delete all customized alarm severity

Notes

Delete all customized alarm severity

Input type

None

Output type

None

/alarm/customization/severity/{alarmTypeId}
-------------------------------------------

### GET

Summary

Get alarm severity config by alarmTypeId

Notes

API returns an object in which keys are alarmTypeIds and values are
Severities

Input type

None

alarmTypeId

-   Description: Alarm type id
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

65536

-   Type: string
-   Required: true
-   Description: \"Critical\"

### DELETE

Summary

Delete alarm severity config by alarmTypeId

Input type

None

alarmTypeId

-   Description: Alarm type id
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/alarm/delayEmail
-----------------

### GET

Summary

Get alarm email delay configuration

Input type

None

Output type

application/json

duration

-   Type: integer
-   Description: Alarm email delay duration in seconds.

### POST

Summary

Create alarm email delay configuration

Input type

application/json

Request Body

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

duration

-   Type: integer
-   Description: Alarm email delay duration in seconds.

Output type

None

### PUT

Summary

Update alarm email delay configuration

Input type

application/json

Request Body

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

duration

-   Type: integer
-   Description: Alarm email delay duration in seconds.

Output type

None

### DELETE

Summary

Delete alarm email delay configuration

Input type

None

Output type

None

/alarm/suppress
---------------

### GET

Summary

Get all disabled alarms configuration

Input type

None

Output type

application/json

alarmTypeIds

-   Type: array
-   Required: true
-   Description: List of alarm type ids.

applianceIds

-   Type: array
-   Required: true
-   Description: List of appliance ids.

### POST

Summary

Update disabled alarms configuration

Input type

application/json: undefined

Request Body

Description: \'action\' can be \'ENABLE\' or \'DISABLE\'. All the fields
are required.

Required: true

Parameter type: POST Body

Data Type: application/json

action

-   Type: string
-   Required: true
-   Description: \'ENABLE\' or \'DISABLE\' are possible values. Field
    can not be empty or null.

alarmTypeIds

-   Type: array
-   Required: true
-   Description: List of alarm type ids. Value can not be null.

applianceIds

-   Type: array
-   Required: true
-   Description: List of appliance ids. Value can not be null.

Output type

None

### DELETE

Summary

Delete disabled alarms configuration

Input type

None

Output type

None

Reference: alarm.json

\

VXOA Netflow configuration
==========================

/netFlow/{neId}
---------------

### GET

Summary

Get flow export data from appliance or gmsdb

Notes

Returned data is an object and contains the following data:
active\_timeout (NetFlow), ipfix\_tmplt\_rfrsh\_t (IPFIX), port1,
if\_wan\_tx, enable, ipaddr1, if\_lan\_rx, if\_lan\_tx, ipaddr2,
if\_wan\_rx and port2, collector\_type1, collector\_type2

Input type

application/json

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: If true, get data from gmsdb, otherwise get from
    appliance.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

active\_timeout

-   Type: integer
-   Required: true
-   Description: Specify NetFlow timeout (1..30) mins

ipfix\_tmplt\_rfrsh\_t

-   Type: integer
-   Required: true
-   Description: Specify IPFIX template refresh timeout (1..1440) mins

port1

-   Type: integer
-   Required: true
-   Description: Need description here

if\_wan\_tx

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

ipaddr1

-   Type: string
-   Required: true
-   Description: valid ip address

if\_lan\_rx

-   Type: boolean
-   Required: true
-   Description: Need description here

if\_lan\_tx

-   Type: boolean
-   Required: true
-   Description: Need description here

ipaddr2

-   Type: string
-   Required: true
-   Description: valid ip address

if\_wan\_rx

-   Type: boolean
-   Required: true
-   Description: Need description here

port2

-   Type: integer
-   Required: true
-   Description: Need description here

optional\_ies

app\_perf\_ies

-   Type: boolean
-   Description: app performance optional IES

zbf\_ies

-   Type: boolean
-   Description: zbf optional IES

collector\_type1

-   Type: string
-   Required: true
-   Description: NetFlow or IPFIX collector type

collector\_type2

-   Type: string
-   Required: true
-   Description: NetFlow or IPFIX collector type

Reference: netFlow.json

\

Orchestrator Backup
===================

/gms/backup
-----------

### GET

Summary

Returns orchestrator backup information

Notes

There is only one orchestrator backup schedule job. Include the
orchestrator configuration and orchestrator schedule information. Both
the configuration and schedule information can be empty.

Input type

None

Output type

application/json

protocol

-   Type: integer
-   Required: true
-   Description: The protocol of the remote server

hostname

-   Type: string
-   Required: true
-   Description: The hostname of the remote server

port

-   Type: integer
-   Required: true
-   Description: The port of the remote server

maxBackups

-   Type: integer
-   Required: true
-   Description: The number of the backup files in the directory

directory

-   Type: string
-   Required: true
-   Description: The directory of the remote server

username

-   Type: string
-   Required: true
-   Description: The username of the remote server

password

-   Type: string
-   Required: true
-   Description: The password of the remote server

/gms/backup/testConnection
--------------------------

### POST

Summary

Test the orchestrator backup remote server whether can connect.

Input type

application/json

gmsBackConfig

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

protocol

-   Type: integer
-   Required: true
-   Description: The protocol of the remote server

hostname

-   Type: string
-   Required: true
-   Description: The hostname of the remote server

port

-   Type: integer
-   Required: true
-   Description: The port of the remote server

maxBackups

-   Type: integer
-   Required: true
-   Description: The number of the backup files in the directory

directory

-   Type: string
-   Required: true
-   Description: The directory of the remote server

username

-   Type: string
-   Required: true
-   Description: The username of the remote server

password

-   Type: string
-   Required: true
-   Description: The password of the remote server

Output type

application/json

message

-   Type: string
-   Required: true
-   Description: Return schedule orchestrator backup successful message.

/gms/backup/config
------------------

### POST

Summary

add or update orchestrator backup config data

Input type

application/json

gmsBackConfig

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

protocol

-   Type: integer
-   Required: true
-   Description: The protocol of the remote server

hostname

-   Type: string
-   Required: true
-   Description: The hostname of the remote server

port

-   Type: integer
-   Required: true
-   Description: The port of the remote server

maxBackups

-   Type: integer
-   Required: true
-   Description: The number of the backup files in the directory

directory

-   Type: string
-   Required: true
-   Description: The directory of the remote server

username

-   Type: string
-   Required: true
-   Description: The username of the remote server

password

-   Type: string
-   Required: true
-   Description: The password of the remote server

Output type

application/json

/gms/backup/exportTemplate?mode={mode}&download={download}
----------------------------------------------------------

### GET

Summary

Create blueprint template

Input type

None

mode

-   Description: \'template\' or \'migration\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

download

-   Description: any value would be considered true
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: gmsBackup.json

\

Orchestrator generic configuration APIs
=======================================

/gmsConfig
----------

### GET

Summary

Returns all Orchestrator configs

Input type

None

Output type

None

### POST

Summary

Creates an Orchestrator config

Input type

application/json: undefined

GmsConfigCreation

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

resourceBase

-   Type: string
-   Required: true
-   Description: name of the main resource

resourceKey

-   Type: string
-   Description: name of the sub-resource. this is optional

configData

-   Type: object
-   Required: true
-   Description: could be any type of json object/array

Output type

None

/gmsConfig/{base}
-----------------

### GET

Summary

Returns specific Orchestrator config. But a specific Orchestrator config
could have multiple sub-resources, that\'s why it returns an array.
However, If \"resourceKey\" is provided, response will be an
object(sub-resource) instead of an array

Input type

None

base

-   Description: name resource
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

resourceKey

-   Description: if provided, response will be an object instead of an
    array
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

### DELETE

Summary

Deletes an Orchestrator config

Input type

None

base

-   Description: resource name
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

resourceKey

-   Description: if provided, it will only delete the sub-resource
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

### PUT

Summary

Updates a versioned Orchestrator config. Current resource version number
needs to be provided when updating

Input type

application/json: undefined

base

-   Description: resource name
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

resourceKey

-   Description: if provided, it will update the sub-resource
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

GmsConfigUpdate

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

configData

-   Type: object
-   Required: true
-   Description: could be any type of json object/array

version

-   Type: integer
-   Required: true
-   Description: version of the orchestrator config resource, gets
    incremented with updates

Output type

None

Reference: gmsConfig.json

\

VXOA threshold crossing alerts
==============================

/tca/{neId}
-----------

### GET

Summary

Get system threshold crossing alerts

Notes

Currently the system tca contains: oop-pre-poc, file-system-utilization,
utilization, lan-side-rx-throughput, optimized-flows, latency,
loss-post-fec, loss-pre-fec, oop-post-poc, wan-side-tx-throughput,
reduction and total-flows.

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: If true, get data from gmsdb, otherwise get from
    appliance.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

oop-pre-poc

allow\_falling

-   Type: boolean
-   Required: true
-   Description: Need description here

allow\_disable

-   Type: boolean
-   Required: true
-   Description: Need description here

UI

units

-   Type: string
-   Required: true
-   Description: Need description here

allow\_config

-   Type: boolean
-   Required: true
-   Description: Need description here

descr

-   Type: string
-   Required: true
-   Description: Need description here

inst\_bname

\<a number from 1 to 1000\>

descr

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: integer
-   Required: true
-   Description: The value should be a number and equal with its parent
    node key(a number from 1 to 1000)

name

-   Type: string
-   Required: true
-   Description: Need description here

allow\_rising

-   Type: boolean
-   Required: true
-   Description: Need description here

dft

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

threshold\_max

-   Type: integer
-   Required: true
-   Description: Need description here

threshold\_min

-   Type: integer
-   Required: true
-   Description: Need description here

file-system-utilization

allow\_falling

-   Type: boolean
-   Required: true
-   Description: Need description here

allow\_disable

-   Type: boolean
-   Required: true
-   Description: Need description here

UI

units

-   Type: string
-   Required: true
-   Description: Need description here

allow\_config

-   Type: boolean
-   Required: true
-   Description: Need description here

descr

-   Type: string
-   Required: true
-   Description: Need description here

inst\_bname

\<a number from 1 to 1000\>

descr

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: integer
-   Required: true
-   Description: The value should be a number and equal with its parent
    node key(a number from 1 to 1000)

name

-   Type: string
-   Required: true
-   Description: Need description here

allow\_rising

-   Type: boolean
-   Required: true
-   Description: Need description here

dft

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

threshold\_max

-   Type: integer
-   Required: true
-   Description: Need description here

threshold\_min

-   Type: integer
-   Required: true
-   Description: Need description here

utilization

allow\_falling

-   Type: boolean
-   Required: true
-   Description: Need description here

allow\_disable

-   Type: boolean
-   Required: true
-   Description: Need description here

UI

units

-   Type: string
-   Required: true
-   Description: Need description here

allow\_config

-   Type: boolean
-   Required: true
-   Description: Need description here

descr

-   Type: string
-   Required: true
-   Description: Need description here

inst\_bname

\<a number from 1 to 1000\>

descr

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: integer
-   Required: true
-   Description: The value should be a number and equal with its parent
    node key(a number from 1 to 1000)

name

-   Type: string
-   Required: true
-   Description: Need description here

allow\_rising

-   Type: boolean
-   Required: true
-   Description: Need description here

dft

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

threshold\_max

-   Type: integer
-   Required: true
-   Description: Need description here

threshold\_min

-   Type: integer
-   Required: true
-   Description: Need description here

lan-side-rx-throughput

allow\_falling

-   Type: boolean
-   Required: true
-   Description: Need description here

allow\_disable

-   Type: boolean
-   Required: true
-   Description: Need description here

UI

units

-   Type: string
-   Required: true
-   Description: Need description here

allow\_config

-   Type: boolean
-   Required: true
-   Description: Need description here

descr

-   Type: string
-   Required: true
-   Description: Need description here

inst\_bname

\<a number from 1 to 1000\>

descr

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: integer
-   Required: true
-   Description: The value should be a number and equal with its parent
    node key(a number from 1 to 1000)

name

-   Type: string
-   Required: true
-   Description: Need description here

allow\_rising

-   Type: boolean
-   Required: true
-   Description: Need description here

dft

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

threshold\_max

-   Type: integer
-   Required: true
-   Description: Need description here

threshold\_min

-   Type: integer
-   Required: true
-   Description: Need description here

optimized-flows

allow\_falling

-   Type: boolean
-   Required: true
-   Description: Need description here

allow\_disable

-   Type: boolean
-   Required: true
-   Description: Need description here

UI

units

-   Type: string
-   Required: true
-   Description: Need description here

allow\_config

-   Type: boolean
-   Required: true
-   Description: Need description here

descr

-   Type: string
-   Required: true
-   Description: Need description here

inst\_bname

\<a number from 1 to 1000\>

descr

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: integer
-   Required: true
-   Description: The value should be a number and equal with its parent
    node key(a number from 1 to 1000)

name

-   Type: string
-   Required: true
-   Description: Need description here

allow\_rising

-   Type: boolean
-   Required: true
-   Description: Need description here

dft

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

threshold\_max

-   Type: integer
-   Required: true
-   Description: Need description here

threshold\_min

-   Type: integer
-   Required: true
-   Description: Need description here

latency

allow\_falling

-   Type: boolean
-   Required: true
-   Description: Need description here

allow\_disable

-   Type: boolean
-   Required: true
-   Description: Need description here

UI

units

-   Type: string
-   Required: true
-   Description: Need description here

allow\_config

-   Type: boolean
-   Required: true
-   Description: Need description here

descr

-   Type: string
-   Required: true
-   Description: Need description here

inst\_bname

\<a number from 1 to 1000\>

descr

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: integer
-   Required: true
-   Description: The value should be a number and equal with its parent
    node key(a number from 1 to 1000)

name

-   Type: string
-   Required: true
-   Description: Need description here

allow\_rising

-   Type: boolean
-   Required: true
-   Description: Need description here

dft

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

threshold\_max

-   Type: integer
-   Required: true
-   Description: Need description here

threshold\_min

-   Type: integer
-   Required: true
-   Description: Need description here

loss-post-fec

allow\_falling

-   Type: boolean
-   Required: true
-   Description: Need description here

allow\_disable

-   Type: boolean
-   Required: true
-   Description: Need description here

UI

units

-   Type: string
-   Required: true
-   Description: Need description here

allow\_config

-   Type: boolean
-   Required: true
-   Description: Need description here

descr

-   Type: string
-   Required: true
-   Description: Need description here

inst\_bname

\<a number from 1 to 1000\>

descr

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: integer
-   Required: true
-   Description: The value should be a number and equal with its parent
    node key(a number from 1 to 1000)

name

-   Type: string
-   Required: true
-   Description: Need description here

allow\_rising

-   Type: boolean
-   Required: true
-   Description: Need description here

dft

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

threshold\_max

-   Type: integer
-   Required: true
-   Description: Need description here

threshold\_min

-   Type: integer
-   Required: true
-   Description: Need description here

loss-pre-fec

allow\_falling

-   Type: boolean
-   Required: true
-   Description: Need description here

allow\_disable

-   Type: boolean
-   Required: true
-   Description: Need description here

UI

units

-   Type: string
-   Required: true
-   Description: Need description here

allow\_config

-   Type: boolean
-   Required: true
-   Description: Need description here

descr

-   Type: string
-   Required: true
-   Description: Need description here

inst\_bname

\<a number from 1 to 1000\>

descr

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: integer
-   Required: true
-   Description: The value should be a number and equal with its parent
    node key(a number from 1 to 1000)

name

-   Type: string
-   Required: true
-   Description: Need description here

allow\_rising

-   Type: boolean
-   Required: true
-   Description: Need description here

dft

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

threshold\_max

-   Type: integer
-   Required: true
-   Description: Need description here

threshold\_min

-   Type: integer
-   Required: true
-   Description: Need description here

oop-post-poc

allow\_falling

-   Type: boolean
-   Required: true
-   Description: Need description here

allow\_disable

-   Type: boolean
-   Required: true
-   Description: Need description here

UI

units

-   Type: string
-   Required: true
-   Description: Need description here

allow\_config

-   Type: boolean
-   Required: true
-   Description: Need description here

descr

-   Type: string
-   Required: true
-   Description: Need description here

inst\_bname

\<a number from 1 to 1000\>

descr

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: integer
-   Required: true
-   Description: The value should be a number and equal with its parent
    node key(a number from 1 to 1000)

name

-   Type: string
-   Required: true
-   Description: Need description here

allow\_rising

-   Type: boolean
-   Required: true
-   Description: Need description here

dft

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

threshold\_max

-   Type: integer
-   Required: true
-   Description: Need description here

threshold\_min

-   Type: integer
-   Required: true
-   Description: Need description here

wan-side-tx-throughput

allow\_falling

-   Type: boolean
-   Required: true
-   Description: Need description here

allow\_disable

-   Type: boolean
-   Required: true
-   Description: Need description here

UI

units

-   Type: string
-   Required: true
-   Description: Need description here

allow\_config

-   Type: boolean
-   Required: true
-   Description: Need description here

descr

-   Type: string
-   Required: true
-   Description: Need description here

inst\_bname

\<a number from 1 to 1000\>

descr

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: integer
-   Required: true
-   Description: The value should be a number and equal with its parent
    node key(a number from 1 to 1000)

name

-   Type: string
-   Required: true
-   Description: Need description here

allow\_rising

-   Type: boolean
-   Required: true
-   Description: Need description here

dft

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

threshold\_max

-   Type: integer
-   Required: true
-   Description: Need description here

threshold\_min

-   Type: integer
-   Required: true
-   Description: Need description here

reduction

allow\_falling

-   Type: boolean
-   Required: true
-   Description: Need description here

allow\_disable

-   Type: boolean
-   Required: true
-   Description: Need description here

UI

units

-   Type: string
-   Required: true
-   Description: Need description here

allow\_config

-   Type: boolean
-   Required: true
-   Description: Need description here

descr

-   Type: string
-   Required: true
-   Description: Need description here

inst\_bname

\<a number from 1 to 1000\>

descr

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: integer
-   Required: true
-   Description: The value should be a number and equal with its parent
    node key(a number from 1 to 1000)

name

-   Type: string
-   Required: true
-   Description: Need description here

allow\_rising

-   Type: boolean
-   Required: true
-   Description: Need description here

dft

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

threshold\_max

-   Type: integer
-   Required: true
-   Description: Need description here

threshold\_min

-   Type: integer
-   Required: true
-   Description: Need description here

total-flows

allow\_falling

-   Type: boolean
-   Required: true
-   Description: Need description here

allow\_disable

-   Type: boolean
-   Required: true
-   Description: Need description here

UI

units

-   Type: string
-   Required: true
-   Description: Need description here

allow\_config

-   Type: boolean
-   Required: true
-   Description: Need description here

descr

-   Type: string
-   Required: true
-   Description: Need description here

inst\_bname

\<a number from 1 to 1000\>

descr

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: integer
-   Required: true
-   Description: The value should be a number and equal with its parent
    node key(a number from 1 to 1000)

name

-   Type: string
-   Required: true
-   Description: Need description here

allow\_rising

-   Type: boolean
-   Required: true
-   Description: Need description here

dft

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

threshold\_max

-   Type: integer
-   Required: true
-   Description: Need description here

threshold\_min

-   Type: integer
-   Required: true
-   Description: Need description here

/tca/tunnel/{neId}
------------------

### GET

Summary

Get tunnel threshold crossing alerts

Notes

Currently the tunnel tca contains: reduction, oop-post-poc,
loss-pre-fec, loss-post-fec, latency, utilization and oop-pre-poc.

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: If true, get data from gmsdb, otherwise get from
    appliance.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

reduction

allow\_falling

-   Type: boolean
-   Required: true
-   Description: Need description here

allow\_disable

-   Type: boolean
-   Required: true
-   Description: Need description here

UI

units

-   Type: string
-   Required: true
-   Description: Need description here

allow\_config

-   Type: boolean
-   Required: true
-   Description: Need description here

descr

-   Type: string
-   Required: true
-   Description: Need description here

inst\_bname

\<a number from 1 to 1000\>

descr

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: integer
-   Required: true
-   Description: The value should be a number and equal with its parent
    node key(a number from 1 to 1000)

name

-   Type: string
-   Required: true
-   Description: Need description here

allow\_rising

-   Type: boolean
-   Required: true
-   Description: Need description here

dft

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

threshold\_max

-   Type: integer
-   Required: true
-   Description: Need description here

threshold\_min

-   Type: integer
-   Required: true
-   Description: Need description here

wc

\<tunnel name\>

self

-   Type: string
-   Required: true
-   Description: The value should be the same as its parent node tunnel
    name.

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

oop-post-poc

allow\_falling

-   Type: boolean
-   Required: true
-   Description: Need description here

allow\_disable

-   Type: boolean
-   Required: true
-   Description: Need description here

UI

units

-   Type: string
-   Required: true
-   Description: Need description here

allow\_config

-   Type: boolean
-   Required: true
-   Description: Need description here

descr

-   Type: string
-   Required: true
-   Description: Need description here

inst\_bname

\<a number from 1 to 1000\>

descr

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: integer
-   Required: true
-   Description: The value should be a number and equal with its parent
    node key(a number from 1 to 1000)

name

-   Type: string
-   Required: true
-   Description: Need description here

allow\_rising

-   Type: boolean
-   Required: true
-   Description: Need description here

dft

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

threshold\_max

-   Type: integer
-   Required: true
-   Description: Need description here

threshold\_min

-   Type: integer
-   Required: true
-   Description: Need description here

wc

\<tunnel name\>

self

-   Type: string
-   Required: true
-   Description: The value should be the same as its parent node tunnel
    name.

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

loss-pre-fec

allow\_falling

-   Type: boolean
-   Required: true
-   Description: Need description here

allow\_disable

-   Type: boolean
-   Required: true
-   Description: Need description here

UI

units

-   Type: string
-   Required: true
-   Description: Need description here

allow\_config

-   Type: boolean
-   Required: true
-   Description: Need description here

descr

-   Type: string
-   Required: true
-   Description: Need description here

inst\_bname

\<a number from 1 to 1000\>

descr

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: integer
-   Required: true
-   Description: The value should be a number and equal with its parent
    node key(a number from 1 to 1000)

name

-   Type: string
-   Required: true
-   Description: Need description here

allow\_rising

-   Type: boolean
-   Required: true
-   Description: Need description here

dft

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

threshold\_max

-   Type: integer
-   Required: true
-   Description: Need description here

threshold\_min

-   Type: integer
-   Required: true
-   Description: Need description here

wc

\<tunnel name\>

self

-   Type: string
-   Required: true
-   Description: The value should be the same as its parent node tunnel
    name.

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

loss-post-fec

allow\_falling

-   Type: boolean
-   Required: true
-   Description: Need description here

allow\_disable

-   Type: boolean
-   Required: true
-   Description: Need description here

UI

units

-   Type: string
-   Required: true
-   Description: Need description here

allow\_config

-   Type: boolean
-   Required: true
-   Description: Need description here

descr

-   Type: string
-   Required: true
-   Description: Need description here

inst\_bname

\<a number from 1 to 1000\>

descr

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: integer
-   Required: true
-   Description: The value should be a number and equal with its parent
    node key(a number from 1 to 1000)

name

-   Type: string
-   Required: true
-   Description: Need description here

allow\_rising

-   Type: boolean
-   Required: true
-   Description: Need description here

dft

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

threshold\_max

-   Type: integer
-   Required: true
-   Description: Need description here

threshold\_min

-   Type: integer
-   Required: true
-   Description: Need description here

wc

\<tunnel name\>

self

-   Type: string
-   Required: true
-   Description: The value should be the same as its parent node tunnel
    name.

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

latency

allow\_falling

-   Type: boolean
-   Required: true
-   Description: Need description here

allow\_disable

-   Type: boolean
-   Required: true
-   Description: Need description here

UI

units

-   Type: string
-   Required: true
-   Description: Need description here

allow\_config

-   Type: boolean
-   Required: true
-   Description: Need description here

descr

-   Type: string
-   Required: true
-   Description: Need description here

inst\_bname

\<a number from 1 to 1000\>

descr

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: integer
-   Required: true
-   Description: The value should be a number and equal with its parent
    node key(a number from 1 to 1000)

name

-   Type: string
-   Required: true
-   Description: Need description here

allow\_rising

-   Type: boolean
-   Required: true
-   Description: Need description here

dft

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

threshold\_max

-   Type: integer
-   Required: true
-   Description: Need description here

threshold\_min

-   Type: integer
-   Required: true
-   Description: Need description here

wc

\<tunnel name\>

self

-   Type: string
-   Required: true
-   Description: The value should be the same as its parent node tunnel
    name.

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

utilization

allow\_falling

-   Type: boolean
-   Required: true
-   Description: Need description here

allow\_disable

-   Type: boolean
-   Required: true
-   Description: Need description here

UI

units

-   Type: string
-   Required: true
-   Description: Need description here

allow\_config

-   Type: boolean
-   Required: true
-   Description: Need description here

descr

-   Type: string
-   Required: true
-   Description: Need description here

inst\_bname

\<a number from 1 to 1000\>

descr

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: integer
-   Required: true
-   Description: The value should be a number and equal with its parent
    node key(a number from 1 to 1000)

name

-   Type: string
-   Required: true
-   Description: Need description here

allow\_rising

-   Type: boolean
-   Required: true
-   Description: Need description here

dft

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

threshold\_max

-   Type: integer
-   Required: true
-   Description: Need description here

threshold\_min

-   Type: integer
-   Required: true
-   Description: Need description here

wc

\<tunnel name\>

self

-   Type: string
-   Required: true
-   Description: The value should be the same as its parent node tunnel
    name.

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

oop-pre-poc

allow\_falling

-   Type: boolean
-   Required: true
-   Description: Need description here

allow\_disable

-   Type: boolean
-   Required: true
-   Description: Need description here

UI

units

-   Type: string
-   Required: true
-   Description: Need description here

allow\_config

-   Type: boolean
-   Required: true
-   Description: Need description here

descr

-   Type: string
-   Required: true
-   Description: Need description here

inst\_bname

\<a number from 1 to 1000\>

descr

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: integer
-   Required: true
-   Description: The value should be a number and equal with its parent
    node key(a number from 1 to 1000)

name

-   Type: string
-   Required: true
-   Description: Need description here

allow\_rising

-   Type: boolean
-   Required: true
-   Description: Need description here

dft

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

threshold\_max

-   Type: integer
-   Required: true
-   Description: Need description here

threshold\_min

-   Type: integer
-   Required: true
-   Description: Need description here

wc

\<tunnel name\>

self

-   Type: string
-   Required: true
-   Description: The value should be the same as its parent node tunnel
    name.

falling

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

enable

-   Type: boolean
-   Required: true
-   Description: Need description here

rising

raiseVal

-   Type: integer
-   Required: true
-   Description: Need description here

clearVal

-   Type: integer
-   Required: true
-   Description: Need description here

sustain

-   Type: integer
-   Required: true
-   Description: Need description here

is\_set

-   Type: boolean
-   Required: true
-   Description: Need description here

Reference: tca.json

\

VXOA backup and restore
=======================

/appliance/backup
-----------------

### POST

Summary

Backup appliance configuration to Orchestrator database

Notes

The response of this operation is a client key which can be polled to
get progress of the requested operation. To poll for status, perform GET
/action/status?key=

Input type

application/json

operation

Description: List of appliance primary keys and comments.

Required: undefined

Parameter type: POST Body

Data Type: application/json

neList

-   Type: array
-   Description: List of appliance primary keys

comment

-   Type: string
-   Required: true

Output type

application/json

clientKey

-   Type: string
-   Required: true
-   Description: a client key which can be polled to get progress of the
    requested operation

/appliance/backup/{neId}
------------------------

### GET

Summary

Get backup history.

Input type

None

neId

-   Description: Primary key of the appliance
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/appliance/backup/{backupFilePk}
--------------------------------

### DELETE

Summary

Delete one specified backup record from the database table.

Notes

The backup record must exist in Orchestrator.

Input type

None

backupFilePk

-   Description: Primary key of the backup record.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: number

Output type

None

/appliance/restore/{nePk}
-------------------------

### POST

Summary

Restore the appliance from the specified back.

Notes

Backup file primary key can be obtained using GET /appliance/backup/.
The response of this operation is a client key which can be polled to
get progress of the requested operation. To poll for status, perform GET
/backgroundTask/status/

Input type

application/json: undefined

operation

Description: Backup file

Required: true

Parameter type: POST Body

Data Type: application/json

backupFilePk

-   Type: integer
-   Required: true
-   Description: The backup file\'s id.

nePk

-   Description: Appliance primary key
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

clientKey

-   Type: string
-   Required: true
-   Description: a client key which can be polled to get progress of the
    requested operation

Reference: applianceBackup.json

\

VXOA interfaces
===============

/interfaceState/{neId}
----------------------

### GET

Summary

Get node configuration data from Orchestrator database or from the
specified appliance.

Input type

None

cached

-   Description: True means to get data from Orchestrator database, and
    false means to get data from appliance.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

neId

-   Description: The node Id of the appliance.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

sysConfig

mode

-   Type: string
-   Required: true
-   Description: The appliance\'s deployment mode.

submode

-   Type: string
-   Required: true
-   Description: The appliance\'s deployment submode.

bonding

-   Type: boolean
-   Required: true
-   Description: Need description here.

scalars

-   Type: object
-   Required: true
-   Description: Need description here.

ifInfo

-   Type: array
-   Required: true
-   Description: The network interface configuration information,
    including 7 objects, which are separately for mgmt0, mgmt1, wan0,
    wan1, lan0, lan1m and bvi0, because each object has too many
    properties and the array of a fixed length can not be shown
    correctly in this version of schema so this array is not expanded.

macIfs

-   Type: array
-   Required: true
-   Description: MACs\' configuration information of this appliance,
    including six strings which are wan0, wan1, lan0, lan1, mgmt0 and
    mgmt1.

availMACs

-   Type: array
-   Required: true
-   Description: Available MACs\' addresses.

Reference: interfaceState.json

\

VXOA subnet sharing
===================

/subnets/configured/{neId}
--------------------------

### POST

Summary

Configure an appliance\'s subnets.

Notes

This api is used only for the older appliance whose version is less than
8.1.7.0.\
\
The subnets list in the request body will be compared to the list of
current subnets configured on the appliance. Any subnets which are in
posted list but not on the appliance are added. Similarly, missing
subnets are removed. Matching subnets are updated.

Input type

application/json: undefined

neId

-   Description: The node Id of the appliance.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

operation

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

ipv4

\<SubnetMask\>

local

-   Type: boolean
-   Description: Whether this subnet is local.

advert

-   Type: boolean
-   Description: Whether it advertises to peers.

metric

-   Type: integer
-   Description: The metric value.

comment

-   Type: string
-   Description: Some comments.

Output type

None

/subnets/{getCachedData}/{neId}
-------------------------------

### GET

Summary

Get an appliance\'s subnets information.

Input type

None

getCachedData

-   Description: True means to get the information from Orchestrator
    database, while false means to get from the appliance .
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

neId

-   Description: The node id of the appliance.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

subnet

-   Description: The subnet to filter the routes received from the
    appliance(s). Supports IPv4 and IPv6 address types.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

subnets

count

-   Type: integer
-   Required: true
-   Description: The number of the subnets.

max

-   Type: integer
-   Required: true
-   Description: The max number of the subnets this appliance can have.

entries

-   Type: array
-   Required: true
-   Description: Each element of this array is one object describing a
    subnet\'s detail information, and the minimum number of the elements
    is 0

peers

-   Type: array
-   Required: true
-   Description: Each element of this array is an object describing one
    peer\'s detail information, and the minimum number of the elements
    is 0

moduleInfo

my system id

-   Type: string
-   Required: true
-   Description: My system id.

last peer index

-   Type: string
-   Required: true
-   Description: Need description here.

local change

-   Type: string
-   Required: true
-   Description: Need description here.

last time

-   Type: string
-   Required: true
-   Description: Need description here.

last change

-   Type: string
-   Required: true
-   Description: Need description here.

time since change

-   Type: string
-   Required: true
-   Description: The seconds that have passed since last change.

last tx trans

-   Type: string
-   Required: true
-   Description: Need description here.

last tx rec count

-   Type: string
-   Required: true
-   Description: Need description here.

last tx msg count

-   Type: string
-   Required: true
-   Description: Need description here.

rx invalid msg

-   Type: string
-   Required: true
-   Description: Need description here.

rx unknown msg

-   Type: string
-   Required: true
-   Description: Need description here.

/subnets/forDiscovered/{discoveredId}
-------------------------------------

### GET

Summary

Get a discovered appliance\'s subnets information.

Input type

None

discoveredId

-   Description: The appliance\'s discovered ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

subnets

count

-   Type: integer
-   Required: true
-   Description: The number of the subnets.

max

-   Type: integer
-   Required: true
-   Description: The max number of the subnets this appliance can have.

entries

-   Type: array
-   Required: true
-   Description: Each element of this array is one object describing a
    subnet\'s detail information, and the minimum number of the elements
    is 0

peers

-   Type: array
-   Required: true
-   Description: Each element of this array is an object describing one
    peer\'s detail information, and the minimum number of the elements
    is 0

moduleInfo

my system id

-   Type: string
-   Required: true
-   Description: My system id.

last peer index

-   Type: string
-   Required: true
-   Description: Need description here.

local change

-   Type: string
-   Required: true
-   Description: Need description here.

last time

-   Type: string
-   Required: true
-   Description: Need description here.

last change

-   Type: string
-   Required: true
-   Description: Need description here.

time since change

-   Type: string
-   Required: true
-   Description: The seconds that have passed since last change.

last tx trans

-   Type: string
-   Required: true
-   Description: Need description here.

last tx rec count

-   Type: string
-   Required: true
-   Description: Need description here.

last tx msg count

-   Type: string
-   Required: true
-   Description: Need description here.

rx invalid msg

-   Type: string
-   Required: true
-   Description: Need description here.

rx unknown msg

-   Type: string
-   Required: true
-   Description: Need description here.

/subnets/setSubnetSharingOptions/{neId}
---------------------------------------

### POST

Summary

Configure appliance\'s subnet sharing options

Notes

Configure an appliance\'s subnet sharing options (enable/disable use
shared subnet information and automatically include local subnets.

Input type

application/json: undefined

neId

-   Description: The node Id of the appliance.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

SubnetsSystemJson

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

auto\_subnet

-   Type: AutoSubnet
-   Description: Subnet settings

Output type

None

Reference: subnets.json

\

VXOA SaaS optimization
======================

/cloudApplications/config/{neId}?cached={cached}
------------------------------------------------

### GET

Summary

Get SaaS Optimization configuration information for appliance

Notes

This api provides the following information: SaaS Optimization flag
(enabled/disabled), RTT Calculation Interval, RTT Ping Interface, and a
list of Cloud Applications downloaded from portal

Input type

None

neId

-   Description: Unique key assigned by Orchestrator to each appliance,
    for eg: \'0.NE\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

num\_pings

-   Type: number
-   Description: Number of ping requests per ip address when averaging
    out RTT values

application

1

domain

-   Type: object
-   Description: Collection of domain names associated with this cloud
    app. Indexed by domain name

self

-   Type: string
-   Description: SaaS Id value

ssl\_enabled

-   Type: boolean
-   Description: No longer used, default to false

enabled

-   Type: boolean
-   Description: Field to indicate if Advertise flag is turned on for
    this Cloud App

app\_name

-   Type: string

ports

-   Type: string

client\_enabled

-   Type: boolean
-   Description: Field to indicate if Optimization flag is turned on for
    this Cloud App

rtt\_threshold

-   Type: number
-   Description: If Advertise/Gateway enable is turned on, RTTs above
    this threshold will be advertised

user\_modified

-   Type: boolean

rtt\_interval

-   Type: number
-   Description: How often to run RTT re-calculation (in seconds)

ping\_src\_intf

-   Type: string
-   Description: Ping to the specified interface. Format for labels:
    side/interfaceId. E.g. lan/7

enable

-   Type: boolean
-   Description: Flag to enable/disable SaaS Optimization

user\_modifed

-   Type: boolean
-   Description: Flag to tell if configuration has been modified by user
    (used internally)

/cloudApplications/monitor/{neId}?cached={cached}
-------------------------------------------------

### GET

Summary

Get reachability/RTT values for SaaS App subnets enabled for
advertisement

Input type

None

neId

-   Description: Unique key assigned by Orchestrator to each appliance,
    for eg: \'0.NE\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

subnets

-   Type: array

domains

-   Type: array

Reference: saasOptimization.json

\

VXOA VRRP configuration
=======================

/vrrp/{neId}?cached={cached}
----------------------------

### GET

Summary

Get a list of VRRP instances configured on the appliance

Input type

None

neId

-   Description: Unique key assigned by Orchestrator to each appliance,
    for eg: \'0.NE\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

Reference: vrrp.json

\

Silver Peak Portal related apis
===============================

/spPortal/connectivity
----------------------

### GET

Summary

Get Orchestrator to Silver Peak Portal connectivity status

Notes

This API returns enums for portal connectivity status. 0: Unable to
connect 1: Connected 2: Connecting

Input type

None

Output type

application/json

portal

-   Type: integer

/spPortal/internetDb/serviceIdToSaasId
--------------------------------------

### GET

Summary

Get service id to service details mapping

Notes

This API returns collection of service id to service details
\[serviceId, saasId, country, countryCode, org, saasAppName,
displayName, priority\] mapping. If no filters are specified this
returns top 100 results only coz the data may be too large. You can
specify filters to query the dataset.

Input type

None

matchAny

-   Description: Wildcard match on country, org, saasAppName
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

org

-   Description: Wildcard match on org
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

serviceIds

-   Description: Only return these serviceIds. Separate multiple
    serviceIds with a comma \',\'
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

top

-   Description: Return top x matches
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

serviceId

-   Type: array
-   Description: Array representing service details \[serviceId, saasId,
    country, countryCode, org, saasAppName, displayName, priority\]

/spPortal/internetDb/serviceIdToSaasId/countries
------------------------------------------------

### GET

Summary

Get a list of unique countries in the internet services database

Notes

This API returns collection of service id to country details
\[serviceId, saasId, country, countryCode, org, saasAppName,
displayName, priority\] mapping.

Input type

None

Output type

application/json

country

-   Type: array
-   Description: Array representing service details \[serviceId, saasId,
    country, countryCode, org, saasAppName, displayName, priority\]

/spPortal/internetDb/serviceIdToSaasId/saasApps
-----------------------------------------------

### GET

Summary

Get a list of unique saas applications in the internet services database

Notes

This API returns a list of saas applications

Input type

None

Output type

None

/spPortal/internetDb/serviceIdToSaasId/count
--------------------------------------------

### GET

Summary

Get count of internet services on this appliance

Input type

None

Output type

application/json

count

-   Type: number

/spPortal/internetDb/geoLocateIp/{ip}
-------------------------------------

### GET

Summary

Get geolocation information for a single ip from portal

Notes

IP address should be provided in 32-bit integer format

Input type

None

ip

-   Description: IP Address to be geo located in 32-bit integer format
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

\<ip\>

ip

-   Type: integer
-   Description: IP address for which geo location is calculated

rangeIpStart

-   Type: integer
-   Description: Start IP of subnet that the geolocated IP falls under

rangeIpEnd

-   Type: integer
-   Description: End IP of subnet that the geolocated IP falls under

isp

-   Type: string

org

-   Type: string

country

-   Type: string

countryCode

-   Type: string

city

-   Type: string

postalCode

-   Type: integer

latitude

-   Type: number

longitude

-   Type: number

/spPortal/internetDb/geoLocateIp
--------------------------------

### POST

Summary

Get geolocation information for multiple ip\'s from portal

Notes

Returns a collection of GeoLocationObjects indexed by ip address value.
IP addresses should be provided as array of 32-bit integers

Input type

application/json

GeoLocationPostBody

-   Description: JSON object containing array of ip\'s to be geo located
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

application/json

\<ip\>

ip

-   Type: integer
-   Description: IP address for which geo location is calculated

rangeIpStart

-   Type: integer
-   Description: Start IP of subnet that the geolocated IP falls under

rangeIpEnd

-   Type: integer
-   Description: End IP of subnet that the geolocated IP falls under

isp

-   Type: string

org

-   Type: string

country

-   Type: string

countryCode

-   Type: string

city

-   Type: string

postalCode

-   Type: integer

latitude

-   Type: number

longitude

-   Type: number

/spPortal/internetDb/ipIntelligence/info
----------------------------------------

### GET

Summary

Get the last update time for application definition data for Address Map
from portal

Notes

Returns a timestamp for application definition data. It is used as
version number.

Input type

None

Output type

application/json

lastUpdateTime

-   Type: string

/spPortal/internetDb/ipIntelligence
-----------------------------------

### GET

Summary

Get application definition data for Address Map from portal

Notes

Returns an array of IP intelligence data which starts at start id with a
total limit. Start id starts from 0. Maximum limit is 10000.

Input type

None

start

-   Description: start id, start from 0
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

limit

-   Description: limit number, maximum is 10000
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/spPortal/internetDb/ipIntelligence/total
-----------------------------------------

### GET

Summary

Get count for application definition data for Address Map from portal

Input type

None

Output type

None

/spPortal/internetDb/ipIntelligence/search
------------------------------------------

### GET

Summary

Search application definition data for Address Map from portal

Notes

If provides a valid IPv4 address in 32-bit integer format, it will
search through ip. Otherwise it will search by match filter\[app name,
description, country, organization\]. Returns an array of IP
intelligence data with top 5000 results only, coz the data may be too
large.

Input type

None

ip

-   Description: IPv4 address in 32-bit integer format
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

filter

-   Description: String filter
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/spPortal/portProtocolClassification
------------------------------------

### GET

Summary

Get application definition for IP Protocol, TCP Port, UDP Port from
portal

Notes

Returns a collection of application definition

Input type

None

Output type

application/json

applicationTags

-   Type: object

portProtocolWithPriority

portNumber

-   Type: array

/spPortal/portProtocolClassification/info
-----------------------------------------

### GET

Summary

Get the hash code for application definition data for IP Protocol, TCP
Port, UDP Port from portal

Notes

Returns a hash code for application definition data. It is used as
version number.

Input type

None

Output type

application/json

hashVal

-   Type: string

/spPortal/dnsClassification
---------------------------

### GET

Summary

Get application definition for Domain Name from portal

Notes

Returns an array of application definition

Input type

None

Output type

None

/spPortal/dnsClassification/info
--------------------------------

### GET

Summary

Get the hash code for application definition data for Domain Name from
portal

Notes

Returns a hash code for application definition data. It is used as
version number.

Input type

None

Output type

application/json

hashVal

-   Type: string

/spPortal/meterFlowClassification
---------------------------------

### GET

Summary

Get application definition for Meter Flow from portal

Notes

Returns a collection of application definition

Input type

None

Output type

application/json

flowType

-   Type: array

/spPortal/meterFlowClassification/info
--------------------------------------

### GET

Summary

Get the hash code for application definition data for Meter Flow from
portal

Notes

Returns a hash code for application definition data. It is used as
version number.

Input type

None

Output type

application/json

hashVal

-   Type: string

/spPortal/compoundClassification
--------------------------------

### GET

Summary

Get application definition for Compound data from portal

Notes

Returns a collection of application definition

Input type

None

Output type

application/json

id\#

id

-   Type: number
-   Required: true

protocol

-   Type: string
-   Required: true
-   Description: protocol

src\_ip

-   Type: string
-   Required: true
-   Description: src ip(IPv4)

dst\_ip

-   Type: string
-   Required: true
-   Description: destination ip(IPv4)

either\_ip

-   Type: string
-   Required: true
-   Description: either ip(IPv4)

src\_service

-   Type: string
-   Required: true
-   Description: src service (saas app name or organization)

dst\_service

-   Type: string
-   Required: true
-   Description: destination service (saas app name or organization)

either\_service

-   Type: string
-   Required: true
-   Description: either service (saas app name or organization)

src\_dns

-   Type: string
-   Required: true
-   Description: src dns

dst\_dns

-   Type: string
-   Required: true
-   Description: destination dns

either\_dns

-   Type: string
-   Required: true
-   Description: either dns

src\_geo

-   Type: string
-   Required: true
-   Description: src geo location

dst\_geo

-   Type: string
-   Required: true
-   Description: destination geo location

either\_geo

-   Type: string
-   Required: true
-   Description: either geo location

src\_port

-   Type: string
-   Required: true
-   Description: src port number or range

dst\_port

-   Type: string
-   Required: true
-   Description: destination port number or range

either\_port

-   Type: string
-   Required: true
-   Description: either src or destination port number or range

vlan

-   Type: string
-   Required: true
-   Description: interface

dscp

-   Type: string
-   Required: true
-   Description: DSCP

name

-   Type: string
-   Required: true
-   Description: application name

description

-   Type: string
-   Required: true

confidence

-   Type: number
-   Required: true
-   Description: minimum 0, maximum 100

disabled

-   Type: boolean
-   Required: true

/spPortal/compoundClassification/info
-------------------------------------

### GET

Summary

Get the hash code for application definition data for Compound data from
portal

Notes

Returns a hash code for application definition data. It is used as
version number.

Input type

None

Output type

application/json

hashVal

-   Type: string

/spPortal/saasClassification
----------------------------

### GET

Summary

Get application definition for SaaS data from portal

Notes

Returns a collection of application definition

Input type

None

Output type

application/json

id\#

domain

-   Type: object
-   Description: Collection of domain names associated with this cloud
    app. Indexed by domain name

self

-   Type: string
-   Description: SaaS Id value

ssl\_enabled

-   Type: boolean
-   Description: No longer used, default to false

enabled

-   Type: boolean
-   Description: Field to indicate if Advertise flag is turned on for
    this Cloud App

app\_name

-   Type: string

ports

-   Type: string

client\_enabled

-   Type: boolean
-   Description: Field to indicate if Optimization flag is turned on for
    this Cloud App

rtt\_threshold

-   Type: number
-   Description: If Advertise/Gateway enable is turned on, RTTs above
    this threshold will be advertised

user\_modified

-   Type: boolean

/spPortal/saasClassification/info
---------------------------------

### GET

Summary

Get the hash code for application definition data for SaaS data from
portal

Notes

Returns a hash code for application definition data. It is used as
version number.

Input type

None

Output type

application/json

hashVal

-   Type: string

/spPortal/applicationTags
-------------------------

### GET

Summary

Get application groups from portal

Notes

Returns a collection of application groups

Input type

None

Output type

application/json

/spPortal/applicationTags/info
------------------------------

### GET

Summary

Get the hash code for application groups data from portal

Notes

Returns a hash code for application groups data. It is used as version
number.

Input type

None

Output type

application/json

hashVal

-   Type: string

/spPortal/trafficBehavior
-------------------------

### GET

Summary

Get traffic behavior categories from portal

Notes

Returns a collection of traffic behavior configuration and names

Input type

None

Output type

application/json

TB\_CONFIG

-   Type: object
-   Required: true
-   Description: traffic behavior configuration key value pairs

TB\_NAME

-   Type: object
-   Required: true
-   Description: an array of traffic behavior names, e.g Voice, Video
    conferencing, Video Streaming, Bulk Data, Interactive, Idle or
    Undetermined

/spPortal/trafficBehavior/info
------------------------------

### GET

Summary

Get the hash code for traffic behavior categories data from portal

Notes

Returns a hash code for traffic behavior data. It is used as version
number.

Input type

None

Output type

application/json

hashVal

-   Type: string

/spPortal/registration
----------------------

### GET

Summary

Get current GMS-Portal registration status

Input type

None

Output type

application/json

emails

-   Type: array
-   Description: Array of email addresses associated with this Portal
    account

site

-   Type: string
-   Description: Account site name. \'Unassigned\' if empty.

accountName

-   Type: string

registered

-   Type: boolean
-   Description: Flag to denote if Orchestrator has registered with
    Portal

enabled

-   Type: boolean
-   Description: Flag to denote if Orchestrator has registered with
    Portal and has been (manually or auto) approved by Administrator

accountKey

-   Type: string
-   Description: Unique key per account generated by Portal.

pendingPoll

-   Type: boolean
-   Description: Flag to denote if Orchestrator is currently polling for
    registration status from portal

group

-   Type: string
-   Description: Account group name. \'Unassigned\' if empty.

/spPortal/registration
----------------------

### POST

Summary

Initiate GMS-Portal Registration

Notes

Use this API to reset the GMS-Portal registration state and initiate new
Registration cycle. There is no post data required for this API, to edit
portal credentials (Account Key, Account Name, Group, Site) use
/gmsConfig/spPortal. Registration cycle may take up to two minutes to
get an update from Portal, use GET /spPortal/registration to poll.

Input type

application/json

Output type

application/json

emails

-   Type: array
-   Description: Array of email addresses associated with this Portal
    account

site

-   Type: string
-   Description: Account site name. \'Unassigned\' if empty.

accountName

-   Type: string

registered

-   Type: boolean
-   Description: Flag to denote if Orchestrator has registered with
    Portal

enabled

-   Type: boolean
-   Description: Flag to denote if Orchestrator has registered with
    Portal and has been (manually or auto) approved by Administrator

accountKey

-   Type: string
-   Description: Unique key per account generated by Portal.

pendingPoll

-   Type: boolean
-   Description: Flag to denote if Orchestrator is currently polling for
    registration status from portal

group

-   Type: string
-   Description: Account group name. \'Unassigned\' if empty.

/spPortal/status
----------------

### GET

Summary

Debug API to view current status of all Portal related services

Input type

None

Output type

application/json

\<serviceName\>

inProgress

-   Type: boolean

state

tickCount

-   Type: number
-   Description: Number of minutes remaining until next execution.

pollInterval

-   Type: number
-   Description: Polling schedule in minutes.

pollFailInterval

-   Type: number
-   Description: Polling schedule after failure in minutes.

ok

-   Type: number
-   Description: Number of successful runs since Orchestrator service
    start.

fail

-   Type: number
-   Description: Number of failed runs since Orchestrator service start.

lastOk

-   Type: number
-   Description: Timestamp of last successful run.

lastFail

-   Type: number
-   Description: Timestamp of last failed run.

enabled

-   Type: boolean

/spPortal/config
----------------

### GET

Summary

Get Silver Peak Cloud Portal registration information

Input type

None

Output type

application/json

host

-   Type: string

port

-   Type: number

registration

accountId

-   Type: string

account

-   Type: string

key

-   Type: string

oldKey

-   Type: string

group

-   Type: string

site

-   Type: string

/spPortal/config
----------------

### POST

Summary

Post Silver Peak Cloud Portal registration information

Input type

application/json: undefined

SpPortalConfig

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

host

-   Type: string

port

-   Type: number

registration

accountId

-   Type: string

account

-   Type: string

key

-   Type: string

oldKey

-   Type: string

group

-   Type: string

site

-   Type: string

Output type

None

/spPortal/topSites
------------------

### GET

Summary

Get list of top internet sites

Notes

This API returns array of top internet sites fetched from Silver Peak
Cloud Portal

Input type

None

Output type

None

/spPortal/tcpUdpPorts
---------------------

### GET

Summary

Get TCP/UDP ports data

Notes

This API returns list of TCP, UDP ports and their applications

Input type

None

Output type

None

/spPortal/ipProtocolNumbers
---------------------------

### GET

Summary

Get IP Protocol Numbers data

Notes

This API returns list of IP Protocol Numbers

Input type

None

Output type

None

/spPortal/applianceWSStatus/{neId}
----------------------------------

### GET

Summary

Test if appliance is reachable via websocket from orchestrator

Notes

Returns status of orchestrator \> portal \> appliance websocket and
orchestrator \> appliance websocket connections. Must provide valid
neId.

Input type

None

neId

-   Description: Unique key assigned by Orchestrator to each appliance,
    for eg: \'0.NE\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

portalWS

reachable

-   Type: boolean

err

-   Type: string

directWS

reachable

-   Type: boolean

err

-   Type: string

/spPortal/account/license/type
------------------------------

### GET

Summary

Get account license type from Portal

Input type

None

Output type

application/json

name

-   Type: string

displayName

-   Type: string

/spPortal/account/license/feature
---------------------------------

### GET

Summary

Get account license feature from Portal

Input type

None

Output type

None

/spPortal/account/license/appliance/ecsp/status
-----------------------------------------------

### GET

Summary

Get EC-SP licenses from Portal

Input type

None

Output type

None

/spPortal/account/license/appliance/ecsp/assign
-----------------------------------------------

### POST

Summary

Assign EC-SP licenses on Portal

Input type

application/json

spPortalECSPLicenseAssign

Description: JSON object containing appliance to license id mappings

Required: true

Parameter type: POST Body

Data Type: application/json

licenses

-   Type: array
-   Required: true

Output type

None

/spPortal/account/license/appliance/ecsp/unassign
-------------------------------------------------

### POST

Summary

Unassign EC-SP licenses on Portal

Input type

application/json

spPortalECSPLicenseUnassign

Description: JSON object containing license ids to be unassigned

Required: true

Parameter type: POST Body

Data Type: application/json

licenses

-   Type: array
-   Required: true

Output type

None

/spPortal/account/key/changeCount
---------------------------------

### GET

Summary

Number of times account key has been changed

Notes

This API contacts Portal to get the number of times this account key has
been changed. Requires Portal connectivity.

Input type

None

Output type

application/json

count

-   Type: number
-   Required: true
-   Description: Number of times account key has been changed for this
    account. 0 if it\'s never been changed.

/spPortal/account/key/changeStatus
----------------------------------

### GET

Summary

Current account key change status

Input type

None

Output type

application/json

generateTimestamp

-   Type: number
-   Required: true
-   Description: Latest epoch time in milliseconds when new account key
    was generated. Null if account key has never been changed.

committedToPortalTimestamp

-   Type: number
-   Required: true
-   Description: Latest epoch time in milliseconds when old account key
    was delete from Portal. Null if current account key change has not
    been committed to Portal.

/spPortal/account/key/generate
------------------------------

### PUT

Summary

Request Portal to generate a new account key

Notes

Requires Portal connectivity.

Input type

Output type

application/json

accountKey

-   Type: string
-   Required: true
-   Description: Current account key

oldAccountKey

-   Type: string
-   Required: true
-   Description: Old account key

/spPortal/account/oldKey
------------------------

### DELETE

Summary

Delete old account key from Portal

Notes

Acts as \"commit\" for a previous account key change. Make sure all
managed appliances have the new account key before calling this.
Successful response code is 204: no content. Requires Portal
connectivity.

Input type

None

Output type

None

/spPortal/createCaseWithPortal
------------------------------

### POST

Summary

Create a case

Notes

Create a case for the Orchestrator or an appliance. caseEmail: If
Orchestrator is not registered to Portal, a valid Silver Peak support
account email address is required in order to create a case.
orchSerialNum: For Orchestrators, leave this field empty (\"\"). For
appliances, enter the nePk (E.g. \"0.NE\"). The corresponding serial
number will then be retrieved based on these inputs.

Input type

application/json

spPortalCreateCaseWithPortal

Description: JSON object containing case information that will be used
to create a case.

Required: true

Parameter type: POST Body

Data Type: application/json

caseName

-   Type: string
-   Required: true
-   Description: Contact name

caseEmail

-   Type: string
-   Required: true
-   Description: Contact email address. If Orchestrator is not
    registered to Portal, a valid Silver Peak support account email
    address is required in order to create a case.

casePhone

-   Type: string
-   Required: true
-   Description: Contact phone number

orchSerialNum

-   Type: string
-   Required: true
-   Description: Serial number for Orchestrator or appliance. NOTE: For
    Orchestrators, leave this field empty (\"\"). For appliances, enter
    the nePk (E.g. \"0.NE\"). The corresponding serial number will then
    be retrieved based on these inputs

casePrio

-   Type: string
-   Required: true
-   Description: Priority. Accepted values: \"P1 - Urgent\", \"P2 -
    High\", \"P3 - Normal\", \"P4 - Low\"

caseSubject

-   Type: string
-   Required: true
-   Description: Subject

caseDesc

-   Type: string
-   Required: true
-   Description: Description

Output type

None

Reference: spPortal.json

\

VXOA save configuration changes
===============================

/appliance/saveChanges
----------------------

### POST

Summary

Save the configuration changes in memory to a file for an array of
appliances in Orchestrator

Notes

the data to post contains ids, ids is an array of neIds.

Input type

application/json

SaveChangesPostBody

Description: JSON object containing an array of neIds

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array
-   Required: true
-   Description: an array of neIds, neId is the device key assigned by
    Orchestrator, usually look like \'0.NE\'.

Output type

None

/appliance/saveChanges/{nePk}
-----------------------------

### POST

Summary

Save the configuration changes in memory to a file for an appliances in
Orchestrator

Notes

The nePk is the id of the appliance in Orchestrator.

Input type

application/json: undefined

nePk

-   Description: The neId.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

SaveChangesPostBody

-   Description: This JSON object should contain nothing.
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

Reference: saveChanges.json

\

VXOA RADIUS and TACACS+
=======================

/authRadiusTacacs/{neId}
------------------------

### GET

Summary

Get the authentication order and authorization information

Notes

the returned data contains aaa, radius and tacacs objects. aaa object
includes auth\_method and author objects. auth\_method represents
Authentication Order for the appliance, You can have upto 3 levels of
Authentication Order (local, radius and tacacs).\
author represents Authorization Information for the appliance and it
consists of default user and map order.\
radius and tacacs contains server object, The return value is a hash map
of RADIUS/TACACS settings keyed by their Order.

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

aaa

auth\_method

\<a number from 1 to 3\>

self

-   Type: integer
-   Required: true
-   Description: Order of Authentication method

name

-   Type: string
-   Required: true
-   Description: Authentication method

author

default-user

-   Type: string
-   Required: true
-   Description: default-user to be used for authorization

map-order

-   Type: string
-   Required: true
-   Description: map-order to be used for authorization

radius

server

\<a number from 1 to 3\>

auth-port

-   Type: integer
-   Required: true
-   Description: Port used by RADIUS server

address

-   Type: string
-   Required: true
-   Description: IP Address of RADIUS server

self

-   Type: integer
-   Required: true
-   Description: Order of RADIUS Servers

enable

-   Type: boolean
-   Required: true
-   Description: To enable/disable RADIUS server

key

-   Type: string
-   Required: true
-   Description: Shared key used for RADIUS authentication requests

retransmit

-   Type: integer
-   Required: true
-   Description: Number of retransmit limits for RADIUS server

timeout

-   Type: integer
-   Required: true
-   Description: timeout in seconds for authentication requests

tacacs

server

\<a number from 1 to 3\>

auth-type

-   Type: string
-   Required: true
-   Description: Authentication type used by TACACS server

auth-port

-   Type: integer
-   Required: true
-   Description: Port used by TACACS server

address

-   Type: string
-   Required: true
-   Description: IP Address of TACACS server

self

-   Type: integer
-   Required: true
-   Description: Order of TACACS Servers

enable

-   Type: boolean
-   Required: true
-   Description: To enable/disable TACACS server

key

-   Type: string
-   Required: true
-   Description: Shared key used for TACACS authentication requests

retransmit

-   Type: integer
-   Required: true
-   Description: Number of retransmit limits for TACACS server

timeout

-   Type: integer
-   Required: true
-   Description: timeout in seconds for authentication requests

Reference: authentication.json

\

VXOA disk information
=====================

/configReportDisk/{neId}
------------------------

### GET

Summary

Get disks information.

Notes

the return data contains disks, controller objects and diskImage. disks
object is a map of disk partitions keyed by their disk ids. controller
object is a map of hard disk drive keyed by their disk ids.

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

disks

\<a number from 0\>

self

-   Type: string
-   Required: true
-   Description: Disk ID

status

-   Type: string
-   Required: true
-   Description: Status

percent\_complete

-   Type: integer
-   Required: true
-   Description: Completion percentage

is\_removable

-   Type: boolean
-   Required: true
-   Description: Whether it is removable

serial\_num

-   Type: string
-   Required: true
-   Description: Serial number

pair

-   Type: integer
-   Required: true
-   Description: ID of the pair disk, if no pair, it is -1

size

-   Type: integer
-   Required: true
-   Description: Size (GB)

controller

\<a number from 0\>

fw\_ver

-   Type: string
-   Required: true
-   Description: Firmware version

self

-   Type: string
-   Required: true
-   Description: Controller ID

drives

-   Type: string
-   Required: true
-   Description: ID list of disks controlled like \'1, 0\'

type

-   Type: string
-   Required: true
-   Description: Type

status

-   Type: string
-   Required: true
-   Description: Status

percent\_complete

-   Type: integer
-   Required: true
-   Description: Completion percentage

card\_model

-   Type: string
-   Required: true
-   Description: Card model

configuration

-   Type: string
-   Required: true
-   Description: Configuration mode

size

-   Type: integer
-   Required: true
-   Description: Size (GB)

diskImage

-   Type: string
-   Required: true
-   Description: Filename of disk image

Reference: disks.json

\

VXOA users and login sessions
=============================

/userAccount/{neId}
-------------------

### GET

Summary

Gets all user details and sessions of appliance

Notes

the return data contains users and sessions object. users object is a
map of user details keyed by username. sessions object is a map of
sessions keyed by session number

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: \'cached = true\' will retrieve the user account
    details from cache .
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

users

\<username\>

self

-   Type: string
-   Required: true
-   Description: Username

enable

-   Type: boolean
-   Required: true
-   Description: Whether a user account is active or inactive

gid

-   Type: integer
-   Required: true
-   Description: Role ID: admin(0), monitor(1001)

password

-   Type: string
-   Required: true
-   Description: Password of the user account

sessions

\<a number of from 1\>

port

-   Type: string
-   Required: true
-   Description: The value is web by default

gid

-   Type: integer
-   Required: true
-   Description: gid of the user

idle\_time

-   Type: string
-   Required: true
-   Description: Idle time of the user

login\_time

-   Type: string
-   Required: true
-   Description: User login time

remote\_host

-   Type: string
-   Required: true
-   Description: Remote host IP address

username

-   Type: string
-   Required: true
-   Description: Logged in user name

Reference: userAccount.json

\

Orchestrator active sessions
============================

/session/activeSessions
-----------------------

### GET

Summary

Returns all the current active sessions

Input type

None

Output type

None

Reference: activeSessions.json

\

Orchestrator server hostname
============================

/gmsHostname
------------

### GET

Summary

Get the host name of orchestrator.

Input type

None

Output type

application/json

gms\_hostname

-   Type: string
-   Description: HostName of Orchestrator

Reference: hostName.json

\

Orchestrator topology maps
==========================

/maps/deleteUploadedMap
-----------------------

### POST

Summary

Delete certain uploaded map

Input type

string

imageName

-   Description: name of file to be deleted
-   Required: true
-   Parameter type: POST Body
-   Data Type: string

Output type

None

/maps/getUploadedMaps
---------------------

### GET

Summary

Get list of uploaded maps

Input type

None

Output type

None

Reference: maps.json

\

VXOA DNS information
====================

/resolver/{neId}?cached={cached}
--------------------------------

### GET

Summary

Get the current DNS IP address and domain names configured for an
appliance.

Input type

None

neId

-   Description: Unique key assigned by Orchestrator to each appliance,
    for eg: \'0.NE\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

domain\_search

1

self

-   Type: integer
-   Required: true

domainname

-   Type: string
-   Required: true
-   Description: Primary Domain Name

2

self

-   Type: integer
-   Required: true

domainname

-   Type: string
-   Required: true
-   Description: Primary Domain Name

3

self

-   Type: integer
-   Required: true

domainname

-   Type: string
-   Required: true
-   Description: Primary Domain Name

nameserver

1

self

-   Type: integer
-   Required: true
-   Description: Order of Name Server

address

-   Type: string
-   Required: true
-   Description: IP Address of Name Server

2

self

-   Type: integer
-   Required: true
-   Description: Order of Name Server

address

-   Type: string
-   Required: true
-   Description: IP Address of Name Server

3

self

-   Type: integer
-   Required: true
-   Description: Order of Name Server

address

-   Type: string
-   Required: true
-   Description: IP Address of Name Server

Reference: dns.json

\

VXOA erase network memory
=========================

/networkMemory
--------------

### POST

Summary

Erase Network Memory on one or more appliances.

Input type

application/json

ApplianceKeys

Description: JSON object containing list of appliance keys

Required: true

Parameter type: POST Body

Data Type: application/json

neList

-   Type: array
-   Required: true
-   Description: Array of appliance keys, for eg: \[\'0.NE\',\'1.NE\'\]

erase

-   Type: boolean
-   Required: true
-   Description: Should be always true, if you want to erase NM

Output type

None

Reference: networkMemory.json

\

Orchestrator server user management
===================================

/users
------

### GET

Summary

Returns all the users in the system

Input type

None

Output type

None

/users/{userName}
-----------------

### GET

Summary

Gets user by username

Input type

None

userName

-   Description: User name
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

userPk

-   Type: string
-   Required: true

username

-   Type: string
-   Required: true

firstName

-   Type: string
-   Required: true

lastName

-   Type: string
-   Required: true

phone

-   Type: string
-   Required: true

email

-   Type: string
-   Required: true

password

-   Type: string
-   Required: true

createTime

-   Type: integer
-   Required: true

status

-   Type: string
-   Required: true

role

-   Type: string
-   Required: true

isTwoFactorEmail

-   Type: boolean
-   Required: true

isTwoFactorTime

-   Type: boolean
-   Required: true

salt

-   Type: string
-   Required: true

/users/{userId}/{userName}
--------------------------

### DELETE

Summary

Deletes the specified user

Input type

None

userId

-   Description: Appliance ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

userName

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/users/{newUser}
----------------

### POST

Summary

Creates a new user or updates it. \'Status: Active/Inactive. Role: Admin
Manager/Network Monitor\'

Input type

application/json: undefined

newUser

-   Description: Flag to indicate new user(=true) or update user
    details(=false)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

SaveUpdateUserBody

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

userPk

-   Type: string
-   Required: true
-   Description: Primary key of the user object. Leave it empty if you
    are creating new user

firstName

-   Type: string
-   Required: true

lastName

-   Type: string
-   Required: true

phone

-   Type: string
-   Required: true

email

-   Type: string
-   Required: true

createDisplayTime

-   Type: string
-   Required: true

status

-   Type: string
-   Required: true
-   Description: Takes values \'Active\' or \'Inactive\'

role

-   Type: string
-   Required: true
-   Description: \'Admin Manager\' for admin role, \'Network Monitor\'
    for monitor role

username

-   Type: string
-   Required: true

password

-   Type: string
-   Required: true

repeatPassword

-   Type: string
-   Required: true

isTwoFactorEmail

-   Type: boolean
-   Required: true

isTwoFactorTime

-   Type: boolean
-   Required: true

Output type

None

/users/{username}/password
--------------------------

### POST

Summary

Changes user password

Input type

application/json: undefined

username

-   Description: user name
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

ChangePasswordBody

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

oldPassword

-   Type: string
-   Required: true

newPassword

-   Type: string
-   Required: true

Output type

None

/users/resetPassword
--------------------

### POST

Summary

Resets user password

Input type

application/json: undefined

ResetPasswordBody

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

password

-   Type: string
-   Required: true
-   Description: password

confirmPassword

-   Type: string
-   Required: true
-   Description: confirmPassword

id

-   Type: string
-   Required: true
-   Description: Reset Password Identifier

tfaApp

-   Type: boolean
-   Required: true
-   Description: Use application based two factor authentication

tfaEmail

-   Type: boolean
-   Required: true
-   Description: Use email based two factor authentication

Output type

application/json

barcodeBase64Jpeg

-   Type: string
-   Description: Base 64 encoded string of a jpeg for a 2D barcode.

/users/forgotPassword
---------------------

### POST

Summary

Forgot password

Input type

application/json: undefined

ForgotPasswordBody

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

userName

-   Type: string
-   Required: true
-   Description: User name

Output type

None

/users/newTfaKey
----------------

### GET

Summary

Returns a barcode to scan with an authentication application to set up
app based two factor authentication for your account

Input type

None

Output type

application/json

barcodeBase64Jpeg

-   Type: string
-   Description: Base 64 encoded string of a jpeg for a 2D barcode.

Reference: user.json

\

VXOA SSL certificates
=====================

/ssl/{neId}?cached={cached}
---------------------------

### GET

Summary

Get all SSL certificates on the appliance

Notes

The schema shows a single SSL certificate object. The return value is a
list of SSL certificates keyed by unique hash code value

Input type

None

neId

-   Description: Internal Id of the appliance from which you want to
    retrieve status information
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

config

host

Unique key that identify the cert

cert

-   Type: string
-   Description: Certificate information

self

-   Type: string

key

-   Type: string
-   Description: Key information

state

host

Unique key that identify the cert

subject

-   Type: string
-   Description: The name of the individual, computer, device, or CA to
    whom the certificate is issued.

self

-   Type: string

method

-   Type: string

key\_type

-   Type: string

serial

-   Type: string
-   Description: The unique serial number that the issuing certification
    authority assigns to the certificate

id

-   Type: string
-   Description: Unique key that identify the cert

builtin

-   Type: string

valid\_from

-   Type: string
-   Description: The beginning date for the period in which the
    certificate is valid

issuer

-   Type: string
-   Description: Information regarding the CA that issued the
    certificate

pretty

-   Type: string

valid\_to

-   Type: string
-   Description: The final date for the period in which the certificate
    is valid

verified

-   Type: boolean
-   Description: Whether certificate is verified or not

Reference: ssl.json

\

VXOA Substitute SSL certificates
================================

/sslSubstituteCertificate/{neId}?cached={cached}
------------------------------------------------

### GET

Summary

Get all SSL Substitute certificates on the appliance.

Input type

None

id

-   Description: Internal Id of the appliance from which you want to
    retrieve status information
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

enable

config

builtin\_signing

-   Type: boolean

cert-substitution

-   Type: boolean

sslcert

signing

Unique key that identify the cert

subject

-   Type: string
-   Description: The name of the individual, computer, device, to whom
    the certificate is issued

self

-   Type: string

method

-   Type: string

key\_type

-   Type: string

serial

-   Type: string
-   Description: The unique serial number that the issuing certification
    authority assigns to the certificate

id

-   Type: string
-   Description: Unique key that identify the cert

builtin

-   Type: string

valid\_from

-   Type: string
-   Description: The beginning date for the period in which the
    certificate is valid

issuer

-   Type: string
-   Description: Information regarding the CA that issued the
    certificate

pretty

-   Type: string

valid\_to

-   Type: string
-   Description: The final date for the period in which the certificate
    is valid

verified

-   Type: string
-   Description: Whether certificate is verified or not

/sslSubstituteCertificate/validation
------------------------------------

### POST

Summary

Validate the SSL Substitute certificate

Input type

application/json

sslSubstituteCertValidation

Description: A JSON object representing SSL Substitute certificate file

Required: true

Parameter type: POST Body

Data Type: application/json

certificateData

-   Type: string
-   Required: true
-   Description: Certificate data

isPFX

-   Type: boolean
-   Required: true
-   Description: To specify whether certificate containing the private
    key or not

password

-   Type: string
-   Description: Password for the certificate

passPhrase

-   Type: string
-   Description: Passphrase for the certificate

isSslCACert

-   Type: boolean
-   Required: true

Output type

None

Reference: sslSubstituteCert.json

\

VXOA SSL CA certificates
========================

/sslCACertificate/{neId}?cached={cached}
----------------------------------------

### GET

Summary

Get all CA SSL certificates on the appliance.

Input type

None

neId

-   Description: Internal Id of the appliance from which you want to
    retrieve status information
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

auth

Unique key that identify the cert

subject

-   Type: string
-   Description: The name of the individual, computer, device, to whom
    the certificate is issued

self

-   Type: string

method

-   Type: string

key\_type

-   Type: string

serial

-   Type: string
-   Description: The unique serial number that the issuing certification
    authority assigns to the certificate

id

-   Type: string
-   Description: Unique key that identify the cert

builtin

-   Type: string

valid\_from

-   Type: string
-   Description: The beginning date for the period in which the
    certificate is valid

issuer

-   Type: string
-   Description: Information regarding the CA that issued the
    certificate

pretty

-   Type: string

valid\_to

-   Type: string
-   Description: The final date for the period in which the certificate
    is valid

verified

-   Type: string
-   Description: Whether certificate is verified or not

/sslCACertificate/getText
-------------------------

### POST

Summary

This API will take certificate date and return certificate information
in text

Input type

application/json

sslCACertsFile

Description: A JSON object representing CA SSL certificate data

Required: true

Parameter type: POST Body

Data Type: application/json

certificateData

-   Type: string
-   Required: true

password

-   Type: string

isPFX

-   Type: boolean

Output type

None

/sslCACertificate/validation
----------------------------

### POST

Summary

Validate the SSL CA certificate file.

Input type

application/json

sslCACertValidation

Description: A JSON object representing SSL certificate

Required: true

Parameter type: POST Body

Data Type: application/json

certificateData

-   Type: string
-   Required: true

password

-   Type: string

passPhrase

-   Type: string

isPFX

-   Type: boolean

isSslCACert

-   Type: boolean
-   Required: true

Output type

None

/sslCertificate/getText
-----------------------

### POST

Summary

This API will take certificate date and return certificate information
in text

Input type

application/json

sslCertGetTextPost

Description: A JSON object representing SSL certificate

Required: true

Parameter type: POST Body

Data Type: application/json

certificateData

-   Type: string
-   Required: true

password

-   Type: string

isPFX

-   Type: boolean

Output type

None

/sslCertificate/getInfo
-----------------------

### POST

Summary

This API will take certificate date and return certificate information

Input type

application/json

sslCertGetInfoPost

Description: A JSON object representing SSL certificate

Required: true

Parameter type: POST Body

Data Type: application/json

certificateData

-   Type: string

keyData

-   Type: string

password

-   Type: string

passPhrase

-   Type: string

isPFX

-   Type: boolean
-   Required: true

isSslCACert

-   Type: boolean
-   Required: true

Output type

None

Reference: sslCACertificate.json

\

Upload files to Orchestrator server
===================================

/fileCreation/fineUploader
--------------------------

### POST

Summary

This API is to be used with fineuploader.js plugin. See fineuploader.com

Input type

multipart/form-data

type

-   Description: Type of upload
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

qqfile

-   Description: Name of the file
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

qqfile

-   Description: Name of the file
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: fileCreation.json

\

VXOA software upgrade
=====================

/vxoaImages
-----------

### GET

Summary

Get VXOA images

Notes

the return data contains a list of VXOA image object, an image object
contains file name, version and build date time.

Input type

None

Output type

None

/vxoaImages/{imageFile}
-----------------------

### DELETE

Summary

Delete VXOA image

Notes

the last url part is the file name of image to delete

Input type

None

imageFile

-   Description: the file name of VXOA image to delete
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: applianceUpgrade.json

\

Orchestrator scheduled jobs
===========================

/gms/job
--------

### GET

Summary

Get all the scheduled jobs in Orchestrator

Notes

Return all the scheduled jobs.

**\"Minute schedule - Every minute starting at effectiveTime(in epoch
sec )\"**\
\"schedule\": {\
\"effectiveTime\": 1431323520,\
\"recurrence\": {\
\"minutes\": 1\
}\
},

\

**\"Hourly Schedule - every seven hours starting from effectiveTime\"**\
\"schedule\": {\
\"effectiveTime\": 1431323520,\
\"recurrence\": {\
\"hourly\": 7\
}\
},

\

**\"Daily Schedule - every weekday at 2:03 am\"\
\"**schedule\": {\
\"effectiveTime\": 1431968786,\
\"endTime\": 1494435986,\
\"recurrence\": {\
\"daily\" : {\
\"everyWeekDay\" : true,\
\"occursAt\" : {\
\"hour\": 2,\
\"minute\": 3\
}\
}\
},\
\"timezoneOffset\": \"-08:00\"\
},

\

**\"Daily Schedule - every day at 2:03 am\"**\
\"schedule\": {\
\"effectiveTime\": 1431323520,\
\"recurrence\": {\
\"daily\" : {\
\"everyDay\" : true,\
\"occursAt\" : {\
\"hour\": 2,\
\"minute\": 3\
}\
}\
},\
\"timezoneOffset\": \"-08:00\"\
},

\

**\"Weekly Schedule - every sun, mon, tue and sat at 1:03 am. Timezone
offset if the client offset\"**\
\"schedule\": {\
\"effectiveTime\": 1431323520,\
\"recurrence\": {\
\"weekly\": {\
\"daysOfTheWeek\": \[1,2,3,7\],\
\"occursAt\" : {\
\"hour\": 1,\
\"minute\": 3\
}\
}\
},\
\"timezoneOffset\": \"-08:00\"\
},

\

**\"Monthly Schedule - every 2nd day of every 3 months at 12:06 pm.
Timezone offset if the client offset\"**\
\"schedule\": {\
\"effectiveTime\": 1431323520,\
\"recurrence\": {\
\"monthly\": {\
\"type3\": {\
\"day\": 2,\
\"everyXMonths\": 3\
},\
\"occursAt\" : {\
\"hour\": 12,\
\"minute\": 6\
}\
}\
},\
\"timezoneOffset\": \"-08:00\"\
},

\

**\"Monthly Schedule - First Tue of every 4 months at 12:06 pm. Rank -
1,2,3,4 dayOfTheWeek - 1 (Sun), 2(Mon)\...7(Sat)\"**\
\"schedule\": {\
\"effectiveTime\": 1431323520,\
\"recurrence\": {\
\"monthly\": {\
\"type4\" : {\
\"rank\": 1,\
\"dayOfTheWeek\": 3,\
\"everyXMonths\": 4\
},\
\"occursAt\" : {\
\"hour\": 12,\
\"minute\": 6\
}\
}\
},\
\"timezoneOffset\": \"-08:00\"\
},

\

**\"Monthly Schedule - First day of every month at 12:00 pm\"**\
\"schedule\": {\
\"effectiveTime\": 1431323520,\
\"recurrence\": {\
\"monthly\": {\
\"type1\" : {\
\"firstDayOfEveryMonth\": true\
},\
\"occursAt\" : {\
\"hour\": 12,\
\"minute\": 0\
}\
}\
},\
\"timezoneOffset\": \"-7:00\"\
},

\

**\"Monthly Schedule - Last day of every month at 12:00 pm\"**\
\"schedule\": {\
\"effectiveTime\": 1431323520,\
\"recurrence\": {\
\"monthly\": {\
\"type2\" : {\
\"lastDayOfEveryMonth\": true\
},\
\"occursAt\" : {\
\"hour\": 12,\
\"minute\": 0\
}\
}\
},\
\"timezoneOffset\": \"-7:00\"\
},

\

**\"Yearly Schedule - Jan 2nd every year at 12:02 pm. month - 1(Jan),
2(Feb)\...\"**\
\"schedule\": {\
{\
\"effectiveTime\": 12234343,\
\"recurrence\" : {\
\"yearly\": {\
\"type1\": {\
\"month\": 1,\
\"day\": 2\
}\
\"occursAt\" : {\
\"hour\": 12,\
\"minute\": 2\
}\
}\
}\
},\
\"timezoneOffset\": \"-7:00\"\
},

\

**\"Yearly Schedule - 1st Monday of Jan every year at 12:02 pm. rank -
1,2,3,4 dayOfTheWeek - 1(Sun), 2(Mon)\... 7(Sat) month - 1(Jan),
2(Feb)\...\"**\
\"schedule\": {\
{\
{\
\"effectiveTime\": 12234343,\
\"recurrence\" : {\
\"yearly\": {\
\"type2: {\
\"rank\": 1,\
\"dayOfTheWeek\": 2\
\"month\": 1\
}\
\"occursAt\" : {\
\"hour\": 12,\
\"minute\": 2\
}\
}\
}\
},\
\"timezoneOffset\": \"-7:00\"\
},

\

**\"Run now job schedule\"**\
\"schedule\": {\
\"runNow\": true\
}

Input type

None

Output type

None

### POST

Summary

Schedule a new job in Orchestrator

Notes

**\"Job category for Orchestrator jobs. \"**\
2 - Orchestrator Report,\
3 - Orchestrator Backup,\
4 - Appliance Reboot or Shutdown,\
5 - Appliance QOS Scheduling,\
6 - IPsec Pre-shared Key Rotation;

\

**\"Sample POST\"**\
{\
\"jobCategory\": 2,\
\"schedule\": {\
\"effectiveTime\": 1431323520,\
\"recurrence\": {\
\"minutes\": 1\
}\
},\
\"config\": {\"a\": 1},\
\"description\": \"minute schedule\",\
\"targetAppliance\": {\
\"groupPks\" : \[\"1.Network\", \"2.Network\"\],\
\"nePks\": \[\"1.NE\", \"2.NE\"\]\
}\
}

Input type

application/json

scheduledJobs2Post

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

jobCategory

-   Type: string
-   Required: true
-   Description: Identifies the type of job. 1=TestJob, 2=Orchestrator
    Report, 3=Orchestrator Backup, 4 - Appliance Reboot/Shutdown, 5 =
    Appliance QOS Scheduling, 6 = IPsec Pre-shared Key Rotation

schedule

-   Type: string
-   Required: true
-   Description: Schedule of this job

enabled

-   Type: boolean
-   Required: true
-   Description: The job is enabled or disabled

config

-   Type: string
-   Required: true
-   Description: Job configuration

description

-   Type: string
-   Required: true
-   Description: Job description

targetAppliance

groupPks

-   Type: array
-   Description: comma separated list of groupPk (primary key of group).
    Job will be run on every appliance in the group

nePks

-   Type: array
-   Description: comma separated list of nepk (primary key of appliance)

Output type

None

/gms/job/{jobId}
----------------

### DELETE

Summary

Deletes a schedule job

Input type

None

jobId

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

### PUT

Summary

Modify a scheduled job.

Notes

**\"Job category for Orchestrator jobs. \"**\
2 - Orchestrator Report,\
3 - Orchestrator Backup,\
4 - Appliance Reboot or Shutdown,\
5 - Appliance QOS Scheduling,\
6 - IPsec Pre-shared Key Rotation;

\

**\"Sample POST\"**\
{\
\"jobCategory\": 2,\
\"schedule\": {\
\"effectiveTime\": 1431323520,\
\"recurrence\": {\
\"minutes\": 1\
}\
},\
\"config\": {\"a\": 1},\
\"description\": \"minute schedule\",\
\"targetAppliance\": {\
\"groupPks\" : \[\"1.Network\", \"2.Network\"\],\
\"nePks\": \[\"1.NE\", \"2.NE\"\]\
}\
}

Input type

application/json: undefined

jobId

-   Description: The id of the job.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

scheduledJob2Post

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

jobCategory

-   Type: string
-   Required: true
-   Description: Identifies the type of job. 1=TestJob, 2=Orchestrator
    Report, 3=Orchestrator Backup, 4 - Appliance Reboot/Shutdown, 5 =
    Appliance QOS Scheduling, 6 = IPsec Pre-shared Key Rotation

schedule

-   Type: string
-   Required: true
-   Description: Schedule of this job

enabled

-   Type: boolean
-   Required: true
-   Description: The job is enabled or disabled

config

-   Type: string
-   Required: true
-   Description: Job configuration

description

-   Type: string
-   Required: true
-   Description: Job description

targetAppliance

groupPks

-   Type: array
-   Description: comma separated list of groupPk (primary key of group).
    Job will be run on every appliance in the group

nePks

-   Type: array
-   Description: comma separated list of nepk (primary key of appliance)

Output type

None

/gms/job/{jobId}
----------------

### GET

Summary

Get scheduled jobs list for one specified appliance based on the nePk Id

Input type

None

jobId

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/gms/job/{jobId}/stop
---------------------

### POST

Summary

Stop a scheduled job

Input type

Unknown

jobId

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/gms/job/historical
-------------------

### GET

Summary

Get the historical jobs by parameter. If all parameter is empty. Get
1000 latest historical jobs entities.

Notes

Every job on its completion, will make an entry to historical jobs
table. This api will return all the historical jobs

Input type

None

lastXDays

-   Description: Jobs ended in last x days, Can combine use with
    scheduleId
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

scheduleId

-   Description: get historical Jobs by scheduleId
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

latest

-   Description: get latest historical Job, Can combine use with
    scheduleId
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/gms/job/historical/{jobId}
---------------------------

### GET

Summary

Get the historical job by Id.

Notes

Every job on its completion, will make an entry to historical jobs
table.

Input type

None

jobId

-   Description: Get historical Job by Id.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

id

-   Type: integer
-   Required: true
-   Description: Every job is uniquely identified using this primary key

assocId

-   Type: integer
-   Required: true
-   Description: Every job is related to one scheduled job identified
    using this primary key

jobType

-   Type: integer
-   Required: true
-   Description: 1=User defined job and 2=System job

jobCategory

-   Type: string
-   Required: true
-   Description: Identifies the type of job. 1=TestJob, 2=Orchestrator
    Report, 3=Orchestrator Backup, 4 - Appliance Reboot/Shutdown, 5 =
    Appliance QOS Scheduling

config

-   Type: string
-   Required: true
-   Description: Job configuration

targetAppliance

groupPks

-   Type: array
-   Description: comma separated list of groupPk (primary key of group).
    Job will be run on every appliance in the group

nePks

-   Type: array
-   Description: comma separated list of nepk (primary key of appliance)

description

-   Type: string
-   Required: true
-   Description: Job description

startTime

-   Type: integer
-   Required: true
-   Description: Time when job started

endTime

-   Type: integer
-   Required: true
-   Description: Time when job ended

status

-   Type: integer
-   Required: true
-   Description: 1-Started, 2-Success, 3 - Failed

statusText

-   Type: integer
-   Required: true
-   Description: Job Status summary

Reference: scheduledJobs2.json

\

Orchestrator server info
========================

/gmsserver/hello
----------------

### GET

Summary

Returns hello message.

Input type

None

Output type

None

/gmsserver/info
---------------

### GET

Summary

Returns orchestrator server information such as used disk space,
hostname,release etc\...

Input type

None

Output type

application/json

usedDiskSpace

-   Type: string
-   Description: Total disk Memory used so far

hostName

-   Type: string
-   Description: host name of the orchestrator server

role

-   Type: integer
-   Description: role

serialNumber

-   Type: string
-   Description: orchestrator server serialNumber

hwRev

-   Type: string
-   Description: hwRev

numCpus

-   Type: integer
-   Description: Number of CPUs

release

-   Type: string
-   Description: orchestrator release version

freeDiskSpace

-   Type: string
-   Description: Remaining disk Memory

osRev

-   Type: string
-   Description: Operating System Version

uptime

-   Type: string
-   Description: uptime

domain

-   Type: string
-   Description: Orchestrator domain name

host

-   Type: string
-   Description: orchestrator host IP number

numActiveUsers

-   Type: integer
-   Description: number of active user accessing the Orchestrator

model

-   Type: string
-   Description: Orchestrator server model number

loadAverage

-   Type: integer
-   Description: loadAverage

time

-   Type: integer
-   Description: time

memSize

-   Type: integer
-   Description: Total memory of the server

inContainerMode

-   Type: boolean
-   Description: container mode

/gmsserver/ping
---------------

### GET

Summary

Returns orchestrator server information such as used disk hostname, up
time, version etc\...

Input type

None

Output type

application/json

hostName

-   Type: string
-   Description: host name of the orchestrator server

timeStr

-   Type: string
-   Description: timeStr

time

-   Type: integer
-   Description: time

message

-   Type: string
-   Description: status message of Server

version

-   Type: string
-   Description: Current GSM version

uptime

-   Type: string
-   Description: Duration since the server is up

/gmsserver/briefInfo
--------------------

### GET

Summary

Returns orchestrator server information such as used disk hostname, up
time, version etc\...

Input type

None

Output type

application/json

hostName

-   Type: string
-   Description: host name of the orchestrator server

time

-   Type: string
-   Description: current time

version

-   Type: string
-   Description: Current GSM version

uptime

-   Type: string
-   Description: Duration since the server is up

ip

-   Type: string
-   Description: IP address of the GSM

/gms/versions
-------------

### GET

Summary

Returns available orchestrator versions

Input type

None

Output type

application/json

current

-   Type: string
-   Description: Current installed version of gsm

installed

-   Type: array
-   Description: list of 3 available orchestrator version

/gmsOperatingSystem
-------------------

### GET

Summary

Returns Orchestrator Operating system type

Input type

None

Output type

None

Reference: gmsServer.json

\

Recommended VXOA and Orchestrator releases
==========================================

/release
--------

### GET

Summary

Gets all the Releases for orchestrator and vxoa

Input type

None

filter

-   Description: If filter is true it returns only new releases of
    orchestrator and vxoa
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

gmsbuilds

-   Type: array
-   Required: true
-   Description: list of available orchestrator version

vxoabuilds

-   Type: array
-   Required: true
-   Description: list of available vxoa version

/release/notifications
----------------------

### GET

Summary

Get release notifications

Notes

This API will return all the notifications that are meant to be shown in
Orchestrator. This means the notification is new, it\'s \'remind me\'
date is past, and it has not been dismissed.

Input type

None

Output type

application/json

/release/notifications/delay/{version}
--------------------------------------

### POST

Summary

Delay a release notification

Notes

Delay the notificaiton by a number of hours.

Input type

application/json: undefined

version

-   Description: The version of the release the notification is for
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Delay request

-   Description: The number of hours to delay the notification by
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/release/notifications/dismiss/{version}
----------------------------------------

### POST

Summary

Dismiss a release notification

Notes

Dismiss a release, so it\'s no longer shown to the user.

Input type

Unknown

version

-   Description: The version of the release the notification is for
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: releases.json

\

VXOA application trend statistics
=================================

/applicationTrends
------------------

### GET

Summary

Returns bandwidth stats about applications

Notes

\"appliance-name\" key corresponds to a specific appliance name and
\"appName\" corresponds to a specific application name

Input type

None

startDate

-   Description: Beginning date for stats in milliseconds
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endDate

-   Description: End date for stats in milliseconds
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

traffic

-   Description: Traffic type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

bound

-   Description: Traffic direction
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

nePKList

-   Description: List of appliances IDs like so: 0.NE,1.NE
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

customRangeUsed

-   Description: Indicates if custom date rage is used
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

applicationStatsOfAppliances

appliance-name

appName

-   Type: array
-   Required: true

Reference: application.json

\

VXOA user defined application groups
====================================

/application
------------

### GET

Summary

Get all built-in and user-defined applications

Notes

The schema shows a single application group object

Input type

None

Output type

None

/application/userDefinedConfig/{neId}?cached={cached}
-----------------------------------------------------

### GET

Summary

Get all user-defined application group on appliance

Input type

None

neId

-   Description: Internal Id of the appliance from which you want to
    retrieve status information
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<priorityId\>

self

-   Type: integer

dst\_ip\_range

-   Type: string
-   Description: Destination IP address

dscp

-   Type: string
-   Description: Differentiated services code point

vlan

-   Type: string
-   Description: virtual local area network

src\_port\_range

-   Type: string
-   Description: Source port range

dst\_port\_range

-   Type: string
-   Description: Destination port range

id

-   Type: integer

ipver

-   Type: string

src\_ip\_range

-   Type: string
-   Description: Source ip range

name

-   Type: string

proto

-   Type: string
-   Description: Protocol

/application/builtin
--------------------

### GET

Summary

Get all built-in applications from all appliance

Input type

None

Output type

None

/application/userDefined
------------------------

### GET

Summary

Get all userDefined applications from all appliance

Input type

None

Output type

None

/applicationGroups/{neId}?cached={cached}
-----------------------------------------

### GET

Summary

Get all applicationGroups on a appliance

Input type

None

neId

-   Description: Internal Id of the appliance from which you want to
    retrieve status information
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

replication

app\_assure\_replication

-   Type: string

netvault\_replication

-   Type: string

vsphere\_replication\_ini

-   Type: string

app\_assure\_svr\_backup

-   Type: string

zerto\_mgmt

-   Type: string

vsphere\_replication\_ong

-   Type: string

zerto\_replication

-   Type: string

citrix

citrix-cgp

-   Type: string

citrix-ica

-   Type: string

citrix-ima

-   Type: string

citrix-bcast

-   Type: string

interactive

-   Type: object

encrypted

-   Type: object

real-time

-   Type: object

Reference: applicationGroup.json

\

VXOA system state
=================

/systemInfo/{neId}?cached={cached}
----------------------------------

### GET

Summary

Get Appliance System State Information

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

hostName

-   Type: string
-   Required: true
-   Description: Need description here

applianceid

-   Type: integer
-   Required: true
-   Description: Need description here

biosVersion

-   Type: string
-   Required: true
-   Description: Need description here

deploymentMode

-   Type: string
-   Required: true
-   Description: Need description here

rebootRequired

-   Type: boolean
-   Required: true
-   Description: Need description here

alarmSummary

num\_critical

-   Type: integer
-   Required: true
-   Description: Need description here

num\_outstanding

-   Type: integer
-   Required: true
-   Description: Need description here

num\_equipment\_outstanding

-   Type: integer
-   Required: true
-   Description: Need description here

num\_major

-   Type: integer
-   Required: true
-   Description: Need description here

num\_tca\_outstanding

-   Type: integer
-   Required: true
-   Description: Need description here

num\_minor

-   Type: integer
-   Required: true
-   Description: Need description here

num\_software\_outstanding

-   Type: integer
-   Required: true
-   Description: Need description here

num\_traffic\_class\_outstanding

-   Type: integer
-   Required: true
-   Description: Need description here

num\_tunnel\_outstanding

-   Type: integer
-   Required: true
-   Description: Need description here

num\_warning

-   Type: integer
-   Required: true
-   Description: Need description here

timezone

-   Type: string
-   Required: true
-   Description: Need description here

release

-   Type: string
-   Required: true
-   Description: Need description here

uuid

-   Type: string
-   Required: true
-   Description: Need description here

uptime

-   Type: integer
-   Required: true
-   Description: Need description here

datetime

-   Type: string
-   Required: true
-   Description: Need description here

licenseExpiryDate

-   Type: integer
-   Required: true
-   Description: Need description here

serial

-   Type: string
-   Required: true
-   Description: Need description here

gmtOffset

-   Type: integer
-   Required: true
-   Description: Need description here

licenseRequired

-   Type: boolean
-   Required: true
-   Description: Need description here

modelShort

-   Type: string
-   Required: true
-   Description: Need description here

model

-   Type: string
-   Required: true
-   Description: Need description here

isLicenseInstalled

-   Type: boolean
-   Required: true
-   Description: Need description here

uptimeString

-   Type: string
-   Required: true
-   Description: Need description here

releaseWithoutPrefix

-   Type: string
-   Required: true
-   Description: Need description here

licenseExpirationDaysLeft

-   Type: integer
-   Required: true
-   Description: Need description here

hasUnsavedChanges

-   Type: boolean
-   Required: true
-   Description: Need description here

status

-   Type: string
-   Required: true
-   Description: Need description here

Reference: appSystemStateInfo.json

\

VXOA deployment
===============

/systemInfo/system/{neId}?cached={cached}
-----------------------------------------

### GET

Summary

Get Appliance System Deployment Information

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

bw

if\_rx\_target

-   Type: boolean
-   Required: true
-   Description: Target bandwidth enabled

auto\_subnet

self

-   Type: boolean
-   Description: Flag to enable and disable subnet sharing

add\_local

-   Type: boolean
-   Description: Enable automatic advertising local subnets. **Note**:
    Only applicable for appliances whose version is less than 8.1.4.

add\_local\_lan

-   Type: boolean
-   Description: Enable automatic advertising local subnets for LAN
    interfaces only. **Note**: Only applicable for appliances whose
    version is greater than or equal to 8.1.4.

add\_local\_wan

-   Type: boolean
-   Description: Enable automatic advertising local subnets for WAN
    interfaces only. **Note**: Only applicable for appliances whose
    version is greater than or equal to 8.1.4.

add\_local\_metric

-   Type: integer
-   Description: Metric assigned to subnets of interfaces on this
    appliance

redist\_bgp

-   Type: boolean
-   Description: Enable redistribution of learned BGP routes via subnet
    sharing

redist\_ospf

-   Type: boolean
-   Description: Enable redistribution of learned OSPF routes via subnet
    sharing

redist\_ospf\_filter

-   Type: integer
-   Description: Filter for OSPF routes redistributed to subnet sharing

redist\_ospf\_metric

-   Type: integer
-   Description: Add metric to OSPF routes to be redistributed to subnet
    sharing

local\_ospf\_filter

-   Type: integer
-   Description: Filter for locally learned OSPF routes

nm\_fsp\_enable

-   Type: boolean
-   Description: Enable network memory FSP

nat

-   Type: boolean
-   Description: Enable NAT

auto\_syn

-   Type: boolean
-   Description: Enable auto SYN

bridge\_loop\_test

-   Type: boolean
-   Description: Bridge loop test

auto\_tunnel

-   Type: boolean
-   Description: Enable auto tunnel

ipsec\_override

-   Type: boolean
-   Description: SSL IPSec override

auto\_ipid

-   Type: boolean
-   Description: Enable auto IP ID

excess\_flow

dscp\_marking

-   Type: boolean
-   Description: Excess flow DSCP marking

policy

-   Type: string
-   Description: Excess flow policy

dpc

tunfail

-   Type: string
-   Description: DPC tunnel failover behavior

disk\_encrypt\_enable

-   Type: boolean
-   Description: Enable disk encryption

igmp\_snooping

-   Type: boolean
-   Description: Bridge multicast IGMP snooping

nm\_media

-   Type: integer
-   Description: System network memory media

nm\_mode

-   Type: integer
-   Description: System network memory mode

node\_dns

enable

-   Type: boolean
-   Required: true
-   Description: Enable DNS lookup

ipaddr

-   Type: string
-   Description: DNS IP address

smb\_signing

-   Type: boolean
-   Description: SMB signing optimization

udp\_inact\_time

-   Type: integer
-   Description: UDP flow timeout in seconds range from 1 to 65535

quies\_tun\_ka\_intvl

-   Type: integer
-   Description: Quiescent-tunnel keep alive time range from 1 to 65535

max\_tcp\_mss

-   Type: integer
-   Description: Maximum TCP MSS range from 500 to 9000

int\_hairpin

-   Type: boolean
-   Description: Enable internal hairpinning

auto\_pol\_lookup\_intvl

-   Type: integer
-   Description: Auto policy lookup interval

passthru\_to\_sender

-   Type: boolean
-   Description: Disable passthrough L2 return to sender

orch\_guid

-   Type: string
-   Description: Orchestrator GUID

idrc

param\_delta

-   Type: number
-   Description: Parameter delta for IDRC

param\_g

-   Type: number
-   Description: Parameter g for IDRC

param\_m

-   Type: number
-   Description: Parameter m for IDRC

param\_y

-   Type: number
-   Description: Parameter y for IDRC

ha\_if

-   Type: string
-   Description: High availability interface

port\_fwd\_rules

-   Type: string
-   Description: Inbound port forwarding rules

udp\_ipsec\_lcl\_ports

-   Type: string
-   Description: List of ports used locally for UDP IPSec tunnels

udp\_ipsec\_peer\_ports

-   Type: string
-   Description: List of ports used by peers for UDP IPSec tunnels

network\_role

-   Type: integer
-   Description: Network role

options

-   Type: integer
-   Description: Tunnel system level options for future

serverMode

target\_in\_thres

-   Type: number
-   Description: Interface max link threshold

target\_out\_thres

-   Type: number
-   Description: Interface max link threshold

in\_max\_bw

-   Type: integer
-   Description: Server inbound max bandwidth (kbps)

out\_max\_bw

-   Type: integer
-   Description: Server outbound max bandwidth (kbps)

shaperinbound

wan

max\_bw

-   Type: integer
-   Description: Shaper max bandwidth (kbps)

dyn\_bw\_enable

-   Type: boolean
-   Description: Enable/disable dynamic bandwidth for inbound shaper

self

-   Type: string
-   Description: Shaper interface name. It is the same as the parent
    node key \"wan\"

accuracy

-   Type: integer
-   Description: Shaper accuracy (usec)

enable

-   Type: boolean
-   Description: Enable/disable inbound shaper

traffic-class

\<traffic\_class\_id\>

max\_bw

-   Type: number
-   Description: Traffic class max bandwidth %

min\_bw

-   Type: number
-   Description: Traffic class min bandwidth %

max\_bw\_abs

-   Type: integer
-   Description: Traffic class max bandwidth (kbps)

min\_bw\_abs

-   Type: integer
-   Description: Traffic class min bandwidth (kbps)

flow\_limit

-   Type: integer
-   Description: Traffic class flow rate limit

self

-   Type: integer
-   Description: Traffic class ID. It is the same as the parent node key
    \"trafficClassId\"

excess

-   Type: integer
-   Description: Traffic class excess weight

priority

-   Type: integer
-   Description: Traffic class priority

max\_wait

-   Type: integer
-   Description: Traffic class max wait

/systemInfo/systemForDiscovered/{discoveredId}
----------------------------------------------

### GET

Summary

Discovered Appliance System Deployment Information

Notes

Use this API to get the system information for a discovered appliance.

Input type

None

discoveredId

-   Description: The discovered appliance ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

bw

if\_rx\_target

-   Type: boolean
-   Required: true
-   Description: Target bandwidth enabled

auto\_subnet

self

-   Type: boolean
-   Description: Flag to enable and disable subnet sharing

add\_local

-   Type: boolean
-   Description: Enable automatic advertising local subnets. **Note**:
    Only applicable for appliances whose version is less than 8.1.4.

add\_local\_lan

-   Type: boolean
-   Description: Enable automatic advertising local subnets for LAN
    interfaces only. **Note**: Only applicable for appliances whose
    version is greater than or equal to 8.1.4.

add\_local\_wan

-   Type: boolean
-   Description: Enable automatic advertising local subnets for WAN
    interfaces only. **Note**: Only applicable for appliances whose
    version is greater than or equal to 8.1.4.

add\_local\_metric

-   Type: integer
-   Description: Metric assigned to subnets of interfaces on this
    appliance

redist\_bgp

-   Type: boolean
-   Description: Enable redistribution of learned BGP routes via subnet
    sharing

redist\_ospf

-   Type: boolean
-   Description: Enable redistribution of learned OSPF routes via subnet
    sharing

redist\_ospf\_filter

-   Type: integer
-   Description: Filter for OSPF routes redistributed to subnet sharing

redist\_ospf\_metric

-   Type: integer
-   Description: Add metric to OSPF routes to be redistributed to subnet
    sharing

local\_ospf\_filter

-   Type: integer
-   Description: Filter for locally learned OSPF routes

nm\_fsp\_enable

-   Type: boolean
-   Description: Enable network memory FSP

nat

-   Type: boolean
-   Description: Enable NAT

auto\_syn

-   Type: boolean
-   Description: Enable auto SYN

bridge\_loop\_test

-   Type: boolean
-   Description: Bridge loop test

auto\_tunnel

-   Type: boolean
-   Description: Enable auto tunnel

ipsec\_override

-   Type: boolean
-   Description: SSL IPSec override

auto\_ipid

-   Type: boolean
-   Description: Enable auto IP ID

excess\_flow

dscp\_marking

-   Type: boolean
-   Description: Excess flow DSCP marking

policy

-   Type: string
-   Description: Excess flow policy

dpc

tunfail

-   Type: string
-   Description: DPC tunnel failover behavior

disk\_encrypt\_enable

-   Type: boolean
-   Description: Enable disk encryption

igmp\_snooping

-   Type: boolean
-   Description: Bridge multicast IGMP snooping

nm\_media

-   Type: integer
-   Description: System network memory media

nm\_mode

-   Type: integer
-   Description: System network memory mode

node\_dns

enable

-   Type: boolean
-   Required: true
-   Description: Enable DNS lookup

ipaddr

-   Type: string
-   Description: DNS IP address

smb\_signing

-   Type: boolean
-   Description: SMB signing optimization

udp\_inact\_time

-   Type: integer
-   Description: UDP flow timeout in seconds range from 1 to 65535

quies\_tun\_ka\_intvl

-   Type: integer
-   Description: Quiescent-tunnel keep alive time range from 1 to 65535

max\_tcp\_mss

-   Type: integer
-   Description: Maximum TCP MSS range from 500 to 9000

int\_hairpin

-   Type: boolean
-   Description: Enable internal hairpinning

auto\_pol\_lookup\_intvl

-   Type: integer
-   Description: Auto policy lookup interval

passthru\_to\_sender

-   Type: boolean
-   Description: Disable passthrough L2 return to sender

orch\_guid

-   Type: string
-   Description: Orchestrator GUID

idrc

param\_delta

-   Type: number
-   Description: Parameter delta for IDRC

param\_g

-   Type: number
-   Description: Parameter g for IDRC

param\_m

-   Type: number
-   Description: Parameter m for IDRC

param\_y

-   Type: number
-   Description: Parameter y for IDRC

ha\_if

-   Type: string
-   Description: High availability interface

port\_fwd\_rules

-   Type: string
-   Description: Inbound port forwarding rules

udp\_ipsec\_lcl\_ports

-   Type: string
-   Description: List of ports used locally for UDP IPSec tunnels

udp\_ipsec\_peer\_ports

-   Type: string
-   Description: List of ports used by peers for UDP IPSec tunnels

network\_role

-   Type: integer
-   Description: Network role

options

-   Type: integer
-   Description: Tunnel system level options for future

serverMode

target\_in\_thres

-   Type: number
-   Description: Interface max link threshold

target\_out\_thres

-   Type: number
-   Description: Interface max link threshold

in\_max\_bw

-   Type: integer
-   Description: Server inbound max bandwidth (kbps)

out\_max\_bw

-   Type: integer
-   Description: Server outbound max bandwidth (kbps)

shaperinbound

wan

max\_bw

-   Type: integer
-   Description: Shaper max bandwidth (kbps)

dyn\_bw\_enable

-   Type: boolean
-   Description: Enable/disable dynamic bandwidth for inbound shaper

self

-   Type: string
-   Description: Shaper interface name. It is the same as the parent
    node key \"wan\"

accuracy

-   Type: integer
-   Description: Shaper accuracy (usec)

enable

-   Type: boolean
-   Description: Enable/disable inbound shaper

traffic-class

\<traffic\_class\_id\>

max\_bw

-   Type: number
-   Description: Traffic class max bandwidth %

min\_bw

-   Type: number
-   Description: Traffic class min bandwidth %

max\_bw\_abs

-   Type: integer
-   Description: Traffic class max bandwidth (kbps)

min\_bw\_abs

-   Type: integer
-   Description: Traffic class min bandwidth (kbps)

flow\_limit

-   Type: integer
-   Description: Traffic class flow rate limit

self

-   Type: integer
-   Description: Traffic class ID. It is the same as the parent node key
    \"trafficClassId\"

excess

-   Type: integer
-   Description: Traffic class excess weight

priority

-   Type: integer
-   Description: Traffic class priority

max\_wait

-   Type: integer
-   Description: Traffic class max wait

Reference: appSystemDeployInfo.json

\

VXOA network role and site
==========================

/appliance/networkRoleAndSite/{neId}
------------------------------------

### GET

Summary

To verify or change a Network Role (mesh / hub / spoke) for appliances

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

### POST

Summary

To verify or change a Network Role (mesh / hub / spoke) for appliances
and to assign them to a site

Input type

application/json

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

postNetworkRulesAndSites

Description: The data to post to this API

Required: true

Parameter type: POST Body

Data Type: application/json

id

-   Type: string
-   Required: true
-   Description: Need description here

networkRole

-   Type: integer
-   Required: true
-   Description: Need description here

site

-   Type: string
-   Required: true
-   Description: Need description here

sitePriority

-   Type: integer
-   Required: true
-   Description: Need description here

Output type

None

Reference: networkRoleAndSite.json

\

VXOA login banner
=================

/banners/{neId}?cached={cached}
-------------------------------

### GET

Summary

Lists the banner messages on each appliance

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

motd

-   Type: string
-   Required: true
-   Description: The Login Message appears before the login prompt

issue

-   Type: string
-   Required: true
-   Description: The Message of the Day appears after a successful login

Reference: banners.json

\

VXOA bridge interfaces
======================

/appliance/interface/bridge/{neId}?cached={cached}
--------------------------------------------------

### GET

Summary

Lists the Bridge Interfaces State and pass through Tx Interface

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

bridge

lan0

state

-   Type: string
-   Required: true
-   Description: Need description here

link\_state

-   Type: string
-   Required: true
-   Description: Need description here

pthru\_tx\_if

-   Type: string
-   Required: true
-   Description: Need description here

wan0

state

-   Type: string
-   Required: true
-   Description: Need description here

link\_state

-   Type: string
-   Required: true
-   Description: Need description here

pthru\_tx\_if

-   Type: string
-   Required: true
-   Description: Need description here

lan1

state

-   Type: string
-   Required: true
-   Description: Need description here

link\_state

-   Type: string
-   Required: true
-   Description: Need description here

pthru\_tx\_if

-   Type: string
-   Required: true
-   Description: Need description here

wan1

state

-   Type: string
-   Required: true
-   Description: Need description here

link\_state

-   Type: string
-   Required: true
-   Description: Need description here

pthru\_tx\_if

-   Type: string
-   Required: true
-   Description: Need description here

Reference: bridgeInterfaceState.json

\

Broadcast CLI commands to VXOA appliances
=========================================

/broadcastCli
-------------

### POST

Summary

Broadcasts CLI commands to all the selected appliances

Input type

application/json

operation

Description: The data to post to this API

Required: true

Parameter type: POST Body

Data Type: application/json

neList

-   Type: array
-   Required: true
-   Description: List of Device key Ids assigned by Orchestrator,
    usually look like \'0.NE\'

cmdList

-   Type: array
-   Required: true
-   Description: Commands to be executed in a list format

Output type

None

Reference: broadcastCli.json

\

VXOA bypass
===========

/bypass
-------

### POST

Summary

Toggles bypass mode - only effective on NX appliances

Input type

application/json

operation

Description: The data to post to this API

Required: true

Parameter type: POST Body

Data Type: application/json

neList

-   Type: array
-   Required: true
-   Description: List of Device key Ids assigned by Orchestrator,
    usually look like \'0.NE\'

enable

-   Type: boolean
-   Required: true
-   Description: bypass flag

Output type

None

/bypass/{nePk}?cached={cached}
------------------------------

### GET

Summary

Get bypass mode settings of one appliance in Orchestrator.

Notes

Get bypass mode settings of one appliance in Orchestrator.

Input type

None

nePk

-   Description: The neId of the appliance in Orchestrator.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Whether to get the info from Orchestrator database.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

bypass\_config

-   Type: boolean
-   Required: true
-   Description: Tells whether it is currently configured to be in
    bypass mode

bypass\_actual

-   Type: boolean
-   Required: true
-   Description: Tells whether it is actually currently in bypass mode

status

-   Type: string
-   Required: true
-   Description: Tells whether it is actually currently in bypass or
    normal mode by text.

Reference: bypass.json

\

Orchestrator registration on appliance setting
==============================================

/gms/gmsRegistration2
---------------------

### GET

Summary

Gets setting for GMS registration on appliance

Notes

This configuration determines what appliances will use to connect to
Orchestrator. If the customDefaultIP is a blank string, Orchestrator
will send it\'s internal management IP (returned as
internalManagementIP) as the default.\
\
If any labels are configured, it will push that IP/domain instead.

Input type

None

Output type

application/json

customDefaultIP

-   Type: string
-   Required: true
-   Description: The user set default domain or IP to send to appliances

internalManagementIP

-   Type: string
-   Required: true
-   Description: The Orchestrator\'s management IP

label

-   Type: priority
-   Description: list of labels

### POST

Summary

update setting for GMS registration on appliance

Notes

Configure what IP or domain is sent to appliances (to connect to
Orchestrator). If the customDefaultIP is set to blank, Orchestrator will
send it\'s internal management IP to the appliances. Otherwise, the
default will be the customDefaultIP.\
\
Under \'label\' you can also specify which IP or domain to send if the
appliance is using the label in it\'s deployment configuration.\
\
If Orchestrator is managed by Orchestrator SP, the customDefaultIP
cannot be empty

Input type

application/json

dnsDetails

Description: Json object containing the custom default IP (if you don\'t
want to use the Orchestrator\'s internal IP), and the specific label
configuration (not required)

Required: true

Parameter type: POST Body

Data Type: application/json

customDefaultIP

-   Type: string
-   Required: true
-   Description: The user set default domain or IP to send to appliances

label

-   Type: priority
-   Description: list of labels

Output type

None

/gms/gmsRegistration
--------------------

### GET

Summary

Gets setting for GMS registration on appliance

Notes

Please use /gms/gmsRegistration2

Input type

None

Output type

application/json

defaultIPOrDns

-   Type: string
-   Required: true
-   Description: default IP or dns

label

-   Type: priority
-   Description: list of labels

### POST

Summary

update setting for GMS registration on appliance

Notes

Please use /gms/gmsRegistration2

Input type

application/json

dnsDetails

Description: Json object containing primary dns,secondary dns and domain
name

Required: true

Parameter type: POST Body

Data Type: application/json

defaultIPOrDns

-   Type: string
-   Required: true
-   Description: default IP or dns

label

-   Type: priority
-   Description: list of labels

Output type

None

Reference: gmsRegistration.json

\

Orchestrator server SMTP configuration
======================================

/gmsSMTP
--------

### GET

Summary

Gets SMTP details

Input type

None

Output type

application/json

smtp

emailAuthentication

-   Type: boolean
-   Required: true
-   Description: boolean values for emailAuthentication

password

-   Type: string
-   Required: true
-   Description: E-mail server password

userID

-   Type: string
-   Required: true
-   Description: ID

emailSender

-   Type: string
-   Required: true
-   Description: E-mail address used to send E-mails

emailSsl

-   Type: boolean
-   Required: true
-   Description: Enable or disable Enable SSL

server

-   Type: string
-   Required: true
-   Description: Address of the E-mail server

smtpPort

-   Type: integer
-   Required: true
-   Description: SMTP server port

requireEmailVerification

-   Type: boolean
-   Required: true
-   Description: Verification required before sending email

### POST

Summary

used to set SMTP settings

Input type

application/json

SMTPDetails

Description: Json object for setting values of SMPT user name, password,
email server address etc\...

Required: true

Parameter type: POST Body

Data Type: application/json

emailAuthentication

-   Type: boolean
-   Required: true
-   Description: boolean values for emailAuthentication

password

-   Type: string
-   Required: true
-   Description: E-mail server password

userID

-   Type: string
-   Required: true
-   Description: ID

emailSender

-   Type: string
-   Required: true
-   Description: E-mail address used to send E-mails

emailSsl

-   Type: boolean
-   Required: true
-   Description: Enable or disable Enable SSL

server

-   Type: string
-   Required: true
-   Description: Address of the E-mail server

smtpPort

-   Type: integer
-   Required: true
-   Description: SMTP server port

requireEmailVerification

-   Type: boolean
-   Required: true
-   Description: Verification required before sending email

Output type

None

### DELETE

Summary

Delete custom SMTP

Input type

None

Output type

None

/gmsSMTP/testEmail
------------------

### POST

Summary

used to send test mail

Input type

application/json

SMTPDetails

Description: Json object for setting values of SMPT user name, password,
email server address etc\...

Required: true

Parameter type: POST Body

Data Type: application/json

smtp

emailAuthentication

-   Type: boolean
-   Required: true
-   Description: boolean values for emailAuthentication

password

-   Type: string
-   Required: true
-   Description: E-mail server password

userID

-   Type: string
-   Required: true
-   Description: ID

emailSender

-   Type: string
-   Required: true
-   Description: E-mail address used to send E-mails

emailSsl

-   Type: boolean
-   Required: true
-   Description: Enable or disable Enable SSL

server

-   Type: string
-   Required: true
-   Description: Address of the E-mail server

smtpPort

-   Type: integer
-   Required: true
-   Description: SMTP server port

requireEmailVerification

-   Type: boolean
-   Required: true
-   Description: Verification required before sending email

emailRecepients

-   Type: string
-   Required: true
-   Description: Test email address

Output type

None

/gmsSMTP/verifyAddress
----------------------

### GET

Summary

verify an email address

Notes

Use a secure random string in the verification email to verify an email
address

Input type

None

id

-   Description: The secure random string in the verification email to
    identify an email address
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/gmsSMTP/unverifiedEmails
-------------------------

### GET

Summary

Get unverified email addresses

Notes

Get all unverified email addresses from gms db

Input type

None

Output type

None

/gmsSMTP/delUnverifiedEmails
----------------------------

### POST

Summary

Delete unverified emails

Notes

Delete unverified email addresses

Input type

application/json: undefined

emails

-   Description: a list of unverified email addresses to delete
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/gmsSMTP/sendVerificationEmail
------------------------------

### POST

Summary

Send a verification email to an email address

Notes

Send a verification email with a unique verification link including a
secure random string to an address

Input type

Unknown

address

-   Description: the address to whom will the email be sent
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: gmsSMTP.json

\

Orchestrator cache for tunnels and applications
===============================================

/cache/interfaceEndpoints
-------------------------

### GET

Summary

Get all appliances\' interface endpoints\' information including IP
addresses.

Input type

None

Output type

application/json

\<nePk\>

-   Type: array
-   Required: true
-   Description: all interface endpoints\' information of this appliance

/cache/builtinApps
------------------

### GET

Summary

Get all built in applications

Input type

None

Output type

application/json

\<appName&Type\>

-   Type: array
-   Required: true
-   Description: one built in application\'s related appliances
    information

/cache/userApps
---------------

### GET

Summary

Gets the user applications

Input type

None

Output type

application/json

\<appName&Type\>

-   Type: array
-   Required: true
-   Description: one user application\'s related appliances information

Reference: cache.json

\

Orchestrator appliance configuration synchronization
====================================================

/applianceResync
----------------

### POST

Summary

Synchronize a list of appliances.

Notes

Synchronize operation will return a string key. Use
/action/status?key=key to get the status of the operation.

Input type

application/json

ApplianceKeys

Description: JSON object containing list of appliance keys

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array
-   Required: true
-   Description: Array of appliance keys, for eg: \[\'0.NE\',\'1.NE\'\]

Output type

None

Reference: applianceResync.json

\

VXOA SNMP configuration
=======================

/snmp/{nePk}?cached={cached}
----------------------------

### GET

Summary

Get SNMP information.

Input type

None

nePk

-   Description: the primary key of appliance
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

access

rocommunity

-   Type: string
-   Required: true
-   Description: SNMP read-only community string

listen

enable

-   Type: boolean
-   Required: true
-   Description: Is SNMP agent listening at SNMP port (161)

sysdescr

-   Type: string
-   Required: true
-   Description: SNMP MIB II object \'sysDescr\' value

v3

users

admin

self

-   Type: string
-   Required: true
-   Description: admin user, only admin user is available as an SNMP v3
    user

enable

-   Type: boolean
-   Required: true
-   Description: Is admin user enabled as SNMP v3 user

hash\_type

-   Type: string
-   Required: true
-   Description: Hashing algorithm used for encrypting authentication
    password

auth\_key

-   Type: string
-   Required: true
-   Description: Hashed authentication password

privacy\_type

-   Type: string
-   Required: true
-   Description: Hashing algorithm used for encrypting privacy password

privacy\_key

-   Type: string
-   Required: true
-   Description: Hashed privacy password

syscontact

-   Type: string
-   Required: true
-   Description: SNMP MIB II object \'sysContact\' value

syslocation

-   Type: string
-   Required: true
-   Description: SNMP MIB II object \'sysLocation\' value

traps

enable

-   Type: boolean
-   Required: true
-   Description: Is SNMP trap event enabled

trap\_community

-   Type: string
-   Required: true
-   Description: Community string for SNMP trap event

auto\_launch

-   Type: boolean
-   Required: true
-   Description: Is SNMP Agent enabled at system start up

Reference: snmp.json

\

VXOA tunnel configuration
=========================

/tunnels2
---------

### GET

Summary

Search total tunnel count, all appliances

Notes

The returned data is a map of tunnel count.

Input type

None

metaData

-   Description: Sub-resource type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

totalTunnelCount

-   Type: integer
-   Required: true
-   Description: Total tunnel count

/tunnels2/physical
------------------

### GET

Summary

Search physical tunnels, all appliances

Notes

The returned data is a map of nePk -\> .

Input type

None

limit

-   Description: The max number of tunnels to return
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: number

matchingAlias

-   Description: Match the tunnel\'s alias on a text string
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

state

-   Description: The regular expression to match a tunnel state
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

1.NE

-   Type: PhysicalTunnelsIdToTunnelMap
-   Description: The tunnels for the appliance

2.NE

-   Type: PhysicalTunnelsIdToTunnelMap
-   Description: The tunnels for the appliance

/tunnels2/physical/{nePk}
-------------------------

### GET

Summary

Search physical tunnels for one appliance

Notes

The returned data is a map of .

Input type

None

nePk

-   Description: The nePk of the appliance
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: The max number of tunnels to return
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: number

matchingAlias

-   Description: Match the tunnel\'s alias on a text string
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

state

-   Description: The regular expression to match a tunnel state
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

exampleTunnelId1

-   Type: PhysicalTunnelConfig2
-   Description: The bonded tunnel for the tunnel Id

exampleTunnelId2

-   Type: PhysicalTunnelConfig2
-   Description: The bonded tunnel for the tunnel Id

/tunnels2/physical/{nePk}/{tunnelId}
------------------------------------

### GET

Summary

Get physical tunnel

Notes

Get the details of one physical tunnel with the tunnel id

Input type

None

nePk

-   Description: The nePk of the appliance
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

tunnelId

-   Description: The tunnel\'s ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

/tunnels2/getTunnelsBetweenAppliances
-------------------------------------

### POST

Summary

Get all tunnels between appliances

Notes

Use this API to get all the tunnels between a list of appliances. For
all physical tunnels, set overlayId to 0. For all bonded tunnels, set
overlayId to \'all\'. For a specific overlay, you can set overlayId to
the overlay ID

Input type

application/json: undefined

ids

Description: List of appliance IDs to retrieve the tunnels between all
of them

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

limit

-   Description: The max number of tunnels to return
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: number

matchingAlias

-   Description: Match the tunnel\'s alias on a text string
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

overlayId

-   Description: The overlay ID to match tunnels on. 0 would be all
    physical tunnels, all - would be all bonded tunnels
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

state

-   Description: The regular expression to match a tunnel state
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/tunnels/physical/state
-----------------------

### POST

Summary

Get the current list of tunnels state

Notes

The returned data is a tunnels configuration object, it contains tunnel
names like: default, passthrough and passthrough-unshaped.

Input type

application/json: undefined

neIds

Description: neIds is an array of device keys. The device keys is
assigned by Orchestrator, usually look like \'0.NE\'.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

state

-   Description: The state regular expression which is used to match a
    tunnel state
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

\<tunnel name\>

encap\_brief

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_debug

-   Type: string
-   Required: true
-   Description: Need description here

remote\_id

-   Type: integer
-   Required: true
-   Description: Need description here

cur\_set\_max\_bw

-   Type: string
-   Required: true
-   Description: current setted max bandwidth

state\_bin

-   Type: string
-   Required: true
-   Description: Need description here

encap

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_sas

-   Type: string
-   Required: true
-   Description: Need description here

cur\_mtu

-   Type: string
-   Required: true
-   Description: current mtu

rem\_sys\_bw

-   Type: string
-   Required: true
-   Description: Need description here

num\_ipsec\_sas

-   Type: string
-   Required: true
-   Description: Need description here

uptime

-   Type: integer
-   Required: true
-   Description: Tunnel up time in milliseconds

ipsec\_health

-   Type: string
-   Required: true
-   Description: Need description here

refunk

-   Type: boolean
-   Required: true
-   Description: Need description here

auto\_mtu

-   Type: integer
-   Required: true
-   Description: Determines if the tunnel should have auto MTU
    detection.

pkt

cur\_fec\_reset\_intvl

-   Type: string
-   Required: true
-   Description: Need description here

cur\_fec\_enable

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_stats

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: string
-   Required: true
-   Description: The value is a tunnel name. It is the same as parent
    node key - tunnel name

quiescence

-   Type: boolean
-   Required: true
-   Description: Need description here

details

-   Type: string
-   Required: true
-   Description: Need description here

oper

-   Type: string
-   Required: true
-   Description: Need description here

cur\_max\_bw

-   Type: string
-   Required: true
-   Description: current max bandwidth

config\_bin

-   Type: string
-   Required: true
-   Description: Need description here

qos\_stats

-   Type: string
-   Required: true
-   Description: Need description here

cur\_min\_bw

-   Type: string
-   Required: true
-   Description: current min bandwidth

/tunnels/physical/traceroute/{id}
---------------------------------

### POST

Summary

Initiate a traceroute on a tunnel

Input type

string

id

-   Description: tunnel id
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

nePk

-   Description: The nepk of the appliance
-   Required: true
-   Parameter type: POST Body
-   Data Type: string

Output type

None

/tunnels/physical/tracerouteState/{id}
--------------------------------------

### POST

Summary

Get traceroute state for a tunnel

Input type

string

id

-   Description: tunnel id
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

nePk

-   Description: The nepk of the appliance
-   Required: true
-   Parameter type: POST Body
-   Data Type: string

Output type

application/json

/tunnels/physical
-----------------

### POST

Summary

Deprecated. Get the current list of tunnels configuration and their
state information

Notes

The returned data is a tunnels configuration object, it contains tunnel
names like: default, passthrough and passthrough-unshaped.

Input type

application/json: undefined

ids

Description: ids is an array of device keys. The device key is assigned
by Orchestrator, usually look like \'0.NE\'.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

state

-   Description: The state regular expression which is used to match a
    tunnel state
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

default

tag\_name

-   Type: string
-   Required: true
-   Description: Need description here

min\_bw

-   Type: integer
-   Required: true
-   Description: Tunnel minimum bandwidth

max\_bw\_auto

-   Type: boolean
-   Required: true
-   Description: whether to automatically set maximum tunnel bandwidth

max\_bw

-   Type: integer
-   Required: true
-   Description: Tunnel maximum bandwidth

admin

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: string
-   Required: true
-   Description: The value is a tunnel name. It is the same as parent
    node key - tunnel name

ctrl\_pkt

dscp

-   Type: string
-   Required: true
-   Description: description

destination

-   Type: string
-   Required: true
-   Description: Need description here

pkt

frag\_enable

-   Type: boolean
-   Required: true
-   Description: Need description here

fec\_enable\_str

-   Type: string
-   Required: true
-   Description: Need description here

fec\_reset\_intvl

-   Type: integer
-   Required: true
-   Description: Need description here

reorder\_wait

-   Type: integer
-   Required: true
-   Description: Need description here

auto\_mtu

-   Type: boolean
-   Required: true
-   Description: Determines if the tunnel should have auto MTU detection

threshold

dbw\_aimd

-   Type: boolean
-   Required: true
-   Description: Need description here

dbw\_rserc

-   Type: boolean
-   Required: true
-   Description: Need description here

retry\_count

-   Type: integer
-   Required: true
-   Description: how many packets which are sent once per second should
    be missed before a tunnel is declared down

gre\_proto

-   Type: integer
-   Required: true
-   Description: Need description here

udp\_dest\_port

-   Type: integer
-   Required: true
-   Description: Need description here

source

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_enable

-   Type: boolean
-   Required: true
-   Description: Need description here

mode

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_arc\_window

-   Type: string
-   Required: true
-   Description: Need description here

udp\_flows

-   Type: integer
-   Required: true
-   Description: Need description here

id2

-   Type: integer
-   Required: true
-   Description: Need description here

mtu

-   Type: integer
-   Required: true
-   Description: Tunnel MTU. If \'auto\_mtu\' is false, user must set a
    specific tunnel mtu value

options

-   Type: integer
-   Required: true
-   Description: Need description here

type

-   Type: string
-   Required: true
-   Description: Need description here

uptime

-   Type: integer
-   Required: true
-   Description: Tunnel up time in milliseconds

status

-   Type: string
-   Required: true
-   Description: Need description here

isRediscoveringMTU

-   Type: boolean
-   Required: true
-   Description: Need description here

cur\_max\_bw

-   Type: integer
-   Required: true
-   Description: current maximum bandwidth

pass-through-unshaped

tag\_name

-   Type: string
-   Required: true
-   Description: Need description here

min\_bw

-   Type: integer
-   Required: true
-   Description: Tunnel minimum bandwidth

max\_bw\_auto

-   Type: boolean
-   Required: true
-   Description: whether to automatically set maximum tunnel bandwidth

max\_bw

-   Type: integer
-   Required: true
-   Description: Tunnel maximum bandwidth

admin

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: string
-   Required: true
-   Description: The value is a tunnel name. It is the same as parent
    node key - tunnel name

ctrl\_pkt

dscp

-   Type: string
-   Required: true
-   Description: description

destination

-   Type: string
-   Required: true
-   Description: Need description here

pkt

frag\_enable

-   Type: boolean
-   Required: true
-   Description: Need description here

fec\_enable\_str

-   Type: string
-   Required: true
-   Description: Need description here

fec\_reset\_intvl

-   Type: integer
-   Required: true
-   Description: Need description here

reorder\_wait

-   Type: integer
-   Required: true
-   Description: Need description here

auto\_mtu

-   Type: boolean
-   Required: true
-   Description: Determines if the tunnel should have auto MTU detection

threshold

dbw\_aimd

-   Type: boolean
-   Required: true
-   Description: Need description here

dbw\_rserc

-   Type: boolean
-   Required: true
-   Description: Need description here

retry\_count

-   Type: integer
-   Required: true
-   Description: how many packets which are sent once per second should
    be missed before a tunnel is declared down

gre\_proto

-   Type: integer
-   Required: true
-   Description: Need description here

udp\_dest\_port

-   Type: integer
-   Required: true
-   Description: Need description here

source

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_enable

-   Type: boolean
-   Required: true
-   Description: Need description here

mode

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_arc\_window

-   Type: string
-   Required: true
-   Description: Need description here

udp\_flows

-   Type: integer
-   Required: true
-   Description: Need description here

id2

-   Type: integer
-   Required: true
-   Description: Need description here

mtu

-   Type: integer
-   Required: true
-   Description: Tunnel MTU. If \'auto\_mtu\' is false, user must set a
    specific tunnel mtu value

options

-   Type: integer
-   Required: true
-   Description: Need description here

type

-   Type: string
-   Required: true
-   Description: Need description here

uptime

-   Type: integer
-   Required: true
-   Description: Tunnel up time in milliseconds

status

-   Type: string
-   Required: true
-   Description: Need description here

isRediscoveringMTU

-   Type: boolean
-   Required: true
-   Description: Need description here

cur\_max\_bw

-   Type: integer
-   Required: true
-   Description: current maximum bandwidth

\<tunnel name\>

gre\_proto

-   Type: integer
-   Required: true
-   Description: GRE protocol in the GRE header

udp\_flows

-   Type: integer
-   Required: true
-   Description: If tunnel mode is udp, this field determines how many
    different udp flows are used to distribute tunnel traffic

ipsec\_arc\_window

-   Type: string
-   Required: true
-   Description: IPSec ARC Window - not configurable.

destination

-   Type: string
-   Required: true
-   Description: Destination IP address.

admin

-   Type: string
-   Required: true
-   Description: Admin state of the tunnel - takes two values: \'up\' or
    \'down\'.

threshold

dbw\_aimd

-   Type: boolean
-   Description: Obsolete

dbw\_rserc

-   Type: boolean
-   Description: Obsolete

retry\_count

-   Type: integer
-   Description: How many packets which are sent once per second should
    be missed before a tunnel is declared down

ctrl\_pkt

frag\_enable

-   Type: boolean
-   Description: Enable tunnel packet fragmentation and reassembly

fec\_enable\_str

-   Type: string
-   Description: Enable Forward Error Correction(FEC)

fec\_reset\_intvl

-   Type: integer
-   Description: Forward Error Correction Ratio

reorder\_wait

-   Type: integer
-   Description: Amount of time in milliseconds to wait for Packet Order
    Correction algorithm

source

-   Type: string
-   Required: true
-   Description: Source IP address of the tunnel.

type

-   Type: string
-   Description: Not used

max\_bw\_auto

-   Type: boolean
-   Required: true
-   Description: If the tunnel max bandwidth needs to be set to auto,
    this field must be true.

mtu

-   Type: integer
-   Required: true
-   Description: Tunnel MTU. If \'auto\_mtu\' is false, user must set a
    specific tunnel mtu value

mode

-   Type: string
-   Required: true
-   Description: Tunnel mode: it can be one of \'gre\', \'udp\',
    \'ipsec\'

udp\_dest\_port

-   Type: integer
-   Description: If the tunnel is of type \'udp\', the udp port to use.

pkt

frag\_enable

-   Type: boolean
-   Description: Enable tunnel packet fragmentation and reassembly

fec\_enable\_str

-   Type: string
-   Description: Enable Forward Error Correction(FEC)

fec\_reset\_intvl

-   Type: integer
-   Description: Forward Error Correction Ratio

reorder\_wait

-   Type: integer
-   Description: Amount of time in milliseconds to wait for Packet Order
    Correction algorithm

auto\_mtu

-   Type: boolean
-   Required: true
-   Description: Determines if the tunnel should have auto MTU
    detection.

options

-   Type: integer
-   Description: Not used

min\_bw

-   Type: integer
-   Required: true
-   Description: Tunnel minimum bandwidth

self

-   Type: string
-   Required: true
-   Description: The value is the same as the tunnel name.

ipsec\_enable

-   Type: boolean
-   Required: true
-   Description: A quick shortcut for knowing if tunnel mode is IPSec.
    You can get the same information using mode property.

max\_bw

-   Type: integer
-   Required: true
-   Description: If the tunnel max bandwidth is manually configured, use
    this field and set max\_bw\_auto to false. Units Kbps.

allTunnelState

\<tunnel name\>

encap\_brief

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_debug

-   Type: string
-   Required: true
-   Description: Need description here

remote\_id

-   Type: integer
-   Required: true
-   Description: Need description here

cur\_set\_max\_bw

-   Type: string
-   Required: true
-   Description: current setted max bandwidth

state\_bin

-   Type: string
-   Required: true
-   Description: Need description here

encap

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_sas

-   Type: string
-   Required: true
-   Description: Need description here

cur\_mtu

-   Type: string
-   Required: true
-   Description: current mtu

rem\_sys\_bw

-   Type: string
-   Required: true
-   Description: Need description here

num\_ipsec\_sas

-   Type: string
-   Required: true
-   Description: Need description here

uptime

-   Type: integer
-   Required: true
-   Description: Tunnel up time in milliseconds

ipsec\_health

-   Type: string
-   Required: true
-   Description: Need description here

refunk

-   Type: boolean
-   Required: true
-   Description: Need description here

auto\_mtu

-   Type: integer
-   Required: true
-   Description: Determines if the tunnel should have auto MTU
    detection.

pkt

cur\_fec\_reset\_intvl

-   Type: string
-   Required: true
-   Description: Need description here

cur\_fec\_enable

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_stats

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: string
-   Required: true
-   Description: The value is a tunnel name. It is the same as parent
    node key - tunnel name

quiescence

-   Type: boolean
-   Required: true
-   Description: Need description here

details

-   Type: string
-   Required: true
-   Description: Need description here

oper

-   Type: string
-   Required: true
-   Description: Need description here

cur\_max\_bw

-   Type: string
-   Required: true
-   Description: current max bandwidth

config\_bin

-   Type: string
-   Required: true
-   Description: Need description here

qos\_stats

-   Type: string
-   Required: true
-   Description: Need description here

cur\_min\_bw

-   Type: string
-   Required: true
-   Description: current min bandwidth

pass-through

tag\_name

-   Type: string
-   Required: true
-   Description: Need description here

min\_bw

-   Type: integer
-   Required: true
-   Description: Tunnel minimum bandwidth

max\_bw\_auto

-   Type: boolean
-   Required: true
-   Description: whether to automatically set maximum tunnel bandwidth

max\_bw

-   Type: integer
-   Required: true
-   Description: Tunnel maximum bandwidth

admin

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: string
-   Required: true
-   Description: The value is a tunnel name. It is the same as parent
    node key - tunnel name

ctrl\_pkt

dscp

-   Type: string
-   Required: true
-   Description: description

destination

-   Type: string
-   Required: true
-   Description: Need description here

pkt

frag\_enable

-   Type: boolean
-   Required: true
-   Description: Need description here

fec\_enable\_str

-   Type: string
-   Required: true
-   Description: Need description here

fec\_reset\_intvl

-   Type: integer
-   Required: true
-   Description: Need description here

reorder\_wait

-   Type: integer
-   Required: true
-   Description: Need description here

auto\_mtu

-   Type: boolean
-   Required: true
-   Description: Determines if the tunnel should have auto MTU detection

threshold

dbw\_aimd

-   Type: boolean
-   Required: true
-   Description: Need description here

dbw\_rserc

-   Type: boolean
-   Required: true
-   Description: Need description here

retry\_count

-   Type: integer
-   Required: true
-   Description: how many packets which are sent once per second should
    be missed before a tunnel is declared down

gre\_proto

-   Type: integer
-   Required: true
-   Description: Need description here

udp\_dest\_port

-   Type: integer
-   Required: true
-   Description: Need description here

source

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_enable

-   Type: boolean
-   Required: true
-   Description: Need description here

mode

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_arc\_window

-   Type: string
-   Required: true
-   Description: Need description here

udp\_flows

-   Type: integer
-   Required: true
-   Description: Need description here

id2

-   Type: integer
-   Required: true
-   Description: Need description here

mtu

-   Type: integer
-   Required: true
-   Description: Tunnel MTU. If \'auto\_mtu\' is false, user must set a
    specific tunnel mtu value

options

-   Type: integer
-   Required: true
-   Description: Need description here

type

-   Type: string
-   Required: true
-   Description: Need description here

uptime

-   Type: integer
-   Required: true
-   Description: Tunnel up time in milliseconds

status

-   Type: string
-   Required: true
-   Description: Need description here

isRediscoveringMTU

-   Type: boolean
-   Required: true
-   Description: Need description here

cur\_max\_bw

-   Type: integer
-   Required: true
-   Description: current maximum bandwidth

Reference: tunnelsConfiguration.json

\

VXOA bonded tunnel configuration
================================

/tunnels2/bonded
----------------

### GET

Summary

Get bonded tunnels for all appliances

Notes

NOTE: You should use the limit query parameter to limit the bonded
tunnels returned. There could be a lot of bonded tunnels in the
network.\
\
The results are returned in an object/map of nePk -\>

Input type

None

limit

-   Description: The max number of tunnels to return
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: number

matchingAlias

-   Description: Search for all tunnels that have this string in it\'s
    alias
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

overlayId

-   Description: Only return bonded tunnels that belong to this
    overlayId.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: number

state

-   Description: The regular expression to match a tunnel state
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

1.NE

-   Type: BondedTunnelsIdToTunnelMap
-   Description: The tunnels for the appliance

2.NE

-   Type: BondedTunnelsIdToTunnelMap
-   Description: The tunnels for the appliance

/tunnels2/bonded/{nePk}
-----------------------

### GET

Summary

Get bonded tunnels for one appliance

Notes

NOTE: You should use the limit query parameter to limit the bonded
tunnels returned. There could be a lot of bonded tunnels in the
network.\
\
The results are returned in an object/map of

Input type

None

nePk

-   Description: The appliance\'s id/nePk
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: The max number of tunnels to return
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: number

matchingAlias

-   Description: Search for all tunnels that have this string in it\'s
    alias
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

overlayId

-   Description: Only return bonded tunnels that belong to this
    overlayId.
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: number

state

-   Description: The regular expression to match a tunnel state
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

exampleTunnelId1

-   Type: BondedTunnelConfig2
-   Description: The bonded tunnel for the tunnel Id

exampleTunnelId2

-   Type: BondedTunnelConfig2
-   Description: The bonded tunnel for the tunnel Id

/tunnels2/bonded/{nePk}/{bondedTunnelId}
----------------------------------------

### GET

Summary

Get a specific bonded tunnel for one appliance

Input type

None

bondedTunnelId

-   Description: The bonded tunnel id
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

nePk

-   Description: The appliance\'s id/nePk
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

id

-   Type: string
-   Description: The id of the tunnel

alias

-   Type: string
-   Description: The alias, this is the name of the tunnel used in the
    user interface

tag

-   Type: string
-   Description: The overlay name for the bonded tunnel

srcNePk

-   Type: string
-   Description: The nePk of the appliance that this bonded tunnel
    belongs to

destNePk

-   Type: string
-   Description: The nePk of the destination appliance for this tunnel

destTunnelId

-   Type: string
-   Description: The tunnel id of the opposite tunnel on the destination
    appliance

destTunnelAlias

-   Type: string
-   Description: The tunnel alias of the opposite tunnel on the
    destination appliance

operStatus

-   Type: string
-   Description: The current status of the tunnel

adminStatus

-   Type: string
-   Description: The admin status of the tunnel

overlayId

-   Type: number
-   Description: The overlay Id for the bonded tunnel

children

-   Type: array

gmsMarked

-   Type: string
-   Description: Whether or not Orchestrator added or modified this
    tunnel

/tunnels2/bondedTunnelsWithPhysicalTunnel/{nePk}/{physicalTunnelId}
-------------------------------------------------------------------

### GET

Summary

Get bonded tunnels a physical tunnel belongs to

Notes

You can use this API to get all the bonded tunnels that have this
physical tunnel bonded.

Input type

None

nePk

-   Description: The appliance\'s id/nePk
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

physicalTunnelId

-   Description: A physical tunnel ID, all bonded tunnels that have this
    tunnel as a child tunnel will be returned
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

state

-   Description: The regular expression to match a tunnel state
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

exampleTunnelId1

-   Type: BondedTunnelConfig2
-   Description: The bonded tunnel for the tunnel Id

exampleTunnelId2

-   Type: BondedTunnelConfig2
-   Description: The bonded tunnel for the tunnel Id

/tunnels/bonded/state
---------------------

### POST

Summary

Get the current list of tunnels state

Notes

The returned data is a tunnels configuration object, it contains tunnel
names like: default, passthrough and passthrough-unshaped.

Input type

application/json: undefined

neIds

Description: neIds is an array of device keys. The device keys is
assigned by Orchestrator, usually look like \'0.NE\'.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

Output type

application/json

\<tunnel name\>

encap\_brief

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_debug

-   Type: string
-   Required: true
-   Description: Need description here

remote\_id

-   Type: integer
-   Required: true
-   Description: Need description here

cur\_set\_max\_bw

-   Type: string
-   Required: true
-   Description: current setted max bandwidth

state\_bin

-   Type: string
-   Required: true
-   Description: Need description here

encap

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_sas

-   Type: string
-   Required: true
-   Description: Need description here

cur\_mtu

-   Type: string
-   Required: true
-   Description: current mtu

rem\_sys\_bw

-   Type: string
-   Required: true
-   Description: Need description here

num\_ipsec\_sas

-   Type: string
-   Required: true
-   Description: Need description here

uptime

-   Type: integer
-   Required: true
-   Description: Tunnel up time in milliseconds

ipsec\_health

-   Type: string
-   Required: true
-   Description: Need description here

refunk

-   Type: boolean
-   Required: true
-   Description: Need description here

auto\_mtu

-   Type: integer
-   Required: true
-   Description: Determines if the tunnel should have auto MTU
    detection.

pkt

cur\_fec\_reset\_intvl

-   Type: string
-   Required: true
-   Description: Need description here

cur\_fec\_enable

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_stats

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: string
-   Required: true
-   Description: The value is a tunnel name. It is the same as parent
    node key - tunnel name

quiescence

-   Type: boolean
-   Required: true
-   Description: Need description here

details

-   Type: string
-   Required: true
-   Description: Need description here

oper

-   Type: string
-   Required: true
-   Description: Need description here

cur\_max\_bw

-   Type: string
-   Required: true
-   Description: current max bandwidth

config\_bin

-   Type: string
-   Required: true
-   Description: Need description here

qos\_stats

-   Type: string
-   Required: true
-   Description: Need description here

cur\_min\_bw

-   Type: string
-   Required: true
-   Description: current min bandwidth

/tunnels/bonded
---------------

### POST

Summary

Deprecated

Notes

The returned data is a tunnels configuration object, it contains tunnel
names like: default, passthrough and passthrough-unshaped.

Input type

application/json: undefined

neIds

Description: neIds is an array of device keys. The device keys is
assigned by Orchestrator, usually look like \'0.NE\'.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

overlayId

-   Description: ID of the overlay to filter on
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

\<tunnel name\>

bound\_tun

\<boundTunnelName1\>

-   Type: string
-   Description: Physical tunnel name, value should be

\<boundTunnelName2\>

-   Type: string
-   Description: Physical tunnel name

tag\_name

-   Type: string
-   Required: true
-   Description: name of overlay

id2

-   Type: integer
-   Required: true
-   Description: id of overlay 1-7

gre\_proto

-   Type: integer
-   Required: true
-   Description: GRE protocol in the GRE header

udp\_flows

-   Type: integer
-   Required: true
-   Description: If tunnel mode is udp, this field determines how many
    different udp flows are used to distribute tunnel traffic

ipsec\_arc\_window

-   Type: string
-   Required: true
-   Description: IPSec ARC Window - not configurable.

destination

-   Type: string
-   Required: true
-   Description: Destination IP address.

admin

-   Type: string
-   Required: true
-   Description: Admin state of the tunnel - takes two values: \'up\' or
    \'down\'.

threshold

dbw\_aimd

-   Type: boolean
-   Description: Obsolete

dbw\_rserc

-   Type: boolean
-   Description: Obsolete

retry\_count

-   Type: integer
-   Description: How many packets which are sent once per second should
    be missed before a tunnel is declared down

ctrl\_pkt

frag\_enable

-   Type: boolean
-   Description: Enable tunnel packet fragmentation and reassembly

fec\_enable\_str

-   Type: string
-   Description: Enable Forward Error Correction(FEC)

fec\_reset\_intvl

-   Type: integer
-   Description: Forward Error Correction Ratio

reorder\_wait

-   Type: integer
-   Description: Amount of time in milliseconds to wait for Packet Order
    Correction algorithm

source

-   Type: string
-   Required: true
-   Description: Source IP address of the tunnel.

type

-   Type: string
-   Description: Not used

max\_bw\_auto

-   Type: boolean
-   Required: true
-   Description: If the tunnel max bandwidth needs to be set to auto,
    this field must be true.

mtu

-   Type: integer
-   Required: true
-   Description: Tunnel MTU. If \'auto\_mtu\' is false, user must set a
    specific tunnel mtu value

mode

-   Type: string
-   Required: true
-   Description: Tunnel mode: it can be one of \'gre\', \'udp\',
    \'ipsec\'

udp\_dest\_port

-   Type: integer
-   Description: If the tunnel is of type \'udp\', the udp port to use.

pkt

frag\_enable

-   Type: boolean
-   Description: Enable tunnel packet fragmentation and reassembly

fec\_enable\_str

-   Type: string
-   Description: Enable Forward Error Correction(FEC)

fec\_reset\_intvl

-   Type: integer
-   Description: Forward Error Correction Ratio

reorder\_wait

-   Type: integer
-   Description: Amount of time in milliseconds to wait for Packet Order
    Correction algorithm

auto\_mtu

-   Type: boolean
-   Required: true
-   Description: Determines if the tunnel should have auto MTU
    detection.

options

-   Type: integer
-   Description: Not used

min\_bw

-   Type: integer
-   Required: true
-   Description: Tunnel minimum bandwidth

self

-   Type: string
-   Required: true
-   Description: The value is the same as the tunnel name.

ipsec\_enable

-   Type: boolean
-   Required: true
-   Description: A quick shortcut for knowing if tunnel mode is IPSec.
    You can get the same information using mode property.

max\_bw

-   Type: integer
-   Required: true
-   Description: If the tunnel max bandwidth is manually configured, use
    this field and set max\_bw\_auto to false. Units Kbps.

allTunnelState

\<tunnel name\>

encap\_brief

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_debug

-   Type: string
-   Required: true
-   Description: Need description here

remote\_id

-   Type: integer
-   Required: true
-   Description: Need description here

cur\_set\_max\_bw

-   Type: string
-   Required: true
-   Description: current setted max bandwidth

state\_bin

-   Type: string
-   Required: true
-   Description: Need description here

encap

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_sas

-   Type: string
-   Required: true
-   Description: Need description here

cur\_mtu

-   Type: string
-   Required: true
-   Description: current mtu

rem\_sys\_bw

-   Type: string
-   Required: true
-   Description: Need description here

num\_ipsec\_sas

-   Type: string
-   Required: true
-   Description: Need description here

uptime

-   Type: integer
-   Required: true
-   Description: Tunnel up time in milliseconds

ipsec\_health

-   Type: string
-   Required: true
-   Description: Need description here

refunk

-   Type: boolean
-   Required: true
-   Description: Need description here

auto\_mtu

-   Type: integer
-   Required: true
-   Description: Determines if the tunnel should have auto MTU
    detection.

pkt

cur\_fec\_reset\_intvl

-   Type: string
-   Required: true
-   Description: Need description here

cur\_fec\_enable

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_stats

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: string
-   Required: true
-   Description: The value is a tunnel name. It is the same as parent
    node key - tunnel name

quiescence

-   Type: boolean
-   Required: true
-   Description: Need description here

details

-   Type: string
-   Required: true
-   Description: Need description here

oper

-   Type: string
-   Required: true
-   Description: Need description here

cur\_max\_bw

-   Type: string
-   Required: true
-   Description: current max bandwidth

config\_bin

-   Type: string
-   Required: true
-   Description: Need description here

qos\_stats

-   Type: string
-   Required: true
-   Description: Need description here

cur\_min\_bw

-   Type: string
-   Required: true
-   Description: current min bandwidth

Reference: bondedTunnelsConfiguration.json

\

VXOA third party tunnel configuration
=====================================

/tunnels2/passThrough
---------------------

### GET

Summary

Search pass through tunnels

Notes

This API will return all the matching tunnels given the query
parameters. Please use the \'limit\' parameter to ensure the response is
not too big.

Input type

None

limit

-   Description: The max number of tunnels to return
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: number

matchingAlias

-   Description: Search for all tunnels that have this string in it\'s
    alias
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

matchingService

-   Description: Search for all tunnels that have this string in it\'s
    Peer/service
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

state

-   Description: The regular expression to match a tunnel state
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

/tunnels2/passThrough/{nePk}
----------------------------

### GET

Summary

Search pass through tunnels for one appliance

Notes

This API will return all the matching tunnels given the query
parameters. Please use the \'limit\' parameter to ensure the response is
not too big.

Input type

None

nePk

-   Description: The appliance\'s id/nePk
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

limit

-   Description: The max number of tunnels to return
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: number

matchingAlias

-   Description: Search for all tunnels that have this string in it\'s
    alias
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

matchingService

-   Description: Search for all tunnels that have this string in it\'s
    Peer/service
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

state

-   Description: The regular expression to match a tunnel state
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

exampleTunnelId1

-   Type: PassThroughTunnel
-   Description: The pass through tunnel for this tunnel ID

exampleTunnelId2

-   Type: PassThroughTunnel
-   Description: The pass through tunnel for this tunnel ID

/tunnels2/passThrough/{nePk}/{passThroughTunnelId}
--------------------------------------------------

### GET

Summary

Get pass through tunnel

Input type

None

nePk

-   Description: The appliance\'s id/nePk
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

passThroughTunnelId

-   Description: The ID of the pass through tunnel
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

id

-   Type: string
-   Description: The ID of the pass through tunnel

alias

-   Type: string
-   Description: The alias of the tunnel

srcNePk

-   Type: string
-   Description: The nePk of the appliance the tunnel belongs to

destNePk

-   Type: string
-   Description: The nePk of the destination appliance (if applicable)

operStatus

-   Type: string
-   Description: The current state of the tunnel

adminStatus

-   Type: string
-   Description: Whether or not the tunnel is admin\'ed up or down

fecStatus

-   Type: string
-   Description: The status of the FEC

fecRatio

-   Type: number
-   Description: The ratio of the fec

sourceIpAddress

-   Type: string
-   Description: The source IP address of the tunnel

destIpAddress

-   Type: string
-   Description: The destination IP address (if applicable)

mode

-   Type: string
-   Description: The mode of the tunnel

peerName

-   Type: string
-   Description: The name of the service or peer for this pass through
    tunnel

max\_bw

-   Type: number
-   Description: The max bandwidth of the tunnel

max\_bw\_auto

-   Type: boolean

natMode

-   Type: string
-   Description: Configured NAT mode

gmsMarked

-   Type: boolean
-   Description: Whether or not Orchestrator has created/modified this
    tunnel

/tunnels/thirdParty/state
-------------------------

### POST

Summary

Get the current list of tunnels state

Notes

The returned data is a tunnels state object, it contains tunnel names
like: tunnel1, tunnel2, etc.

Input type

application/json: undefined

neIds

Description: neIds is an array of device keys. The device keys is
assigned by Orchestrator, usually look like \'0.NE\'.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

state

-   Description: The state regular expression which is used to match a
    tunnel state
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

\<tunnel name\>

encap\_brief

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_debug

-   Type: string
-   Required: true
-   Description: Need description here

remote\_id

-   Type: integer
-   Required: true
-   Description: Need description here

cur\_set\_max\_bw

-   Type: string
-   Required: true
-   Description: current setted max bandwidth

state\_bin

-   Type: string
-   Required: true
-   Description: Need description here

encap

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_sas

-   Type: string
-   Required: true
-   Description: Need description here

cur\_mtu

-   Type: string
-   Required: true
-   Description: current mtu

rem\_sys\_bw

-   Type: string
-   Required: true
-   Description: Need description here

num\_ipsec\_sas

-   Type: string
-   Required: true
-   Description: Need description here

uptime

-   Type: integer
-   Required: true
-   Description: Tunnel up time in milliseconds

ipsec\_health

-   Type: string
-   Required: true
-   Description: Need description here

refunk

-   Type: boolean
-   Required: true
-   Description: Need description here

auto\_mtu

-   Type: integer
-   Required: true
-   Description: Determines if the tunnel should have auto MTU
    detection.

pkt

cur\_fec\_reset\_intvl

-   Type: string
-   Required: true
-   Description: Need description here

cur\_fec\_enable

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_stats

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: string
-   Required: true
-   Description: The value is a tunnel name. It is the same as parent
    node key - tunnel name

quiescence

-   Type: boolean
-   Required: true
-   Description: Need description here

details

-   Type: string
-   Required: true
-   Description: Need description here

oper

-   Type: string
-   Required: true
-   Description: Need description here

cur\_max\_bw

-   Type: string
-   Required: true
-   Description: current max bandwidth

config\_bin

-   Type: string
-   Required: true
-   Description: Need description here

qos\_stats

-   Type: string
-   Required: true
-   Description: Need description here

cur\_min\_bw

-   Type: string
-   Required: true
-   Description: current min bandwidth

/tunnels/thirdParty
-------------------

### POST

Summary

Deprecated. Get the current list of tunnels configuration and their
state information

Notes

The returned data is a tunnels configuration object, it contains tunnel
names like: tunnel1, tunnel2, etc.

Input type

application/json: undefined

neIds

Description: neIds is an array of device keys. The device keys is
assigned by Orchestrator, usually look like \'0.NE\'.

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array

state

-   Description: The state regular expression which is used to match a
    tunnel state
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

\<tunnel name\>

bound\_tun

\<boundTunnelName1\>

-   Type: string
-   Description: Physical tunnel name, value should be

\<boundTunnelName2\>

-   Type: string
-   Description: Physical tunnel name

tag\_name

-   Type: string
-   Required: true
-   Description: name of overlay

id2

-   Type: integer
-   Required: true
-   Description: id of overlay 1-7

gre\_proto

-   Type: integer
-   Required: true
-   Description: GRE protocol in the GRE header

udp\_flows

-   Type: integer
-   Required: true
-   Description: If tunnel mode is udp, this field determines how many
    different udp flows are used to distribute tunnel traffic

ipsec\_arc\_window

-   Type: string
-   Required: true
-   Description: IPSec ARC Window - not configurable.

destination

-   Type: string
-   Required: true
-   Description: Destination IP address.

admin

-   Type: string
-   Required: true
-   Description: Admin state of the tunnel - takes two values: \'up\' or
    \'down\'.

threshold

dbw\_aimd

-   Type: boolean
-   Description: Obsolete

dbw\_rserc

-   Type: boolean
-   Description: Obsolete

retry\_count

-   Type: integer
-   Description: How many packets which are sent once per second should
    be missed before a tunnel is declared down

ctrl\_pkt

frag\_enable

-   Type: boolean
-   Description: Enable tunnel packet fragmentation and reassembly

fec\_enable\_str

-   Type: string
-   Description: Enable Forward Error Correction(FEC)

fec\_reset\_intvl

-   Type: integer
-   Description: Forward Error Correction Ratio

reorder\_wait

-   Type: integer
-   Description: Amount of time in milliseconds to wait for Packet Order
    Correction algorithm

source

-   Type: string
-   Required: true
-   Description: Source IP address of the tunnel.

type

-   Type: string
-   Description: Not used

max\_bw\_auto

-   Type: boolean
-   Required: true
-   Description: If the tunnel max bandwidth needs to be set to auto,
    this field must be true.

mtu

-   Type: integer
-   Required: true
-   Description: Tunnel MTU. If \'auto\_mtu\' is false, user must set a
    specific tunnel mtu value

mode

-   Type: string
-   Required: true
-   Description: Tunnel mode: it can be one of \'gre\', \'udp\',
    \'ipsec\'

udp\_dest\_port

-   Type: integer
-   Description: If the tunnel is of type \'udp\', the udp port to use.

pkt

frag\_enable

-   Type: boolean
-   Description: Enable tunnel packet fragmentation and reassembly

fec\_enable\_str

-   Type: string
-   Description: Enable Forward Error Correction(FEC)

fec\_reset\_intvl

-   Type: integer
-   Description: Forward Error Correction Ratio

reorder\_wait

-   Type: integer
-   Description: Amount of time in milliseconds to wait for Packet Order
    Correction algorithm

auto\_mtu

-   Type: boolean
-   Required: true
-   Description: Determines if the tunnel should have auto MTU
    detection.

options

-   Type: integer
-   Description: Not used

min\_bw

-   Type: integer
-   Required: true
-   Description: Tunnel minimum bandwidth

self

-   Type: string
-   Required: true
-   Description: The value is the same as the tunnel name.

ipsec\_enable

-   Type: boolean
-   Required: true
-   Description: A quick shortcut for knowing if tunnel mode is IPSec.
    You can get the same information using mode property.

max\_bw

-   Type: integer
-   Required: true
-   Description: If the tunnel max bandwidth is manually configured, use
    this field and set max\_bw\_auto to false. Units Kbps.

allTunnelState

\<tunnel name\>

encap\_brief

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_debug

-   Type: string
-   Required: true
-   Description: Need description here

remote\_id

-   Type: integer
-   Required: true
-   Description: Need description here

cur\_set\_max\_bw

-   Type: string
-   Required: true
-   Description: current setted max bandwidth

state\_bin

-   Type: string
-   Required: true
-   Description: Need description here

encap

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_sas

-   Type: string
-   Required: true
-   Description: Need description here

cur\_mtu

-   Type: string
-   Required: true
-   Description: current mtu

rem\_sys\_bw

-   Type: string
-   Required: true
-   Description: Need description here

num\_ipsec\_sas

-   Type: string
-   Required: true
-   Description: Need description here

uptime

-   Type: integer
-   Required: true
-   Description: Tunnel up time in milliseconds

ipsec\_health

-   Type: string
-   Required: true
-   Description: Need description here

refunk

-   Type: boolean
-   Required: true
-   Description: Need description here

auto\_mtu

-   Type: integer
-   Required: true
-   Description: Determines if the tunnel should have auto MTU
    detection.

pkt

cur\_fec\_reset\_intvl

-   Type: string
-   Required: true
-   Description: Need description here

cur\_fec\_enable

-   Type: string
-   Required: true
-   Description: Need description here

ipsec\_stats

-   Type: string
-   Required: true
-   Description: Need description here

self

-   Type: string
-   Required: true
-   Description: The value is a tunnel name. It is the same as parent
    node key - tunnel name

quiescence

-   Type: boolean
-   Required: true
-   Description: Need description here

details

-   Type: string
-   Required: true
-   Description: Need description here

oper

-   Type: string
-   Required: true
-   Description: Need description here

cur\_max\_bw

-   Type: string
-   Required: true
-   Description: current max bandwidth

config\_bin

-   Type: string
-   Required: true
-   Description: Need description here

qos\_stats

-   Type: string
-   Required: true
-   Description: Need description here

cur\_min\_bw

-   Type: string
-   Required: true
-   Description: current min bandwidth

Reference: thirdPartyTunnelsConfiguration.json

\

Orchestrator Radius and TACACS+
===============================

/gmsRemoteAuth
--------------

### GET

Summary

Get current Radius or TACACS+ configuration

Input type

None

Output type

application/json

protocol

-   Type: integer
-   Required: true
-   Description: Need description here

protocolStr

-   Type: string
-   Required: true
-   Description: Need description here

primaryHost

-   Type: string
-   Required: true
-   Description: Need description here

secondaryHost

-   Type: null
-   Required: true
-   Description: Need description here

primaryPort

-   Type: null
-   Required: true
-   Description: Need description here

secondaryPort

-   Type: null
-   Required: true
-   Description: Need description here

primaryRadiusKey

-   Type: string
-   Required: true
-   Description: Need description here

secondaryRadiusKey

-   Type: null
-   Required: true
-   Description: Need description here

type

-   Type: string
-   Required: true
-   Description: Need description here

radiusType

-   Type: string
-   Required: true
-   Description: Need description here

authOrder

-   Type: integer
-   Required: true
-   Description: Need description here

authOrderStr

-   Type: string
-   Required: true
-   Description: Need description here

netReadWritePrivilege

-   Type: integer
-   Required: true
-   Description: Need description here

netReadPrivilege

-   Type: integer
-   Required: true
-   Description: Need description here

### POST

Summary

Modify Radius or TACACS+ configuration

Input type

application/json

remoteAuthData

Description:

Required: true

Parameter type: POST Body

Data Type: application/json

hasChanges

-   Type: boolean
-   Required: true
-   Description: Need description here

hasError

-   Type: boolean
-   Required: true
-   Description: Need description here

protocolStr

-   Type: string
-   Required: true
-   Description: Need description here

netReadWritePrivilege

-   Type: integer
-   Required: true
-   Description: Need description here

netReadPrivilege

-   Type: integer
-   Required: true
-   Description: Need description here

type

-   Type: string
-   Required: true
-   Description: Need description here

radiusType

-   Type: string
-   Required: true
-   Description: Need description here

authOrderStr

-   Type: string
-   Required: true
-   Description: Need description here

primaryHost

-   Type: string
-   Required: true
-   Description: Need description here

primaryPort

-   Type: string
-   Required: true
-   Description: Need description here

primaryRadiusKey

-   Type: string
-   Required: true
-   Description: Need description here

secondaryHost

-   Type: string
-   Required: true
-   Description: Need description here

secondaryPort

-   Type: string
-   Required: true
-   Description: Need description here

secondaryRadiusKey

-   Type: string
-   Required: true
-   Description: Need description here

privileges

-   Type: array
-   Required: true
-   Description: Consists of the following items :
    \[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15\]

tacascAuth

-   Type: array
-   Required: true
-   Description: Consists of the following items :
    \[\"CHAP\",\"PAP\",\"MSCHAP\"\]

radiusAuth

-   Type: array
-   Required: true
-   Description: Consists of the following items :
    \[\"CHAP\",\"PAP\",\"MSCHAP\",\"MSCHAPv2\",\"EAP-MSCHAPv2\"\]

authenticationOrder

-   Type: array
-   Required: true
-   Description: Consists of the following items : \[\"Local first\",
    \"Remote first\"\]

Output type

None

Reference: gmsRemoteAuth.json

\

Upgrade image for appliances
============================

/upgradeAppliances
------------------

### POST

Summary

Upgrade appliances with option

Notes

The data to post need to contain image filename, install option and nePk
id list .

Input type

application/json

ApplianceUpgradeEntry

Description: Upgrade appliances post data in JSON

Required: true

Parameter type: POST Body

Data Type: application/json

neList

-   Type: array
-   Required: true
-   Description: The nePk id of appliance.

installOption

-   Type: string
-   Required: true
-   Description: The install option. It include: install\_only,
    install\_switch and install\_reboot.

imageName

-   Type: string
-   Required: true
-   Description: The image name that will use to upgrade for the
    appliances.

version

-   Type: string
-   Required: true
-   Description: Version of software to upgrade to

Output type

application/json

clientKey

-   Type: string
-   Required: true
-   Description: The client key of appliances upgrade background task.

Reference: upgradeAppliances.json

\

Get and modify the discovery configuration
==========================================

/gms/discovery
--------------

### GET

Summary

Returns the configuration for Discovery

Input type

None

Output type

application/json

emails

-   Type: array
-   Description: An array of email addresses to send the discovery
    (discovered, approved and denied EdgeConnects) email to.

/gms/discovery
--------------

### POST

Summary

Updates the discovery configuration with the object passed. Note:
Replaces the whole configuration with the configuration passed

Input type

application/json: undefined

emails

-   Description: An array of email addresses to send the discovery
    (discovered, approved and denied EdgeConnects) email to.
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

Reference: discovery.json

\

Get and apply the appliance wizard
==================================

/gms/applianceWizard/apply/{nePk}
---------------------------------

### GET

Summary

Get the handle for the currently applying wizard.

Notes

Returns a handle to poll. Poll handle with
/action/status?key={actionGroupId}

Input type

None

nePk

-   Description: The nePk of the appliance
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

actionGroupId

-   Type: String
-   Description: The handle to poll status with using
    /action/status?key={actionGroupId}

### POST

Summary

Applies the wizard to an appliance in the background

Notes

Applies the wizard to an appliance in the background. The wizard is
broken into steps and each step is represented by an object in
\'wizardData\'. Every step is optional, if you don\'t want a step to be
applied then delete it from the JSON when you POST. Subnets is special
in that it will post to /subnets for \<8.1.7 appliances and /subnets2
for \>=8.1.7 appliances. If you are using an older appliance you will
need to adjust the subnets.data section to match the appliance API
format. The actionGroupId is used to poll the status of applying the
wizard with POST /action/status.

Input type

application/json: undefined

ApplyWizardPost

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

nePk

-   Description: The nePk of the appliance
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

actionGroupId

-   Type: String
-   Description: The handle to poll status with using
    /action/status?key={actionGroupId}

Reference: applianceWizard.json

\

Get and apply appliance preconfigurations
=========================================

/gms/appliance/preconfiguration
-------------------------------

### GET

Summary

Get all preconfigurations

Input type

None

filter

-   Description: Any additional filters to be applied
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

### POST

Summary

Create a preconfiguration

Input type

application/json: undefined

createPreconfig

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

application/json

id

-   Type: string
-   Description: ID of preconfiguration

/gms/appliance/preconfiguration/{preconfigId}
---------------------------------------------

### POST

Summary

Modify one preconfiguration

Input type

application/json: undefined

createPreconfig

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

preconfigId

-   Description: The ID of the preconfiguration
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

### GET

Summary

Get one preconfiguration

Input type

None

preconfigId

-   Description: The ID of the preconfiguration
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

id

-   Type: integer
-   Description: Id of the preconfiguration, used to reference this
    preconfiguration in the other APIs

name

-   Type: string
-   Description: Name of preconfiguration

serialNum

-   Type: string
-   Description: Serial number to match on

tag

-   Type: string
-   Description: Tag to match on

comment

-   Type: string
-   Description: User provided comment for the preconfiguration

autoApply

-   Type: boolean
-   Description: Automatically apply this preconfiguration to the
    matched appliance when Orchestrator discovers it

configData

-   Type: string
-   Description: Base64 encoded preconfiguration YAML string

createdtime

-   Type: integer
-   Description: Time when preconfiguration was created in epoch
    milliseconds

modifiedtime

-   Type: integer
-   Description: Time when preconfiguration was last modified in epoch
    milliseconds

nepk

-   Type: string
-   Description: Appliance ID that preconfig was last applied to

discoveredId

-   Type: string
-   Description: Discovered appliance ID that preconfig was last applied
    to (the id of appliances in the discovered tab are different than
    when the appliance is approved)

guid

-   Type: string
-   Description: ID of the log created in actionlog when the
    preconfiguration was applied

taskStatus

-   Type: integer
-   Description: The current status of the preconfig apply. 0: Not
    Started, 1: In Progress, 2: Finished

completionStatus

-   Type: boolean
-   Description: Whether the apply was successful or not, only look at
    this if taskStatus == 2

starttime

-   Type: integer
-   Description: Start time of last apply in epoch milliseconds

endtime

-   Type: integer
-   Description: End time of last apply in epoch milliseconds

result

-   Type: array

### DELETE

Summary

Delete one preconfiguration

Input type

None

preconfigId

-   Description: The ID of the preconfiguration
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/gms/appliance/preconfiguration/findMatch
-----------------------------------------

### POST

Summary

Get the first matching preconfiguration, matching by serial then tag

Input type

application/json: undefined

matchPreconfig

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

application/json

id

-   Type: integer
-   Description: Id of the preconfiguration, used to reference this
    preconfiguration in the other APIs

name

-   Type: string
-   Description: Name of preconfiguration

serialNum

-   Type: string
-   Description: Serial number to match on

tag

-   Type: string
-   Description: Tag to match on

comment

-   Type: string
-   Description: User provided comment for the preconfiguration

autoApply

-   Type: boolean
-   Description: Automatically apply this preconfiguration to the
    matched appliance when Orchestrator discovers it

configData

-   Type: string
-   Description: Base64 encoded preconfiguration YAML string

createdtime

-   Type: integer
-   Description: Time when preconfiguration was created in epoch
    milliseconds

modifiedtime

-   Type: integer
-   Description: Time when preconfiguration was last modified in epoch
    milliseconds

nepk

-   Type: string
-   Description: Appliance ID that preconfig was last applied to

discoveredId

-   Type: string
-   Description: Discovered appliance ID that preconfig was last applied
    to (the id of appliances in the discovered tab are different than
    when the appliance is approved)

guid

-   Type: string
-   Description: ID of the log created in actionlog when the
    preconfiguration was applied

taskStatus

-   Type: integer
-   Description: The current status of the preconfig apply. 0: Not
    Started, 1: In Progress, 2: Finished

completionStatus

-   Type: boolean
-   Description: Whether the apply was successful or not, only look at
    this if taskStatus == 2

starttime

-   Type: integer
-   Description: Start time of last apply in epoch milliseconds

endtime

-   Type: integer
-   Description: End time of last apply in epoch milliseconds

result

-   Type: array

/gms/appliance/preconfiguration/default
---------------------------------------

### GET

Summary

Get the default preconfiguration, this contains the template YAML which
has all possible configuration items. You will have to base64 decode
configData to see the YAML in plain text

Input type

None

Output type

None

/gms/appliance/preconfiguration/validate
----------------------------------------

### POST

Summary

Get the first matching preconfiguration, matching by serial then tag

Input type

application/json: undefined

createPreconfig

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/gms/appliance/preconfiguration/{preconfigId}/apply/discovered/{discoveredId}
-----------------------------------------------------------------------------

### POST

Summary

Apply preconfiguration to discovered appliance

Notes

Use this API when you want to apply preconfiguration to a discovered
appliance (this is an appliance that is in the discovered tab in the
UI). Applying will happen in the asynchronously, so poll using GET
/gms/appliance/preconfiguration/{preconfigId}/apply.

Input type

Unknown

preconfigId

-   Description: The ID of the preconfiguration
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

discoveredId

-   Description: The ID of the target discovered appliance
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/gms/appliance/preconfiguration/{preconfigId}/apply/{nePk}
----------------------------------------------------------

### POST

Summary

Apply preconfiguration to an already managed appliance

Notes

Use this API when you want to apply preconfiguration to a managed
appliance (this is an appliance that is in the tree in the UI). Applying
will happen in the asynchronously, so poll using GET
/gms/appliance/preconfiguration/{preconfigId}/apply.

Input type

Unknown

preconfigId

-   Description: The ID of the preconfiguration
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

nePk

-   Description: The ID of the target managed appliance
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/gms/appliance/preconfiguration/{preconfigId}/apply
---------------------------------------------------

### GET

Summary

Get the apply status of a preconfiguration

Notes

Get the apply status of a preconfiguration

Input type

None

preconfigId

-   Description: The ID of the preconfiguration
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

id

-   Type: integer
-   Description: Id of the preconfiguration, used to reference this
    preconfiguration in the other APIs

guid

-   Type: string
-   Description: ID of the log created in actionlog when the
    preconfiguration was applied

taskStatus

-   Type: integer
-   Description: The current status of the preconfig apply. 0: Not
    Started, 1: In Progress, 2: Finished

completionStatus

-   Type: boolean
-   Description: Whether the apply was successful or not, only look at
    this if taskStatus == 2

starttime

-   Type: integer
-   Description: Start time of last apply in epoch milliseconds

endtime

-   Type: integer
-   Description: End time of last apply in epoch milliseconds

result

-   Type: array

Reference: appliancePreconfig.json

\

Gets all third party licenses information
=========================================

/thirdPartyLicenses
-------------------

### GET

Summary

Gets all third party licenses information

Input type

None

format

-   Description: Type of the response format(currently it supports
    \'txt\' only).
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

Reference: thirdPartyLicenses.json

\

License info about NX, VX, EC, and CPX appliances
=================================================

/license/nx
-----------

### GET

Summary

Retrieves NX licensed appliances

Input type

None

Output type

None

/license/vx
-----------

### GET

Summary

Retrieves VX licensed appliances

Input type

None

Output type

None

/license/portal/summary
-----------------------

### GET

Summary

Retrieves summary of portal licensed appliances

Input type

None

Output type

application/json

licenses

accountKey

key

-   Type: string
-   Required: true

fx

enable

-   Type: boolean
-   Required: true

boost

-   Type: integer
-   Required: true

mini

-   Type: integer
-   Required: true

base

-   Type: integer
-   Required: true

plus

-   Type: integer
-   Required: true

expire

-   Type: integer
-   Required: true

boostExpire

-   Type: integer
-   Required: true

tier

\<tier\>

display

-   Type: string
-   Required: true

count

-   Type: integer
-   Required: true

cpx

enable

-   Type: boolean
-   Required: true

expire

-   Type: integer
-   Required: true

saas

enable

-   Type: boolean
-   Required: true

expire

-   Type: integer
-   Required: true

ecsp

enable

-   Type: boolean
-   Required: true

metered

enable

-   Type: boolean
-   Required: true

expire

-   Type: integer
-   Required: true

licenseState

fx

numBase

-   Type: integer
-   Required: true

numBoost

-   Type: integer
-   Required: true

numPlus

-   Type: integer
-   Required: true

numTier

\<tier\>

-   Type: integer
-   Required: true

/license/portal/appliance
-------------------------

### GET

Summary

Retrieves portal licensed appliances

Input type

None

Output type

None

/license/portal/ec/{nePk}
-------------------------

### POST

Summary

Changes ec appliance license settings

Input type

application/json: undefined

nePk

-   Description: Appliance primary key
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

applianceInfo

Description: Appliance license information

Required: true

Parameter type: POST Body

Data Type: application/json

license

mini

enable

-   Type: boolean
-   Required: true
-   Description: enable or disable mini license request

plus

enable

-   Type: boolean
-   Required: true
-   Description: enable or disable plus license request

tier

display

-   Type: string
-   Required: true
-   Description: Display name for tier license request

bandwidth

-   Type: integer
-   Required: true
-   Description: Tier bandwidth request in kbps

boost

enable

-   Type: boolean
-   Required: true
-   Description: enable or disable boost license request

bandwidth

-   Type: integer
-   Required: true
-   Description: Boost bandwidth request in kbps

Output type

application/json

license

mini

enable

-   Type: boolean
-   Required: true
-   Description: enable or disable mini license request

plus

enable

-   Type: boolean
-   Required: true
-   Description: enable or disable plus license request

tier

display

-   Type: string
-   Required: true
-   Description: Display name for tier license request

bandwidth

-   Type: integer
-   Required: true
-   Description: Tier bandwidth request in kbps

boost

enable

-   Type: boolean
-   Required: true
-   Description: enable or disable boost license request

bandwidth

-   Type: integer
-   Required: true
-   Description: Boost bandwidth request in kbps

/license/portal/appliance/grant/{nePk}
--------------------------------------

### POST

Summary

Grant an appliance a base license via Cloud Portal

Input type

Unknown

nePk

-   Description: Appliance primary key
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/license/portal/appliance/revoke/{nePk}
---------------------------------------

### POST

Summary

Revoke an appliance a base license via Cloud Portal

Input type

Unknown

nePk

-   Description: Appliance primary key
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/license/portal/appliance/license/token/{nePk}
----------------------------------------------

### DELETE

Summary

Delete an appliance\'s license token via the appliance

Notes

Used after revoking an appliance. Warning: This will stop the appliance
from passing traffic immediately.

Input type

None

nePk

-   Description: Appliance primary key
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: license.json

\

Configure the timezone for the scheduled jobs and reports
=========================================================

/gms/scheduleTimezone
---------------------

### GET

Summary

Returns the configured timezone for scheduled jobs and reports

Input type

None

Output type

application/json

defaultTimezone

-   Type: string
-   Description: An array of email addresses to send the discovery
    (discovered, approved and denied EdgeConnects) email to.

/gms/scheduleTimezone
---------------------

### POST

Summary

Updates the schedule timezone used with for the scheduled jobs and
reports.

Input type

string

defaultTimezone

-   Description: The timezone to set for the scheduled jobs and reports.
-   Required: true
-   Parameter type: POST Body
-   Data Type: string

Output type

None

Reference: scheduleTimezone.json

\

Configuration for the topology tab
==================================

/gms/topologyConfig
-------------------

### GET

Summary

Returns the configuration for the topology tab. The key in the
configuration will be the username.

Input type

None

userName

-   Description: The username you want to retrieve the topology config
    for
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

key

-   Type: PreferenceResponse
-   Description: The topology configuration will be stored under the key
    \'default\' or the user\'s username (depending on if you sent the
    request with a username or not.

### POST

Summary

Updates the topology configuration for the given user

Input type

application/json: undefined

userName

-   Description: The username you want to update the topology config for
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

request

-   Description: The topology configuration to save, where the user\'s
    username is the key.
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/gms/topologyConfig/{username}
------------------------------

### GET

Summary

Deprecated. Use GET /gms/topologyConfig with the username query
parameter

Input type

None

username

-   Description: The username of the user to retrieve the topology
    configuration of.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

key

-   Type: PreferenceResponse
-   Description: The topology configuration will be stored under the key
    \'default\' or the user\'s username (depending on if you sent the
    request with a username or not.

### POST

Summary

Deprecated. Use POST /gms/topologyConfig with the username query
parameter

Input type

application/json: undefined

username

-   Description: The username to save the configuration under.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

request

-   Description: The topology configuration to save, where the user\'s
    username is the key.
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/gms/topologyConfig/map
-----------------------

### POST

Summary

Update the map image

Input type

application/json: undefined

default

-   Description: The map image to save.
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

Reference: topologyConfig.json

\

Configuration for new dynamic topology tab
==========================================

/gms/dynamicTopologyConfig
--------------------------

### GET

Summary

Returns the configuration for the dynamic topology tab.

Input type

None

userName

-   Description: The username you want to retrieve the topology config
    for
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

key

-   Type: DynamicPreferenceResponse
-   Description: The dynamic topology configuration will be stored under
    the key \'default\' or the user\'s username (depending on if you
    sent the request with a username or not.

### POST

Summary

Updates the dynamic topology configuration for the given user

Input type

application/json: undefined

userName

-   Description: The username you want to update the topology config for
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

request

-   Description: The topology configuration to save, where the user\'s
    username is the key.
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/gms/dynamicTopologyConfig/{username}
-------------------------------------

### GET

Summary

Deprecated. Use the GET /gms/dynamicTopologyConfig with the userName
query parameter

Input type

None

username

-   Description: The username of the user to retrieve the dynamic
    topology configuration of.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

key

-   Type: DynamicPreferenceResponse
-   Description: The dynamic topology configuration will be stored under
    the key \'default\' or the user\'s username (depending on if you
    sent the request with a username or not.

### POST

Summary

Deprecated. Use POST /gms/dynamicTopologyConfig with the username query
parameter

Input type

application/json: undefined

username

-   Description: The username to save the configuration under.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

request

-   Description: The dynamic topology configuration to save, where the
    user\'s username is the key.
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

Reference: dynamicTopologyConfig.json

\

Get session information
=======================

/loginSessions
--------------

### GET

Summary

Returns all the current sessions

Input type

None

Output type

None

Reference: session.json

\

Save and delete labels for interfaces
=====================================

/gms/interfaceLabels
--------------------

### GET

Summary

Returns all the interface labels saved

Input type

None

active

-   Description: Boolean flag to return only the active (active = true)
    or only inactive (acive = false)
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

wan

-   Type: SampleLabelSet
-   Description: A map of wan labels. The key of the label is the label
    id

lan

-   Type: SampleLabelSet
-   Description: A map of lan labels. The key of the label is the label
    id

### POST

Summary

Save interface labels, will completely replace the current
implementation.

Notes

Save interface labels, will completely replace the current
implementation. You cannot remove labels that are in use in an overlay

Input type

application/json: undefined

New labels

-   Description: Object of labels you wish to save (will overwrite the
    current lan labels list). To remove a label, set the \'active\' to
    false.
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

deleteDependencies

-   Description: Whether or not you want to delete the labels from port
    profiles and templates using it
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

/gms/interfaceLabels/{type}
---------------------------

### GET

Summary

Returns all the labels for the given type

Input type

None

type

-   Description: The type of interface that you want to retrieve the
    list of labels of \[wan, lan\].
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

active

-   Description: Boolean flag to return only the active (active = true)
    or only inactive (acive = false)
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

/interfaceLabels/{nePk}
-----------------------

### POST

Summary

Pushes the active interface labels on Orchestrator to appliance

Input type

Unknown

nePk

-   Description: The nePk of the appliance you wish to apply the labels
    to
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: interfaceLabels.json

\

Add, update, delete overlay configurations
==========================================

/gms/overlays/config
--------------------

### GET

Summary

All the overlays stored in the Orchestrator

Input type

None

Output type

None

### POST

Summary

Saves a new overlay to the Orchestrator

Notes

Use this API to add a new overlay configuration. If successful, the API
will return the ID of the overlay added.\
\
Please see the model for more information (click \'Model\' under Data
Type)\
Valid values:\
Topology type:

-   1 - Mesh
-   2 - Hub and Spoke

\
Bonding Policy:\

-   1 - High availability
-   2 - High quality
-   3 - High throughput
-   4 - High efficiency

\
Overlay Fallback Option\

-   1 - pass-through
-   3 - drop
-   label\_{labelId} - pass-through an interface with a specific label

\
Internet Policy

-   **policyList** - The policy list is what policies to take, in order,
    for internet traffic. If the first policy fails, it will try the
    next one. The items in the list can be one of the options below, or
    a service name. The service name needs to match the peer name of a
    passthrough tunnel on the appliance.
    -   backhaul\_via\_overlay - Send the internet traffic back through
        overlay
    -   local\_breakout - Break out locally through specific interfaces
        (must configure interface label IDs in localBreakout)
-   **localBreakout** - Two lists (primary/backup) of the interface
    labels to break out of (see model for more details)

Input type

application/json: undefined

New overlay

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/gms/overlays/config/regions
----------------------------

### GET

Summary

All the overlays stored in the Orchestrator keyed by overlayId and
regionId

Input type

None

Output type

None

### POST

Summary

Post regional overlay configurations

Notes

Use this API to create an exhaustive representation of regionalized
overlays. The body of this request should be an array of map which keyed
by region id whose value is regional overlay configuration. Please see
the model for more information (click \'Model\' under Data Type)\
Valid values:\
Topology type:

-   1 - Mesh
-   2 - Hub and Spoke

\
Bonding Policy:\

-   1 - High availability
-   2 - High quality
-   3 - High throughput
-   4 - High efficiency

\
Overlay Fallback Option\

-   1 - pass-through
-   3 - drop
-   label\_{labelId} - pass-through an interface with a specific label

\
Internet Policy

-   **policyList** - The policy list is what policies to take, in order,
    for internet traffic. If the first policy fails, it will try the
    next one. The items in the list can be one of the options below, or
    a service name. The service name needs to match the peer name of a
    passthrough tunnel on the appliance.
    -   backhaul\_via\_overlay - Send the internet traffic back through
        overlay
    -   local\_breakout - Break out locally through specific interfaces
        (must configure interface label IDs in localBreakout)
-   **localBreakout** - Two lists (primary/backup) of the interface
    labels to break out of (see model for more details)

Input type

application/json: undefined

All Overlays

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

### PUT

Summary

PUT regional overlay configurations

Notes

Use this API to update an exhaustive representation of regionalized
overlays. The body of this request should be a map keyed by overlayId
whose value is a map of regionId to regional overlay configuration.
Please see the model for more information (click \'Model\' under Data
Type)\
Valid values:\
Topology type:

-   1 - Mesh
-   2 - Hub and Spoke

\
Bonding Policy:\

-   1 - High availability
-   2 - High quality
-   3 - High throughput
-   4 - High efficiency

\
Overlay Fallback Option\

-   1 - pass-through
-   3 - drop
-   label\_{labelId} - pass-through an interface with a specific label

\
Internet Policy

-   **policyList** - The policy list is what policies to take, in order,
    for internet traffic. If the first policy fails, it will try the
    next one. The items in the list can be one of the options below, or
    a service name. The service name needs to match the peer name of a
    passthrough tunnel on the appliance.
    -   backhaul\_via\_overlay - Send the internet traffic back through
        overlay
    -   local\_breakout - Break out locally through specific interfaces
        (must configure interface label IDs in localBreakout)
-   **localBreakout** - Two lists (primary/backup) of the interface
    labels to break out of (see model for more details)

Input type

application/json: undefined

All Overlays

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/gms/overlays/config/{overlayId}
--------------------------------

### GET

Summary

Returns one overlay that has the id of the passed overlayId parameter

Input type

None

overlayId

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

name

-   Type: string
-   Description: The name of the overlay.

id

-   Type: string
-   Description: The id of the overlay.

topology

-   Type: OverlayTopology
-   Description: Topology configuration for this overlay

match

-   Type: OverlayMatch
-   Description: Match configuration

brownoutThresholds

-   Type: BrownoutThreshold
-   Description: Brownout thresholds configuration

wanPorts

-   Type: OverlayWanPorts
-   Description: Wan port configuration

bondingPolicy

-   Type: number
-   Description: The number of the bonding policy for the overlay. 1 -
    high availability, 2 - high throughput.

useBackupOnBrownout

-   Type: boolean
-   Description: Use the backup WAN ports when the primary WAN ports
    meet the brownout conditions.

trafficClass

-   Type: string
-   Description: The shaper traffic class to use in the overlay.

boostTraffic

-   Type: boolean
-   Description: Whether or not to boost the traffic for this overlay.

lanDscp

-   Type: string
-   Description: The LAN QoS to use in the QoS policy created for this
    Overlay. See the QoS policy page for valid values

wanDscp

-   Type: string
-   Description: The WAN QoS to use in the QoS policy created for this
    Overlay. See the QoS policy page for valid values

overlayFallbackOption

-   Type: number
-   Description: The fallback option of the overlay.

internetPolicy

-   Type: InternetPolicy
-   Description: Describes what to do with internet traffic if it
    matches the overlay

hubInternetPolicies

-   Type: HubInternetPolicies
-   Description: Map of appliance id to internet policy for a hub in an
    overlay

tunnelUsagePolicy

-   Type: SampleTunnelUsagePolicy
-   Description: A set of policy details to determine in which order do
    what links get used. The keys are integers as strings, the integers
    will be used to determine the order. The condition and links in the
    policy detail are used to determine if that policy detail is used,
    or if the next one should be used.

### PUT

Summary

Update the properties of an existing overlay

Notes

Use this API to update an existing overlay configuration. If you are
using regions and have customized regional BIO per region, it will be
overridden. To avoid this use the other PUT api which takes regionID as
an input. Any changes made to the overlay configuration will be pushed
to the appliances automatically.\
\
Please see the model for more information (click \'Model\' under Data
Type)\
Valid values:\
Topology type:

-   1 - Mesh
-   2 - Hub and Spoke

\
Bonding Policy:\

-   1 - High availability
-   2 - High quality
-   3 - High throughput
-   4 - High efficiency

\
Overlay Fallback Option\

-   1 - pass-through
-   3 - drop
-   label\_{labelId} - pass-through an interface with a specific label

\
Internet Policy

-   **policyList** - The policy list is what policies to take, in order,
    for internet traffic. If the first policy fails, it will try the
    next one. The items in the list can be one of the options below, or
    a service name. The service name needs to match the peer name of a
    passthrough tunnel on the appliance.
    -   backhaul\_via\_overlay - Send the internet traffic back through
        overlay
    -   local\_breakout - Break out locally through specific interfaces
        (must configure interface label IDs in localBreakout)
-   **localBreakout** - Two lists (primary/backup) of the interface
    labels to break out of (see model for more details)

Input type

application/json: undefined

overlayId

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Updated overlay

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

### DELETE

Summary

Deletes the overlay from the Orchestrator. Removes any appliances from
the overlay and deletes any reports specifically for this overlay.

Input type

None

overlayId

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/gms/overlays/config/regions/{overlayId}/{regionId}
---------------------------------------------------

### GET

Summary

Get regional overlay by overlayId and regionId

Input type

None

overlayId

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

regionId

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

name

-   Type: string
-   Description: The name of the overlay.

id

-   Type: string
-   Description: The id of the overlay.

topology

-   Type: OverlayTopology
-   Description: Topology configuration for this overlay

match

-   Type: OverlayMatch
-   Description: Match configuration

brownoutThresholds

-   Type: BrownoutThreshold
-   Description: Brownout thresholds configuration

wanPorts

-   Type: OverlayWanPorts
-   Description: Wan port configuration

bondingPolicy

-   Type: number
-   Description: The number of the bonding policy for the overlay. 1 -
    high availability, 2 - high throughput.

useBackupOnBrownout

-   Type: boolean
-   Description: Use the backup WAN ports when the primary WAN ports
    meet the brownout conditions.

trafficClass

-   Type: string
-   Description: The shaper traffic class to use in the overlay.

boostTraffic

-   Type: boolean
-   Description: Whether or not to boost the traffic for this overlay.

lanDscp

-   Type: string
-   Description: The LAN QoS to use in the QoS policy created for this
    Overlay. See the QoS policy page for valid values

wanDscp

-   Type: string
-   Description: The WAN QoS to use in the QoS policy created for this
    Overlay. See the QoS policy page for valid values

overlayFallbackOption

-   Type: number
-   Description: The fallback option of the overlay.

internetPolicy

-   Type: InternetPolicy
-   Description: Describes what to do with internet traffic if it
    matches the overlay

hubInternetPolicies

-   Type: HubInternetPolicies
-   Description: Map of appliance id to internet policy for a hub in an
    overlay

tunnelUsagePolicy

-   Type: SampleTunnelUsagePolicy
-   Description: A set of policy details to determine in which order do
    what links get used. The keys are integers as strings, the integers
    will be used to determine the order. The condition and links in the
    policy detail are used to determine if that policy detail is used,
    or if the next one should be used.

### PUT

Summary

Modify regional overlay

Input type

application/json: undefined

overlayId

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

regionId

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

overlay config

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/gms/overlays/config/maxNumOfOverlays
-------------------------------------

### GET

Summary

Get the max number of overlays

Notes

Get the max number of overlays you are allowed to have on this
Orchestrator

Input type

None

Output type

application/json

max

-   Type: number
-   Description: The max number of overlays you are allowed to have on
    this orchestrator

/gms/overlays/priority
----------------------

### GET

Summary

Get the overlay priority order

Notes

This API returns a JSON object where the key is the priority, and the
value of the key is the overlay ID with that priority.\
\
Example:\
{ 1 : 2, 2 : 3, 3 : 1 }\
This would order the overlays with the overlay with ID 2 first, overlay
with ID 3 second, etc.

Input type

None

Output type

application/json

1

-   Type: number

2

-   Type: number

### POST

Summary

Change the overlay priorities

Notes

This API sets the overlay priorities, specifically the order of the
different overlays\' route maps. The priority is a JSON object where the
key is the priority, and the value of the key is the overlay ID with
that priority.\
\
Example:\
{ 1 : 2, 2 : 3, 3 : 1 }\
This would order the overlays with the overlay with ID 2 first, overlay
with ID 3 second, etc.

Input type

application/json: undefined

Overlay priority

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

Reference: overlays.json

\

Manage Deployment Profiles
==========================

/portProfiles/config
--------------------

### GET

Summary

Get a list of all Deployment Profile templates

Input type

None

Output type

None

### POST

Summary

Create a new Deployment Profile template

Input type

application/json

portProfilesPost

Description: JSON object containing Deployment Profile template
configuration data

Required: true

Parameter type: POST Body

Data Type: application/json

id

-   Type: string
-   Required: true
-   Description: System generated unique identifier for the Deployment
    Profile, leave empty or null when creating new Deployment Profile
    template

name

-   Type: string
-   Required: true
-   Description: Deployment Profile template name

config

sysConfig

mode

-   Type: string
-   Required: true
-   Description: System deployment mode

useMgmt0

-   Type: boolean
-   Required: true
-   Description: Use mgmt0 interface as a datapath interface as well.
    (only valid under router mode)

tenG

-   Type: boolean
-   Required: true
-   Description: Use 10Gbps interfaces (only valid for models with
    10Gbps interfaces)

bonding

-   Type: boolean
-   Required: true
-   Description: Use interface bonding (only valid for models with 2 LAN
    interfaces and 2 WAN interfaces)

maxBW

-   Type: integer
-   Required: true
-   Description: System maximum bandwidth in Kbps

propagateLinkDown

-   Type: boolean
-   Required: true
-   Description: Propagate link down state: this is valid in bridge mode
    only. If it is true, if either LAN or WAN side interface of the
    bridge is down, the system brings down the other interface
    automatically

singleBridge

-   Type: boolean
-   Required: true
-   Description: Single 4-port flat bridge configuration (only valid for
    4-port models)

inline

-   Type: boolean
-   Required: true
-   Description: Inline router mode (only valid for router mode). This
    is the recommended mode.

haIf

-   Type: string
-   Description: Name of the interface used to build \'internal\' High
    Availability (HA) VLAN interfaces using which two appliances in a HA
    configuration can communicate with each other

maxInBW

-   Type: integer
-   Description: System maximum inbound bandwidth in Kbps (to enable
    inbound shaping make sure maxInBWEnabled is set to true)

maxInBWEnabled

-   Type: boolean
-   Description: Enable inbound bandwidth shaping (if this is true, you
    must specify maxInBW)

licence

ecMini

-   Type: boolean
-   Description: Is EC Mini license enabled

ecPlus

-   Type: boolean
-   Description: Is EC Plus license enabled

ecTier

-   Type: string
-   Description: EC Tier license name

ecTierBW

-   Type: integer
-   Description: EC Tier bandwidth in Kbps

ecBoost

-   Type: boolean
-   Description: Is EC Boost license enabled

ecBoostBW

-   Type: integer
-   Description: Boost bandwidth in Kbps

(licenseType)

-   Type: object
-   Description: This key is dynamic based on the EC license type (fx,
    ecsp etc). Contains additional details based on license type.

zones

-   Type: array

modeIfs

-   Type: array

dpRoutes

-   Type: array

dhcpFailover

-   Type: object

Output type

None

/portProfiles/config/{portProfileId}
------------------------------------

### GET

Summary

Get a Deployment Profile template matching the id

Input type

None

portProfileId

-   Description: System generated unique identifier for the Deployment
    Profile
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

sysConfig

mode

-   Type: string
-   Required: true
-   Description: System deployment mode

useMgmt0

-   Type: boolean
-   Required: true
-   Description: Use mgmt0 interface as a datapath interface as well.
    (only valid under router mode)

tenG

-   Type: boolean
-   Required: true
-   Description: Use 10Gbps interfaces (only valid for models with
    10Gbps interfaces)

bonding

-   Type: boolean
-   Required: true
-   Description: Use interface bonding (only valid for models with 2 LAN
    interfaces and 2 WAN interfaces)

maxBW

-   Type: integer
-   Required: true
-   Description: System maximum bandwidth in Kbps

propagateLinkDown

-   Type: boolean
-   Required: true
-   Description: Propagate link down state: this is valid in bridge mode
    only. If it is true, if either LAN or WAN side interface of the
    bridge is down, the system brings down the other interface
    automatically

singleBridge

-   Type: boolean
-   Required: true
-   Description: Single 4-port flat bridge configuration (only valid for
    4-port models)

inline

-   Type: boolean
-   Required: true
-   Description: Inline router mode (only valid for router mode). This
    is the recommended mode.

haIf

-   Type: string
-   Description: Name of the interface used to build \'internal\' High
    Availability (HA) VLAN interfaces using which two appliances in a HA
    configuration can communicate with each other

maxInBW

-   Type: integer
-   Description: System maximum inbound bandwidth in Kbps (to enable
    inbound shaping make sure maxInBWEnabled is set to true)

maxInBWEnabled

-   Type: boolean
-   Description: Enable inbound bandwidth shaping (if this is true, you
    must specify maxInBW)

licence

ecMini

-   Type: boolean
-   Description: Is EC Mini license enabled

ecPlus

-   Type: boolean
-   Description: Is EC Plus license enabled

ecTier

-   Type: string
-   Description: EC Tier license name

ecTierBW

-   Type: integer
-   Description: EC Tier bandwidth in Kbps

ecBoost

-   Type: boolean
-   Description: Is EC Boost license enabled

ecBoostBW

-   Type: integer
-   Description: Boost bandwidth in Kbps

(licenseType)

-   Type: object
-   Description: This key is dynamic based on the EC license type (fx,
    ecsp etc). Contains additional details based on license type.

zones

-   Type: array

modeIfs

-   Type: array

dpRoutes

-   Type: array

dhcpFailover

-   Type: object

### PUT

Summary

Update a Deployment Profile template matching the id

Input type

application/json: undefined

portProfileId

-   Description: System generated unique identifier for the Deployment
    Profile
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

portProfilesPost

Description: JSON object containing Deployment Profile template
configuration data

Required: true

Parameter type: POST Body

Data Type: application/json

id

-   Type: string
-   Required: true
-   Description: System generated unique identifier for the Deployment
    Profile, leave empty or null when creating new Deployment Profile
    template

name

-   Type: string
-   Required: true
-   Description: Deployment Profile template name

config

sysConfig

mode

-   Type: string
-   Required: true
-   Description: System deployment mode

useMgmt0

-   Type: boolean
-   Required: true
-   Description: Use mgmt0 interface as a datapath interface as well.
    (only valid under router mode)

tenG

-   Type: boolean
-   Required: true
-   Description: Use 10Gbps interfaces (only valid for models with
    10Gbps interfaces)

bonding

-   Type: boolean
-   Required: true
-   Description: Use interface bonding (only valid for models with 2 LAN
    interfaces and 2 WAN interfaces)

maxBW

-   Type: integer
-   Required: true
-   Description: System maximum bandwidth in Kbps

propagateLinkDown

-   Type: boolean
-   Required: true
-   Description: Propagate link down state: this is valid in bridge mode
    only. If it is true, if either LAN or WAN side interface of the
    bridge is down, the system brings down the other interface
    automatically

singleBridge

-   Type: boolean
-   Required: true
-   Description: Single 4-port flat bridge configuration (only valid for
    4-port models)

inline

-   Type: boolean
-   Required: true
-   Description: Inline router mode (only valid for router mode). This
    is the recommended mode.

haIf

-   Type: string
-   Description: Name of the interface used to build \'internal\' High
    Availability (HA) VLAN interfaces using which two appliances in a HA
    configuration can communicate with each other

maxInBW

-   Type: integer
-   Description: System maximum inbound bandwidth in Kbps (to enable
    inbound shaping make sure maxInBWEnabled is set to true)

maxInBWEnabled

-   Type: boolean
-   Description: Enable inbound bandwidth shaping (if this is true, you
    must specify maxInBW)

licence

ecMini

-   Type: boolean
-   Description: Is EC Mini license enabled

ecPlus

-   Type: boolean
-   Description: Is EC Plus license enabled

ecTier

-   Type: string
-   Description: EC Tier license name

ecTierBW

-   Type: integer
-   Description: EC Tier bandwidth in Kbps

ecBoost

-   Type: boolean
-   Description: Is EC Boost license enabled

ecBoostBW

-   Type: integer
-   Description: Boost bandwidth in Kbps

(licenseType)

-   Type: object
-   Description: This key is dynamic based on the EC license type (fx,
    ecsp etc). Contains additional details based on license type.

zones

-   Type: array

modeIfs

-   Type: array

dpRoutes

-   Type: array

dhcpFailover

-   Type: object

Output type

None

### DELETE

Summary

Delete a Deployment Profile template matching the id

Input type

None

portProfileId

-   Description: System generated unique identifier for the Deployment
    Profile
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/portProfiles/isLabelInUse?id={labelId}&side={labelSide}
--------------------------------------------------------

### GET

Summary

Check if an interface label is in use by any of the existing deployment
profiles

Input type

None

labelId

-   Description: Unique id for interface label
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

labelSide

-   Description: Enum to donate LAN/WAN side label. Valid values (lan or
    wan)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

inUse

-   Type: boolean
-   Required: true
-   Description: Boolean to donate if label is in use

portProfileIds

-   Type: array
-   Description: Array of unique profile ids which are using this label

Reference: portProfiles.json

\

Get Action/Audit Logs
=====================

/action
-------

### GET

Summary

Get action/audit log entries.

Notes

Any operation performed by Orchestrator in the background or performed
by a user is logged. This API is used to retrieve that information.

Input type

None

startTime

-   Description: Long(Signed 64 bits) value of milliseconds since EPOCH
    time indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of milliseconds since EPOCH
    time indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

limit

-   Description: Number of rows to fetch
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: number

logLevel

-   Description: 0=Debug, 1=Info, 2=Error
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: number

appliance

-   Description: Appliance ID
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

username

-   Description: Username
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/action/status
--------------

### POST

Summary

Get status of a particular action using guid key

Notes

Certain actions like appliance upgrade, appliance restore etc., return a
key as in response. Check for status of such operations using the key

Input type

Unknown

key

-   Description: action key (guid)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/action/cancel
--------------

### POST

Summary

Cancel action guid key

Notes

Certain actions like appliance upgrade, appliance restore etc., return a
key as in response. You can cancel such operations using the key

Input type

Unknown

key

-   Description: action key (guid)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: actionLog.json

\

Add and remove appliances from overlays
=======================================

/gms/overlays/association
-------------------------

### GET

Summary

Get the list of appliances associated with all overlays

Notes

This resource returns an object, where the keys are overlay IDs, and the
value is an array of appliances belonging to that overlay.

Input type

None

Output type

application/json

1

-   Type: array
-   Description: An array of appliance IDs (nePks) of appliances
    belonging to the overlay with ID 1.

2

-   Type: array
-   Description: An array of appliance IDs (nePks) of appliances
    belonging to the overlay with ID 2.

### POST

Summary

Add appliances to overlays

Notes

The request body is an object, where the keys are overlay IDs and the
values are appliances to add to the respective overlays

Input type

application/json: undefined

Associations

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/gms/overlays/association/remove
--------------------------------

### POST

Summary

Remove appliances from overlays

Notes

The request body is an object, where the keys are overlay IDs and the
values are appliances to remove from the respective overlays

Input type

application/json: undefined

Appliances to remove

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/gms/overlays/association/{overlayId}
-------------------------------------

### GET

Summary

Get appliances in an overlay

Input type

None

overlayId

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

1

-   Type: array
-   Description: An array of appliance IDs (nePks) of appliances
    belonging to the overlay with ID 1.

/gms/overlays/association/{overlayId}/{nePk}
--------------------------------------------

### DELETE

Summary

Remove an appliance from an overlay

Notes

Removes the appliance from the overlay, given the appliance ID (nePk)
and overlay ID (overlayId).

Input type

None

overlayId

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

nePk

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: overlayAssociation.json

\

Configure DHCP IP Pool & global default settings on Orchestrator
================================================================

/dhcpConfig
-----------

### GET

Summary

Get orchestrator DHCP settings

Notes

These setting are used to manage a global DHCP pool on the orchestrator.
Also they serve as defaults for the DHCP settings in the Deployment
Profiles

Input type

None

Output type

application/json

mode

-   Type: string
-   Description: Default DHCP mode setting per appliance interface,
    options are \[\'server\', \'relay\', \'none\')

serverConfig

blockStartIp

-   Type: string
-   Description: Subnet ip of the DHCP pool

blockMask

-   Type: string
-   Description: Subnet mask of the DHCP pool

defaultSubnetMask

-   Type: string
-   Description: Default subnet mask for a reservation block per
    interface

startIp

-   Type: string
-   Description: Start Ip within the reservation subnet (subnet start +
    start offset)

endIp

-   Type: string
-   Description: End Ip within the reservation subnet. (subnet end - end
    offset)

startOffset

-   Type: number
-   Description: Number of reserved ips at the beginning of subnet.

endOffset

-   Type: number
-   Description: Number of reserved ips at the end of subnet.

defaultLease

-   Type: number
-   Description: In seconds

maxLease

-   Type: number
-   Description: In seconds

defaultGateway

-   Type: string
-   Description: Default gateway ip

defaultGatewayEnabled

-   Type: boolean
-   Description: Pre fill default gateway from the poll?

dnsServers

-   Type: array

ntpServers

-   Type: array

netBiosServers

-   Type: array

netBiosNodeType

-   Type: string
-   Description: Enum with options \[\'B\', \'P\', \'M\', \'H\'\].
    Defaults to \'B\'

reservations

-   Type: array
-   Description: Collection of subnets that have been reserved in the
    DHCP pool

localReservations

-   Type: array
-   Description: Collection of subnets that have been reserved in the
    DHCP pool locally on the orchestrator (this list is built from the
    Deployment Profiles on Orchestrator)

applianceReservations

-   Type: array
-   Description: Collection of subnets that have been reserved in the
    DHCP pool from appliance (this list is built from the data that
    Orchestrator fetches from the appliance)

failover

-   Type: boolean
-   Description: Determine if appliance enable DHCP failover

relayConfig

destDhcpServer

-   Type: string
-   Description: Destination DHCP server ip(s), separate multiple ips
    with comma

opt82

-   Type: boolean

opt82Policy

-   Type: string
-   Description: Options are \[\'append\', \'replace\', \'forward\',
    \'discard\'\]. Defaults to \'append\'

dhcpFoSetting

max\_unack\_updates

-   Type: number
-   Description: Tells the remote DHCP server how many BNDUPD messages
    it can send before it receives a BNDACK from the local system.

peer\_port

-   Type: number
-   Description: Define which TCP port to connect to its failover peer
    for failover messages.

my\_port

-   Type: number
-   Description: Defines which TCP port the server should listen for
    connections from its failover peer.

max\_resp\_delay

-   Type: number
-   Description: Tells the DHCP server how many seconds may pass without
    receiving a message from its failover peer before it assumes that
    connection has failed.

load\_bal\_max

-   Type: number
-   Description: Defines a threshold to compare with the secs field of
    the client\'s DHCP packet in order to override load balancing.

role

-   Type: string
-   Required: true
-   Description: Specify if the server is the primary or secondary.

mclt

-   Type: number
-   Description: Define the Maximum Client Lead Time.

split

-   Type: number
-   Description: Specify the split between the primary and secondary

peer\_ip

-   Type: string
-   Required: true
-   Description: Define which server it should connect to reach its
    failover peer.

my\_ip

-   Type: string
-   Required: true
-   Description: Define the address that the server should listen for
    connections from its failover peer

### POST

Summary

Save orchestrator DHCP settings

Input type

application/json

dhcpConfigPost

Description: JSON object containing DHCP settings

Required: true

Parameter type: POST Body

Data Type: application/json

mode

-   Type: string
-   Description: Default DHCP mode setting per appliance interface,
    options are \[\'server\', \'relay\', \'none\')

serverConfig

blockStartIp

-   Type: string
-   Description: Subnet ip of the DHCP pool

blockMask

-   Type: string
-   Description: Subnet mask of the DHCP pool

defaultSubnetMask

-   Type: string
-   Description: Default subnet mask for a reservation block per
    interface

startIp

-   Type: string
-   Description: Start Ip within the reservation subnet (subnet start +
    start offset)

endIp

-   Type: string
-   Description: End Ip within the reservation subnet. (subnet end - end
    offset)

startOffset

-   Type: number
-   Description: Number of reserved ips at the beginning of subnet.

endOffset

-   Type: number
-   Description: Number of reserved ips at the end of subnet.

defaultLease

-   Type: number
-   Description: In seconds

maxLease

-   Type: number
-   Description: In seconds

defaultGateway

-   Type: string
-   Description: Default gateway ip

defaultGatewayEnabled

-   Type: boolean
-   Description: Pre fill default gateway from the poll?

dnsServers

-   Type: array

ntpServers

-   Type: array

netBiosServers

-   Type: array

netBiosNodeType

-   Type: string
-   Description: Enum with options \[\'B\', \'P\', \'M\', \'H\'\].
    Defaults to \'B\'

reservations

-   Type: array
-   Description: Collection of subnets that have been reserved in the
    DHCP pool

localReservations

-   Type: array
-   Description: Collection of subnets that have been reserved in the
    DHCP pool locally on the orchestrator (this list is built from the
    Deployment Profiles on Orchestrator)

applianceReservations

-   Type: array
-   Description: Collection of subnets that have been reserved in the
    DHCP pool from appliance (this list is built from the data that
    Orchestrator fetches from the appliance)

failover

-   Type: boolean
-   Description: Determine if appliance enable DHCP failover

relayConfig

destDhcpServer

-   Type: string
-   Description: Destination DHCP server ip(s), separate multiple ips
    with comma

opt82

-   Type: boolean

opt82Policy

-   Type: string
-   Description: Options are \[\'append\', \'replace\', \'forward\',
    \'discard\'\]. Defaults to \'append\'

dhcpFoSetting

max\_unack\_updates

-   Type: number
-   Description: Tells the remote DHCP server how many BNDUPD messages
    it can send before it receives a BNDACK from the local system.

peer\_port

-   Type: number
-   Description: Define which TCP port to connect to its failover peer
    for failover messages.

my\_port

-   Type: number
-   Description: Defines which TCP port the server should listen for
    connections from its failover peer.

max\_resp\_delay

-   Type: number
-   Description: Tells the DHCP server how many seconds may pass without
    receiving a message from its failover peer before it assumes that
    connection has failed.

load\_bal\_max

-   Type: number
-   Description: Defines a threshold to compare with the secs field of
    the client\'s DHCP packet in order to override load balancing.

role

-   Type: string
-   Required: true
-   Description: Specify if the server is the primary or secondary.

mclt

-   Type: number
-   Description: Define the Maximum Client Lead Time.

split

-   Type: number
-   Description: Specify the split between the primary and secondary

peer\_ip

-   Type: string
-   Required: true
-   Description: Define which server it should connect to reach its
    failover peer.

my\_ip

-   Type: string
-   Required: true
-   Description: Define the address that the server should listen for
    connections from its failover peer

Output type

None

/dhcpConfig/reservations
------------------------

### GET

Summary

Get list of reserved subnets from the DHCP pool

Input type

None

Output type

None

### POST

Summary

Reserve a subnet in the DHCP pool

Input type

application/json

dhcpConfigPost

Description: JSON object containing DHCP pool reservation data

Required: true

Parameter type: POST Body

Data Type: application/json

startIp

-   Type: string
-   Description: Start IP address for DHCP pool reservation

mask

-   Type: number
-   Description: Mask value of DHCP pool reservation

neId

-   Type: string
-   Description: Unique key assigned by Orchestrator to each appliance,
    for eg: \'0.NE\'

ifName

-   Type: string
-   Description: LAN side interface name on appliance, for eg: lan0

Output type

None

/dhcpConfig/reservations/gms
----------------------------

### POST

Summary

Update DHCP pool reservations list. This api can be used to clear up
local reservations (that have not been configured on appliance yet)

Input type

application/json

dhcpConfigGmsPost

-   Description: JSON object containing DHCP pool reservation data
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/dhcpConfig/reset
-----------------

### POST

Summary

Remove all DHCP pool reservations

Input type

Output type

None

/dhcpConfig/leases/{neId}?cached={cached}
-----------------------------------------

### GET

Summary

Get DHCP leases information.

Notes

There may be dynamic fields in the actual data. For details regarding
schema for leases file checkout: http://linux.die.net/man/5/dhcpd.leases

Input type

None

neId

-   Description: Unique key assigned by Orchestrator to each appliance,
    for eg: \'0.NE\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<leaseIpAddress\>

lease

-   Type: string
-   Description: leased IP address

starts

-   Type: integer
-   Description: leased start time (UTC epoch in seconds)

ends

-   Type: integer
-   Description: leased end time (UTC epoch in seconds)

tstp

-   Type: integer
-   Description: the time the peer has been told the lease expires

cltt

-   Type: integer
-   Description: client\'s last transaction time

state

-   Type: string
-   Description: current lease state

nextState

-   Type: string
-   Description: next lease state

mac

-   Type: string
-   Description: client hardware address

/dhcpConfig/state/failover/{neId}?cached={cached}
-------------------------------------------------

### GET

Summary

Get dhcpd leases report of failover state

Notes

There may be dynamic fields in the actual data. For details regarding
schema for leases file checkout: http://linux.die.net/man/5/dhcpd.leases

Input type

None

neId

-   Description: Unique key assigned by Orchestrator to each appliance,
    for eg: \'0.NE\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<failoverstate\>

failoverstate

-   Type: string
-   Description: Interface name

mystate

-   Type: string
-   Description: Primary server state

mytime

-   Type: string
-   Description: Primary server last communication time (UTC epoch in
    seconds)

partnerstate

-   Type: string
-   Description: Peer server state

partnertime

-   Type: string
-   Description: Peer server last communication time (UTC epoch in
    seconds)

mclt

-   Type: integer
-   Description: Maximum client lead time

Reference: dhcpConfig.json

\

Update the appliance\'s extra information
=========================================

/appliance/extraInfo/{nePk}
---------------------------

### GET

Summary

Get the extra information of an appliance

Notes

This resource returns the extra info of an appliance. Even if the info
has not been saved yet, it will return the extra info with blank fields

Input type

None

nePk

-   Description: The nePk of the appliance
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

location

-   Type: Location
-   Description: The location of the appliance

contact

-   Type: Contact
-   Description: The contact information for the appliance

### POST

Summary

Saves the extra information for the appliance

Notes

Saves or updates the extra information of the appliance. See the Model
(Click Model) for more information. For custom IPSEC UDP Port, set
isUserDefinedIPSecUDPPort flag to true.

Input type

application/json: undefined

ApplianceExtraInfo

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

nePk

-   Description: The nePk of the appliance
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

### DELETE

Summary

Delete the extra information of an appliance

Notes

This will delete the extra info of the appliance from Orchestrator.

Input type

None

nePk

-   Description: The nePk of the appliance
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: applianceExtraInfo.json

\

Get and set the overlay manager properties in the server.
=========================================================

/overlayManagerProperties
-------------------------

### GET

Summary

Get the overlay manager properties in the Orchestrator.

Input type

None

Output type

application/json

enable

-   Type: boolean
-   Required: true
-   Description: Enable to resume the overlay work or disable to pause
    it

autoReclassifyValues

-   Type: integer
-   Required: true
-   Description: The integer value of auto reclassify

resetAllFlows

-   Type: boolean
-   Required: true
-   Description: Whether to reset all flows

tunnelMode

-   Type: string
-   Required: true
-   Description: The tunnel mode property

ipsecPresharedKey

-   Type: string
-   Required: true
-   Description: The IPsec preshared key property

overlayPaused

-   Type: boolean
-   Required: true
-   Description: Whether overlay manager is paused

pollingInterval

-   Type: integer
-   Required: true
-   Description: How often to poll, in seconds, the value should not be
    less than 60.

settings

\<Wan Label Id\>

admin

-   Type: string
-   Required: true
-   Description: admin up or down

retryCount

-   Type: number
-   Required: true
-   Description: retry count in seconds

dscp

-   Type: string
-   Required: true
-   Description: Differentiated Services Code Point

autoMaxBandwidthEnabled

-   Type: boolean
-   Required: true
-   Description: Automatically use max bandwidth

mode

-   Type: string
-   Required: true
-   Description: Tunnel mode - ipsec, udp, or gre

udpDestinationPort

-   Type: number
-   Required: true
-   Description: Receiver\'s port that will receive udp packets

udpFlows

-   Type: number
-   Required: true
-   Description: Max udp flows

ipsecAntiReplayWindow

-   Type: string
-   Required: true
-   Description: IPsec anti replay window

authenticationAlgorithm

-   Type: string
-   Required: true
-   Description: IPsec authentication algorithm

encryptionAlgorithm

-   Type: string
-   Required: true
-   Description: IPsec encryption algorithm

autoMtu

-   Type: boolean
-   Required: true
-   Description: Auto Mtu

mtu

-   Type: number
-   Required: true
-   Description: Mtu

reorderWait

-   Type: number
-   Required: true
-   Description: Reorder Wait time

selectedFec

-   Type: string
-   Required: true
-   Description: Fec

fecMaxRatio

-   Type: number
-   Required: true
-   Description: Fec Maximum Ratio, set this the same as selectedFecVal

fecMinRatio

-   Type: number
-   Required: true
-   Description: Fec Minimum Ratio, set this to 0

dhgroup

-   Type: string
-   Required: true
-   Description: IKE Diffie-Hellman Group

dpdDelay

-   Type: number
-   Required: true
-   Description: IKE Dead Peer Detection Delay time

dpdRetry

-   Type: number
-   Required: true
-   Description: IKE Dead Peer Detection Retry Count

exchangeMode

-   Type: string
-   Required: true
-   Description: IKE Phase 1 Mode

fastfailWaitBase

-   Type: number
-   Required: true
-   Description: Fastfail Wait-time Base Offset

fastfailWaitRtt

-   Type: number
-   Required: true
-   Description: Fastfail RTT Multiplication Factor

idStr

-   Type: string
-   Required: true
-   Description: IKE Identifier string

idType

-   Type: string
-   Required: true
-   Description: IKE Identifier type

ikeAuthenticationAlgorithm

-   Type: string
-   Required: true
-   Description: IKE Authentication Algorithm

ikeEncryptionAlgorithm

-   Type: string
-   Required: true
-   Description: IKE Encryption Algorithm

ikeLifetime

-   Type: number
-   Required: true
-   Description: IKE Lifetime

jitter

-   Type: number
-   Required: true
-   Description: Jitter

latency

-   Type: number
-   Required: true
-   Description: Latency

lifebytes

-   Type: number
-   Required: true
-   Description: IPsec Lifetime in megabytes

lifetime

-   Type: number
-   Required: true
-   Description: IPsec Lifetime in minutes

lossVal

-   Type: number
-   Required: true
-   Description: Loss

pfs

-   Type: boolean
-   Required: true
-   Description: Perfect Forward Secrecy Group Enabled

pfsgroup

-   Type: string
-   Required: true
-   Description: Perfect Forward Secrecy Group number

selectedFastfail

-   Type: number
-   Required: true
-   Description: Fastfail Enabled (1: enable, 2: continue, 3: disable)

enableAutoSaveOnVXOA

-   Type: boolean
-   Required: true
-   Description: When this flag is true, auto save changes on appliances
    in each overlay loop

enableTemplateApply

-   Type: boolean
-   Required: true
-   Description: When this flag is true, auto apply templates to
    appliances in each overlay loop

### POST

Summary

Set the overlay manager properties in the Orchestrator.

Input type

application/json

OverlayManagerProperties

Description: Json object containing overlay manager properties.

Required: true

Parameter type: POST Body

Data Type: application/json

enable

-   Type: boolean
-   Required: true
-   Description: Enable to resume the overlay work or disable to pause
    it

autoReclassifyValues

-   Type: integer
-   Required: true
-   Description: The integer value of auto reclassify

resetAllFlows

-   Type: boolean
-   Required: true
-   Description: Whether to reset all flows

tunnelMode

-   Type: string
-   Required: true
-   Description: The tunnel mode property

ipsecPresharedKey

-   Type: string
-   Required: true
-   Description: The IPsec preshared key property

pollingInterval

-   Type: integer
-   Required: true
-   Description: How often to poll, in seconds, the value should not be
    less than 60.

settings

\<Wan Label Id\>

admin

-   Type: string
-   Required: true
-   Description: admin up or down

retryCount

-   Type: number
-   Required: true
-   Description: retry count in seconds

dscp

-   Type: string
-   Required: true
-   Description: Differentiated Services Code Point

autoMaxBandwidthEnabled

-   Type: boolean
-   Required: true
-   Description: Automatically use max bandwidth

mode

-   Type: string
-   Required: true
-   Description: Tunnel mode - ipsec, udp, or gre

udpDestinationPort

-   Type: number
-   Required: true
-   Description: Receiver\'s port that will receive udp packets

udpFlows

-   Type: number
-   Required: true
-   Description: Max udp flows

ipsecAntiReplayWindow

-   Type: string
-   Required: true
-   Description: IPsec anti replay window

authenticationAlgorithm

-   Type: string
-   Required: true
-   Description: IPsec authentication algorithm

encryptionAlgorithm

-   Type: string
-   Required: true
-   Description: IPsec encryption algorithm

autoMtu

-   Type: boolean
-   Required: true
-   Description: Auto Mtu

mtu

-   Type: number
-   Required: true
-   Description: Mtu

reorderWait

-   Type: number
-   Required: true
-   Description: Reorder Wait time

selectedFec

-   Type: string
-   Required: true
-   Description: Fec

fecMaxRatio

-   Type: number
-   Required: true
-   Description: Fec Maximum Ratio, set this the same as selectedFecVal

fecMinRatio

-   Type: number
-   Required: true
-   Description: Fec Minimum Ratio, set this to 0

dhgroup

-   Type: string
-   Required: true
-   Description: IKE Diffie-Hellman Group

dpdDelay

-   Type: number
-   Required: true
-   Description: IKE Dead Peer Detection Delay time

dpdRetry

-   Type: number
-   Required: true
-   Description: IKE Dead Peer Detection Retry Count

exchangeMode

-   Type: string
-   Required: true
-   Description: IKE Phase 1 Mode

fastfailWaitBase

-   Type: number
-   Required: true
-   Description: Fastfail Wait-time Base Offset

fastfailWaitRtt

-   Type: number
-   Required: true
-   Description: Fastfail RTT Multiplication Factor

idStr

-   Type: string
-   Required: true
-   Description: IKE Identifier string

idType

-   Type: string
-   Required: true
-   Description: IKE Identifier type

ikeAuthenticationAlgorithm

-   Type: string
-   Required: true
-   Description: IKE Authentication Algorithm

ikeEncryptionAlgorithm

-   Type: string
-   Required: true
-   Description: IKE Encryption Algorithm

ikeLifetime

-   Type: number
-   Required: true
-   Description: IKE Lifetime

jitter

-   Type: number
-   Required: true
-   Description: Jitter

latency

-   Type: number
-   Required: true
-   Description: Latency

lifebytes

-   Type: number
-   Required: true
-   Description: IPsec Lifetime in megabytes

lifetime

-   Type: number
-   Required: true
-   Description: IPsec Lifetime in minutes

lossVal

-   Type: number
-   Required: true
-   Description: Loss

pfs

-   Type: boolean
-   Required: true
-   Description: Perfect Forward Secrecy Group Enabled

pfsgroup

-   Type: string
-   Required: true
-   Description: Perfect Forward Secrecy Group number

selectedFastfail

-   Type: number
-   Required: true
-   Description: Fastfail Enabled (1: enable, 2: continue, 3: disable)

enableAutoSaveOnVXOA

-   Type: boolean
-   Description: When this flag is true, auto save changes on appliances
    in each overlay loop

enableTemplateApply

-   Type: boolean
-   Description: When this flag is true, auto apply templates to
    appliances in each overlay loop

Output type

None

Reference: overlayManagerProperties.json

\

Add, update, delete menu type configurations.
=============================================

/gms/menuCustomization
----------------------

### GET

Summary

All the checked menu items with different menu type names

Input type

None

Output type

application/json

menuTypeName

-   Type: menuTypeConfig
-   Required: true
-   Description: The name of the menu type configuration.

### POST

Summary

Store or update the checked menu items for a special menu type name

Input type

application/json: undefined

New menuType

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

### PUT

Summary

Configure users for menu types and set default menu type for monitoring
users

Input type

application/json: undefined

Users update

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/gms/menuCustomization/{menuTypeName}
-------------------------------------

### DELETE

Summary

Delete the a special menu type, including its checked menu items and
configured users

Input type

None

menuTypeName

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: menuTypeConfig.json

\

Tunnel Group Configuration
==========================

/gms/tunnelGroups/config
------------------------

### GET

Summary

Returns all the tunnel group configurations stored in the Orchestrator

Input type

None

Output type

application/json

### POST

Summary

Saves a new tunnel group configuration to the Orchestrator

Input type

application/json: undefined

New tunnel group

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/gms/tunnelGroups/config/{id}
-----------------------------

### GET

Summary

Returns one tunnel group that matches the ID given

Input type

None

id

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

name

-   Type: string

id

-   Type: string

topology

-   Type: OverlayTopology
-   Description: Topology configuration for this overlay

crossConnect

-   Type: boolean
-   Description: Whether or not to cross connect the WAN ports.

useAllAvailableInterfaces

-   Type: boolean
-   Description: Use all the available interfaces found on the
    appliances

wanPorts

-   Type: array
-   Description: An array of WAN label ids to use as the primary WAN
    ports this overlay.

### PUT

Summary

Update the properties of an existing tunnel group

Input type

application/json: undefined

id

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Updated tunnel group

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

### DELETE

Summary

Deletes the tunnel group from the Orchestrator. Removes any appliances
from the tunnel group.

Input type

None

id

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/gms/tunnelGroups/properties
----------------------------

### GET

Summary

Get the tunnel group management properties

Input type

None

Output type

application/json

enable

-   Type: boolean
-   Description: Whether or not the tunnel group manager is enabled

tunnelGroupPaused

-   Type: boolean
-   Description: The current state of whether or not the tunnel group
    manager is paused

### POST

Summary

Set the tunnel group management properties

Input type

application/json: undefined

Tunnel Group Properties

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

Reference: tunnelGroups.json

\

Add and remove appliances from tunnel groups
============================================

/gms/tunnelGroups/association
-----------------------------

### GET

Summary

Get the list of all tunnel group appliance lists

Notes

This resource returns an object, where the keys are tunnel group IDs,
and the value is an array of appliances belonging to that tunnel group.

Input type

None

Output type

application/json

1

-   Type: array
-   Description: An array of appliance IDs (nePks) of appliances
    belonging to the tunnel group with ID 1.

2

-   Type: array
-   Description: An array of appliance IDs (nePks) of appliances
    belonging to the tunnel group with ID 2.

### POST

Summary

Add appliances to tunnel groups

Notes

The request body is an object, where the keys are tunnel group IDs and
the values are appliances to add to the respective tunnel groups

Input type

application/json: undefined

Associations

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/gms/tunnelGroups/association/{tunnelGroupId}
---------------------------------------------

### GET

Summary

Get appliances in a tunnel group

Input type

None

tunnelGroupId

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

1

-   Type: array
-   Description: An array of appliance IDs (nePks) of appliances
    belonging to the tunnel group with ID 1.

/gms/tunnelGroups/association/{tunnelGroupId}/{nePk}
----------------------------------------------------

### DELETE

Summary

Remove an appliance from a tunnel group

Notes

Removes the appliance from the tunnel group, given the appliance ID
(nePk) and tunnel group ID (tunnelGroupId).

Input type

None

tunnelGroupId

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

nePk

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: tunnelGroupAssociation.json

\

Upload GMS HTTP server certificates
===================================

/gms/httpsCertificate/validation
--------------------------------

### POST

Summary

Validate the certificate and key

Input type

application/json

certificateKeyData

Description: A JSON object representing certificate and key files

Required: true

Parameter type: POST Body

Data Type: application/json

certificateData

-   Type: string
-   Required: true

keyData

-   Type: string
-   Required: true

intermediateCertFileContent

-   Type: string

Output type

None

/gms/httpsCertificate
---------------------

### POST

Summary

upload the intermediate certs, certificate and key files

Input type

application/json

certificateKeyData

Description: A JSON object representing intermediate certs, certificate
and key files

Required: true

Parameter type: POST Body

Data Type: application/json

certificateData

-   Type: string
-   Required: true

keyData

-   Type: string
-   Required: true

intermediateCertFileContent

-   Type: string

Output type

None

Reference: gmsHttpsUpload.json

\

Lookup the lat/lon of an address
================================

/location/addressToLocation?address={address}
---------------------------------------------

### GET

Summary

Lookup the latitude,longitude of an address

Notes

Lookup the latitude and longitude of an address. It will return an array
of locations that match the object, in the order of the locations that
match the address the best. So if you\'re looking for the best match,
you should use the first element in the array.

Input type

None

address

-   Description: A string that represents the address to query
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: location.json

\

VXOA deployment configuration
=============================

/deployment/{nePk}
------------------

### GET

Summary

Get the deployment configuration of the appliance, does a direct call to
the appliance.

Input type

None

nePk

-   Description: The appliance ne pk.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

scalars

sysConfig

mode

-   Type: string
-   Required: true
-   Description: System deployment mode

useMgmt0

-   Type: boolean
-   Required: true
-   Description: Use mgmt0 interface as a datapath interface as well.
    (only valid under router mode)

tenG

-   Type: boolean
-   Required: true
-   Description: Use 10Gbps interfaces (only valid for models with
    10Gbps interfaces)

bonding

-   Type: boolean
-   Required: true
-   Description: Use interface bonding (only valid for models with 2 LAN
    interfaces and 2 WAN interfaces)

maxBW

-   Type: integer
-   Required: true
-   Description: System maximum outbound bandwidth in Kbps

propagateLinkDown

-   Type: boolean
-   Required: true
-   Description: Propagate link down state: this is valid in bridge mode
    only. If it is true, if either LAN or WAN side interface of the
    bridge is down, the system brings down the other interface
    automatically

singleBridge

-   Type: boolean
-   Required: true
-   Description: Single 4-port flat bridge configuration (only valid for
    4-port models)

inline

-   Type: boolean
-   Required: true
-   Description: Inline router mode (only valid for router mode). This
    is the recommended mode.

ifLabels

lan

-   Type: array
-   Description: Available LAN interface labels

wan

-   Type: array
-   Description: Available WAN interface labels

haIf

-   Type: string
-   Description: Name of the interface used to build \'internal\' High
    Availability (HA) VLAN interfaces using which two appliances in a HA
    configuration can communicate with each other

maxInBW

-   Type: integer
-   Description: System maximum inbound bandwidth in Kbps (to enable
    inbound shaping make sure maxInBWEnabled is set to true)

maxInBWEnabled

-   Type: boolean
-   Description: Enable inbound bandwidth shaping (if this is true, you
    must specify maxInBW)

licence

ecMini

-   Type: boolean
-   Description: Is EC Mini license enabled

ecPlus

-   Type: boolean
-   Description: Is EC Plus license enabled

ecTier

-   Type: string
-   Description: EC Tier license name

ecTierBW

-   Type: integer
-   Description: EC Tier bandwidth in Kbps

ecBoost

-   Type: boolean
-   Description: Is EC Boost license enabled

ecBoostBW

-   Type: integer
-   Description: Boost bandwidth in Kbps

zones

-   Type: array

mgmtIfData

(ifName)

dhcp

-   Type: boolean
-   Description: Is DHCP on or off on this management interface

nexthop

-   Type: string
-   Description: Management interface gateway IP address

ip

-   Type: string
-   Description: Management interface IP address

mask

-   Type: number
-   Description: Management interface network mask

modeIfs

-   Type: array

dpRoutes

-   Type: array

vifs

pppoe

-   Type: array
-   Description: List of PPPoE interface names configured on this
    appliance

dhcpFailover

(ifname)

max\_unack\_updates

-   Type: number
-   Description: Tells the remote DHCP server how many BNDUPD messages
    it can send before it receives a BNDACK from the local system.

peer\_port

-   Type: number
-   Description: Define which TCP port to connect to its failover peer
    for failover messages.

my\_port

-   Type: number
-   Description: Defines which TCP port the server should listen for
    connections from its failover peer.

max\_resp\_delay

-   Type: number
-   Description: Tells the DHCP server how many seconds may pass without
    receiving a message from its failover peer before it assumes that
    connection has failed.

load\_bal\_max

-   Type: number
-   Description: Defines a threshold to compare with the secs field of
    the client\'s DHCP packet in order to override load balancing.

role

-   Type: string
-   Required: true
-   Description: Specify if the server is the primary or secondary.

mclt

-   Type: number
-   Description: Define the Maximum Client Lead Time.

split

-   Type: number
-   Description: Specify the split between the primary and secondary

peer\_ip

-   Type: string
-   Required: true
-   Description: Define which server it should connect to reach its
    failover peer.

my\_ip

-   Type: string
-   Required: true
-   Description: Define the address that the server should listen for
    connections from its failover peer

/deployment/validate/{nePk}
---------------------------

### POST

Summary

Validate deployment configuration

Notes

Validate a deployment configuration, and see whether or not the
deployment will require a reboot after apply.

Input type

application/json: undefined

nePk

-   Description: The appliance ne pk.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

deployment

Description: The deployment configuration to validate on the appliance

Required: true

Parameter type: POST Body

Data Type: application/json

sysConfig

mode

-   Type: string
-   Required: true
-   Description: System deployment mode

useMgmt0

-   Type: boolean
-   Required: true
-   Description: Use mgmt0 interface as a datapath interface as well.
    (only valid under router mode)

tenG

-   Type: boolean
-   Required: true
-   Description: Use 10Gbps interfaces (only valid for models with
    10Gbps interfaces)

bonding

-   Type: boolean
-   Required: true
-   Description: Use interface bonding (only valid for models with 2 LAN
    interfaces and 2 WAN interfaces)

maxBW

-   Type: integer
-   Required: true
-   Description: System maximum outbound bandwidth in Kbps

propagateLinkDown

-   Type: boolean
-   Required: true
-   Description: Propagate link down state: this is valid in bridge mode
    only. If it is true, if either LAN or WAN side interface of the
    bridge is down, the system brings down the other interface
    automatically

singleBridge

-   Type: boolean
-   Required: true
-   Description: Single 4-port flat bridge configuration (only valid for
    4-port models)

inline

-   Type: boolean
-   Required: true
-   Description: Inline router mode (only valid for router mode). This
    is the recommended mode.

ifLabels

lan

-   Type: array
-   Description: Available LAN interface labels

wan

-   Type: array
-   Description: Available WAN interface labels

haIf

-   Type: string
-   Description: Name of the interface used to build \'internal\' High
    Availability (HA) VLAN interfaces using which two appliances in a HA
    configuration can communicate with each other

maxInBW

-   Type: integer
-   Description: System maximum inbound bandwidth in Kbps (to enable
    inbound shaping make sure maxInBWEnabled is set to true)

maxInBWEnabled

-   Type: boolean
-   Description: Enable inbound bandwidth shaping (if this is true, you
    must specify maxInBW)

licence

ecMini

-   Type: boolean
-   Description: Is EC Mini license enabled

ecPlus

-   Type: boolean
-   Description: Is EC Plus license enabled

ecTier

-   Type: string
-   Description: EC Tier license name

ecTierBW

-   Type: integer
-   Description: EC Tier bandwidth in Kbps

ecBoost

-   Type: boolean
-   Description: Is EC Boost license enabled

ecBoostBW

-   Type: integer
-   Description: Boost bandwidth in Kbps

zones

-   Type: array

modeIfs

-   Type: array
-   Required: true

dpRoutes

-   Type: array
-   Required: true

dhcpFailover

(ifname)

max\_unack\_updates

-   Type: number
-   Description: Tells the remote DHCP server how many BNDUPD messages
    it can send before it receives a BNDACK from the local system.

peer\_port

-   Type: number
-   Description: Define which TCP port to connect to its failover peer
    for failover messages.

my\_port

-   Type: number
-   Description: Defines which TCP port the server should listen for
    connections from its failover peer.

max\_resp\_delay

-   Type: number
-   Description: Tells the DHCP server how many seconds may pass without
    receiving a message from its failover peer before it assumes that
    connection has failed.

load\_bal\_max

-   Type: number
-   Description: Defines a threshold to compare with the secs field of
    the client\'s DHCP packet in order to override load balancing.

role

-   Type: string
-   Required: true
-   Description: Specify if the server is the primary or secondary.

mclt

-   Type: number
-   Description: Define the Maximum Client Lead Time.

split

-   Type: number
-   Description: Specify the split between the primary and secondary

peer\_ip

-   Type: string
-   Required: true
-   Description: Define which server it should connect to reach its
    failover peer.

my\_ip

-   Type: string
-   Required: true
-   Description: Define the address that the server should listen for
    connections from its failover peer

Output type

application/json

err

-   Type: string
-   Required: true
-   Description: Deployment data validation result - OK: null, or in
    case of error, the error message here

rebootRequired

-   Type: boolean
-   Required: true
-   Description: Indicates whether reboot is required if the deployment
    data provided is applied,

/deployment/validateDiscovered/{discoveredApplianceId}
------------------------------------------------------

### POST

Summary

Validate deployment configuration for a discovered appliance.

Notes

Validate the deployment of an appliance that is discovered but not yet
managed.

Input type

application/json: undefined

discoveredApplianceId

-   Description: The ID of the discovered appliance
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

deployment

Description: The deployment configuration to validate on the appliance

Required: true

Parameter type: POST Body

Data Type: application/json

sysConfig

mode

-   Type: string
-   Required: true
-   Description: System deployment mode

useMgmt0

-   Type: boolean
-   Required: true
-   Description: Use mgmt0 interface as a datapath interface as well.
    (only valid under router mode)

tenG

-   Type: boolean
-   Required: true
-   Description: Use 10Gbps interfaces (only valid for models with
    10Gbps interfaces)

bonding

-   Type: boolean
-   Required: true
-   Description: Use interface bonding (only valid for models with 2 LAN
    interfaces and 2 WAN interfaces)

maxBW

-   Type: integer
-   Required: true
-   Description: System maximum outbound bandwidth in Kbps

propagateLinkDown

-   Type: boolean
-   Required: true
-   Description: Propagate link down state: this is valid in bridge mode
    only. If it is true, if either LAN or WAN side interface of the
    bridge is down, the system brings down the other interface
    automatically

singleBridge

-   Type: boolean
-   Required: true
-   Description: Single 4-port flat bridge configuration (only valid for
    4-port models)

inline

-   Type: boolean
-   Required: true
-   Description: Inline router mode (only valid for router mode). This
    is the recommended mode.

ifLabels

lan

-   Type: array
-   Description: Available LAN interface labels

wan

-   Type: array
-   Description: Available WAN interface labels

haIf

-   Type: string
-   Description: Name of the interface used to build \'internal\' High
    Availability (HA) VLAN interfaces using which two appliances in a HA
    configuration can communicate with each other

maxInBW

-   Type: integer
-   Description: System maximum inbound bandwidth in Kbps (to enable
    inbound shaping make sure maxInBWEnabled is set to true)

maxInBWEnabled

-   Type: boolean
-   Description: Enable inbound bandwidth shaping (if this is true, you
    must specify maxInBW)

licence

ecMini

-   Type: boolean
-   Description: Is EC Mini license enabled

ecPlus

-   Type: boolean
-   Description: Is EC Plus license enabled

ecTier

-   Type: string
-   Description: EC Tier license name

ecTierBW

-   Type: integer
-   Description: EC Tier bandwidth in Kbps

ecBoost

-   Type: boolean
-   Description: Is EC Boost license enabled

ecBoostBW

-   Type: integer
-   Description: Boost bandwidth in Kbps

zones

-   Type: array

modeIfs

-   Type: array
-   Required: true

dpRoutes

-   Type: array
-   Required: true

dhcpFailover

(ifname)

max\_unack\_updates

-   Type: number
-   Description: Tells the remote DHCP server how many BNDUPD messages
    it can send before it receives a BNDACK from the local system.

peer\_port

-   Type: number
-   Description: Define which TCP port to connect to its failover peer
    for failover messages.

my\_port

-   Type: number
-   Description: Defines which TCP port the server should listen for
    connections from its failover peer.

max\_resp\_delay

-   Type: number
-   Description: Tells the DHCP server how many seconds may pass without
    receiving a message from its failover peer before it assumes that
    connection has failed.

load\_bal\_max

-   Type: number
-   Description: Defines a threshold to compare with the secs field of
    the client\'s DHCP packet in order to override load balancing.

role

-   Type: string
-   Required: true
-   Description: Specify if the server is the primary or secondary.

mclt

-   Type: number
-   Description: Define the Maximum Client Lead Time.

split

-   Type: number
-   Description: Specify the split between the primary and secondary

peer\_ip

-   Type: string
-   Required: true
-   Description: Define which server it should connect to reach its
    failover peer.

my\_ip

-   Type: string
-   Required: true
-   Description: Define the address that the server should listen for
    connections from its failover peer

Output type

application/json

err

-   Type: string
-   Required: true
-   Description: Deployment data validation result - OK: null, or in
    case of error, the error message here

rebootRequired

-   Type: boolean
-   Required: true
-   Description: Indicates whether reboot is required if the deployment
    data provided is applied,

Reference: deployment.json

\

Change appliance hostname
=========================

/hostname/{nePk}
----------------

### POST

Summary

add/updates the gms hostname

Notes

Change the hostname of a managed appliance

Input type

application/json

nePk

-   Description: The appliance\'s ne pk
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

hostName

-   Description: Json object containing the appliance host name
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

Reference: vxoaHostname.json

\

Tunnels in orchestrator
=======================

/tunnelsConfiguration/overlayInfo
---------------------------------

### GET

Summary

Get the tunnels in each overlay

Notes

This API returns all the tunnels in Orchestrator, in groups of; all
physical tunnels, tunnels in each overlay, and all tunnels. The
different groups can be found using the root object\'s keys:\

-   0 - all physical tunnels
-   1 - 7 - All bonded tunnels in that overlay (denoted by the underlay
    id, 1-7)
-   all - all physical and bonded tunnels

\
The tunnels are under two keys, the first is the source appliance
(nePk), and the second is the destination appliance (nePk)

Input type

None

Output type

application/json

0

-   Type: SourceNePkToDestNePkPhysicalTunnelsMap
-   Description: All the physical tunnels in Orchestrator.

1-7

-   Type: SourceNePkToDestNePkTunnelsMap
-   Description: All the bonded tunnels in a particular overlay. The key
    is the overlay ID.

all

-   Type: SourceNePkToDestNePkTunnelsMap
-   Description: All physical and bonded tunnels in the Orchestrator.

/tunnelsConfiguration/deployment
--------------------------------

### GET

Summary

Get deployment information for appliances managed by the Orchestrator.

Notes

This API returns the current deployment mode and list of lan/wan
interface details for each appliance managed by this Orchestrator

Input type

None

Output type

application/json

nePk

-   Type: TunnelsDeploymentInfoObj

/tunnelsConfiguration/passThroughTunnelsInfo
--------------------------------------------

### GET

Summary

Get all pass through tunnels from all appliances

Notes

Deprecated. Please use \'/tunnels2/passthrough\' to search passthrough
tunnels.

Input type

None

Output type

application/json

nePk

-   Type: nepkToPassThroughTunnelsMap

Reference: tunnelsConfig.json

\

Get domain name by IP address from dns stats table
==================================================

/stats/dnsInfo/{ip}
-------------------

### GET

Summary

Get domain name by IP address from dns stats table

Notes

Get domain name by IP address from dns stats table

Input type

None

ip

-   Description: IP address to get dns name.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

startTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endTime

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

dns

-   Type: string

Reference: dnsInfo.json

\

Appliance health summary
========================

/health
-------

### POST

Summary

Returns health summary of all appliances

Notes

\"appliance-id\" key corresponds to a specific appliance id

Input type

application/json

HealthSummaryGetPostBody

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

from

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range

to

-   Type: integer
-   Required: true
-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the ending time boundary of data time range

applianceIds

-   Type: array
-   Required: true
-   Description: Contains appliance IDs

verticals

-   Type: array
-   Required: true
-   Description: Contains vertical IDs

overlayId

-   Type: integer
-   Required: true
-   Description: Overlay id. -1 == all overlays

Output type

application/json

appliance-id

-   Type: array
-   Required: true

/health/alarmPeriodSummary
--------------------------

### GET

Summary

Returns summary of alarms for a given appliance for that time period

Input type

None

from

-   Description: Long(Signed 64 bits) value of milliseconds since EPOCH
    time indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

to

-   Description: Long(Signed 64 bits) value of milliseconds since EPOCH
    time indicating the ending time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

applianceId

-   Description: Appliance ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

critical

sequenceId

-   Type: integer
-   Required: true
-   Description: Alarm sequence id

description

-   Type: string
-   Required: true
-   Description: Alarm description

time

-   Type: integer
-   Required: true
-   Description: Time alarm occurred

minor

sequenceId

-   Type: integer
-   Required: true
-   Description: Alarm sequence id

description

-   Type: string
-   Required: true
-   Description: Alarm description

time

-   Type: integer
-   Required: true
-   Description: Time alarm occurred

major

sequenceId

-   Type: integer
-   Required: true
-   Description: Alarm sequence id

description

-   Type: string
-   Required: true
-   Description: Alarm description

time

-   Type: integer
-   Required: true
-   Description: Time alarm occurred

warning

sequenceId

-   Type: integer
-   Required: true
-   Description: Alarm sequence id

description

-   Type: string
-   Required: true
-   Description: Alarm description

time

-   Type: integer
-   Required: true
-   Description: Time alarm occurred

alarmCountsInPeriod

CRITICAL

-   Type: integer

MINOR

-   Type: integer

MAJOR

-   Type: integer

WARNING

-   Type: integer

/health/lossPeriodSummary
-------------------------

### GET

Summary

Returns summary of loss for a given appliance for that time period

Input type

None

time

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

overlayId

-   Description: Overlay id. -1 == all overlays
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

applianceId

-   Description: Appliance ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

criticalMins

-   Type: integer
-   Required: true
-   Description: Total critical loss minutes

majorMins

-   Type: integer
-   Required: true
-   Description: Total major loss minutes

normalMins

-   Type: integer
-   Required: true
-   Description: Total normal minutes

maxLossPercentage

-   Type: double
-   Required: true
-   Description: Max loss percentage

tunnelId

-   Type: string
-   Required: true
-   Description: Tunnel on which loss occurred

healthStatus

-   Type: string
-   Required: true
-   Description: health status

/health/latencyPeriodSummary
----------------------------

### GET

Summary

Returns summary of latency for a given appliance for that time period

Input type

None

time

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

overlayId

-   Description: Overlay id. -1 == all overlays
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

applianceId

-   Description: Appliance ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

criticalMins

-   Type: integer
-   Required: true
-   Description: Total critical loss minutes

majorMins

-   Type: integer
-   Required: true
-   Description: Total major loss minutes

normalMins

-   Type: integer
-   Required: true
-   Description: Total normal minutes

max

-   Type: integer
-   Required: true
-   Description: Max latency

tunnelId

-   Type: string
-   Required: true
-   Description: Tunnel on which latency occurred

healthStatus

-   Type: string
-   Required: true
-   Description: health status

/health/jitterPeriodSummary
---------------------------

### GET

Summary

Returns summary of jitter for a given appliance for that time period

Input type

None

time

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

overlayId

-   Description: Overlay id. -1 == all overlays
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

applianceId

-   Description: Appliance ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

criticalMins

-   Type: integer
-   Required: true
-   Description: Total critical loss minutes

majorMins

-   Type: integer
-   Required: true
-   Description: Total major loss minutes

normalMins

-   Type: integer
-   Required: true
-   Description: Total normal minutes

max

-   Type: integer
-   Required: true
-   Description: Max jitter

tunnelId

-   Type: string
-   Required: true
-   Description: Tunnel on which latency occurred

healthStatus

-   Type: string
-   Required: true
-   Description: health status

/health/mosPeriodSummary
------------------------

### GET

Summary

Returns summary of mos for a given appliance for that time period

Input type

None

time

-   Description: Long(Signed 64 bits) value of seconds since EPOCH time
    indicating the starting time boundary of data time range
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

overlayId

-   Description: Overlay id. -1 == all overlays
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

applianceId

-   Description: Appliance ID
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

criticalMins

-   Type: integer
-   Required: true
-   Description: Total critical MOS minutes

majorMins

-   Type: integer
-   Required: true
-   Description: Total major MOS minutes

normalMins

-   Type: integer
-   Required: true
-   Description: Total normal minutes

max

-   Type: integer
-   Required: true
-   Description: Max MOS

tunnelId

-   Type: string
-   Required: true
-   Description: Tunnel on which latency occurred

healthStatus

-   Type: string
-   Required: true
-   Description: health status

Reference: health.json

\

Orchestrator advance properties
===============================

/gms/advancedProperties
-----------------------

### GET

Summary

Get Orchestrator advanced properties

Input type

None

Output type

application/json

ParallelStatsTasks

-   Type: integer
-   Description: Default value is 20

jettyMinThreads

-   Type: integer
-   Description: Default value is 10

ParallelOrchestrationTasks

-   Type: integer
-   Description: Default value is 50

restRequestTimeout

-   Type: integer
-   Description: Default value is 60

excludedTableNames

-   Type: string
-   Description: Default value is
    \'dailyapp,dailydrc,dailydrops,dailydscp,dailyflow,dailyinterface,dailyjitter,dailymemory,dailynxtraffic,dailyshaper,dailytrafficclass,dailytunneltraffic,hourlyapp,hourlydns,hourlydrc,hourlydrops,hourlydscp,hourlyflow,hourlyflowapp,hourlyflowapp2,hourlyinterface,hourlyjitter,hourlylatency,hourlyloss,hourlynxtraffic,hourlyport,hourlyshaper,hourlytoptalkers,hourlytrafficclass,hourlytunneltraffic,minuteapp,minutedrc,minutedrops,minutedscp,minuteflow,minuteinterface,minutejitter,minutenxtraffic,minuteshaper,minutetrafficclass,minutetunneltraffic,neconfig2,hourlyBehavioral,minuteinterfaceoverlay,hourlyinterfaceoverlay,dailyinterfaceoverlay,restrequesttimestats,minutemos,hourlymos,dailymos,minuteboost,hourlyboost,dailyboost,minutesecuritypolicy,hourlysecuritypolicy,dailysecuritypolicy\'

sslExcludeCiphers

-   Type: string
-   Description: Default value is
    \'.\*NULL.\*,.\*RC4.\*,.\*MD5.\*,.\*DES.\*,.\*DSS.\*\'

ContentSecurityPolicyHeaderEnabled

-   Type: boolean
-   Description: Default value is true

excludeTables

-   Type: boolean
-   Description: Default value is false

jettyAcceptQueueSize

-   Type: integer
-   Description: Default value is 1000

bridgeCacheExpireTime

-   Type: integer
-   Description: Default value is 120

sslIncludeProtocols

-   Type: string
-   Description: Default value is \'TLSv1.2\'

dbPoolMaxConnectionLifeTime

-   Type: integer
-   Description: Default value is 300000

dbPoolIdleTimeout

-   Type: integer
-   Description: Default value is 120000

sslIncludeCiphers

-   Type: string
-   Description: Default value is \'TLS\_DHE\_RSA.\*,TLS\_ECDHE.\*\'

sslExcludeProtocols

-   Type: string
-   Description: Default value is
    \'SSL,SSLv3,SSLv2,SSLv2Hello,TLSv1,TLSv1.1\'

modifyTunnelBatchSize

-   Type: integer
-   Description: Default value is 500

dbPoolValidationTimeout

-   Type: integer
-   Description: Default value is 3000

mgmtInterface

-   Type: string
-   Description: Default value is \'eth0\'

dbPoolMinimumIdleConnections

-   Type: integer
-   Description: Default value is 10

emailImagesMaxSize

-   Type: integer
-   Description: Default value is 10

newSoftwareReleasesNotification

-   Type: boolean
-   Description: Default value is true

dbPoolConnectionTimeout

-   Type: integer
-   Description: Default value is 30000

ParallelActionTasks

-   Type: integer
-   Description: Default value is 50

threadPoolSize

-   Type: integer
-   Description: Default value is 1000

dbPoolLeakDetectionThreshold

-   Type: integer
-   Description: Default value is 300000

denyApplianceOnDelete

-   Type: boolean
-   Description: Default value is true

failedLoginAttemptThreshold

-   Type: integer
-   Description: Default value is 5

ParallelReachabilityTasks

-   Type: integer
-   Description: Default value is 20

restRequestStatsCollection

-   Type: boolean
-   Description: Default value is true

jettyMaxThreads

-   Type: integer
-   Description: Default value is 128

jettyIdleTimeout

-   Type: integer
-   Description: Default value is 60000

dbPoolMaxConnections

-   Type: integer
-   Description: Default value is 1000

MultipleOrchestratorsForOneZscalerAccount

-   Type: boolean
-   Description: Default value is false

bondedTunnelReorderWaitTime

-   Type: integer
-   Description: Default value is 100

fileOpsChunkSize

-   Type: integer
-   Description: Default value is 1024 \* 1024

### PUT

Summary

Update Orchestrator advanced properties

Input type

application/json

Advanced Properties

Description: JSON object for Orchestrator Advanced Properties.

Required: true

Parameter type: POST Body

Data Type: application/json

ParallelStatsTasks

-   Type: integer
-   Description: Default value is 20

jettyMinThreads

-   Type: integer
-   Description: Default value is 10

ParallelOrchestrationTasks

-   Type: integer
-   Description: Default value is 50

restRequestTimeout

-   Type: integer
-   Description: Default value is 60

excludedTableNames

-   Type: string
-   Description: Default value is
    \'dailyapp,dailydrc,dailydrops,dailydscp,dailyflow,dailyinterface,dailyjitter,dailymemory,dailynxtraffic,dailyshaper,dailytrafficclass,dailytunneltraffic,hourlyapp,hourlydns,hourlydrc,hourlydrops,hourlydscp,hourlyflow,hourlyflowapp,hourlyflowapp2,hourlyinterface,hourlyjitter,hourlylatency,hourlyloss,hourlynxtraffic,hourlyport,hourlyshaper,hourlytoptalkers,hourlytrafficclass,hourlytunneltraffic,minuteapp,minutedrc,minutedrops,minutedscp,minuteflow,minuteinterface,minutejitter,minutenxtraffic,minuteshaper,minutetrafficclass,minutetunneltraffic,neconfig2,hourlyBehavioral,minuteinterfaceoverlay,hourlyinterfaceoverlay,dailyinterfaceoverlay,restrequesttimestats,minutemos,hourlymos,dailymos,minuteboost,hourlyboost,dailyboost,minutesecuritypolicy,hourlysecuritypolicy,dailysecuritypolicy\'

sslExcludeCiphers

-   Type: string
-   Description: Default value is
    \'.\*NULL.\*,.\*RC4.\*,.\*MD5.\*,.\*DES.\*,.\*DSS.\*\'

ContentSecurityPolicyHeaderEnabled

-   Type: boolean
-   Description: Default value is true

excludeTables

-   Type: boolean
-   Description: Default value is false

jettyAcceptQueueSize

-   Type: integer
-   Description: Default value is 1000

bridgeCacheExpireTime

-   Type: integer
-   Description: Default value is 120

sslIncludeProtocols

-   Type: string
-   Description: Default value is \'TLSv1.2\'

dbPoolMaxConnectionLifeTime

-   Type: integer
-   Description: Default value is 300000

dbPoolIdleTimeout

-   Type: integer
-   Description: Default value is 120000

sslIncludeCiphers

-   Type: string
-   Description: Default value is \'TLS\_DHE\_RSA.\*,TLS\_ECDHE.\*\'

sslExcludeProtocols

-   Type: string
-   Description: Default value is
    \'SSL,SSLv3,SSLv2,SSLv2Hello,TLSv1,TLSv1.1\'

modifyTunnelBatchSize

-   Type: integer
-   Description: Default value is 500

dbPoolValidationTimeout

-   Type: integer
-   Description: Default value is 3000

mgmtInterface

-   Type: string
-   Description: Default value is \'eth0\'

dbPoolMinimumIdleConnections

-   Type: integer
-   Description: Default value is 10

emailImagesMaxSize

-   Type: integer
-   Description: Default value is 10

newSoftwareReleasesNotification

-   Type: boolean
-   Description: Default value is true

dbPoolConnectionTimeout

-   Type: integer
-   Description: Default value is 30000

ParallelActionTasks

-   Type: integer
-   Description: Default value is 50

threadPoolSize

-   Type: integer
-   Description: Default value is 1000

dbPoolLeakDetectionThreshold

-   Type: integer
-   Description: Default value is 300000

denyApplianceOnDelete

-   Type: boolean
-   Description: Default value is true

failedLoginAttemptThreshold

-   Type: integer
-   Description: Default value is 5

ParallelReachabilityTasks

-   Type: integer
-   Description: Default value is 20

restRequestStatsCollection

-   Type: boolean
-   Description: Default value is true

jettyMaxThreads

-   Type: integer
-   Description: Default value is 128

jettyIdleTimeout

-   Type: integer
-   Description: Default value is 60000

dbPoolMaxConnections

-   Type: integer
-   Description: Default value is 1000

MultipleOrchestratorsForOneZscalerAccount

-   Type: boolean
-   Description: Default value is false

bondedTunnelReorderWaitTime

-   Type: integer
-   Description: Default value is 100

fileOpsChunkSize

-   Type: integer
-   Description: Default value is 1024 \* 1024

Output type

None

/gms/advancedProperties/metadata
--------------------------------

### GET

Summary

Get Orchestrator metadata of the advanced properties

Input type

None

Output type

application/json

ParallelStatsTasks

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

jettyMinThreads

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

ParallelOrchestrationTasks

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

restRequestTimeout

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

excludedTableNames

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

sslExcludeCiphers

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

ContentSecurityPolicyHeaderEnabled

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

excludeTables

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

jettyAcceptQueueSize

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

bridgeCacheExpireTime

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

sslIncludeProtocols

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

dbPoolMaxConnectionLifeTime

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

dbPoolIdleTimeout

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

sslIncludeCiphers

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

sslExcludeProtocols

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

modifyTunnelBatchSize

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

dbPoolValidationTimeout

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

mgmtInterface

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

dbPoolMinimumIdleConnections

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

emailImagesMaxSize

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

newSoftwareReleasesNotification

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

dbPoolConnectionTimeout

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

ParallelActionTasks

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

threadPoolSize

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

dbPoolLeakDetectionThreshold

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

denyApplianceOnDelete

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

failedLoginAttemptThreshold

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

ParallelReachabilityTasks

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

restRequestStatsCollection

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

jettyMaxThreads

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

jettyIdleTimeout

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

dbPoolMaxConnections

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

MultipleOrchestratorsForOneZscalerAccount

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

bondedTunnelReorderWaitTime

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

fileOpsChunkSize

customValue

-   Type: string
-   Description: User changed value. Note: The data type \'string\' may
    vary based on the property. It can be either of integer,boolean or
    string

defaultValue

-   Type: string
-   Description: The initial default value. Note: The data type
    \'string\' may vary based on the property. It can be either of
    integer,boolean or string

isRestartRequired

-   Type: boolean
-   Description: Flag indicate whether to restart GMS or not to affect
    the property value change in the application

Reference: advancedProperties.json

\

returns all Appliances reboot history
=====================================

/gms/applianceRebootHistory
---------------------------

### GET

Summary

Return or sends reboot history of all the appliances

Notes

Depending on action parameter if the value is empty it returns all the
appliance history otherwise it will send all the reboot reports to
portal

Input type

None

action

-   Description: if the value is empty it returns all the appliance
    history otherwise it will send all the reboot reports to portal
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: applianceRebootHistory.json

\

Orchestrator peer priority
==========================

/appliance/peerPriorityList/{neId}?cached={cached}
--------------------------------------------------

### GET

Summary

Get all peer priority data on the appliance

Input type

None

neId

-   Description: Internal Id of the appliance from which you want to
    retrieve status information
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

peerlist

\<peerName\>

peer\_weight

-   Type: integer
-   Description: Peer weight

self

-   Type: string

Reference: peerPriority.json

\

Next hop health config
======================

/appliance/wanNextHopHealth/{neId}?cached={cached}
--------------------------------------------------

### GET

Summary

Get wan next hop health config on the appliance

Input type

None

neId

-   Description: Internal Id of the appliance from which you want to
    retrieve config information
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

enable

-   Type: boolean
-   Required: true
-   Description: Enable or disable health check

hold\_down\_count

-   Type: integer
-   Required: true
-   Description: Hold down count

interval

-   Type: integer
-   Required: true
-   Description: Interval

retry\_count

-   Type: integer
-   Required: true
-   Description: Retry count

Reference: wanNextHopHealth.json

\

Auto logout and Max sessions
============================

/gms/sessionTimeout
-------------------

### GET

Summary

Returns information of auto logout and max login session

Input type

None

Output type

application/json

resourceBase

-   Type: string
-   Required: true
-   Description: name of the main resource

resourceKey

-   Type: string
-   Description: name of the sub-resource. this is optional

configData

autoLogout

-   Type: integer
-   Required: true

maxSession

-   Type: integer

version

-   Type: integer
-   Description: id

/gms/sessionTimeout
-------------------

### PUT

Summary

Update auto logout and max login session

Input type

application/json

sessionDetail

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

autoLogout

-   Type: integer
-   Required: true

maxSession

-   Type: integer

Output type

None

Reference: sessionTimeout.json

\

User idle time
==============

/idle/clear
-----------

### GET

Summary

Clear idle time

Input type

None

Output type

None

/idle/increment
---------------

### GET

Summary

Increment idle time

Input type

None

Output type

application/json

isTimeout

-   Type: string
-   Required: true
-   Description: IF need port the waring logout dialog

Reference: IdleTime.json

\

returns and posts all Appliances crash history
==============================================

/gms/applianceCrashHistory
--------------------------

### GET

Summary

Return or sends crash history of all the appliances

Notes

Depending on action parameter if the value is empty it returns all the
appliance history otherwise it will send all the crash reports to portal

Input type

None

action

-   Description: if the value is empty it returns all the appliance
    history otherwise it will send all the crash reports to portal
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: applianceCrashHistory.json

\

Services list
=============

/gms/services
-------------

### GET

Summary

Returns all the services

Notes

The list of services is used to provide options in the Overlay editor\'s
Internet Policy section. For every service, a \'Send to \' option will
be shown in the available options list. The service name will correspond
to the peer name of the pass through tunnel the internet traffic will go
on. It is the user\'s responsibility to create these pass through
tunnels on appliances.

Input type

None

Output type

application/json

service\_1

-   Type: Service

service\_2

-   Type: Service

### POST

Summary

Save the service list

Notes

Saving a new service list will completely replace the current
implementation. Any service IDs that were saved previously, but not
included in the POST body will be removed. These services will also be
removed from the overlay\'s policy list.

Input type

application/json: undefined

New services

-   Description: Object of services you wish to save.
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/gms/thirdPartyServices
-----------------------

### GET

Summary

Returns all the Third Party Services

Notes

The list of services is used to provide options in the Overlay editor\'s
Internet Policy section. For every service, a dynamically generated
options list will be shown in the available options list. The service
name will correspond to the peer name of the pass through tunnel the
internet traffic will go on. Pass through tunnels will be generated
automatically.

Input type

None

Output type

application/json

\<service\_id\>

-   Type: ThirdPartyService

Reference: services.json

\

Internal Subnets
================

/gms/internalSubnets2
---------------------

### GET

Summary

Get the internal subnets

Notes

This is the list of internal subnets used to classify internet traffic.
Any traffic not matching these internal subnets will be classified as
internet traffic. This list will be pushed to appliances.

Input type

None

Output type

application/json

ipv4

-   Type: array

ipv6

-   Type: array

nonDefaultRoutes

-   Type: boolean

### POST

Summary

Save internal subnets

Notes

Save the list of internal subnets to use to classify internet traffic.
Any traffic not matching the internal subnets will be classified as
internet traffic. This list will be pushed to all appliances. User can
configure up to 512 subnets in each ipv4 and ipv6 entry.

Input type

application/json: undefined

undefined

-   Description: undefined
-   Required: undefined
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

Reference: internalSubnets.json

\

Access IP Whitelist
===================

/gms/ipwhitelist/external
-------------------------

### GET

Summary

Get the external IP/mask white list

Notes

This is the list of external subnet addresses allowed to access this
server.

Input type

None

Output type

None

### POST

Summary

Set the external IP/mask white list

Notes

This is the list of external subnet addresses allowed to access this
server.

Input type

application/json: undefined

subnets

-   Description: This is the list of external subnet addresses allowed
    to access this server.
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/gms/ipwhitelist/drops
----------------------

### GET

Summary

Get the IP addresses of the dropped requests to this server

Notes

Get the IP addresses of the dropped requests to this server

Input type

None

Output type

None

Reference: ipWhitelist.json

\

Application Definition
======================

/applicationDefinition/portProtocolClassification
-------------------------------------------------

### GET

Summary

return user defined data for IP/TCP/UDP classification

Notes

Use this api to view user defined application for IP/TCP/UDP
classification

Input type

None

resourceKey

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/applicationDefinition/portProtocolClassification/{port}/{protocol}
-------------------------------------------------------------------

### POST

Summary

Create/modify user defined application related to IP Protocol, TCP Port,
UDP Port

Notes

Use this api to create or modify a user defined application for IP
Protocol(port=0), TCP Protocol(protocol=6), UDP Protocol(protocol=17)

Input type

application/json: undefined

port

-   Description: port number
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

protocol

-   Description: protocol number
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

portProtocolConfig

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

port

-   Type: number
-   Required: true
-   Description: Port number, user 0 for IP Protocol application

protocol

-   Type: number
-   Required: true
-   Description: Protocol number, user 6 for TCP Port application , 17
    for UDP Port application

name

-   Type: string
-   Required: true
-   Description: application name

description

-   Type: string
-   Required: true

priority

-   Type: number
-   Required: true
-   Description: minimum 0, maximum 100

disabled

-   Type: boolean
-   Required: true

Output type

None

### DELETE

Summary

Delete user defined application related to IP Protocol, TCP Port, UDP
Port

Notes

Use this api to delete a user defined application for IP
Protocol(port=0), TCP Protocol(protocol=6), UDP Protocol(protocol=17)

Input type

None

port

-   Description: port number
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

protocol

-   Description: protocol number
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/applicationDefinition/dnsClassification
----------------------------------------

### GET

Summary

return user defined data for Domain Name classification

Notes

Use this api to view user defined application for Domain Name
classification

Input type

None

resourceKey

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/applicationDefinition/dnsClassification/{domain}
-------------------------------------------------

### POST

Summary

Create/modify user defined application related to Domain Name

Notes

Use this api to create or modify a user defined application for domain
name

Input type

application/json: undefined

domain

-   Description: domain
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

dnsConfig

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

domain

-   Type: string
-   Required: true
-   Description: Domain name, no slash

name

-   Type: string
-   Required: true
-   Description: application name

description

-   Type: string
-   Required: true

priority

-   Type: number
-   Required: true
-   Description: minimum 0, maximum 100

disabled

-   Type: boolean
-   Required: true

Output type

None

### DELETE

Summary

Delete user defined application related to Domain Name

Notes

Use this api to delete a user defined application for Domain Name

Input type

None

domain

-   Description: domain
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/applicationDefinition/ipIntelligenceClassification
---------------------------------------------------

### GET

Summary

return user defined data for Address Map classification

Notes

Use this api to view user defined application for Address Map
classification

Input type

None

resourceKey

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/applicationDefinition/ipIntelligenceClassification/{ipStart}/{ipEnd}
---------------------------------------------------------------------

### POST

Summary

Create/modify user defined application related to Address Map

Notes

Use this api to create or modify a user defined application for Address
Map

Input type

application/json: undefined

ipStart

-   Description: starting ip, IPv4 address in 32-bit integer format
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

ipEnd

-   Description: ending ip, IPv4 address in 32-bit integer format
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

ipIntelligenceConfig

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

ip\_start

-   Type: integer
-   Description: Starting IPv4 address in 32-bit integer format

ip\_end

-   Type: integer
-   Description: Ending IPv4 address in 32-bit integer format

name

-   Type: string
-   Required: true
-   Description: application name

description

-   Type: string
-   Required: true

priority

-   Type: number
-   Required: true
-   Description: minimum 0, maximum 100

country

-   Type: string
-   Required: true

country\_code

-   Type: string
-   Required: true

org

-   Type: string
-   Required: true

Output type

None

### DELETE

Summary

Delete user defined application related to Address Map

Notes

Use this api to delete a user defined application for Address Map

Input type

None

ipStart

-   Description: starting ip, IPv4 address in 32-bit integer format
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

ipEnd

-   Description: ending ip, IPv4 address in 32-bit integer format
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/applicationDefinition/meterFlowClassification
----------------------------------------------

### GET

Summary

return user defined data for Meter Flow classification

Notes

Use this api to view user defined application for Meter Flow
classification

Input type

None

resourceKey

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/applicationDefinition/meterFlowClassification/{flowType}/{mid}
---------------------------------------------------------------

### POST

Summary

Create/modify user defined application related to Meter Flow

Notes

Use this api to create or modify a user defined application for Meter
Flow

Input type

application/json: undefined

flowType

-   Description: flow type, e.g. MF
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

mid

-   Description: id
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

meterFlowConfig

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

flowType

-   Type: string
-   Required: true
-   Description: flow type, e.g. MF

mid

-   Type: number
-   Required: true
-   Description: id, starting from 1

name

-   Type: string
-   Required: true
-   Description: application name

description

-   Type: string
-   Required: true

priority

-   Type: number
-   Required: true
-   Description: minimum 0, maximum 100

disabled

-   Type: boolean
-   Required: true

Output type

None

### DELETE

Summary

Delete user defined application related to Meter Flow

Notes

Use this api to delete a user defined application for Meter Flow

Input type

None

flowType

-   Description: flow type, e.g. MF
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

mid

-   Description: id
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/applicationDefinition/compoundClassification
---------------------------------------------

### GET

Summary

return user defined data for Compound classification

Notes

Use this api to view user defined application for Compound
classification. Records with id greater 50000 are records from portal
modified by user.

Input type

None

resourceKey

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/applicationDefinition/compoundClassification/{id}
--------------------------------------------------

### POST

Summary

Create/modify user defined application related to Compound data

Notes

Use this api to create or modify a user defined application for Compound
data

Input type

application/json: undefined

id

-   Description: id
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

compoundConfig

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

id

-   Type: number
-   Required: true
-   Description: Record id. id \> 50000 is the record from portal
    modified by user. Please use the get api to find id

protocol

-   Type: string
-   Required: true
-   Description: protocol, e.g \'tcp\'. Leave this empty if you don\'t
    want to config this field

src\_ip

-   Type: string
-   Required: true
-   Description: src ip(IPv4) with subnet or range, e.g \'1.1.1.1/32\'
    or \'1.1.1.1-222\' or \'1.1.1.1/32, 1.1.1.1-222\'. Leave this empty
    if you don\'t want to config this field

dst\_ip

-   Type: string
-   Required: true
-   Description: destination ip(IPv4) with subnet or range, e.g
    \'1.1.1.1/32\' or \'1.1.1.1-222\' or \'1.1.1.1/32, 1.1.1.1-222\'.
    Leave this empty if you don\'t want to config this field

either\_ip

-   Type: string
-   Required: true
-   Description: either ip(IPv4) with subnet or range, e.g
    \'1.1.1.1/32\' or \'1.1.1.1-222\' or \'1.1.1.1/32, 1.1.1.1-222\'. If
    you specified the \'src\_ip\' or \'dst\_ip\', please leave this
    empty. Leave this empty if you don\'t want to config this field

src\_service

-   Type: string
-   Required: true
-   Description: src service (saas app name or organization). Leave this
    empty if you don\'t want to config this field

dst\_service

-   Type: string
-   Required: true
-   Description: destination service (saas app name or organization).
    Leave this empty if you don\'t want to config this field

either\_service

-   Type: string
-   Required: true
-   Description: either service (saas app name or organization). If you
    specified the \'src\_service\' or \'dst\_service\', please leave
    this empty. Leave this empty if you don\'t want to config this field

src\_dns

-   Type: string
-   Required: true
-   Description: src dns. Leave this empty if you don\'t want to config
    this field

dst\_dns

-   Type: string
-   Required: true
-   Description: destination dns. Leave this empty if you don\'t want to
    config this field

either\_dns

-   Type: string
-   Required: true
-   Description: either dns. If you specified the \'src\_dns\' or
    \'dst\_dns\', please leave this empty. Leave this empty if you
    don\'t want to config this field

src\_geo

-   Type: string
-   Required: true
-   Description: src geo location. Leave this empty if you don\'t want
    to config this field

dst\_geo

-   Type: string
-   Required: true
-   Description: destination geo location. Leave this empty if you
    don\'t want to config this field

either\_geo

-   Type: string
-   Required: true
-   Description: either geo location. If you specified the \'src\_geo\'
    or \'dst\_geo\', please leave this empty. Leave this empty if you
    don\'t want to config this field

src\_port

-   Type: string
-   Required: true
-   Description: src port number or range, e.g \'12345\' or \'123-456\'
    or \'33,44,55-66\'. Leave this empty if you don\'t want to config
    this field

dst\_port

-   Type: string
-   Required: true
-   Description: destination port number or range, e.g \'12345\' or
    \'123-456\' or \'33,44,55-66\'. Leave this empty if you don\'t want
    to config this field

either\_port

-   Type: string
-   Required: true
-   Description: either src or destination port number or range, e.g
    \'12345\' or \'123-456\' or \'33,44,55-66\'. If you specified the
    \'src\_port\' or \'dst\_port\', please leave this empty. Leave this
    empty if you don\'t want to config this field

vlan

-   Type: string
-   Required: true
-   Description: interface. Leave this empty if you don\'t want to
    config this field

dscp

-   Type: string
-   Required: true
-   Description: DSCP. Leave this empty if you don\'t want to config
    this field

name

-   Type: string
-   Required: true
-   Description: application name

description

-   Type: string
-   Required: true

confidence

-   Type: number
-   Required: true
-   Description: minimum 0, maximum 100

disabled

-   Type: boolean
-   Required: true

Output type

None

### DELETE

Summary

Delete user defined application related to Compound data

Notes

Use this api to delete a user defined application for Compound data

Input type

None

id

-   Description: id
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/applicationDefinition/compoundClassification/reorder
-----------------------------------------------------

### POST

Summary

Change the order of user defined application related to Compound data

Notes

Use this api to change the order for all user defined application for
Compound data. Please use the GET api to find all the ids. You need to
include all the ids which are less or equal to 50000. You are not
allowed to change the order for rules from portal.

Input type

application/json: undefined

compoundIdsConfig

-   Description: an array of comma separated ids. Please excludes ids
    greater than 50000. Please check the get api to find those ids.
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/applicationDefinition/legacyAppliancesUdas
-------------------------------------------

### GET

Summary

return legacy UDA configuration on each appliance

Notes

Use this api to view the legacy UDAs on each appliance. If you do not
see anything, this means you do not have any UDAs available on appliance
at migration time.

Input type

None

Output type

None

/applicationDefinition/applicationTags/wildcard
-----------------------------------------------

### POST

Summary

Search application groups based on wildcard and limit

Notes

Use this api to search application groups

Input type

application/json: undefined

searchWildcard

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

pattern

-   Type: string
-   Required: true

limit

-   Type: number
-   Description: default is 30

Output type

None

/applicationDefinition/applications/wildcard
--------------------------------------------

### POST

Summary

Search applications based on wildcard and limit

Notes

Use this api to search application

Input type

application/json: undefined

searchWildcard

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

pattern

-   Type: string
-   Required: true

limit

-   Type: number
-   Description: default is 30

Output type

None

/applicationDefinition/applicationTags
--------------------------------------

### GET

Summary

return user defined application groups

Notes

Use this api to find user defined application groups

Input type

None

resourceKey

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

### POST

Summary

Create user defined application groups

Notes

Warning: this api is not used for editing a single application group.
Please use the same GET api to find all user defined application groups
information first. Use this api to create application groups.

Input type

application/json: undefined

appTagsBody

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

\<userDefinedGroupName\>

apps

-   Type: array

parentGroup

-   Type: array

Output type

None

/applicationDefinition/applicationTags/{mode}
---------------------------------------------

### POST

Summary

Change mode to application groups and user defined applications

Notes

You are not allowed to change back to legacy mode if you are currently
using our cloud based application definition

Input type

Unknown

mode

-   Description: either \'old\' or \'new\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/applicationDefinition/saasClassification
-----------------------------------------

### GET

Summary

return user defined data for SaaS classification

Notes

Use this api to view user defined application for SaaS classification.

Input type

None

resourceKey

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/applicationDefinition/saasClassification/{id}
----------------------------------------------

### POST

Summary

Create/modify user defined application related to SaaS data

Notes

Use this api to create or modify a user defined application for SaaS
data. Set the saasId to -1 if you need to create a new SaaS application.

Input type

application/json: undefined

id

-   Description: id
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

saasConfig

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

addresses

-   Type: array
-   Description: Array of subnets associated with this cloud
    application.

application

-   Type: string
-   Description: Name of cloud application

processedAt

-   Type: string
-   Description: For internal use

saasId

-   Type: number
-   Description: Unique identifier for cloud application

domains

-   Type: array
-   Description: Array of domain names associated with this cloud
    application.

threshold

-   Type: number
-   Description: Default RTT threshold value from portal

ports

-   Type: array
-   Description: Array of ports associated with this cloud application

enabled

-   Type: boolean
-   Description: For internal use

Output type

None

### DELETE

Summary

Delete user defined application related to SaaS data

Notes

Use this api to delete a user defined application for SaaS data

Input type

None

id

-   Description: id
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

Reference: applicationDefinition.json

\

High Availability (HA) appliance groups
=======================================

/haGroups
---------

### GET

Summary

Returns a collection of appliances paired in a HA configuration

Input type

None

Output type

application/json

\<indexNumber\>

appliances

-   Type: array
-   Required: true
-   Description: Appliance pair

subnet

-   Type: string
-   Required: true
-   Description: Subnet to be used to allocate IPs to internal HA VLAN
    interfaces, default is 169.254.1.0/24.

mask

-   Type: number
-   Required: true
-   Description: Mask value to be assigned to internal HA VLAN
    interfaces. This value will be used to carve out smaller ip blocks
    from above larger subnet, default is 30

vlanStart

-   Type: number
-   Required: true
-   Description: Starting VLAN id value to be assigned to internal HA
    VLAN interfaces. Will increment by 1 for multiple VLAN interfaces,
    default start value is 100

### POST

Summary

Modify appliances paired in a HA configuration

Input type

application/json

haGroups

Description: JSON object containing collection of appliances paired in a
HA configuration

Required: true

Parameter type: POST Body

Data Type: application/json

\<indexNumber\>

appliances

-   Type: array
-   Required: true
-   Description: Appliance pair

subnet

-   Type: string
-   Required: true
-   Description: Subnet to be used to allocate IPs to internal HA VLAN
    interfaces, default is 169.254.1.0/24.

mask

-   Type: number
-   Required: true
-   Description: Mask value to be assigned to internal HA VLAN
    interfaces. This value will be used to carve out smaller ip blocks
    from above larger subnet, default is 30

vlanStart

-   Type: number
-   Required: true
-   Description: Starting VLAN id value to be assigned to internal HA
    VLAN interfaces. Will increment by 1 for multiple VLAN interfaces,
    default start value is 100

Output type

None

Reference: haGroups.json

\

Orchestrator AVC mode
=====================

/avcMode
--------

### GET

Summary

Returns Orchestrator avc Mode

Input type

None

Output type

application/json

avc

-   Type: string
-   Required: true
-   Description: \'mixed\', it means that there are both old
    appliances(below 8.1.6 version) and new appliances(8.1.6 and above)
    in Orch.; \'all\', it means that all of appliances in Orch are
    new(8.1.6 and above).\'none\', it means that all of appliances in
    Orch are old(below 8.1.6 version)

Reference: avcMode.json

\

UDP IPSec Key Status
====================

/ikelessSeedStatus
------------------

### GET

Summary

Get UDP IPSec key status for all appliances

Notes

Returns a representation of key status with whether or not the current
key is present, its activation status, and an arbitrary seed number. If
an appliance has an arbitrary id of 0, it has the same key as
orchestrator. If two appliances have 1 and 2 as their arbitrary ids, it
means that their keys do not match orchestrator, nor do they match one
another\'s

Input type

None

Output type

application/json

\<nePk\>

arbitrary

-   Type: integer
-   Description: arbitrary id for key for debugging

hasCurrentSeed

-   Type: boolean
-   Description: If the appliance has the current key

activationStatus

-   Type: boolean
-   Description: If the key is active

Reference: ikeless.json

\

Enable/disable stats collection by orchestrator
===============================================

/gms/statsCollection
--------------------

### GET

Summary

Gets stats collection enable/disable details

Input type

None

Output type

application/json

Application

-   Type: boolean

Dns

-   Type: boolean

Drc

-   Type: boolean

Drops

-   Type: boolean

Dscp

-   Type: boolean

Flow

-   Type: boolean

Interface

-   Type: boolean

Jitter

-   Type: boolean

Port

-   Type: boolean

Shaper

-   Type: boolean

TopTalkers

-   Type: boolean

Tunnel

-   Type: boolean

### POST

Summary

Used to enable/disable stats collection by orchestrator

Input type

application/json

statsCollection

Description: Json object for enable/disable stats collection by
orchestrator

Required: true

Parameter type: POST Body

Data Type: application/json

Application

-   Type: boolean

Dns

-   Type: boolean

Drc

-   Type: boolean

Drops

-   Type: boolean

Dscp

-   Type: boolean

Flow

-   Type: boolean

Interface

-   Type: boolean

Jitter

-   Type: boolean

Port

-   Type: boolean

Shaper

-   Type: boolean

TopTalkers

-   Type: boolean

Tunnel

-   Type: boolean

Output type

None

/gms/statsCollection/default
----------------------------

### GET

Summary

To get default values for stats collection

Input type

application/json

Output type

application/json

Application

-   Type: boolean

Dns

-   Type: boolean

Drc

-   Type: boolean

Drops

-   Type: boolean

Dscp

-   Type: boolean

Flow

-   Type: boolean

Interface

-   Type: boolean

Jitter

-   Type: boolean

Port

-   Type: boolean

Shaper

-   Type: boolean

TopTalkers

-   Type: boolean

Tunnel

-   Type: boolean

Reference: gmsStatsCollection.json

\

Managing Orchestrator Database Partitions
=========================================

/dbPartition/info
-----------------

### GET

Summary

Gets Orchestrator database partition details

Input type

None

table

-   Description: Name of the table
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

partition

-   Description: Name of the partition
-   Required: undefined
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/dbPartition/{table}/{partition}
--------------------------------

### DELETE

Summary

Deletes the table\'s partition

Notes

Delete default & current partition cannot be deleted

Input type

None

table

-   Description: Name of the table
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

partition

-   Description: Name of the partition
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: dbPartition.json

\

Admin Distance
==============

/appliance/adminDistance/{neId}?cached={cached}
-----------------------------------------------

### GET

Summary

Gets Admin Distance configurations

Input type

None

neId

-   Description: Internal Id of the appliance from which you want to
    retrieve config information, usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false).
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

sub\_shared\_bgp

-   Type: integer
-   Description: Distance of Subnet Shared - BGP Remote

local

-   Type: integer
-   Description: Distance of Local

bgp\_br

-   Type: integer
-   Description: Distance of BGP Branch

bgp\_tr

-   Type: integer
-   Description: Distance of BGP Transit

bgp\_pe

-   Type: integer
-   Description: Distance of BGP PE

sub\_shared

-   Type: integer
-   Description: Distance of Subnet Shared - Static Routes

ospf

-   Type: integer
-   Description: Distance of OSPF

sub\_shared\_ospf

-   Type: integer
-   Description: Distance of Subnet Shared - OSPF Remote

Reference: adminDistance.json

\

Getting one VXOA port forwarding rules
======================================

/portForwarding/{neId}
----------------------

### GET

Summary

Get port forwarding rules on a VXOA.

Notes

Get port forwarding rules on a VXOA.

Input type

None

neId

-   Description: Ne Id of the VXOA to get
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: portForwarding.json

\

Gets bgp config and state information
=====================================

/bgp/config/system/{neId}
-------------------------

### GET

Summary

Get BGP system configuration data.

Input type

None

neId

-   Description: neId of the appliance
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

rtr\_id

-   Type: string
-   Description: router id

asn

-   Type: integer
-   Description: Autonomous System Number

enable

-   Type: boolean
-   Description: Flag to enable/disable bgp

redist\_ospf

-   Type: boolean
-   Description: Flag to control redistribution of routes to ospf

redist\_ospf\_filter

-   Type: integer
-   Description: used to filter redistributed routes to ospf

graceful\_restart\_en

-   Type: boolean
-   Description: Flag to control graceful restart. This filed is only
    applicable from 8.1.9.3 or higher appliances.Remove this field
    before applying to older appliances.

max\_restart\_time

-   Type: integer
-   Description: Max waiting time for a bgp peer to restart before
    deleting its advertised routes from routing tables. value must be
    between 1 to 3600(inclusive). This filed is only applicable from
    8.1.9.3 or higher appliances. Remove this field before applying to
    older appliances

stale\_path\_time

-   Type: integer
-   Description: The stale\_path\_time specifies the maximum time (in
    seconds) following a peer restart that SP waits before removing
    stale routes associated with that peer.value must be between 1 to
    3600(inclusive). This filed is only applicable from 8.1.9.3 or
    higher appliances.

remote\_as\_path\_advertise

-   Type: boolean
-   Description: Flag to control remote as path propagation. This filed
    is only applicable from 8.1.9.4 or higher appliances.Remove this
    field before applying to older appliances.

/bgp/config/neighbor/{neId}
---------------------------

### GET

Summary

Get specific neighbor configuration data.

Input type

None

neId

-   Description: neId of the appliance
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

IpAddress1

self

-   Type: string
-   Required: true
-   Description: Ip address of the neighbor

remote\_as

-   Type: integer
-   Required: true
-   Description: Remote Autonomous System Number of the neighbor

type

-   Type: string
-   Required: true
-   Description: Peer Type of the neighbor

import\_rtes

-   Type: boolean
-   Required: true
-   Description: flag to enable/disable importing routes of the neighbor

export\_map

-   Type: integer
-   Required: true
-   Description: Integer value of the \'Routes Export Policies\'
    bitmask. Right now we have nine Route Export policies for each Peer
    Type namely\
    1) Locally configured,\
    2) Learned via subnet sharing,\
    3) Learned from a local BGP branch peer,\
    4) Learned from a local BGP branch-transit peer,\
    5)Learned from a local BGP PE router,\
    6)Remote BGP,\
    7) Remote BGP branch-transit peer, 8) Learned from a local OSPF
    peer,\
    9) Learned from a Remote OSPF peer.\
    These seven policies are represented with 9 binary values from right
    to left (1111111). We can turn on/off the policy by simple modifying
    respective binary value. For Example, to turn off export route
    policies 1 and 3 the bit map will look like 111111010. The
    equivalent decimal value for the bitmask is 506. So the rest API
    field should look like \'export\_map\': 506. Some of the predefined
    import\_map values for the different Peer Types are {\'Branch\':511,
    \'Branch-transit\':431 and \'PE-router\':141}.By default we send a
    decimal value 4294967295 indicating the predefined bitmask of the
    Peer Type is not modified

password

-   Type: string
-   Required: true
-   Description: MD5 Password of the neighbor

enable

-   Type: boolean
-   Description: flag to enable/disable neighbor bgp session

loc\_pref

-   Type: integer
-   Required: true
-   Description: Local preference for advertised routes to the neighbor

med

-   Type: integer
-   Required: true
-   Description: Multi-Exit Discriminator to use for advertised routes
    to the neighbor

in\_med

-   Type: integer
-   Required: true
-   Description: input metric of routes received from the neighbor

as\_prepend

-   Type: integer
-   Required: true
-   Description: Count of additional times to prepend the local AS in
    the AS path

ka

-   Type: integer
-   Required: true
-   Description: Time in seconds - specifies how frequently the device
    will send KEEP ALIVE messages to its BGP neighbors

hold

-   Type: integer
-   Required: true
-   Description: Time in seconds - specifies how long the device will
    wait for a KEEP ALIVE or UPDATE message from a neighbor before
    concluding that the neighbor is dead

next\_hop\_self

-   Type: boolean
-   Description: Flag to send own ip as nexthop when advertising route
    to others. This filed is only applicable from 8.1.9.4 or higher
    appliances.Remove this field before applying to older appliances.

/bgp/state/{neId}
-----------------

### GET

Summary

Get specific appliance bgp state details.

Input type

None

neId

-   Description: neId of the appliance
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

neighbor

neighborCount

-   Type: integer
-   Description: Count of number of neighbor

neighborState

0

peer\_ip

-   Type: string
-   Description: Ip address of the neighbor

asn

-   Type: integer
-   Description: Autonomous System Number of the neighbor

last\_err

-   Type: integer
-   Description: last error code

last\_err\_subcode

-   Type: integer
-   Description: Last Subnet Error Code

local\_ip

-   Type: string
-   Description: Local Ip address of the neighbor

peer\_caps

-   Type: string

peer\_state

-   Type: integer
-   Description: state of the neighbor session

peer\_state\_str

-   Type: srting
-   Description: string representation of neighbor state

rcvd\_last\_err

-   Type: integer
-   Description: Last received error code

sent\_last\_err

-   Type: integer
-   Description: Last error code send by neighbor

rcvd\_last\_err\_subcode

-   Type: integer
-   Description: Last received subnet error code

sent\_last\_err\_subcode

-   Type: integer
-   Description: Last subnet error code send by neighbor

rcvd\_last\_err\_time

-   Type: timestamp
-   Description: Last received error time

sent\_last\_err\_time

-   Type: integer
-   Description: Last sent error time

rcvd\_pfxs

-   Type: integer
-   Description: Number of routes received from neighbor

sent\_pfxs

-   Type: integer
-   Description: Number of routes send by neighbor

rcvd\_updates

-   Type: integer
-   Description: Number of updates received from neighbor

sent\_updates

-   Type: integer
-   Description: Number of updates send by neighbor

rtr\_id

-   Type: string
-   Description: Router ip address

time\_established

-   Type: integer
-   Description: bgp session established time

time\_last\_update

-   Type: integer
-   Description: last updated received time

1

peer\_ip

-   Type: string
-   Description: Ip address of the neighbor

asn

-   Type: integer
-   Description: Autonomous System Number of the neighbor

last\_err

-   Type: integer
-   Description: last error code

last\_err\_subcode

-   Type: integer
-   Description: Last Subnet Error Code

local\_ip

-   Type: string
-   Description: Local Ip address of the neighbor

peer\_caps

-   Type: string

peer\_state

-   Type: integer
-   Description: state of the neighbor session

peer\_state\_str

-   Type: srting
-   Description: string representation of neighbor state

rcvd\_last\_err

-   Type: integer
-   Description: Last received error code

sent\_last\_err

-   Type: integer
-   Description: Last error code send by neighbor

rcvd\_last\_err\_subcode

-   Type: integer
-   Description: Last received subnet error code

sent\_last\_err\_subcode

-   Type: integer
-   Description: Last subnet error code send by neighbor

rcvd\_last\_err\_time

-   Type: timestamp
-   Description: Last received error time

sent\_last\_err\_time

-   Type: integer
-   Description: Last sent error time

rcvd\_pfxs

-   Type: integer
-   Description: Number of routes received from neighbor

sent\_pfxs

-   Type: integer
-   Description: Number of routes send by neighbor

rcvd\_updates

-   Type: integer
-   Description: Number of updates received from neighbor

sent\_updates

-   Type: integer
-   Description: Number of updates send by neighbor

rtr\_id

-   Type: string
-   Description: Router ip address

time\_established

-   Type: integer
-   Description: bgp session established time

time\_last\_update

-   Type: integer
-   Description: last updated received time

summary

bgp\_state

-   Type: integer
-   Description: Overall state of the routerd & bgp processes. The state
    will be one of the following({0 =Not Enabled, 1 = Down, 2=Mgmt Stub
    Initializing, 3=Mgmt Stub Active,4=RTM Initializing, 5=RTM Active,
    6=RM Initializing,7=RM Active, 8=NM Intitializing, 9=Active,
    10=Unknown})

bgp\_state\_str

-   Type: integer
-   Description: String representation of the bgp state

local\_asn

-   Type: integer
-   Description: local asn number

local\_ip

-   Type: string
-   Description: local ip address

mgmt\_stub\_last\_err

-   Type: integer
-   Description: last error code from routerd

mgmt\_stub\_last\_err\_str

-   Type: string
-   Description: last error string from routerd

mgmt\_stub\_last\_err\_subcode

-   Type: integer

mgmt\_stub\_last\_err\_time

-   Type: integer
-   Description: last routerd error time

mgmt\_stub\_tot\_errors

-   Type: integer
-   Description: total number of errors from routerd

num\_bgp\_rtes\_rcvd

-   Type: integer
-   Description: number of subnets learned from BGP peers

num\_ebgp\_rtes

-   Type: integer
-   Description: number of subnets learned from EBGP peers

num\_ibgp\_rtes

-   Type: integer
-   Description: number of subnets learned from IBGP peers

num\_peers

-   Type: integer
-   Description: Total number of bgp peers configured

num\_peers\_active

-   Type: integer
-   Description: active peers count

num\_rib\_rtes

-   Type: integer

num\_rtm\_rtes

-   Type: integer

num\_subs\_installed

-   Type: integer
-   Description: Number of subnets advertised to BGP peers

reject\_mismatches

-   Type: integer
-   Description: subnets rejected due to bad scope

reject\_unpreferred

-   Type: integer
-   Description: subnets rejected because other preferred

rm\_status

-   Type: integer

rtm\_status

-   Type: integer

rtr\_id

-   Type: string
-   Description: router id

socket\_retry\_cnt

-   Type: integer

tunbgp\_last\_err

-   Type: integer
-   Description: last error code from tunneld-bgp

tunbgp\_last\_err\_str

-   Type: string
-   Description: last error string from tunneld-bgp

tunbgp\_last\_err\_subcode

-   Type: integer

tunbgp\_last\_err\_time

-   Type: integer
-   Description: last error time from tunneld-bgp

tunbgp\_tot\_errors

-   Type: integer
-   Description: total tunneld-bgp errors

Reference: bgp.json

\

Add usage count for one UI feature
==================================

/uiUsageStats/{uiName}
----------------------

### POST

Summary

Add usage count for one UI feature

Notes

Add usage count for one UI feature.

Input type

Unknown

uiName

-   Description: The title of the dialogue or the name of the tab used
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: uiUsageStats.json

\

Get time used info of rest requests sent to appliances through web socket.
==========================================================================

/restRequestTimeStats/summary
-----------------------------

### GET

Summary

Returns summary of time used info of rest requests sent to appliances

Notes

The summary is grouped by nepk, resource and portalWS

Input type

None

nePk

-   Description: nePk of the appliance, \'all\' means all appliances
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

resource

-   Description: Base resource of the appliance to sent to, \'all\'
    means all resources
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

portalWS

-   Description: Through what web socket the requests were sent, true
    \-- portal web socket, false \-- direct web socket
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

timedout

-   Description: Whether the requests timedout
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

from

-   Description: The minimum epoch time when the requests were sent
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

to

-   Description: The maximum epoch time when the requests were sent, 0
    stands for till now
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/restRequestTimeStats/{nePk}/%2F{resource}/{portalWS}/{method}
--------------------------------------------------------------

### GET

Summary

The time used details of all rest requests sent to an appliance\'s
specific resource through portal web socket or web socket

Input type

None

nePk

-   Description: nePk of the appliance
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

resource

-   Description: Base resource of the appliance to sent to, for example
    /webconfig should just be typed webconfig here
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

portalWS

-   Description: Through what web socket this request was sent, true \--
    portal web socket, false \-- direct web socket
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

method

-   Description: The request method
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

from

-   Description: The minimum epoch time when the requests were sent
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

to

-   Description: The maximum epoch time when the requests were sent, 0
    stands for till now
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

Reference: restRequestTimeStats.json

\

apis for get config and state of ospf
=====================================

/ospf/config/system/{neId}
--------------------------

### GET

Summary

Get appliance\'s OSPF system level configuration data.

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

enable

-   Type: boolean
-   Description: Flag to enable/disable OSPF

routerId

-   Type: string
-   Description: router id

redistBgp

-   Type: boolean
-   Description: boolean Flag to control redistribution of BGP routes to
    OSPF

redistLocal

-   Type: boolean
-   Description: boolean Flag to control redistribution of local routes
    to OSPF

redistSubnetShare

-   Type: boolean
-   Description: boolean Flag to control redistribution of Silver Peak
    to OSPF

bgpRedistMetricType

-   Type: integer
-   Description: used to specify metric type for redistributed bgp
    routes to OSPF (1 - E1 and 2 - E2)

bgpRedistMetric

-   Type: integer
-   Description: metric assign to the redistributed bgp routes to OSPF

bgpRedistTag

-   Type: integer
-   Description: tag assign to the redistributed bgp routes to OSPF

localRedistMetricType

-   Type: integer
-   Description: used to specify metric type for redistributed local
    routes to OSPF (1 - E1 and 2 - E2)

localRedistMetric

-   Type: integer
-   Description: metric assign to the redistributed local routes to OSPF

localRedistTag

-   Type: integer
-   Description: tag assign to the redistributed local routes to OSPF

subnetShareRedistMetricType

-   Type: integer
-   Description: used to specify metric type for redistributed silver
    peak routes to OSPF (1 - E1 and 2 - E2)

subnetShareRedistMetric

-   Type: integer
-   Description: metric assign to the redistributed silver peak routes
    to OSPF

subnetShareRedistTag

-   Type: integer
-   Description: tag assign to the redistributed silver peak routes to
    OSPF

/ospf/config/interfaces/{neId}
------------------------------

### GET

Summary

Get OSPF interfaces configuration data.

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

\<nePk\>

-   Type: array
-   Required: true
-   Description: all interface endpoints\' information of this appliance

/ospf/state/system/{neId}
-------------------------

### GET

Summary

Gets the state of the OSPF.

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

ospf\_enabled

-   Type: boolean
-   Description: Flag to enable/disable OSPF

max\_ls\_intvl

-   Type: integer
-   Description: Maximum link state advertisement interval

min\_lsa\_interval

-   Type: integer
-   Description: Minimum link state advertisement interval

hold\_ls\_intvl

-   Type: integer
-   Description: hold link state advertisement interval

num\_nbrs

-   Type: integer
-   Description: neighbors count

extnl\_lsa\_refresh\_intvl

-   Type: integer
-   Description: external link state advertisement refresh interval

extnl\_lsa\_cnt

-   Type: integer
-   Description: external lsa count

min\_lsa\_arrival

-   Type: integer
-   Description: min link state advertisement count

originated\_new\_lsas

-   Type: integer
-   Description: Originated new LSAs

router\_id

-   Type: string
-   Description: ospf router id

redist\_bgp

-   Type: boolean
-   Description: boolean Flag to control redistribution of BGP routes to
    OSPF

redist\_local

-   Type: boolean
-   Description: boolean Flag to control redistribution of local routes
    to OSPF

redist\_subshared

-   Type: boolean
-   Description: boolean Flag to control redistribution of Silver Peak
    to OSPF

redist\_bgp\_route\_type

-   Type: integer
-   Description: used to specify metric type for redistributed bgp
    routes to OSPF (1 - E1 and 2 - E2)

redist\_local\_route\_type

-   Type: integer
-   Description: used to specify metric type for redistributed local
    routes to OSPF (1 - E1 and 2 - E2)

redist\_subshared\_route\_type

-   Type: integer
-   Description: used to specify metric type for redistributed silver
    peak routes to OSPF (1 - E1 and 2 - E2)

redist\_bgp\_route\_tag

-   Type: integer
-   Description: tag assign to the redistributed bgp routes to OSPF

redist\_local\_route\_tag

-   Type: integer
-   Description: tag assign to the redistributed local routes to OSPF

redist\_subshared\_route\_tag

-   Type: integer
-   Description: tag assign to the redistributed silver peak routes to
    OSPF

redist\_bgp\_metric

-   Type: integer
-   Description: metric assign to the redistributed bgp routes to OSPF

redist\_local\_metric

-   Type: integer
-   Description: metric assign to the redistributed local routes to OSPF

redist\_subshared\_metric

-   Type: integer
-   Description: metric assign to the redistributed silver peak routes
    to OSPF

route\_calc\_max\_delay

-   Type: integer
-   Description: Route calculation max delay

num\_learned\_extnl\_ospf

-   Type: integer

proto\_version

-   Type: integer
-   Description: Protocol version

pm\_admin\_state

-   Type: string
-   Description: Process manager admin state

/ospf/state/interfaces/{neId}
-----------------------------

### GET

Summary

Gets the state of the OSPF interfaces.

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

interfaceState

-   Type: array

/ospf/state/neighbors/{neId}
----------------------------

### GET

Summary

Gets the state of the OSPF neighbors.

Input type

None

neId

-   Description: neId is the device key assigned by Orchestrator,
    usually look like \'0.NE\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

interfaceState

-   Type: array

Reference: ospf.json

\

apis for manage remote log receiver
===================================

/remoteLogReceiver
------------------

### GET

Summary

To get all remote log receiver configurations

Notes

Remote log receiver configurations are returned in an array of objects.
Response body: Array of Config Objects. HTTP(S)ReceiverConfig: { \"id\":
integer, \"receiverType\": integer, \"enabled\": boolean, \"name\":
string, \"logType\": integer, \"url\": string } KAFKAProducerConfig: {
\"id\": integer, \"receiverType\": integer, \"enabled\": boolean,
\"name\": string, \"logType\": integer, \"topic\": string, \"brokers\":
string, \"acks\": string, \"retries\": integer, \"batchSize\": integer,
\"bufferSize\": integer, \"lingerTime\": integer } SYSLOGReceiverConfig:
{ \"id\": integer, \"receiverType\": integer, \"enabled\": boolean,
\"name\": string, \"logType\": integer, \"hostname\": string, \"port\":
integer, \"protocol\": integer, \"customData\": string,
\"auditFacility\": integer, \"alarmFacility\": integer,
\"auditErrorSeverity\": integer \"auditDebugSeverity\": integer,
\"auditInfoSeverity\": integer, \"alarmCriticalSeverity\": integer,
\"alarmMajorSeverity\": integer, \"alarmMinorSeverity\": integer,
\"alarmWarningSeverity\": integer }

Input type

None

Output type

None

### POST

Summary

Add remote log receiver(s) with configuration provided in the request
body

Notes

Post body is an array of objects WITHOUT id field. \[ {
\"receiverType\": 0, \"enabled\": true, \"name\": \"HTTP\_SERVER01\",
\"logType\": 0, \"url\": \"www.example.com\" } \]

Input type

application/json

RequestBody

-   Description: Post body is an array of objects WITHOUT id field
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

### PUT

Summary

Update remote log receiver(s) with data provided in the request body

Notes

PUT request body is an array of objects with valid receiver ids. \[ {
\"id\": 0, \"receiverType\": 0, \"enabled\": true, \"name\":
\"HTTP\_SERVER01\", \"logType\": 0, \"url\": \"www.example.com\" } \]

Input type

application/json

RequestBody

-   Description: PUT request body is an array of objects with valid
    receiver ids
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/remoteLogReceiver/{receiverId}
-------------------------------

### DELETE

Summary

Delete a remote log receiver

Input type

application/json

receiverId

-   Description: The valid id of the receiver to be deleted.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/remoteLogReceiver/{receiverId}/subscribe
-----------------------------------------

### POST

Summary

To query specific log(s) with log type and sequence id(s) provided in
the request body

Input type

application/json

receiverId

-   Description: The valid id of the receiver.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

fetchingParam

Description: Parameter for fetch specific log message. The id field is
an array of strings, item in the array can be a single number or a
range, for example: id: \[\"1\", \"3\", \"4-6\"\], messages with id 1,
3, 4, 5, 6 will be sent.

Required: true

Parameter type: POST Body

Data Type: application/json

logType

-   Type: integer
-   Required: true
-   Description: The type of log

id

-   Type: array
-   Required: true

Output type

None

Reference: remoteLogReceiver.json

\

Apply RMA Wizard
================

/rmaWizard/apply/{nePk}
-----------------------

### POST

Summary

Applies the RMA wizard

Notes

notes

Input type

application/json: undefined

ApplyRmaWizard

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

nePk

-   Description: The nePk of the appliance to be replaced
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

existingAppliance

-   Type: ApplianceItem
-   Description: Representation of existing appliance to be replaced.

discoveredAppliance

-   Type: AllDiscoveredAppliance
-   Description: Representation of the discovered appliance that will
    replace the existing appliance.

backup

-   Type: BackupGetResponse
-   Description: Optional backup selection. Backup is required for
    downgrades, however.

selectedImage

-   Type: ImageItem
-   Description: Representation of intended software version to upgrade
    or downgrade to. If using cloud orchestrator, use keys \'version\'
    and \'imageId\' instead of keys with prefix \'NxImage\'

Reference: rmaWizard.json

\

Set or get appliances nePks which are paused from orchestration
===============================================================

/pauseOrchestration
-------------------

### GET

Summary

Get all appliances\' nePks which are paused from orchestration

Notes

Get all appliances\' nePks which are paused from orchestration

Input type

None

Output type

None

### POST

Summary

Set all appliances\' nePks which are intended to be paused from
orchestration

Notes

Set all appliances\' nePks which are intended to be paused from
orchestration

Input type

application/json: undefined

nePks

-   Description: The nePks of appliances which are intended to be paused
    from orchestration
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

Reference: pauseOrchestration.json

\

Manage Zones
============

/zones
------

### GET

Summary

Returns all Zones defined in Orchestrator

Input type

None

Output type

application/json

\<zoneId\>

name

-   Type: string
-   Required: true
-   Description: Zone name

### POST

Summary

Add/Edit Zones

Input type

application/json: undefined

zones

Description: JSON object containing all Zones

Required: true

Parameter type: POST Body

Data Type: application/json

\<zoneId\>

name

-   Type: string
-   Required: true
-   Description: Zone name

deleteDependencies

-   Description: If true, Zones deleted here will be removed from
    overlays, policies, interfaces and deployment profiles currently
    using those zones
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

None

/zones/nextId
-------------

### GET

Summary

Returns Next Id that should be assigned to a new Zone

Input type

None

Output type

application/json

nextId

-   Type: number
-   Required: true
-   Description: Zone Next Id

### POST

Summary

Update Next Id

Input type

application/json: undefined

nextIdPost

Description: JSON object containing Next Id

Required: true

Parameter type: POST Body

Data Type: application/json

nextId

-   Type: number
-   Required: true
-   Description: Zone Next Id

Output type

None

Reference: zones.json

\

Gets Appliance Security Policies
================================

/securityMaps/{neId}?cached={cached}
------------------------------------

### GET

Summary

Get Security Policies configured on the appliance

Notes

Sample response: { \"map1\": { \"self\": \"map1\", \"20\_21\": {
\"self\": \"20\_21\", \"prio\": { \"1000\": { \"match\": { \"acl\":
\"\", \"either\_dns\": \"\*google.com\" }, \"self\": 1000, \"misc\": {
\"rule\": \"enable\", \"logging\": \"disable\", \"logging\_priority\":
\"0\" }, \"comment\": \"\", \"gms\_marked\": false, \"set\": {
\"action\": \"allow\" } }, \"65535\": { \"match\": { \"acl\": \"\" },
\"self\": 65535, \"misc\": { \"rule\": \"enable\", \"logging\":
\"disable\", \"logging\_priority\": \"0\" }, \"comment\": \"\",
\"gms\_marked\": false, \"set\": { \"action\": \"deny\" } } } } }}

Input type

None

neId

-   Description: Unique key assigned by Orchestrator to each appliance,
    for eg: \'0.NE\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

map1

\<zoneFromId\_zoneToId\>

prio

\<priorityNumber\>

match

-   Type: object
-   Description: Object containing rules to match traffic.

self

misc

-   Type: object
-   Description: Object to hold miscellaneous field like rule, logging,
    logging priority, etc.

comment

-   Type: string

gms\_marked

-   Type: boolean
-   Description: For internal use. Flag to determine if this rule was
    created by Orchestrator.

set

-   Type: object
-   Description: Object to hold set action eg: action: allow/deny

Reference: securityMaps.json

\

Customize Orchestrator Brand
============================

/brandCustomization
-------------------

### GET

Summary

Get text brand customization configurations

Input type

None

Output type

application/json

footer

useDefault

-   Type: boolean
-   Description: Use default brand

custom

-   Type: string
-   Description: Text content

### POST

Summary

Create new text brand customization configurations

Input type

application/json

RequestBody

Description: POST body is an object of text customization configuration
object

Required: true

Parameter type: POST Body

Data Type: application/json

footer

useDefault

-   Type: boolean
-   Description: Use default brand

custom

-   Type: string
-   Description: Text content

Output type

None

### PUT

Summary

Update text brand customization configurations

Input type

application/json

RequestBody

Description: PUT body is an object of text customization configuration
object

Required: true

Parameter type: POST Body

Data Type: application/json

footer

useDefault

-   Type: boolean
-   Description: Use default brand

custom

-   Type: string
-   Description: Text content

Output type

None

### DELETE

Summary

Delete all text brand customization configurations

Input type

None

Output type

None

/brandCustomization/image/
--------------------------

### GET

Summary

Return file names for all the uploaded images

Input type

None

metaData

-   Description: Sub-resource type
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

backgroundImage

fileName

-   Type: string
-   Description: File name

/brandCustomization/image/{type}
--------------------------------

### POST

Summary

Upload image for given type of brand customization

Input type

Unknown

type

-   Description: The type of customization. e.g. \'backgroundImage\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

qqfile

-   Description: File name
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

qqfile

-   Description: Image file
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

### PUT

Summary

Update image for given type of brand customization

Input type

Unknown

type

-   Description: The type of customization. e.g. \'backgroundImage\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

qqfile

-   Description: File name
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

qqfile

-   Description: Image file
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

### DELETE

Summary

Delete image for given type of brand customization and restore default

Input type

None

type

-   Description: The type of customization. e.g. \'backgroundImage\'.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: brandCustomization.json

\

GET built-in policy information
===============================

/saMap/{nePk}
-------------

### GET

Summary

Get built-in policy information

Notes

Get built-in policies from appliance by given appliance key(nePk)

Input type

None

nePk

-   Description: Appliance nePk
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

mapName

-   Type: string
-   Description: The name of map

state

-   Type: string
-   Description: Map state, sa map is always active

numOfEntries

-   Type: string
-   Description: Number of map entries in the current map

entries

-   Type: array
-   Description: Data of may entries

Reference: builtInPolicies.json

\

Get and set the REST API config
===============================

/restApiConfig
--------------

### GET

Summary

Get the REST API config

Notes

Get the REST API config

Input type

None

Output type

application/json

communicateWithApplianceViaPortal

-   Type: boolean
-   Required: true
-   Description: the control value of connecting VXOA via portal web
    socket, true means allow connecting VXOA via portal web socket,
    false means disallow

### POST

Summary

Set the REST API config

Notes

Set the REST API config

Input type

application/json: undefined

restApiConfigPost

Description: Set the REST API config

Required: true

Parameter type: POST Body

Data Type: application/json

communicateWithApplianceViaPortal

-   Type: boolean
-   Required: true
-   Description: the control value of connecting VXOA via portal web
    socket, true means allow connecting VXOA via portal web socket,
    false means disallow

Output type

None

Reference: restApiConfig.json

\

Exception configuration
=======================

/exception/tunnel
-----------------

### GET

Summary

Get all tunnel exceptions

Input type

None

Output type

application/json

id

-   Type: integer
-   Description: ID of tunnel exception entry

appliance\_id\_1

-   Type: string
-   Description: Id of appliance

interface\_label\_1

-   Type: string
-   Description: Interface label name

appliance\_id\_2

-   Type: string
-   Description: Id of appliance

interface\_label\_2

-   Type: string
-   Description: Interface label name

description

-   Type: string
-   Description: Description of this tunnel exception

### POST

Summary

Create tunnel exceptions

Notes

POST body is an array of tunnel exception entries.

Input type

application/json

Request Body

-   Description: Array of tunnel Exception entry, \'id\' of the
    exception must be 0 or not present
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

application/json

id

-   Type: integer
-   Description: ID of tunnel exception entry

appliance\_id\_1

-   Type: string
-   Description: Id of appliance

interface\_label\_1

-   Type: string
-   Description: Interface label name

appliance\_id\_2

-   Type: string
-   Description: Id of appliance

interface\_label\_2

-   Type: string
-   Description: Interface label name

description

-   Type: string
-   Description: Description of this tunnel exception

### PUT

Summary

Update tunnel exceptions

Notes

Use this API to update multiple tunnel exceptions in one REST call.

Input type

application/json

Request Body

-   Description: Array of tunnel exception entries, \'id\'s of entries
    must be valid existing
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

### DELETE

Summary

Delete all tunnel exceptions

Notes

Use this API to delete all current tunnel exceptions

Input type

None

Output type

None

/exception/tunnel/{id}
----------------------

### PUT

Summary

Update a single tunnel exceptions

Notes

Use this API to update a specific tunnel exception.

Input type

application/json: undefined

id

-   Description: The valid id of the an existing tunnel exception.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Request Body

Description: Tunnel exception entry

Required: true

Parameter type: POST Body

Data Type: application/json

id

-   Type: integer
-   Description: ID of tunnel exception entry

appliance\_id\_1

-   Type: string
-   Description: Id of appliance

interface\_label\_1

-   Type: string
-   Description: Interface label name

appliance\_id\_2

-   Type: string
-   Description: Id of appliance

interface\_label\_2

-   Type: string
-   Description: Interface label name

description

-   Type: string
-   Description: Description of this tunnel exception

Output type

None

### DELETE

Summary

Delete single tunnel exceptions

Notes

Use this API to delete a specific tunnel exception.

Input type

None

id

-   Description: The valid id of the an existing tunnel exception.
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

Reference: exception.json

\

Set or get regions and region appliance associations
====================================================

/regions
--------

### GET

Summary

Get all regions

Input type

None

Output type

application/json

regionId

-   Type: integer
-   Description: ID of region

regionName

-   Type: string
-   Description: Name of region

### POST

Summary

Create region

Input type

application/json

Request Body

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/regions/{regionId}
-------------------

### GET

Summary

Get region by regionId

Input type

None

regionId

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

regionId

-   Type: integer
-   Description: ID of region

regionName

-   Type: string
-   Description: Name of region

### PUT

Summary

Update regions

Notes

Use this API to change region name

Input type

application/json

regionId

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Request Body

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

### DELETE

Summary

Delete region by regionId

Notes

Use this API to delete region

Input type

None

regionId

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/regions/appliances
-------------------

### GET

Summary

Get all region association

Notes

Get all region association

Input type

None

Output type

None

### POST

Summary

Create region association

Input type

application/json

Request Body

-   Description: Map of appliance nePK and regionId, example:
    {\'0.NE\':\'1\', \'1.NE\':\'3\'}
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/regions/appliances/{nePK}
--------------------------

### PUT

Summary

Update region association

Notes

Use this API to update region association

Input type

application/json

NePK

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Request Body

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/regions/appliances/nePk/{nePK}
-------------------------------

### GET

Summary

Get region association by nePK

Input type

None

NePK

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

regionId

-   Type: integer
-   Description: ID of region

regionName

-   Type: string
-   Description: name of region

nePk

-   Type: string
-   Description: Appliance ID

/regions/appliances/regionId/{regionId}
---------------------------------------

### GET

Summary

Get region association by region ID

Input type

None

regionId

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

application/json

regionId

-   Type: integer
-   Description: ID of region

regionName

-   Type: string
-   Description: name of region

nePk

-   Type: string
-   Description: Appliance ID

Reference: regions.json

\

Third Party Services Orchestration
==================================

/thirdPartyServices/zscaler/subscription
----------------------------------------

### GET

Summary

Returns Zscaler subscription

Input type

None

Output type

application/json

cloud

-   Type: string
-   Required: true
-   Description: Zscaler Cloud. e.g. \'admin.zscalerthree.net\'

username

-   Type: string
-   Required: true
-   Description: Zscaler Third Party Admin Username. e.g.
    \'adminuser\@company.com\'

password

-   Type: string
-   Description: Zscaler Third Party Admin User Password. Required for
    the first time or if you want to change password

partnerKey

-   Type: string
-   Required: true
-   Description: Zscaler Third Party Key for SilverPeak.

domain

-   Type: string
-   Required: true
-   Description: Domain to use for VPN credentials

### POST

Summary

Add/Update Subscription

Input type

application/json: undefined

subscription

Description:

Required: true

Parameter type: POST Body

Data Type: application/json

cloud

-   Type: string
-   Required: true
-   Description: Zscaler Cloud. e.g. \'admin.zscalerthree.net\'

username

-   Type: string
-   Required: true
-   Description: Zscaler Third Party Admin Username. e.g.
    \'adminuser\@company.com\'

password

-   Type: string
-   Description: Zscaler Third Party Admin User Password. Required for
    the first time or if you want to change password

partnerKey

-   Type: string
-   Required: true
-   Description: Zscaler Third Party Key for SilverPeak.

domain

-   Type: string
-   Required: true
-   Description: Domain to use for VPN credentials

Output type

None

### DELETE

Summary

Delete Subscription

Notes

Make sure to deselect Zscaler Policies in Business Intent Overlay first
and wait until all Zscaler config has been deleted

Input type

None

Output type

None

/thirdPartyServices/zscaler/connectivity
----------------------------------------

### GET

Summary

Returns Zscaler connectivity status

Input type

None

Output type

application/json

connectivity

-   Type: string
-   Description: 1 for connected, 0 for not connected

/thirdPartyServices/zscaler/configuration
-----------------------------------------

### GET

Summary

Returns Zscaler configuration, including tunnel setting, selected
interface label order

Input type

None

Output type

application/json

interfaces

-   Type: array
-   Required: true
-   Description: interface label order, e.g \[\[\'1\',
    \'2\'\],\[\'3\'\]\], the first array element is for primary
    interfaces, the second arry element in the array is for backup
    interfaces

tunnelSetting

-   Type: TunnelSetting
-   Required: true

locationSetting

-   Type: LocationSecuritySetting
-   Required: true
-   Description: Zscaler Security setting for location. Currently,
    Orchestrator does not manager this setting.

### POST

Summary

Update Returns Zscaler configuration, including tunnel setting, selected
interface label order

Input type

application/json: undefined

configuration

Description:

Required: true

Parameter type: POST Body

Data Type: application/json

interfaces

-   Type: array
-   Required: true
-   Description: interface label order, e.g \[\[\'1\',
    \'2\'\],\[\'3\'\]\], the first array element is for primary
    interfaces, the second arry element in the array is for backup
    interfaces

tunnelSetting

-   Type: TunnelSetting
-   Required: true

locationSetting

-   Type: LocationSecuritySetting
-   Required: true
-   Description: Zscaler Security setting for location. Currently,
    Orchestrator does not manager this setting.

Output type

None

/thirdPartyServices/zscaler/defaultTunnelSetting
------------------------------------------------

### GET

Summary

Returns recommend tunnel setting for Zscaler

Input type

None

Output type

application/json

admin

-   Type: string
-   Required: true
-   Description: admin status

mode

-   Type: string
-   Required: true
-   Description: Pass through tunnel mode

autoMaxBandwidthEnabled

-   Type: boolean
-   Required: true
-   Description: auto max bandwidth

presharedKey

-   Type: string
-   Description: pre-shared key

ikeAuthenticationAlgorithm

-   Type: string
-   Required: true
-   Description: IKE authentication algorithm

ikeEncryptionAlgorithm

-   Type: string
-   Required: true
-   Description: IKE encryption algorithm

dhgroup

-   Type: string
-   Required: true
-   Description: Diffie-Hellman Group

ikeLifetime

-   Type: number
-   Required: true
-   Description: IKE Lifetime

dpdDelay

-   Type: number
-   Required: true
-   Description: Dead Peer Detection delay time in seconds

dpdRetry

-   Type: number
-   Required: true
-   Description: Dead Peer Detection Retry Count

idType

-   Type: string
-   Description: IKE Identifier type

idStr

-   Type: string
-   Description: IKE Identifier string

exchangeMode

-   Type: string
-   Required: true
-   Description: IKE Phase 1 Mode

ikeVersion

-   Type: integer
-   Required: true
-   Description: IKE version, 1 or 2

authenticationAlgorithm

-   Type: string
-   Required: true
-   Description: IPsec authentication algorithm

encryptionAlgorithm

-   Type: string
-   Required: true
-   Description: IPsec encryption algorithm

ipsecAntiReplayWindow

-   Type: string
-   Required: true
-   Description: IPsec Anti-replay Window

lifetime

-   Type: number
-   Required: true
-   Description: IPsec Lifetime in minutes

lifebytes

-   Type: number
-   Required: true
-   Description: IPsec Lifetime in megabytes

pfs

-   Type: boolean
-   Required: true
-   Description: Perfect Forward Secrecy Group (disabled)

pfsgroup

-   Type: string
-   Required: true
-   Description: Perfect Forward Secrecy Group

/thirdPartyServices/zscaler/remoteEndpointException
---------------------------------------------------

### GET

Summary

Returns all user configured ZEN Override data

Input type

None

Output type

application/json

\<nePk\>

-   Type: EndpointExceptionLabelMap
-   Description: appliance id(nePk)

/thirdPartyServices/zscaler/remoteEndpointException/{nePk}/{labelId}
--------------------------------------------------------------------

### POST

Summary

Add/Update user configured ZEN override data

Input type

application/json: undefined

nePk

-   Description: appliance id
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

labelId

-   Description: Interface Label Id
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

endpointException

Description:

Required: true

Parameter type: POST Body

Data Type: application/json

ip1

-   Type: string
-   Required: true
-   Description: Primary IPv4 address

ip2

-   Type: string
-   Required: true
-   Description: Secondary IPv4 address

Output type

None

### DELETE

Summary

Delete user configured ZEN override data

Input type

None

nePk

-   Description: appliance id
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

labelId

-   Description: Interface Label Id
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: application/json

Output type

None

/thirdPartyServices/zscaler/vpnLocationEndpointExceptionSetting
---------------------------------------------------------------

### POST

Summary

Get all VPN, Location, discovered and configured ZEN override data for
one or more appliances

Input type

application/json: undefined

applianceList

-   Description: an array of comma separated nepks.
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/thirdPartyServices/zscaler/pauseOrchestration
----------------------------------------------

### GET

Summary

Returns Pause Zscaler Orchestration configuration

Input type

None

Output type

application/json

paused

-   Type: boolean
-   Required: true
-   Description: boolean flag to set pause zscaler orchestration

### POST

Summary

update pause Zscaler Orchestration configuration

Input type

application/json: undefined

zscalerPauseObj

Description:

Required: true

Parameter type: POST Body

Data Type: application/json

paused

-   Type: boolean
-   Required: true
-   Description: boolean flag to set pause zscaler orchestration

Output type

None

/thirdPartyServices/checkPoint/subscription
-------------------------------------------

### GET

Summary

Get check point subscription

Notes

This is the URL and account info used to login Check Point CloudGuard
Connect, and returns 404 if not set yet.

Input type

None

Output type

application/json

portalUrl

-   Type: string
-   Required: true
-   Description: The url of Check Point CloudGuard Connect

clientId

-   Type: string
-   Required: true
-   Description: The account client id used to login

accessKey

-   Type: string
-   Required: true
-   Description: The access key used to login

### POST

Summary

Set check point subscription

Notes

Set the URL and account info used to login Check Point CloudGuard
Connect.

Input type

application/json

checkPointSubscriptionPostBody

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

portalUrl

-   Type: string
-   Description: The url of Check Point CloudGuard Connect

clientId

-   Type: string
-   Required: true
-   Description: The account client id used to login

accessKey

-   Type: string
-   Required: true
-   Description: The access key used to login

Output type

None

### DELETE

Summary

Delete check point subscription

Notes

Delete the URL and account info used to login Check Point CloudGuard
Connect.

Input type

application/json

Output type

None

/thirdPartyServices/checkPoint/connectivity
-------------------------------------------

### GET

Summary

Get check point connectivity status

Notes

The possible results are: 0 - Auth Failed, 1 - Connected, 2 -
Unreachable.

Input type

None

Output type

application/json

connectivity

-   Type: string
-   Description: The connectivity status to Check Point CloudGuard
    Connect: 0 - Auth Failed, 1 - Connected, 2 - Unreachable

/thirdPartyServices/checkPoint/interfaces
-----------------------------------------

### GET

Summary

Get interfaces configured for which sites will be created on check
point.

Notes

Get interfaces configured for which sites will be created on check
point.

Input type

None

Output type

None

### POST

Summary

Set interfaces configured for which sites will be created on check
point.

Notes

Set interfaces configured for which sites will be created on check
point.

Input type

application/json

checkPointInterfaces

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array
-   Required: true
-   Description: Two arrays of interfaces for which sites would be
    created on Check Point CloudGuard Connect

Output type

None

/thirdPartyServices/checkPoint/tunnelSettings
---------------------------------------------

### GET

Summary

Get tunnel settings configured to build Check Point CloudGuard Connect
pass through tunnels.

Notes

Get tunnel settings configured to build Check Point CloudGuard Connect
pass through tunnels.

Input type

None

Output type

application/json

admin

-   Type: string
-   Required: true
-   Description: admin status

mode

-   Type: string
-   Required: true
-   Description: Pass through tunnel mode

autoMaxBandwidthEnabled

-   Type: boolean
-   Required: true
-   Description: auto max bandwidth

presharedKey

-   Type: string
-   Description: pre-shared key

ikeAuthenticationAlgorithm

-   Type: string
-   Required: true
-   Description: IKE authentication algorithm

ikeEncryptionAlgorithm

-   Type: string
-   Required: true
-   Description: IKE encryption algorithm

dhgroup

-   Type: string
-   Required: true
-   Description: Diffie-Hellman Group

ikeLifetime

-   Type: number
-   Required: true
-   Description: IKE Lifetime

dpdDelay

-   Type: number
-   Required: true
-   Description: Dead Peer Detection delay time in seconds

dpdRetry

-   Type: number
-   Required: true
-   Description: Dead Peer Detection Retry Count

idType

-   Type: string
-   Description: IKE Identifier type

idStr

-   Type: string
-   Description: IKE Identifier string

exchangeMode

-   Type: string
-   Required: true
-   Description: IKE Phase 1 Mode

ikeVersion

-   Type: integer
-   Required: true
-   Description: IKE version, 1 or 2

authenticationAlgorithm

-   Type: string
-   Required: true
-   Description: IPsec authentication algorithm

encryptionAlgorithm

-   Type: string
-   Required: true
-   Description: IPsec encryption algorithm

ipsecAntiReplayWindow

-   Type: string
-   Required: true
-   Description: IPsec Anti-replay Window

lifetime

-   Type: number
-   Required: true
-   Description: IPsec Lifetime in minutes

lifebytes

-   Type: number
-   Required: true
-   Description: IPsec Lifetime in megabytes

pfs

-   Type: boolean
-   Required: true
-   Description: Perfect Forward Secrecy Group (disabled)

pfsgroup

-   Type: string
-   Required: true
-   Description: Perfect Forward Secrecy Group

### POST

Summary

Set tunnel settings configured to build Check Point CloudGuard Connect
pass through tunnels.

Notes

Set tunnel settings configured to build Check Point CloudGuard Connect
pass through tunnels.

Input type

application/json

checkPointTunnelSettings

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

admin

-   Type: string
-   Required: true
-   Description: admin status

mode

-   Type: string
-   Required: true
-   Description: Pass through tunnel mode

autoMaxBandwidthEnabled

-   Type: boolean
-   Required: true
-   Description: auto max bandwidth

presharedKey

-   Type: string
-   Description: pre-shared key

ikeAuthenticationAlgorithm

-   Type: string
-   Required: true
-   Description: IKE authentication algorithm

ikeEncryptionAlgorithm

-   Type: string
-   Required: true
-   Description: IKE encryption algorithm

dhgroup

-   Type: string
-   Required: true
-   Description: Diffie-Hellman Group

ikeLifetime

-   Type: number
-   Required: true
-   Description: IKE Lifetime

dpdDelay

-   Type: number
-   Required: true
-   Description: Dead Peer Detection delay time in seconds

dpdRetry

-   Type: number
-   Required: true
-   Description: Dead Peer Detection Retry Count

idType

-   Type: string
-   Description: IKE Identifier type

idStr

-   Type: string
-   Description: IKE Identifier string

exchangeMode

-   Type: string
-   Required: true
-   Description: IKE Phase 1 Mode

ikeVersion

-   Type: integer
-   Required: true
-   Description: IKE version, 1 or 2

authenticationAlgorithm

-   Type: string
-   Required: true
-   Description: IPsec authentication algorithm

encryptionAlgorithm

-   Type: string
-   Required: true
-   Description: IPsec encryption algorithm

ipsecAntiReplayWindow

-   Type: string
-   Required: true
-   Description: IPsec Anti-replay Window

lifetime

-   Type: number
-   Required: true
-   Description: IPsec Lifetime in minutes

lifebytes

-   Type: number
-   Required: true
-   Description: IPsec Lifetime in megabytes

pfs

-   Type: boolean
-   Required: true
-   Description: Perfect Forward Secrecy Group (disabled)

pfsgroup

-   Type: string
-   Required: true
-   Description: Perfect Forward Secrecy Group

Output type

None

/thirdPartyServices/checkPoint/defaultTunnelSetting
---------------------------------------------------

### GET

Summary

Get default tunnel settings configured to build Check Point CloudGuard
Connect pass through tunnels.

Notes

Get default tunnel settings configured to build Check Point CloudGuard
Connect pass through tunnels.

Input type

None

Output type

application/json

admin

-   Type: string
-   Required: true
-   Description: admin status

mode

-   Type: string
-   Required: true
-   Description: Pass through tunnel mode

autoMaxBandwidthEnabled

-   Type: boolean
-   Required: true
-   Description: auto max bandwidth

presharedKey

-   Type: string
-   Description: pre-shared key

ikeAuthenticationAlgorithm

-   Type: string
-   Required: true
-   Description: IKE authentication algorithm

ikeEncryptionAlgorithm

-   Type: string
-   Required: true
-   Description: IKE encryption algorithm

dhgroup

-   Type: string
-   Required: true
-   Description: Diffie-Hellman Group

ikeLifetime

-   Type: number
-   Required: true
-   Description: IKE Lifetime

dpdDelay

-   Type: number
-   Required: true
-   Description: Dead Peer Detection delay time in seconds

dpdRetry

-   Type: number
-   Required: true
-   Description: Dead Peer Detection Retry Count

idType

-   Type: string
-   Description: IKE Identifier type

idStr

-   Type: string
-   Description: IKE Identifier string

exchangeMode

-   Type: string
-   Required: true
-   Description: IKE Phase 1 Mode

ikeVersion

-   Type: integer
-   Required: true
-   Description: IKE version, 1 or 2

authenticationAlgorithm

-   Type: string
-   Required: true
-   Description: IPsec authentication algorithm

encryptionAlgorithm

-   Type: string
-   Required: true
-   Description: IPsec encryption algorithm

ipsecAntiReplayWindow

-   Type: string
-   Required: true
-   Description: IPsec Anti-replay Window

lifetime

-   Type: number
-   Required: true
-   Description: IPsec Lifetime in minutes

lifebytes

-   Type: number
-   Required: true
-   Description: IPsec Lifetime in megabytes

pfs

-   Type: boolean
-   Required: true
-   Description: Perfect Forward Secrecy Group (disabled)

pfsgroup

-   Type: string
-   Required: true
-   Description: Perfect Forward Secrecy Group

/thirdPartyServices/checkPoint/pauseOrchestration
-------------------------------------------------

### GET

Summary

Get check point pause orchestration configuration.

Notes

Get check point pause orchestration configuration.

Input type

None

Output type

application/json

paused

-   Type: boolean
-   Required: true
-   Description: True means paused, false means resumed

### POST

Summary

Set check point pause orchestration configuration.

Notes

Set check point pause orchestration configuration.

Input type

application/json

cpPauseOrchestration

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

paused

-   Type: boolean
-   Required: true
-   Description: True means paused, false means resumed

Output type

None

/thirdPartyServices/checkPoint/sites
------------------------------------

### POST

Summary

Get check point sites configuration.

Notes

Get check point sites configuration.

Input type

application/json

nePkList

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

ids

-   Type: array
-   Required: true
-   Description: nePk list

Output type

None

/thirdPartyServices/checkPoint/subnets
--------------------------------------

### GET

Summary

Get configured LAN subnets of the appliance.

Notes

This list will be appended to the automatic LAN subnets of the
appliance.

Input type

None

Output type

None

### POST

Summary

Add configured LAN subnets of the appliance.

Notes

Add configured LAN subnets of the appliance.

Input type

application/json

checkPointSubnets

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

### PUT

Summary

Change existed configured LAN subnets of the appliance.

Notes

Change existed configured LAN subnets of the appliance.

Input type

application/json

checkPointSubnets

-   Description: undefined
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

### DELETE

Summary

Delete existed configured LAN subnets of the appliance.

Notes

Delete existed configured LAN subnets of the appliance.

Input type

application/json

nePks

-   Description: nePks of the appliances joint by commas
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/thirdPartyServices/azure/subscription
--------------------------------------

### GET

Summary

Returns Azure subscription

Input type

None

Output type

application/json

subscriptionId

-   Type: string
-   Required: true
-   Description: object id

tenantId

-   Type: string
-   Required: true
-   Description: tenant id

clientId

-   Type: string
-   Required: true
-   Description: client id

clientSecret

-   Type: string
-   Description: secret key

### POST

Summary

Add/Update Azure Subscription

Input type

application/json: undefined

account

Description:

Required: true

Parameter type: POST Body

Data Type: application/json

subscriptionId

-   Type: string
-   Required: true
-   Description: object id

tenantId

-   Type: string
-   Required: true
-   Description: tenant id

clientId

-   Type: string
-   Required: true
-   Description: client id

clientSecret

-   Type: string
-   Description: secret key

Output type

None

### DELETE

Summary

Delete Azure Subscription

Notes

Make sure to deselect Azure Policies in Business Intent Overlay first
and wait until all Azure config has been deleted

Input type

None

Output type

None

/thirdPartyServices/azure/connectivity
--------------------------------------

### GET

Summary

Returns Azure connectivity status

Input type

None

Output type

application/json

/thirdPartyServices/azure/azureconfigurations
---------------------------------------------

### GET

Summary

Returns list of Azure Configuration entries for every VPN Sites created
for Appliances.

Input type

None

Output type

application/json

### POST

Summary

Update list of Azure Configuration entries for every VPN Sites created
for Appliances.

Input type

application/json: undefined

azureConfigurationPost

-   Description:
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/thirdPartyServices/azure/defaultTunnelSetting
----------------------------------------------

### GET

Summary

Returns recommended tunnel setting for Azure

Input type

None

Output type

application/json

admin

-   Type: string
-   Required: true
-   Description: admin status

mode

-   Type: string
-   Required: true
-   Description: Pass through tunnel mode

autoMaxBandwidthEnabled

-   Type: boolean
-   Required: true
-   Description: auto max bandwidth

presharedKey

-   Type: string
-   Description: pre-shared key

ikeAuthenticationAlgorithm

-   Type: string
-   Required: true
-   Description: IKE authentication algorithm

ikeEncryptionAlgorithm

-   Type: string
-   Required: true
-   Description: IKE encryption algorithm

dhgroup

-   Type: string
-   Required: true
-   Description: Diffie-Hellman Group

ikeLifetime

-   Type: number
-   Required: true
-   Description: IKE Lifetime

dpdDelay

-   Type: number
-   Required: true
-   Description: Dead Peer Detection delay time in seconds

dpdRetry

-   Type: number
-   Required: true
-   Description: Dead Peer Detection Retry Count

idType

-   Type: string
-   Description: IKE Identifier type

idStr

-   Type: string
-   Description: IKE Identifier string

exchangeMode

-   Type: string
-   Required: true
-   Description: IKE Phase 1 Mode

ikeVersion

-   Type: integer
-   Required: true
-   Description: IKE version, 1 or 2

authenticationAlgorithm

-   Type: string
-   Required: true
-   Description: IPsec authentication algorithm

encryptionAlgorithm

-   Type: string
-   Required: true
-   Description: IPsec encryption algorithm

ipsecAntiReplayWindow

-   Type: string
-   Required: true
-   Description: IPsec Anti-replay Window

lifetime

-   Type: number
-   Required: true
-   Description: IPsec Lifetime in minutes

lifebytes

-   Type: number
-   Required: true
-   Description: IPsec Lifetime in megabytes

pfs

-   Type: boolean
-   Required: true
-   Description: Perfect Forward Secrecy Group (disabled)

pfsgroup

-   Type: string
-   Required: true
-   Description: Perfect Forward Secrecy Group

/thirdPartyServices/azure/interfaces
------------------------------------

### GET

Summary

Returns preferred interface order for Azure

Input type

None

Output type

application/json

\<nePk\>

-   Type: EndpointExceptionLabelMap
-   Description: appliance id(nePk)

### POST

Summary

Posts interface order for Azure

Input type

Output type

application/json

\<nePk\>

-   Type: EndpointExceptionLabelMap
-   Description: appliance id(nePk)

/thirdPartyServices/azure/virtualWanAssociation
-----------------------------------------------

### GET

Summary

Returns Appliance to VWAN association for Azure

Input type

None

Output type

None

### POST

Summary

Posts Appliance to VWAN association for Azure

Input type

Output type

None

/thirdPartyServices/azure/tunnelSettings
----------------------------------------

### GET

Summary

Returns tunnel settings for Azure

Input type

None

Output type

None

### POST

Summary

Posts tunnel setting for Azure

Input type

Output type

None

/thirdPartyServices/azure/virtualWanAssociation
-----------------------------------------------

### GET

Summary

Returns Appliance to Virtual WAN association for Azure

Input type

None

Output type

None

### POST

Summary

Posts Appliance to Virtual WAN association for Azure

Input type

Output type

None

/thirdPartyServices/azure/pauseOrchestration
--------------------------------------------

### GET

Summary

Returns Azure Orchestration Pause state

Input type

None

Output type

None

### POST

Summary

Posts Azure Orchestration Pause state

Input type

Output type

None

Reference: thirdPartyServices.json

\

Gets Appliance multicast config and state
=========================================

/multicast/enable/{neId}?cached={cached}
----------------------------------------

### GET

Summary

Get multicast enabled info on the appliance

Input type

None

neId

-   Description: Unique key assigned by Orchestrator to each appliance,
    for eg: \'0.NE\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

enable

-   Type: boolean
-   Required: true
-   Description: enable or disable multicast

/multicast/config/{neId}?cached={cached}
----------------------------------------

### GET

Summary

Get multicast config info on the appliance

Input type

None

neId

-   Description: Unique key assigned by Orchestrator to each appliance,
    for eg: \'0.NE\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

rp

-   Type: string
-   Required: true
-   Description: rendezvous point IP (IPv4)

pim

\<interface\>

enable

-   Type: boolean

igmp

\<interface\>

enable

-   Type: boolean

/multicast/state/interfaces/{neId}
----------------------------------

### GET

Summary

Get multicast interface state info on the appliance

Input type

None

neId

-   Description: Unique key assigned by Orchestrator to each appliance,
    for eg: \'0.NE\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

interface

-   Type: string
-   Required: true
-   Description: Interface

interfaceIP

-   Type: string
-   Required: true
-   Description: Interface IP

DRIP

-   Type: string
-   Required: true
-   Description: designated router IP

DRPriority

-   Type: string
-   Required: true
-   Description: designated router priority

generationID

-   Type: string
-   Required: true
-   Description: generation id

/multicast/state/neighbors/{neId}
---------------------------------

### GET

Summary

Get multicast neighbor state info on the appliance

Input type

None

neId

-   Description: Unique key assigned by Orchestrator to each appliance,
    for eg: \'0.NE\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

interface

-   Type: string
-   Required: true
-   Description: Interface

neighborIP

-   Type: string
-   Required: true
-   Description: neighbor IP

neighborDRPriority

-   Type: string
-   Required: true
-   Description: neighbor designated router priority

neighborGenerationID

-   Type: string
-   Required: true
-   Description: neighbor generation id

/multicast/state/routes/{neId}
------------------------------

### GET

Summary

Get multicast route state info on the appliance

Input type

None

neId

-   Description: Unique key assigned by Orchestrator to each appliance,
    for eg: \'0.NE\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

group

-   Type: string
-   Required: true
-   Description: group

source

-   Type: string
-   Required: true
-   Description: source IP

inIntf

-   Type: string
-   Required: true
-   Description: incoming interface

outIntfList

-   Type: string
-   Required: true
-   Description: outgoing interfaces

Reference: multicast.json

\

Gets Appliance NAT configurations
=================================

/nat/{neId}?cached={cached}
---------------------------

### GET

Summary

Returns all NAT Config

Input type

None

neId

-   Description: Unique key assigned by Orchestrator to each appliance,
    for eg: \'0.NE\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

maps

prio

\<priorityNumber\>

match

intf

-   Type: string
-   Description: Interface name or label id

src\_subnet

-   Type: string
-   Description: Source IP address range to be translated. Must be a
    valid subnet

dst\_subnet

-   Type: string
-   Description: Destination IP address range to be translated. Must be
    a valid subnet

dir

-   Type: string
-   Description: Traffic direction to perform NAT on. Valid values are
    \[\'outbound\', \'inbound\'\]

comment

-   Type: string

gms\_marked

-   Type: boolean
-   Description: For internal use. Flag to determine if this rule was
    created by Orchestrator

set

trans\_src

-   Type: string
-   Description: Translated source subnet or NAT Pool ID

trans\_dst

-   Type: string
-   Description: Translated destination subnet

natPools

\<NATPool Id\>

name

-   Type: string
-   Required: true
-   Description: Name of NATPool

dir

-   Type: string
-   Required: true
-   Description: Traffic direction to perform NAT on. Valid values are
    \[\'outbound\', \'inbound\'\]

comment

-   Type: string
-   Description: User comments

pat

-   Type: number
-   Required: true
-   Description: Enable Port Address Translation. Valid values are
    0:Disabled or 1:Enabled

subnet

-   Type: string
-   Required: true
-   Description: Pool of IPs to be used for NAT

/nat/{neId}/natPools?cached={cached}
------------------------------------

### GET

Summary

Returns all NAT Pools

Input type

None

neId

-   Description: Unique key assigned by Orchestrator to each appliance,
    for eg: \'0.NE\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<NATPool Id\>

name

-   Type: string
-   Required: true
-   Description: Name of NATPool

dir

-   Type: string
-   Required: true
-   Description: Traffic direction to perform NAT on. Valid values are
    \[\'outbound\', \'inbound\'\]

comment

-   Type: string
-   Description: User comments

pat

-   Type: number
-   Required: true
-   Description: Enable Port Address Translation. Valid values are
    0:Disabled or 1:Enabled

subnet

-   Type: string
-   Required: true
-   Description: Pool of IPs to be used for NAT

/nat/{neId}/maps?cached={cached}
--------------------------------

### GET

Summary

Get NAT Maps

Notes

Sample response:
{\"map1\":{\"prio\":{\"10\":{\"dir\":\"outbound\",\"enable\":true,\"comment\":\"\",\"gms\_marked\":false,\"match\":{\"intf\":\"lan0\",\"src\_subnet\":\"10.16.112.0/24\",\"dst\_subnet\":\"10.16.55.0/24\"},\"set\":{\"trans\_src\":\"1.1.1.0/24\",\"trans\_dst\":\"10.16.112.0/24\"}},\"20\":{\"dir\":\"inbound\",\"enable\":true,\"comment\":\"\",\"gms\_marked\":false,\"match\":{\"intf\":\"lan1\",\"dst\_subnet\":\"100.100.0.0/24\"},\"set\":{\"trans\_dst\":\"100.100.100.0/24\"}}}}}

Input type

None

neId

-   Description: Unique key assigned by Orchestrator to each appliance,
    for eg: \'0.NE\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

map1

prio

\<priorityNumber\>

match

intf

-   Type: string
-   Description: Interface name or label id

src\_subnet

-   Type: string
-   Description: Source IP address range to be translated. Must be a
    valid subnet

dst\_subnet

-   Type: string
-   Description: Destination IP address range to be translated. Must be
    a valid subnet

dir

-   Type: string
-   Description: Traffic direction to perform NAT on. Valid values are
    \[\'outbound\', \'inbound\'\]

comment

-   Type: string

gms\_marked

-   Type: boolean
-   Description: For internal use. Flag to determine if this rule was
    created by Orchestrator

set

trans\_src

-   Type: string
-   Description: Translated source subnet or NAT Pool ID

trans\_dst

-   Type: string
-   Description: Translated destination subnet

Reference: nat.json

\

RBAC: Get, Add, Update, Delete appliance access groups / assets
===============================================================

/rbac/asset
-----------

### GET

Summary

Get all appliance access groups / assets

Input type

None

assigned

-   Description: To get assigned appliance access group / asset provide
    assigned = true; Otherwise it will return list of all appliance
    access groups / assets
-   Required: false
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<applianceAccessGroupName1\>

-   Type: applianceAccess
-   Description: appliances

\<applianceAccessGroupName2\>

-   Type: applianceAccess
-   Description: appliances

### POST

Summary

Create or update appliance access group / asset.

Input type

application/json: undefined

body

-   Description: Either applianceGroups or applianceRegions should be
    empty (not null).
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/rbac/asset/{applianceAccessGroupName}
--------------------------------------

### GET

Summary

Get appliance access group / asset by name

Input type

None

applianceAccessGroupName

-   Description: appliance access group/asset name
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

applianceGroups

-   Type: array
-   Required: true
-   Description: list of accessible Group\_Ids

applianceRegions

-   Type: array
-   Required: true
-   Description: list of accessible Region\_Ids

### DELETE

Summary

Delete appliance access group / asset by name

Input type

None

applianceAccessGroupName

-   Description: appliance access group/asset name
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: rbacApplianceAccessGroup.json

\

RBAC: Get, Add, Update, Delete assignments
==========================================

/rbac/assignment
----------------

### GET

Summary

Get all rbac assignments

Input type

None

Output type

None

### POST

Summary

Create or update rbacAssignment.

Input type

application/json: undefined

body

-   Description: If you do not want to assign asset or roles to a user,
    then send \'null\' value for the respective property.
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/rbac/assignment/{username}
---------------------------

### GET

Summary

Get rbacAssignment by username

Input type

None

username

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

### DELETE

Summary

Delete rbacAssignment by username

Input type

None

username

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

Reference: rbacAssignment.json

\

RBAC: Get, Add, Update, Delete roles
====================================

/rbac/role
----------

### GET

Summary

Get all roles.

Input type

None

Output type

application/json

\<rolename\>

-   Type: Role
-   Required: true

### POST

Summary

Create role or Update existing role.

Input type

application/json: undefined

body

-   Description: menuTypeItems can not be null or empty
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

/rbac/role/{roleName}
---------------------

### GET

Summary

Get role by name if exists

Input type

None

roleName

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

application/json

menuTypeItems

-   Type: array
-   Required: true
-   Description: It should contain at least one menuId. It should not be
    empty or null

net\_read

-   Type: boolean
-   Required: true
-   Description: assign true value to grant read-write level access

### DELETE

Summary

Delete role By name. If role is assigned to one or more users then API
will return HTTP 423

Input type

None

roleName

-   Description: undefined
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

Output type

None

/rbac/role/menuAssigned
-----------------------

### GET

Summary

Get list of accessible menus

Input type

None

Output type

application/json

net\_read

-   Type: array
-   Required: true

net\_read\_write

-   Type: array
-   Required: true

Reference: rbacRole.json

\

Gets Appliance Loopback interfaces config
=========================================

/virtualif/loopback/{neId}?cached={cached}
------------------------------------------

### GET

Summary

Get loopback interface configuration on the appliance

Input type

None

neId

-   Description: Unique key assigned by Orchestrator to each appliance,
    for eg: \'0.NE\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<interface\>

admin

-   Type: boolean
-   Required: true
-   Description: admin status

ipaddr

-   Type: string
-   Required: true
-   Description: Interface IP

nmask

-   Type: number
-   Required: true
-   Description: mask

label

-   Type: string
-   Required: true
-   Description: label id

zone

-   Type: number
-   Required: true
-   Description: zone id

Reference: loopback.json

\

Gets Appliance VTI interfaces config
====================================

/virtualif/vti/{neId}?cached={cached}
-------------------------------------

### GET

Summary

Get VTI interface configuration on the appliance

Input type

None

neId

-   Description: Unique key assigned by Orchestrator to each appliance,
    for eg: \'0.NE\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

\<interface\>

admin

-   Type: boolean
-   Required: true
-   Description: admin status

ipaddr

-   Type: string
-   Required: true
-   Description: Interface IP

nmask

-   Type: number
-   Required: true
-   Description: mask

side

-   Type: string
-   Required: true
-   Description: \'lan\' or \'wan\'

label

-   Type: string
-   Required: true
-   Description: label id

zone

-   Type: number
-   Required: true
-   Description: zone id

tunnel

-   Type: string
-   Required: true
-   Description: Third party passthrough tunnel alias

Reference: vti.json

\

Get or set maintenance mode for appliances
==========================================

/maintenanceMode
----------------

### GET

Summary

Get maintenance mode appliances

Notes

Includes pause orchestration list and suppress alarm list.

Input type

None

Output type

application/json

pauseOrchestration

-   Type: array
-   Required: true
-   Description: NePk list whose orchestrations are or would be paused

suppressAlarm

-   Type: array
-   Required: true
-   Description: NePk list whose alarms are or would be disabled

### POST

Summary

Set maintenance mode for appliances

Notes

Includes pause orchestration list and suppress alarm list.

Input type

application/json: undefined

MaintenanceModePostBody

-   Description: Maintenance Mode Configuration
-   Required: true
-   Parameter type: POST Body
-   Data Type: application/json

Output type

None

Reference: maintenanceMode.json

\

notification banner: Get, Add, Update, Delete of the message
============================================================

/notification
-------------

### GET

Summary

Get the message of notification banner.

Notes

Get the message of notification banner.

Input type

None

Output type

None

### POST

Summary

Add or update the message of notification banner.

Notes

Set the message of notification banner.

Input type

application/json: undefined

NotificationConfig

Description: undefined

Required: true

Parameter type: POST Body

Data Type: application/json

message

-   Type: string
-   Required: true
-   Description: the message of notification

author

-   Type: string
-   Required: true
-   Description: the user who wrote the message

Output type

None

### DELETE

Summary

Remove the message of notification banner.

Notes

Remove the message of notification banner.

Input type

None

Output type

None

Reference: gmsNotification.json

\

Get the DNS proxy config
========================

/dnsProxy/config/{neId}?cached={cached}
---------------------------------------

### GET

Summary

Get the DNS proxy config

Notes

The returned data is an object. It includes all of the DNS proxy
configurations; notably the profiles, domain groups and maps.

Input type

None

neId

-   Description: Unique key assigned by Orchestrator to each appliance,
    for eg: \'0.NE\'
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: string

cached

-   Description: Get data from cache (true) or from appliance (false)
-   Required: true
-   Parameter type: URL Parameter
-   Data Type: boolean

Output type

application/json

enable

-   Type: boolean
-   Required: true
-   Description: Enable DNS proxy

interface

-   Type: string
-   Required: true
-   Description: Loopback interface

active

-   Type: string
-   Required: true
-   Description: Active map

profile

\<profile\_name\>

servers

-   Type: string
-   Required: true
-   Description: DNS server addresses

caching

-   Type: string
-   Required: true
-   Description: Enable caching

domaingroup

\<group\_name\>

domains

-   Type: string
-   Required: true
-   Description: Domains

map

map1

prio

\<prio\_id\>

match

-   Type: string
-   Required: true
-   Description: Match the domains under the specified domain group.
    Format: \"domain\_group:group\_name\" or \"any\"

set

-   Type: string
-   Required: true
-   Description: Set action to the specified profile. Format:
    profile\_name:profile\_name

comment

-   Type: string

gms\_marked

-   Type: boolean
-   Description: For internal use. Flag to determine if this rule was
    created by Orchestrator.

Reference: dnsProxy.json
:::
