.. Read the Docs Template documentation master file, created by
   sphinx-quickstart on Tue Aug 26 14:19:49 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Silver Peak Doc Site
===================================

Welcome to the Silver Peak documentation site. We are in the process of migrating our existing PDF and HTML documentation to this new site. Keep checking back if you don't see what you're looking for.

Contents
========

.. toctree::
   :maxdepth: 6
   :glob:

   orch/index.rst
   *

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
