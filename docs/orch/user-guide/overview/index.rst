Overview of SD-WAN Prerequisites
================================

With Orchestrator, you create virtual network overlays to apply business intent to network segments. Provisioning a device is managed by applying profiles.

- Interface Labels associate each interface with a use.

  - LAN labels refer to traffic type, such as VoIP, data, or replication.
  - WAN labels refer to the service or connection type, such as MPLS, internet, or Verizon.

- Deployment Profiles configure the interfaces and map the labels to them, to characterize the appliance.

- Business Intent Overlays use the Labels specified in Deployment Profiles to define how traffic is routed and optimized between sites. These overlays can specify preferred paths and can link bonding policies based on application, VLAN, or subnet, independent of the brand and physical routing attributes of the underlay.

  This diagram shows the basic architecture and capabilities of Overlays.

  .. image:: img/overlays.png

Including a new appliance into the Unity fabric consists of two basic steps:

Registration and discovery. After you Accept the discovered appliance, the Configuration Wizard opens.
Provisioning. Since the wizard prompts you to select profiles, it’s easiest to create these ahead of time.
Figure 1. The process of installing and provisioning an appliance for SD-WAN.


.. toctree::
   :maxdepth: 6
   :glob:

   overlays/index.rst
   *
