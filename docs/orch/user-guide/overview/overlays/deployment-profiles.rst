Deployment Profiles
===================

Instead of configuring each appliance separately, you can create various Deployment Profiles and provision a device by applying the profile you want. For example, you can create a standard format for your branch.


TIP  For a smoother workflow, complete the Configuration > DHCP Server tab before creating Deployment Profiles.

You can use Deployment Profiles to simplify provisioning, whether or not you choose to create and use Business Intent Overlays.

NOTE  You cannot edit IP/Mask fields because they are appliance-specific.

Mapping Labels to Interfaces
On the LAN side, labels identify the data, such as data, VoIP, or replication.
On the WAN side, labels identify the service, such as MPLS or Internet.
To create a global pool of labels, either:
Click the Edit icon next to Label.
Select Configuration > Interface Labels.
If you edit a label, that change propagates appropriately. For example, it renames tunnels that use that labeled interface.
LAN–side Configuration: DHCP
By default, each LAN IP acts as a DHCP Server when the appliance is in (the default) Router mode.
The global defaults are set in Configuration > DHCP Server and pre-populate this page. The other choices are No DHCP and having the appliance act as a DHCP Relay.
Enter the IP address of the specific LAN interface above the NO DHCP link.
The firewall zones you have already configured will be in the list under FW Zone. Select the FW zone you want to apply to the LAN you are deploying.
WAN–side Configuration
Enter the IP address of the specific WAN interface above the v4/v6 link. You can select the DHCP version you want to apply: v4 or v6.
Label
Firewall Zone: Zone-based firewalls are created on the Orchestrator. A zone is applied to an Interface. By default, traffic is allowed between interfaces labeled with the same zone. Any traffic between interfaces with different zones is dropped. Users can create exception rules (Security Policies) to allow traffic between interfaces with different zones. The firewall zones you have already configured will be in the list under FW Zone. Select the FW zone you want to apply to the WAN you are deploying.
Firewall Mode: Four options are available at each WAN interface:

Allow All permits unrestricted communication.
Stateful only allows communication from the LAN-side to the WAN-side.

Use this if the interface is behind the WAN edge router.

Stateful with SNAT applies Source NAT to outgoing traffic.

Use this if the interface is directly connected to the Internet.

Harden
For traffic inbound from the WAN, the appliance accepts only IPSec tunnel packets that terminate on a Silver Peak appliance.
For traffic outbound to the WAN, the appliance only allows IPSec tunnel packets and management traffic that terminate on a Silver Peak appliance.


WARNING  Activating fail-to-wire will DISABLE ALL firewall rules.

NAT Settings: When using NAT, use in-line Router mode to ensure that addressing works properly. That means you configure paired single or dual WAN and LAN interfaces on the appliance. Select one of the following options:

If the appliance is behind a NAT-ed interface, select NAT.
If the appliance is not behind a NAT-ed interface, select Not behind NAT.
Enter an IP address to assign a destination IP for tunnels being built from the network to this WAN interface.


Shaping: You can limit bandwidth selectively on each WAN interface.

Total Outbound bandwidth is licensed by model. It's the same as max system bandwidth.
To enter values for shaping inbound traffic, which is optional, you must first select Shape Inbound Traffic.


EdgeConnect Licensing: Only visible on EC appliances

For additional bandwidth, you can purchase Plus, and then select it here for this profile.
If you've purchased a reserve of Boost for your network, you can allocate a portion of it in a Deployment Profile. You can also direct allocations to specific types of traffic in the Business Intent Overlays.
To view how you've distributed Plus and Boost, view the Configuration > Licenses tab.
Select the appropriate licensing you have applied to your EC appliance from the menu. The licenses will only display depending on the licenses you have for that particular account. You can select the following licensing options:
Mini
Base
Base + Plus
50 Mbps
200 Mbps
500 Mbps
1 Gbps
2 Gbps
Unlimited
NOTE  You must have the correct hardware to support the license selected.



BONDING

When using an NX or EC appliance with four 1Gbps Ethernet ports, you can bond like pairs into a single 2Gbps port with one IP address. For example, wan0 plus wan1 bond to form bwan0. This increases throughput on a very high-end appliance and/or provides interface-level redundancy.
For bonding on a virtual appliance, you would need configure the host instead of the appliance. For example, on a VMware ESXi host, you would configure NIC teaming to get the equivalent of etherchannel bonding.
Whether you use a physical or a virtual appliance, etherchannel must also be configured on the directly connected switch/router. Refer to the Silver Peak user documentation.
Definitions
DHCP Server
Field Name

Description

Default gateway	When selected, indicates the default gateway is being used.
Default lease,

Maximum lease

Specify, in hours, how long an interface can keep a DHCP–assigned IP address.

DNS server(s)	Specifies the associated Domain Name System server(s).
Exclude first N addresses	Specifies how many IP addresses are not available at the beginning of the subnet's range.
Exclude last N addresses

Specifies how many IP addresses are not available at the end of the subnet's range.

NetBIOS name server(s)

Used for Windows (SMB) type sharing and messaging. It resolves the names when you are mapping a drive or connecting to a printer.

NetBIOS node type

The NetBIOS node type of a networked computer relates to how it resolves NetBIOS names to IP addresses. There are four node types:

B-node = 0x01 Broadcast
P-node = 0x02 Peer (WINS only)
M-node = 0x04 Mixed (broadcast, then WINS)
H-node = 0x08 Hybrid (WINS, then broadcast)
NTP server(s)	Specifies the associated Network Time Protocol server(s).
Start Offset
Specifies how many addresses not to allocate at the beginning of the subnet's range. For example, entering 10 means that the first ten IP addresses in the subnet aren't available.

Subnet Mask
A mask that specifies the default number of IP addresses reserved for any subnet. For example, entering 24 reserves 256 IP addresses.

DHCP/BOOTP Relay
Field Name

Description

Destination DHCP/BOOTP Server
The IP address of the DHCP server assigning the IP addresses.

Enable Option 82
When selected, inserts additional information into the packet header to identify the client's point of attachment.

Option 82 Policy
Tells the relay what to do with the hex string it receives. The choices are append, replace, forward, or discard.

A More Comprehensive Guide to Basic Deployments
This section discusses the basics of three deployment modes: Bridge, Router, and Server modes.

It describes common scenarios, considerations when selecting a deployment, redirection concerns, and some adaptations.

For detailed deployment examples, refer to the Silver Peak website for various deployment guides.



In Bridge Mode and in Router Mode, you can provide security on any WAN-side interface by hardening the interface. This means:

For traffic inbound from the WAN, the appliance accepts only IPSec tunnel packets.
For traffic outbound to the WAN, the appliance only allows IPSec tunnel packets and management traffic.


Bridge Mode
Single WAN-side Router

In this deployment, the appliance is in-line between a single WAN router and a single LAN-side switch.




Dual WAN-side Routers

This is the most common 4-port bridge configuration.


2 WAN egress routers / 1 or 2 subnets / 1 appliance
2 separate service providers or WAN services (MPLS, IPsec VPN, MetroEthernet, etc.)


Considerations for Bridge Mode Deployments

Do you have a physical appliance or a virtual appliance?
A virtual appliance has no fail-to-wire, so you would need a redundant network path to maintain connectivity if the appliance fails.
If your LAN destination is behind a router or L3 switch, you need to add a LAN-side route (a LAN next-hop).
If the appliance is on a VLAN trunk, then you need to configure VLANs on the Silver Peak so that the appliance can tag traffic with the appropriate VLAN tag.


Router Mode
There are four options to consider:

Single LAN interface & single WAN interface
Dual LAN interfaces & dual WAN interfaces
Single WAN interface sharing LAN and WAN traffic
Dual WAN interfaces sharing LAN and WAN traffic
For best performance, visibility, and control, Silver Peak recommends Options #1 and #2, which use separate LAN and WAN interfaces. And when using NAT, use Options #1 or #2 to ensure that addressing works properly.

#1 - Single LAN Interface & Single WAN Interface

routerMode_1-LAN_1-WAN
For this deployment, you have two options:

You can put Silver Peak in-path. In this case, if there is a failure, you need other redundant paths for high availability.
You can put Silver Peak out-of-path. You can redirect LAN-side traffic and WAN-side traffic from a router or L3 switch to the corresponding Silverpeak interface, using WCCP or PBR (Policy-Based Routing).
To use this deployment with a single router that has only one interface, you could use multiple VLANs.

#2 - Dual LAN Interfaces & Dual WAN Interfaces
routerMode_2-LAN_2-WAN
This deployment redirects traffic from two LAN interfaces to two WAN interfaces on a single Silver Peak appliance.

2 WAN next-hops / 2 subnets / 1 appliance
2 separate service providers or WAN services (MPLS, IPsec VPN, MetroEthernet, etc.)
Out-of-path dual LAN and dual WAN interfaces2 services
For this deployment, you have two options:

You can put Silverpeak in-path. In this case, if there is a failure, you need other redundant paths for high availability.
You can put Silverpeak out-of-path. You can redirect LAN-side traffic and WAN-side traffic from a router or L3 switch to the corresponding Silverpeak interface, using WCCP or PBR (Policy-Based Routing).


#3 - Single WAN Interface Sharing LAN and WAN traffic
routerMode_share_1-IF
This deployment redirects traffic from a single router (or L3 switch) to a single subnet on the Silver Peak appliance.

This mode only supports out-of-path.
When using two Silver Peaks at the same site, this is also the most common deployment for high availability (redundancy) and load balancing.
For better performance, control, and visibility, Silver Peak recommends Router mode Option #1 instead of this option.
#4 - Dual WAN Interfaces Sharing LAN and WAN traffic
routerMode_2-share
This deployment redirects traffic from two routers to two interfaces on a single Silver Peak appliance.

This is also known as Dual-Homed Router Mode.

2 WAN next-hops / 2 subnets / 1 appliance
2 separate service providers or WAN services (MPLS, IPsec VPN, MetroEthernet, etc.)
This mode only supports out-of-path.
For better performance, control, and visibility, Silver Peak recommends Router mode Option #2 instead of this option.


Considerations for Router Mode Deployments
Do you want your traffic to be in-path or out-of-path? This mode supports both deployments. In-path deployment offers much simpler configuration.
Does your router support VRRP, WCCP, or PBR? If so, you may want to consider out-of-path Router mode deployment. You can set up more complex configurations, which offer load balancing and high availability.
Are you planning to use host routes on the server/end station?
In the rare case when you need to send inbound WAN traffic to a router other than the WAN next-hop router, use LAN-side routes.


Examining the Need for Traffic Redirection

Whenever you place an appliance out-of-path, you must redirect traffic from the client to the appliance.

There are three methods for redirecting outbound packets from the client to the appliance (known as LAN-side redirection, or outbound redirection):

PBR (Policy-Based Routing) — configured on the router. No other special configuration required on the appliance. This is also known as FBR (Filter-Based Forwarding).

If you want to deploy two Silver Peaks at the site, for redundancy or load balancing, then you also need to use VRRP (Virtual Router Redundancy Protocol).

WCCP (Web Cache Communication Protocol) — configured on both the router and the Silver Peak appliance. You can also use WCCP for redundancy and load balancing.

Host routing — the server/end station has a default or subnet-based static route that points to the Silver Peak appliance as its next hop. Host routing is the preferred method when a virtual appliance is using a single interface, mgmt0, for datapath traffic (also known as Server Mode).

To ensure end-to-end connectivity in case of appliance failure, consider using VRRP between the appliance and a router, or the appliance and another redundant Silver Peak.


How you plan to optimize traffic also affects whether or not you also need inbound redirection from the WAN router (known as WAN-side redirection):

If you use subnet sharing (which relies on advertising local subnets between Silver Peak appliances) or route policies (which specify destination IP addresses), then you only need LAN-side redirection.
If, instead, you rely on TCP-based or IP-based auto-optimization (which relies on initial handshaking outside a tunnel), then you must also set up inbound and outbound redirection on the WAN router.
For TCP flows to be optimized, both directions must travel through the same client and server appliances. If the TCP flows are asymmetric, you need to configure flow redirection among local appliances.


A tunnel must exist before auto-optimization can proceed. There are three options for tunnel creation:

If you enable auto-tunnel, then the initial TCP-based or IP-based handshaking creates the tunnel. That means that the appropriate LAN-side and WAN-side redirection must be in place.
You can let the Initial Configuration Wizard create the tunnel to the remote appliance.
You can create a tunnel manually on the Configuration - Tunnels page.


Server Mode
This mode uses the mgmt0 interface for management and datapath traffic.


ADDING DATA INTERFACES

You can create additional data-plane Layer 3 interfaces, to use as tunnel endpoints.
To add a new logical interface, click +IP.
