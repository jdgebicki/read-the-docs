Unity Overlays
==============

These topics describe the pages related to deploying a WAN optimization network or a software-defined Wide Area Network (SD-WAN).

From a configuration standpoint, an SD-WAN uses Business Intent Overlays (BIOs), whereas a WANop network does not.


.. toctree::
   :maxdepth: 6
   :glob:

   *
