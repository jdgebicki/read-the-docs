Business Intent Overlays
========================

Business Intent Overlays
Use the Business Intent Overlays (BIOs) tab to create separate, logical networks that are individually customized to your applications and requirements within your network. By default, there are several predefined overlays matching a range of traffic within your network.

The overlay summary table is used for easy comparison of values between your various configured overlays. You can select any link in the table and the Overlay Configuration dialog launches. You can also temporarily save your changes before officially applying those changes to your overlay. The pending configuration updates are indicated by an orange box around the edited item. Select Save and Apply Changes to Overlays when you are ready to apply the changes and select Cancel if you want to delete the changes.

Overview
Orchestrator matches traffic to an ACL, progressing down the ordered, priority list of overlays until it identifies the first one that matches. The matched traffic is then analyzed against the overlay's Internet Traffic configuration, and forwarded within the fabric, or broken out to the internet based on the preferred policy order. If the software determines that the traffic is not destined for the internet, it refers to the WAN Links & Bonding Policy configuration and forwards traffic accordingly within the overlay.

SD-WAN traffic to internal subnets
Overlay Configuration

You can begin to configure or modify a default overlay in the Overlay column. You can also select any icon on the Business Intent Overlay page and the selected editor or dialog opens.

Complete the following steps to configure your overlay.

Select the name of the overlay. The Overlay Configuration window opens. If you want to edit the default overlay or create a new overlay, enter the new name of the overlay in the Name field.
Select the Match field and choose the match criteria from the menu.
Select the Edit icon next to the ACL field. To apply default ACL's or create your own, select Add Rule in the Associate ACL window.
Select Save.
Region

To view your associated region within your overlay, select the Regions icon in the Region column in the overlay summary table. You can modify, remove, or edit overlay settings for a selected region by expanding the list at the right-top of the Overlay Configuration window . For more information regarding Regions, refer to the help in the tab.

Topology

Select the type of topology you want to apply to your overlay and network. You can choose between the following types of topology:

Mesh: Choose Mesh if you want to make a local network.
Hub & Spoke: Hubs are used to build tunnels in Hub & Spoke networks, and to route traffic between regions. If you choose Hub & Spoke, any appliance set as a hub will serve as a hub in any overlay applied to it. Hubs in different regions mesh with each other to support regional routing. To configure hubs, select the Hubs link at the top of the page.

Regional Mesh and Regional Hub & Spoke: To streamline the number of tunnels created between groups of appliances that are geographically dispersed, you can assign appliances to Regions and select Regional Mesh or Regional Hub & Spoke.
At the top of the page, select Regions.
You can add and remove a region or view the status of each overlay within a selected region.
Building SD-WAN using these interfaces
You can select which WAN interfaces you want to use for each device to connect to the SD-WAN. First, you assign for your traffic to go to the Primary interfaces. If the primary interface is unavailable or not meeting the desired Service Level Objectives configured, the Backup interfaces are used. Move the desired interfaces between Primary and Backup. The interfaces are grayed out until moved into the Primary or Backup boxes.

Cross Connect allows you to define tunnels built between each interface label. Each appliance has a maximum number of tunnels that it can support, and using Cross Connect increases the number of tunnels created.
Add Backup if Primary Are: Specifies when the system should use the Backup interfaces.
Service Level objective
Traffic is routed through the primary interfaces exclusively, unless the service level thresholds for Loss, Latency, or Jitter have been exceeded. If this occurs, backup interfaces are added so that the service level objective can be met.

NOTE  Primary interfaces may still be used to support the overall Service Level Objective.

Link Bonding Policy
You can select the following Link Bonding Policies when you need to specify the criteria for selecting the best route possible when data is sent between multiple tunnels and appliances.

Field

Definition

High Availability

For critical services that cannot accept any interruption at all. For example, call center voice or critical VDI traffic.
High Quality

For typical real-time services, such as VoIP or video conferencing. For example, WebEx or business-quality Skype, VDI traffic.

High Throughput

For anything where maximum speed is more important than quality. For example, data replication, NFS, file transfers, etc.

High Efficiency

For everything else. This option sends load balance information on multiple links, with no FEC or overhead.



QoS, Security, & Optimization
To further customize your overlay configuration, enter the appropriate information for the following fields.

Field

Definition

FW Zone

Select the firewall zone you want to restrict traffic to from an overlay.
Boost

Select True or False if you want to apply any purchased Boost to your overlay.

Peer Unavailable Option

Select the following options you want your traffic to go if a peer is unavailable: Use MPLS, Use Internet, Use LTE, Use Best Route, Drop.

Traffic Class

Channels traffic to the desired queue based on the applied service. Select Best Route or Drop.
LAN DSCP

Select the DSCP you want to apply as a filter to the LAN interface.

WAN DSCP

Select the DSCP you want to apply as a filter to the WAN interface.

Breakout Traffic to Internet & Cloud Services
You can use the Breakout Traffic to Internet & Cloud Services to monitor and manage traffic coming to or from the internet.

Hub versus branch breakout settings
You can create different breakout policies for hubs. Any hub you select in the Topology section also displays at the top of the Internet Traffic to Web, Cloud Services tab. When you select an individual hub, the Use Branch Settings displays, selected, to the right of the screen. Complete the following steps to create a custom breakout policy for that hub:

Clear the check box for Use Branch Settings.
Configure the now accessible parameters.
Select OK.
Preferred Policy Order and Available Policies
You can move policies back and forth between the Preferred Policy Order and the Available Policies columns. You can also change their order within a column. The defaults provided are Backhaul via Overlay, Break Out Locally, and Drop.
When you choose Break Out Locally, confirm that any selected interface that is directly connected to the Internet has Stateful Firewall specified in the deployment profile.
You can add services (such as Zscaler, Fortigate, or Palo Alto). The service requires a corresponding Internet-breakout (Passthrough) tunnel for each appliance traffic to that service. To add a service, select the Edit icon next to Available Policies.
The Default policy you configure for internet breakout is pushed to all appliances that use the selected Overlay. However, you might want to push different breakout rules to your hubs.
